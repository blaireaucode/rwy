/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import L from './L.js';
// import { Link } from 'react-router-dom';

class Start extends Component {

    // constructor(props) {
    //     super(props);
    //     // this.s = require('./images/cover.png');
    // }

    render() {
        return (
            <div  className='Book-character'>

              <L to="1">1</L><br/>
              <L to="2">2</L><br/>
              <L to="3">3</L><br/>
              <L to="4">4</L><br/>
              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Start);
