/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from '@material-ui/core/Input';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import * as charac from './characteristics.js';
import * as dh from './field_helpers';

class FieldInput extends Component {

    constructor(props) {
        super(props);
        const element = dh.field_get_element(this.props);
        const fadj = this.props.f+'_adj';
        var adj = false;
        const el = element;        
        if (el && fadj in el) adj = fadj;
        const ch = charac.characteristics[this.props.f];
        const value = dh.get_field_str_value(this.props);
        this.state = {
            // element: element,
            charact:ch,
            adj: (adj ? fadj:false),
            value: el? value:'* error *',
        };
    }

    onChange = (event) => {
        var v = event.target.value;
        if (this.props.type === 'number')
            v = parseInt(v, 10);
        this.setState({value:v});
        const p = this.props.f;
       const element = dh.field_get_element(this.props);
        if (this.state.charact.from_str)
            v = this.state.charact.from_str(v);
        // console.log('pro', this.props, this.state);
        const ee = update(element, {[p]:{$set:v}});
        dh.field_set_element(this.props, ee);
    }

    componentWillReceiveProps(nextProps){
        //console.log('next props ', this.state, nextProps);
        const element = dh.field_get_element(nextProps);
        if (!element) return;
        const v = dh.get_field_str_value(nextProps);
        if (this.state.value !== v) {
            this.setState({value:v});
            // console.log('this.state', this.state);
        }
    }

    render() {

        // Common props
        const  inputProps = { ...this.props.inputProps,
                              value: this.state.value,
                              onChange:this.onChange
                            };
        return (            
            <Input
              disableUnderline={true}
              inputProps={inputProps}
            /* startAdornment={sign} */
            />
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FieldInput);
