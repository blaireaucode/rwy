/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React from 'react';

export const characteristics = [];

// ------------------------------------------------------------------------
characteristics['fighting_prowess'] = {
    short:'Force',
    type: 'number',
    label: 'Force',
    help:(props) => { return (
        <span>
          Force
        </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['psychic_ability'] = {
    short:'Pouvoir',
    type: 'number',
    label: 'Pouvoir',
    help:(props) => { return (
        <span>
          Pouvoir
        </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['awareness'] = {
    short:'Habileté',
    type: 'number',
    label: 'Habileté',
    help:(props) => { return (
        <span>
          Habileté
        </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['endurance'] = {
    short:'Endurance',
    type: 'number',
    label: 'Endurance',
    help:(props) => {
        // const hp = parseInt(props.adventurer.hp,10)+parseInt(props.adventurer.hp_adj,10);
        return (
            <span>
              endurance
            </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['class'] = {
    short:'Classe',
    type: 'text',
    label: 'Classe',
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['name'] = {
    short:'',
    type: 'text',
    label: 'Name',
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['rep'] = {
    short:'Rep',
    type: 'number',
    label: 'Reputation',
    help:(props) => {
        const v = props.adventurer.rep;
        return (
            <span>
              REPUTATION (Rep)  <br/>
              Reputation plays an important part between Quests when
              the Adventurer is getting ready to begin a new one (see
              Before Your Next Quest) and may be a reward or penalty
              for successfully completing or failing a Quest, but it
              can never be raised above 10 or be reduced to less than
              1. <br/>
              Current Rep : {v}.
            </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['fate'] = {
    short:'Fate',
    type: 'number',
    label: 'Fate',
    help:(props) => {
        const v = props.adventurer.fate;
        return (
            <span>
              FATE POINTS <br/>
              Fate points are rare blessings that have been granted by
              the gods to the Adventurers because they have fallen
              into their favour. They can be spent to re-roll any die,
              or dice just rolled, a d6, d10 or either the tens die
              (00-90) or the unit die (0-9), or a combination of dice
              used for any roll. If the player is unhappy with the new
              result they can carry on spending Fate points and
              re-rolling until they are satisfied with the new result
              or wish to stop.<br/>
              Current Fate : {v}.
            </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['life'] = {
    short:'Life',
    type: 'number',
    label: 'Life',
    help:(props) => {
        const v = props.adventurer.life;
        return (
            <span>
              LIFE <br/>
              Like a cat has nine lives and can cheat death on many
              occasions, the Adventurers are blessed with the same
              luck and have a number of lives which can be used each
              time they would lose enough HP to kill them.  Whenever
              an Adventurers HP is reduced to zero or less, the player
              may spend one Life point to cheat death and instantly
              restore all of their lost HP and remove all Disease and
              Poison pips shaded on the Adventure Sheet. Sadly when an
              Adventurer has no Life points remaining and have lost
              their last HP, they have died, and it is time to begin a
              new Adventurer
              <br/>
              Current Life : {v}.
            </span>) ;}
};
// ------------------------------------------------------------------------



// ------------------------------------------------------------------------
characteristics['gp'] = {
    short:'GP',
    type: 'number',
    label: 'Gold Pieces',
    help:(props) => {
        const v = props.adventurer.gp;
        return (
            <span>
              GOLD PIECES <br/>
              Current number of Gold Pieces : {v}.
            </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['hero_path'] = {
    short:'Path',
    type: 'text',
    label: 'Hero Path',
    help:(props) => {
        const v = props.adventurer.hero_path;
        return (
            <span>
              HERO PATH<br/>
              Each Adventurer has a path they have chosen to follow
              and have dedicated their life towards learning its
              skills and abilities. By choosing a path they will
              benefit from a greater understanding, and will
              accelerate their learning in its direction. There are
              three Hero Paths used in D100 Dungeon, which are
              Warrior, Rogue and Sorcerer, so either roll on Table H -
              Hero Path or choose a path from the table for your
              Adventurer and write it on your Adventure sheet. Then
              apply the modifiers from the table to the Primary
              Characteristics and add the bonuses for Skills, and
              shade in all Experience Stars ?  shown for the
              Adventurers Hero Path.
              <br/>
              Current hero path : {v}.
            </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['race'] = {
    short:'Race',
    type: 'text',
    label: 'Race',
    help:(props) => {
        const v = props.adventurer.race;
        return (
            <span>
              RACE <br/>
              Dwarves, Elves and Humans dominant the lands and our
              Adventurer will belong to one of those three
              races. Either roll on Table R - Race or choose one to
              determine which Race the Adventurer will be and record
              it on the Adventurer Sheet, apply the modifiers from the
              table to the Primary Characteristics and add the bonuses
              for Skills, then shade in all Experience Stars ? shown
              for the Adventurers Race
              <br/>
              Current race : {v}.
            </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['av'] = {
    short:'AV',
    type: 'number',
    label: 'Attack Value',
    help:(props) => {
        const v = props.encounter.av || '-';
        return (
            <span>
              Attack Value. You will have to roll above this value
              ({v} + modifier) to avoid its attack.
            </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['def'] = {
    short:'Def',
    type: 'number',
    label: 'Defense Modifier',
    help:(props) => {
        const v = props.encounter.def || '-';
        return (
            <span>
              DEFENSE modifier (Def) <br/> When you hit the monster, this value
              ({v}) will be deducted from the damage dice.              
            </span>) ;}
};
/*
  
  DEFENCE (DEF) - An Adventurers Defence (Def) is a combined value of
  all of their equipped items that is helping them and making it
  easier for them to dodge blows and be harder to hit. Sometimes
  better made Armour is easier to move around in, and magic items may
  enhance and provide a better defence. When a damage roll scores a
  natural result equal to or less than the Adventurers current Def
  value, the damage is reduced by the Adventures Def Value (See
  Combat).

*/
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['dmg'] = {
    short:'Dmg',
    type: 'number',
    label: 'Damage Modifier',
    help:(props) => {
        const v = props.encounter.dmg || '-';
        return (
            <span>
              DAMAGE modifier (Dmg) <br/>

              When the monster hits you, this value
              ({v}) will be added to the damage you get.<br/>
              
              Magic Items and better quality weapons may deal
              additional damage when striking a target. The damage
              bonus of all equipped items is totalled together and
              applied to the damage dice roll after making a
              successful attack.              
            </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['loot'] = {
    short:'Loot',
    type: 'text',
    label: 'Loot',
    help:(props) => {
        return (
            <span>
              Indicate the tables you may loot when the monster is
              killed ('K'). For example, [K:Table I/TA] allow you to
              loot in table I (Items) or table TA (Treasure
              A).
            </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['armour'] = {
    short:'A',
    type: 'number',
    label: 'Armour',
    help:(props) => { return (
        <span>          
          ARMOUR (A) <br/> Some Armour offers protection when an Adventurer
          takes damage. The Armour value shown protects a specific
          location and is deducted from a Monsters damage score.          
        </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['shield'] = {
    short:'S',
    type: 'number',
    label: 'Shield',
    help:(props) => { return (
        <span>   
          SHIELD (S) <br/> The Shield value determines the
          number of damage points an Adventurer can deflect to
          the shield when struck in combat.
        </span>) ;}
};
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
characteristics['slot'] = {
    short:'Slot',
    type: 'text',
    label: 'Slot',
    help:(props) => { return (
        <span>   
          Slot :  FIXME
        </span>) ;}
};
// ------------------------------------------------------------------------

// ------------------------------------------------------------------------
characteristics['fix'] = {
    short:'Fix',
    type: 'number',
    label: 'Fix',
    help:(props) => { return (
        <span>   
          Fix :  FIXME
        </span>) ;}
};
// ------------------------------------------------------------------------

// ------------------------------------------------------------------------
characteristics['as'] = {
    short:'AS',
    type: 'text',
    label: 'AS',
    help:(props) => { return (
        <span>   
          AS :  FIXME
        </span>) ;}
};
// ------------------------------------------------------------------------
