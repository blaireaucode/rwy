/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
//import { Link } from 'react-router-dom';
// import SimpleImage from './SimpleImage.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
// import * as bh from './book_helpers.js';

class Passage extends Component {
    constructor(props) {
        super(props);
        // console.log('Passage const', this.props);
        const b = {...this.props.book,
                   current_passage:this.props.path};
        this.props.set('book', b);
    }
    render() {
        
        // var dead = bh.is_dead(this.props.character);

        // FIXME 
        // bh.history_add_passage(this.props, this.props.path);
        
        // console.log('p', dead, this.props.path, this.props.character, this.props.book);
        // if (dead) {          
        //     dead = (
        //         <div className="Book-dead">
        //           <p/>
        //           Vous êtes mort. <p/>
        //           <SimpleImage width={80} src={'skull.png'}/>
        //           <p/>
        //           <Link to='/edit'>Recommencer ...</Link>
        //         </div>
        //     );
        // }
        // else dead = '';

        const children = React.Children.map(this.props.children, child => {
            const c = React.cloneElement(child, {
                team:this.props.team,
                current_passage:this.props.path,
                book:this.props.book,
                options:this.props.options,
                set: this.props.set,
            });              
            return c;
        });
        return (
            <Grid justify="space-between" container>
              {children}
              {/* {dead} */}
            </Grid>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Passage);
