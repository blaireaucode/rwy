/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import update from 'immutability-helper';
import DiceRoller from './DiceRoller.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import * as fh from './fight_helpers';

class FightAttack extends Component {

    constructor(props) {
        super(props);
    }



    render() {
        const { fight } = this.props;
        if (fight.status !== 'attack') return '';

        console.log('fight ', fight);
        /*
          info about who is attacking
          button throw 2 dices with info about Strength

          struct this.props.fight
          - map
          - encounter
          - round
        */

        return (
            <span>
              Attack
              <DiceRoller dice={1}/>
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FightAttack);
