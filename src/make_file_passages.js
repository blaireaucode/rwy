/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */


var max_p = 570;
var fs = require('fs');

var txt = "import Start from '../Start.js';";

for(var i=1; i<=max_p; i++) {
    txt = txt+"import P"+i+" from './"+i+".js';\n";
}

txt = txt + "const temp = [];\n";
txt = txt + "temp.push(Start);\n";

for(i=1; i<=max_p; i++) {
    txt = txt+"temp.push(P"+i+");\n";
}

txt = txt + "export default temp;\n";


console.log(txt);

var fn = './src/passages/passages.js';
//fs.copyFileSync(fn, fn+'.backup');

fs.writeFileSync(fn, txt);
console.log("Successfully written to file "+fn);

