/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P254 extends Component {
    render() {
        return (
            <div>

               Vous vous réveillez en sursaut, le front ruisselant de sueur. Autour de vous, les cadavres sont demeurés en place, immobiles sous la Lune Bleue. Pourtant, vous sentez encore sur votre corps les meurtrissures dues à leurs coups répétés... Soudain, des gémissements s'élèvent ! Vos compagnons luttent dans leur sommeil et sont agités de tremblements nerveux. Chaque joueur endormi perd encore 2 dés + 3 d'ENDURANCE. Sans perdre un instant, vous les secouez pour les réveiller. Rendez-vous au <L to='539'>539</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P254);
