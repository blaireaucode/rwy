/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P21 extends Component {
    render() {
        return (
            <div>

               Vous devez maintenant calculer la Force Guerrière de vos troupes. Légions du Selentium Bataillon des Ombres Hordes Barbares Compagnie Blanche Guerriers Crocs Spectres de Brume Cavalerie des Bois  Puissance 4 Puissance 4 Puissance 3 Puissance 2 Puissance 2 Puissance 1 Puissance 3  Additionnez toutes les Puissances que vous avez rassemblées, de manière à obtenir la Force Guerrière de votre armée. Priez maintenant pour qu'elle soit de taille à affronter les Squelettes. Si votre Force Guerrière est supérieure ou égale à 13, rendez-vous au <L to='359'>359</L>. Si elle est comprise entre 10 et 12, rendez-vous au <L to='459'>459</L>. Si elle est comprise entre 7 et 9, rendez-vous au <L to='519'>519</L>. Si elle est comprise entre 4 et 6, rendez-vous au <L to='357'>357</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P21);
