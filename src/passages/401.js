/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P401 extends Component {
    render() {
        return (
            <div>

               Vous approchez les joyaux de vos yeux, dans l'espoir qu'ils modifieront votre perception et permettront de trouver une issue. Hélas, rien de tout cela n'arrive et votre vision déformée ne vous permet pas d'esquiver l'attaque brutale des tentacules ! Chaque joueur perd 2 dés d'ENDURANCE. Les joyaux, projetés au sol, s'enfoncent dans la glace et disparaissent. Vous n'avez plus d'autre solution que de combattre. Rendez-vous au <L to='457'>457</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P401);
