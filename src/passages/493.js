/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P493 extends Component {
    render() {
        return (
            <div>

               Vous foncez dans la pièce en suivant les espaces ménagés entre les dalles, sans vous préoccuper des formes inquiétantes qui s'y trouvent. Vous atteignez finalement une sortie et débouchez à l'air libre, dans une cour. Une bâtisse s'élève devant vous. Après avoir repris votre souffle, vous avancez dans sa direction, rendez-vous au <L to='35'>35</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P493);
