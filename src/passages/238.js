/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P238 extends Component {
    render() {
        return (
            <div>

               Vous vous asseyez pour réfléchir à la situation. Auguste devait disposer d'un moyen plus pratique que son tapis volant pour évoluer dans les étages de sa tour. Vous retournez dans la galerie et observez minutieusement les lieux. Vous découvrez deux minces fils de cristal, courant sur toute la longueur de la paroi. En touchant celui de droite, vous êtes téléporté à l'étage inférieur. Celui de gauche permet de remonter. Vous pouvez maintenant explorer la tour à votre guise. Rendez-vous au <L to='470'>470</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P238);
