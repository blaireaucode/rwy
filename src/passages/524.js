/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P524 extends Component {
    render() {
        return (
            <div>

               La galerie inférieure possède plusieurs portes donnant sur des chambres. Vous pensez avoir affaire aux appartements des invités quand vous constatez que toutes les portes peuvent être bloquées de l'extérieur. Malgré une fouille minutieuse, vous ne trouvez rien. Rendez-vous au <L to='470'>470</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P524);
