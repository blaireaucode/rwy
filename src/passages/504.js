/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P504 extends Component {
    render() {
        return (
            <div>

               « Misérable félon ! Ni toi ni les tiens n'auront le fourreau de l'Épée de Légende ! » Auguste vous fixe un instant, un sourire amusé au coin des lèvres. « Fort bien, dit-il. Dans quelques minutes, nous serons en mon château. Je laisserai mes esclaves s'amuser avec toi pendant que j'admirerai le fourreau ! » Si vous voulez le laisser atterrir, rendez-vous au <L to='208'>208</L>. Si vous préférez l'attaquer dès maintenant, rendez-vous au <L to='536'>536</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P504);
