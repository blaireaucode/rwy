/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P447 extends Component {
    render() {
        return (
            <div>

               Vous courez à perdre haleine, craignant à chaque instant qu'un monstre ne surgisse devant vous. Dans votre dos, des hurlements déchirent le silence nocturne, d'autres chiens sont venus grossir la meute. Vos jambes vous font mal, vous ne pourrez plus tenir longtemps. Le chemin bifurque : le sentier de droite se perd dans la forêt, celui de gauche aboutit à une caverne éclairée. Vous hésitez. Cette grotte sera-t-elle votre refuge ? Elle pourrait aussi être votre tombeau... Si vous décidez de courir vers la caverne, rendez-vous au <L to='399'>399</L>. Si vous préférez vous enfoncer dans la forêt, rendez-vous au <L to='61'>61</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P447);
