/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P122 extends Component {
    render() {
        return (
            <div>

               Le Squelette arrive à votre hauteur. Vous ne pouvez lui échapper. Il va falloir l'éliminer, sans quoi, il pourrait vous traquer jusqu'à la nuit des temps...  SQUELETTE (Protection 2, Dommages 3 dés + 3) FORCE : 9 HABILETÉ : 8 POUVOIR : 9 ENDURANCE : 50 Ajustez son total d'ENDURANCE Si vous l'avez blessé à coups de flèches. Mais les Dommages que vous auriez pu lui infliger précédemment ont disparu. Si vous désirez fuir grâce à la Téléportation, rendez-vous au <L to='308'>308</L>. Si vous parvenez à le vaincre, rendez-vous au <L to='263'>263</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P122);
