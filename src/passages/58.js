/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P58 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Pour la première fois depuis que vous traitez avec lui, le Faltyn s'adresse à vous avec du respect dans la voix. « Tes largesses n'auront pas été vaines, dit-il. Lorsque tu traverseras la pièce, ne foule que les dalles noires. Des cinq objets que tu trouveras dans le coffre, choisis la sphère de Feu. Cet objet, lorsqu'il est activé, se transforme en fournaise impossible à contrôler. Uses-en à bon escient... » Sur ces mots, la créature disparaît. Suivant ses conseils, vous atteignez le coffre et vous en soulevez le couvercle. Rendez-vous au <L to='294'>294</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P58);
