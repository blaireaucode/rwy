/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P259 extends Component {
    render() {
        return (
            <div>

               « Ton charabia ne mérite pas même une pièce de cuivre ! » dites-vous. La vieille pousse un grognement de rage et sort une carte. « Le Temple Foudroyé ! Il annonce de graves ennuis à ceux qui ne savent pas se montrer charitables ! » Vous lui arrachez le jeu des mains et cherchez rapidement la lame qui vous intéresse. « Regarde, vieille folle, répliquezvous. Voici le Pendu ! Ma prophétie est claire et je pourrais facilement prêter main forte au destin ! » Vous lui jetez le jeu à la figure avant de vous éloigner. Rendez-vous au <L to='126'>126</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P259);
