/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P101 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous déversez le contenu de la bouteille de lait sur la flamme, plongeant la pièce dans l'obscurité. Les rubans spiralés arrêtent leur progression et s'en retournent dans le marbre. Rendez-vous au <L to='145'>145</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P101);
