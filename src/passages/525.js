/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P525 extends Component {
    render() {
        return (
            <div>

               Des plaintes se répercutent sous la glace. Le monstre est blessé à mort. Un sang noir coule des blessures de ses tentacules. Bientôt ils rentrent sous la glace, et la forme gigantesque que vous voyiez par transparence disparaît dans les eaux grises. Sans perdre un instant, vous foncez en direction de la côte alors que la nuit commence à tomber. Rendez-vous au <L to='117'>117</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P525);
