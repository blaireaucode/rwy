/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P362 extends Component {
    render() {
        return (
            <div>

               Vous pressez le pas pour les rejoindre, allant jusqu'à courir, mais les deux jeunes gens maintiennent leur avance. Enfin, ils interrompent leur ballet et vous arrivez à leur hauteur à l'instant où leur chant se termine. Le sifflement du vent nocturne cède la place à la mélodie... Le jeune garçon et la jeune fille se tournent vers vous en souriant. Rendez-vous au <L to='309'>309</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P362);
