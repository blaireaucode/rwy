/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P133 extends Component {
    render() {
        return (
            <div>

               Bientôt, le cadavre d'Auguste s'écrase sur la banquise. Vous devez maintenant faire face à un nouveau problème : comment descendre de ce tapis volant ? Si le prêtre désire intervenir, rendez-vous au <L to='445'>445</L>. Si le voleur désire intervenir, rendez-vous au <L to='2'>2</L>. Si le magicien désire tenter quelque chose, rendez-vous au <L to='452'>452</L>. Si vous ne pouvez recourir à aucune des ces options, rendez-vous au <L to='389'>389</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P133);
