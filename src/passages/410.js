/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P410 extends Component {
    render() {
        return (
            <div>

               Le Squelette vous répond d'une voix caverneuse « Je suis envoyé par l'archimage Tor. Lui seul peut se prétendre de cette lignée. Il a survécu au Cataclysme. Il cherche à vous tuer. Par moi seul, vous pourrez échapper à votre destin. » Si vous désirez lui demander de quelle manière, rendez-vous au <L to='158'>158</L>. Si vous préférez le tuer sur-le-champ, rendez-vous au <L to='303'>303</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P410);
