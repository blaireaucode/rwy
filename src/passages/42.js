/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P42 extends Component {
    render() {
        return (
            <div>

               MAGICIEN: Vous placez les joyaux devant vos yeux et vous les maintenez en place en fronçant les sourcils. Votre intuition était juste : vous pouvez apercevoir les frêles silhouettes des créatures. Rendez-vous au <L to='100'>100</L> pour combattre, mais procédez normalement à votre lancer avec deux dés, au lieu des trois mentionnés dans le texte, puisque les Sylphes ne sont plus invisibles pour vous.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P42);
