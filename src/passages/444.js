/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P444 extends Component {
    render() {
        return (
            <div>

               Vous regardez le vieux chevalier s'éloigner avant de reprendre votre route vers l'est. Rendez-vous au <L to='116'>116</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P444);
