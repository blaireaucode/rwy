/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P338 extends Component {
    render() {
        return (
            <div>

               Vous réprimez un frisson en comprenant que vous êtes suivi. Vous pouvez entendre des halètements saccadés. Une meute est à vos trousses ! Vous jetez un rapide coup d'oeil en arrière et entrevoyez une silhouette entre les buissons. Ce sont les Chiens de Givre, leur fourrure est luisante de glace et leurs crocs semblables à des cristaux. Vous forcez l'allure, allant jusqu'à courir, mais la meute ne vous lâche pas... Rendez-vous au <L to='447'>447</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P338);
