/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P14 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Quel objet comptez-vous employer les yeux joyaux (rendez-vous au <L to='42'>42</L>) ou la corne à boire en ivoire (rendez-vous au <L to='488'>488</L>) ?

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P14);
