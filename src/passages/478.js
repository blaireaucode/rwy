/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P478 extends Component {
    render() {
        return (
            <div>

               Les marins gisent tout autour de vous. « Bel affrontement ! s'exclame une voix au-dessus de vous. N'ayez aucun remords, cette racaille aurait tué père et mère pour quelques Pièces d'Or! » Vous relevez la tête, stupéfait, pour apercevoir un homme sur un tapis volant. L'inconnu descend à votre hauteur et vous enjoint de grimper à ses côtés. « La garde arrive, reprendil. Dépêchez-vous ! Le tapis d'Auguste va vous tirer d'embarras. » L'homme prononce une parole magique et le tapis s'envole dans les airs à une vitesse surprenante. Rendez-vous au <L to='424'>424</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P478);
