/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P174 extends Component {
    render() {
        return (
            <div>

               Vous jetez un dernier regard alentour avant de quitter cette horrible cathédrale d'ossements. Vous remarquez alors, dans une zone d'ombre, une arche s'ouvrant sur une autre salle. Si vous désirez explorer cet endroit, rendez-vous au <L to='411'>411</L>. Si vous préférez abandonner ce lieu sinistre au plus vite, rendez-vous au <L to='547'>547</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P174);
