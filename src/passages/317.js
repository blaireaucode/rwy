/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P317 extends Component {
    render() {
        return (
            <div>

               Vous ouvrez la porte avec une extrême prudence et pénétrez dans ce qui devait autrefois être une salle de musée. Les toiles accrochées aux murs sont noircies par les moisissures. Une couche de poussière recouvre les sculptures. La traînée de sang traverse la pièce et disparaît derrière un retable. Un grondement sourd vous avertit que le monstre est tapi derrière. Si vous préférez rebrousser chemin et ouvrir l'autre porte, rendez-vous au <L to='318'>318</L>. Si vous désirez affronter le monstre, rendez-vous au <L to='440'>440</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P317);
