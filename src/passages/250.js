/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P250 extends Component {
    render() {
        return (
            <div>

               Une voix connue s'élève dans votre dos. « Attendez une minute, capitaine. Je dois d'abord régler un petit différend avec ce lascar ! » Jadhak, l'arme au poing, s'avance vers vous. Quelques marins s'emparent de harpons et se regroupent à sa suite. Le capitaine s'éloigne en ricanant. « D'accord, les gars ! Amusez-vous un peu!» Si vous désirez combattre, rendez-vous au <L to='242'>242</L>. Sinon, il ne vous reste qu'à sauter par-dessus bord (rendez-vous au <L to='29'>29</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P250);
