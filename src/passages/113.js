/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P113 extends Component {
    render() {
        return (
            <div>

               Vous dérivez pendant des heures sous un ciel lourd mais, en fin d'aprèsmidi, le soleil perce enfin l'épais manteau de nuages. La côte se profile à l'ouest et la voile rouge d'un navire marchand vogue dans votre direction ! Bientôt, vous pouvez lire son nom, peint sur la coque. La Madonna, attirée par vos cris et gesticulations, change de cap et arrive à votre hauteur. Des mains secourables vous hissent à bord. Le capitaine est un riche marchand de Ferromaine. A l'écoute de vos mésaventures, il soupire : « Hélas, tous ces marins sont des ignorants et des superstitieux ! La vraie foi n'a pas encore illuminé leurs âmes. Je n'aime guère naviguer dans ces eaux barbares, mais je dois me rendre au port de Dourhaven et la Mer de Rymchaeld est bloquée par les glaces à cette époque. » Rendez-vous au <L to='565'>565</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P113);
