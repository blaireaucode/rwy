/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P371 extends Component {
    render() {
        return (
            <div>

               Vous descendez les marches dans un silence absolu jusqu'à une vaste salle dont le plafond se perd dans les ténèbres. Vous attendez que la procession se soit éloignée pour partir dans la direction opposée. Rendez-vous au <L to='547'>547</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P371);
