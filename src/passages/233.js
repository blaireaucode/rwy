/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P233 extends Component {
    render() {
        return (
            <div>

               Vous commencez à établir votre campement pour la nuit. Un silence pesant s'est abattu sur la forêt. Soudain, vous tendez l'oreille. Ce n'est qu'une branche qui vient de laisser choir son fardeau de neige. Vous vous apprêtiez à reprendre vos activités quand vous repérez deux yeux noirs qui vous observent. Aussitôt, vous vous emparez de votre arme. Des créatures quittent la pénombre, leurs babines retroussées révélant des crocs semblables à des cristaux de glace...  CHIEN DE GIVRE (Dommages 2 dés + 1) FORCE : 8 HABILETÉ : 9 POUVOIR : 9 ENDURANCE :25 Si vous remportez ce combat contre ces monstres, rendez-vous au <L to='464'>464</L>. Si vous parvenez à briser le cercle et à fuir, rendez-vous au <L to='447'>447</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P233);
