/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P373 extends Component {
    render() {
        return (
            <div>

               MAGICIEN: Vous avez vu à plusieurs reprises Auguste manipuler la pierre blanche qu'il portait en pendentif. Peut-être contrôlait-il le tapis par le biais de cette amulette ? Si vous possédez l'amulette, rendez-vous au <L to='23'>23</L>. Si vous l'avez laissée choir en même temps que le cadavre d'Auguste, rendez-vous au <L to='287'>287</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P373);
