/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P108 extends Component {
    render() {
        return (
            <div>

               Varadaxor contemple un instant la tête éclatée de la Dame Grise et achève de la pulvériser à coups de talon. « Maintenant, les âmes de mes frères reposeront en paix », murmure-t-il. Si vous désirez quitter la pièce sans plus tarder, rendez-vous au <L to='406'>406</L>. Si vous voulez fouiller les lieux, malgré l'écoeurante poussière grise qui s'échappe du corps de la créature, rendez-vous au <L to='311'>311</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P108);
