/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P207 extends Component {
    render() {
        return (
            <div>

               « Désolé », dites-vous en quittant votre siège. « Le jour va se lever et des tâches plus urgentes nous attendent. » Auguste vous foudroie du regard, mais vous lisez aussi de la tristesse au fond de ses yeux. « Vous autres, jeunes, n'avez plus aucun respect pour les grands principes de chevalerie. Peu vous importe l'honneur, seules la gloire et la richesse guident vos pas... » Vous quittez l'auberge en haussant les épaules. Rendez-vous au <L to='116'>116</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P207);
