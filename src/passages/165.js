/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P165 extends Component {
    render() {
        return (
            <div>

               Vous cheminez sans trêve pendant des heures. Enfin, la Lune Bleue disparaît derrière la cime des arbres, cédant la place à la vraie lune. Vous pouvez alors repérer un sentier qui vous conduit bientôt à la lisière des bois. Malgré la fatigue, vous poursuivez votre périple pendant une partie de la matinée et atteignez un village. Votre attention se porte sur l'unique auberge. Si vous désirez y entrer, rendez-vous au <L to='429'>429</L>. Si vous préférez d'abord questionner un passant, rendez-vous au <L to='352'>352</L>. Si vous jugez plus sage de reprendre la route, rendez-vous au <L to='267'>267</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P165);
