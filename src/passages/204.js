/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P204 extends Component {
    render() {
        return (
            <div>

               Vous vous enfuyez entre les arbres, indifférent aux branches basses qui vous fouettent le visage et lacèrent vos vêtements. Vous arrivez, haletant, au bord d'un lac. Non loin de là, une barque est amarrée. Le Squelette est sur vos talons. Vous foncez vers la barque et empoignez les rames. Rendez-vous au <L to='315'>315</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P204);
