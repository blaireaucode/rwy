/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P8 extends Component {
    render() {
        return (
            <div>

               Il s'avance avec des gestes de pantin. Son regard est vide. Il ne vous reconnaît pas. Si vous n'agissez pas rapidement, il plongera ses griffes acérées dans vos chairs et se repaîtra de votre sang. Si vous désirez le combattre, rendez-vous au <L to='37'>37</L>. Si vous préférez recourir à un objet, rendez-vous au <L to='449'>449</L>. Si votre groupe compte un prêtre désirant intervenir, rendez-vous au <L to='206'>206</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P8);
