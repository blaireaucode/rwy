/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P187 extends Component {
    render() {
        return (
            <div>

               Maître Derzu est le capitaine de la Magdalena. Il scelle votre accord d'une solide poignée de mains et déclare : « Nous levons l'ancre demain à l'aube. Je vous conseille de prendre du repos car nous aurons une rude journée devant nous. Avez-vous un endroit où dormir ? » Si vous avez payé votre chambre à l'auberge d'Ulrik, rendez-vous au <L to='205'>205</L>. Sinon, rendez-vous au <L to='480'>480</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P187);
