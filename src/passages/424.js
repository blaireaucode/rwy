/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P424 extends Component {
    render() {
        return (
            <div>

               Vous volez dans les airs à plusieurs centaines de mètres au-dessus des flots. Les navires semblent des jouets d'enfants. « Il vous aurait fallu plusieurs jours pour vous rendre à Wyrd, dit Auguste. Mais grâce à mon tapis volant, nous y arriverons en quelques heures. » Vous vous accroupissez pour offrir moins de prise au vent glacial. Bientôt, vous survolez des icebergs et une vaste étendue immaculée apparaît à l'horizon. Auguste murmure une parole magique. Aussitôt le tapis perd de l'altitude et part vers l'est. Si vous manifestez de l'inquiétude pour ce brusque changement de direction, rendez-vous au <L to='16'>16</L>. Si vous préférez laisser faire, rendez-vous au <L to='65'>65</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P424);
