/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P208 extends Component {
    render() {
        return (
            <div>

               Le tapis volant se pose en douceur au sommet de la plus haute tour. Auguste frappe dans ses mains avec un sourire méprisant. « Bienvenue dans mon domaine », dit-il. Aussitôt quatre créatures démoniaques accourent dans votre direction. Vous regardez en contrebas, mais toute fuite est impossible. « Je vous accorde une dernière chance, reprend-il. Donnez-moi le fourreau de votre plein gré ou je vous livre à mes créatures. » Les êtres contrefaits s'emparent de longues dagues. Si vous acceptez de remettre le fourreau, rendez-vous au <L to='419'>419</L>. Si vous êtes prêt à combattre, rendez-vous au <L to='76'>76</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P208);
