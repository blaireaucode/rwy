/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P196 extends Component {
    render() {
        return (
            <div>

               Laissant derrière vous cette vision d'horreur, vous vous engagez dans le couloir du fond. Il aboutit à une lourde porte de bois où sont gravées des runes. Vous poussez le battant, découvrant une cour intérieure. Au centre, juché sur un piédestal, un Démon attend. Vous repérez une ouverture à l'extrémité de la cour mais, pour y parvenir, il faut passer devant la bête. De brèves pulsations lumineuses éclairent la scène au-dessus de vos têtes.  Apparemment, la créature n'a pas pris conscience de votre présence. Si vous désirez refermer la porte et réfléchir à un plan, rendez-vous au <L to='88'>88</L>. Si vous préférez vous engouffrer dans la cour et attaquer le Démon, rendez-vous au <L to='249'>249</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P196);
