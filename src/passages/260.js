/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P260 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Deux possibilités s'offrent à vous vous téléporter à hauteur de la cage, mais la tentative est risquée, ou bien invoquer un second Faltyn pour délivrer le premier. Si vous désirez vous téléporter, rendez-vous au <L to='150'>150</L>. Si vous préférez invoquer le Faltyn, rendez-vous au <L to='114'>114</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P260);
