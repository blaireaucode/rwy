/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P517 extends Component {
    render() {
        return (
            <div>

               Vous descendez vers la vallée par un sentier bordé de congères. Vous longez une rivière gelée et parvenez enfin en vue du village. Il est constitué d'une vaste maison commune au toit de chaume, entourée de cinq huttes de pierre, qui font certainement office de granges. Une colonne de fumée s'échappe par un trou pratiqué au milieu du toit. Comme vous vous approchez de l'entrée, un chien aboie derrière la porte. A peine avez-vous frappé au panneau que la porte s'ouvre sur un homme portant une lanterne. Aveuglé par la lumière, vous ne pouvez distinguer ses traits. Des odeurs de cuisine, mêlées à des effluves animaux, vous parviennent aux narines. « Que voulez-vous ? » demande l'homme. « Un abri pour la nuit », répondez-vous. L'homme vous observe et vous fait entrer. « Faites vite, dit un vieillard édenté, blotti près du feu. Quand le froid entre dans les maisons, il n'en ressort pas avant le printemps. » La pièce est remplie d'humains et d'animaux. Tout le village est rassemblé ici pour l'hiver. L'homme qui vous a fait entrer vous mène près du feu, tandis qu'une femme vous sert une bouillie dans des bols de bois. « Mon nom est Shanhans, dit-il. Je suis le chef de cette communauté. En temps normal, nous ne pouvons accepter les étrangers mais, maintenant, toutes les routes sont bloquées et les Solons ne viendront pas nous visiter avant un mois ou deux. » Un jeune garçon d'une maigreur effrayante vient vous apporter un pichet d'eau fraîche. Vous le remerciez d'un signe de tête. « Quel chemin faut-il prendre pour aller au Château de la Nuit Éternelle ? » demandez-vous à Shanhans. L'homme vous fixe l'espace d'un instant et détourne aussitôt la tête. « Le  bébé va-t-il mieux ? » dit-il soudain en s'adressant à un groupe de femmes à l'air désolé. « Hélas ! répond l'une d'elles, je crains qu'il ne passe pas la nuit. » L'homme pousse un profond soupir. « Ce matin, un baquet d'eau bouillante s'est renversé sur son berceau... La mort va mettre un terme à ses souffrances. » Si votre groupe compte un prêtre qui désire soigner l'enfant, rendez-vous au <L to='5'>5</L>. Si vous pouvez le guérir avec un baume ou une potion de soins, rayez une dose de votre <Link to='/adv'>Feuille d'Aventure</Link> et rendez-vous au <L to='564'>564</L>. Si vous ne pouvez rien faire, rendez-vous au <L to='84'>84</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P517);
