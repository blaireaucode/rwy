/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P429 extends Component {
    render() {
        return (
            <div>

               L'aubergiste vous accueille d'une voix tonnante. «Bienvenue, étranger. Il vous en coûtera chacun 1 Pièce d'Or pour une chambre et 1 autre si vous désirez dîner. » Si vous pouvez payer 2 Pièces d'Or, rendez-vous au <L to='80'>80</L>. Si vous ne pouvez vous permettre de payer cette somme, rendez-vous au <L to='103'>103</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P429);
