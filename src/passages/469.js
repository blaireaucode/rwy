/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P469 extends Component {
    render() {
        return (
            <div>

               Bildad, le second, vient à votre rencontre et vous entraîne à l'écart. « J'ai observé votre conversation avec le capitaine, dit-il. A en juger par son exaltation, ce devait être important. Que se passe-t-il ? » Si vous désirez lui dire que ce n'est pas son affaire, rendez-vous au <L to='336'>336</L>. Si vous préférez lui faire part des projets du capitaine, rendez-vous au <L to='466'>466</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P469);
