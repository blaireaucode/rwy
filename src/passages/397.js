/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P397 extends Component {
    render() {
        return (
            <div>

               Tapi à l'ombre des arbres, vous voyez la lueur se poser au centre de la clairière et exploser en une multitude de particules bleues. Le sifflement augmente d'intensité, tandis qu'une épaisse fumée s'élève du point d'impact. Au centre du brouillard, vous pouvez apercevoir une pierre qui se fissure bientôt comme un neuf qui éclôt. Une silhouette se déploie. C'est un squelette revêtu d'une robe noire. Une flamme bleue brûle dans ses orbites vides. Il tourne la tête de droite à gauche, semblant humer l'air. Si le prêtre désire l'exorciser, rendez-vous au <L to='131'>131</L>. Si le voleur ou le prêtre désirent lui décocher une flèche, rendez-vous au <L to='491'>491</L>. Si le magicien désire jeter un sortilège, rendez-vous au <L to='68'>68</L>. Si vous jugez plus prudent de vous éloigner furtivement, rendez-vous au <L to='492'>492</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P397);
