/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P326 extends Component {
    render() {
        return (
            <div>

               « Ohé, de la Magdalena l vous écriez-vous. Combien pour nous mener au Royaume de Wyrd ? » Une haute silhouette se penche par-dessus le bastingage. « Nous n'allons pas aussi loin, répond-il. De toute manière, Wyrd est prise par les glaces à cette saison. Nous pouvons vous emmener jusqu'à Dourhaven. De là, vous pourrez rejoindre Wyrd en traversant la  banquise. Nous ne faisons pas payer le passage, mais il faudra nous aider à ramer. » Si vous acceptez cette offre, rendez-vous au <L to='187'>187</L>. Si la perspective de ramer vous répugne, rendez-vous au <L to='72'>72</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P326);
