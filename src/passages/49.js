/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P49 extends Component {
    render() {
        return (
            <div>

               Une nuée de dards ensanglantés surgit de la sphère. Leur taille augmente et, bientôt, une muraille d'épées acérées s'aligne, flottant dans les airs. Si vous êtes en cours d'affrontement, ou sur le point de combattre, les lames fondent sur vos ennemis et leur font perdre un total de 1 dé x 1 dé d'ENDURANCE avant de disparaître. Les Dommages sont répartis entre tous les ennemis en présence (arrondir au chiffre supérieur). L'armure assure la protection habituelle. Si vous n'êtes pas au combat lorsque vous activez la sphère, les lames se retournent contre vous. Si vous survivez, retournez au paragraphe que vous venez de quitter.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P49);
