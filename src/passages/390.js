/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P390 extends Component {
    render() {
        return (
            <div>

               Le bourreau brandit sa hache d'un air triomphal et disparaît dans un gigantesque éclat de rire. Le joueur qui s'est volatilisé pourra désormais tenir le rôle des monstres s'il le désire, mais conserve sa <Link to='/adv'>Feuille d'Aventure</Link> quelque temps encore. Vous reprenez votre progression, perturbé par la perte de votre compagnon, et pénétrez dans une longue gale rie. La paroi de droite est ornée de miroirs. Étrangement, ils ne reflètent pas votre image... La galerie se termine devant un large escalier surplombant un passage à ciel ouvert, bordé de hauts murs. Soudain, une procession passe en silence. Des hommes encagoulés tiennent des cierges bleus et entourent un cadavre allongé sur une civière. Dans quelques instants, elle arrivera à hauteur de l'escalier. Si vous désirez descendre pour observer le cortège de plus près, rendez-vous au <L to='177'>177</L>. Si vous préférez attendre dans l'ombre qu'il soit passé, rendez-vous au <L to='500'>500</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P390);
