/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P406 extends Component {
    render() {
        return (
            <div>

               De nouveau à l'extérieur, Varadaxor tire de son havresac un crucifix d'argent. « Veuillez accepter cette relique, dit-il. Elle renferme l'index de saint Ashanax. Puissent tous les saints veiller sur vous, mes amis. » Notez le crucifix d'argent sur votre Feuille d'Aventure (si vous êtes en groupe, désignez celui qui va le conserver). Apaisé, le vieux chevalier prend congé et s'éloigne vers le nord. Si votre groupe compte un prêtre, rendez -vous au 286. Sinon, rendez-vous au <L to='444'>444</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P406);
