/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P539 extends Component {
    render() {
        return (
            <div>

               Un maléfice plane sur la clairière. Vos rêves se sont matérialisés au point de vous blesser. De crainte d'une nouvelle agression, vous rassemblez votre paquetage à la hâte et fuyez dans la forêt environnante. Rendez-vous au <L to='165'>165</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P539);
