/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P480 extends Component {
    render() {
        return (
            <div>

               Le capitaine Derzu vous recommande l'auberge du Bon Tonneau, près des docks. Une couche vous coûtera 1 Pièce d'Or. La nuit de repos va vous permettre de récupérer la moitié de votre total de départ en points d'ENDURANCE (arrondi au chiffre supérieur). Le déjeuner coûte 1 Pièce d'Or et vous permet de récupérer 1 nouveau point d'ENDURANCE (sans excéder votre total de départ). Si certains de vos compagnons ont dormi à l'auberge des Os d'Ulrik, ils se rendent au <L to='205'>205</L>. A leur retour, rendez-vous au <L to='4'>4</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P480);
