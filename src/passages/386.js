/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P386 extends Component {
    render() {
        return (
            <div>

               MAGICIEN: Votre esprit sonde le futur. Les images se bousculent devant vos yeux. Soudain, vous entrevoyez la face grimaçante d'un Squelette. La créature s'avance lentement, les bras tendus. Vous sentez confusément que la mort est au bout de ses doigts décharnés... La vision s'interrompt brusquement. La lueur s'approche dangereusement du sol. Vous avez juste le temps de fuir avant qu'elle ne se pose. Rendez-vous au <L to='397'>397</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P386);
