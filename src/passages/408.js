/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P408 extends Component {
    render() {
        return (
            <div>

               La muraille se referme derrière vous juste avant que la créature ne vous atteigne. Un hurlement de rage est suivi de coups répétés contre la paroi. Vous poussez un soupir de soulagement, mais il meurt dans votre gorge, tandis que le sol se dérobe sous vos pieds ! Vous atterrissez sur une dalle de marbre quelques mètres plus bas, un peu étourdi. Vous vous trouvez à présent dans une vaste pièce faible ment éclairée d'un halo bleuté. La pénombre ne vous permet pas de déterminer les limites de la pièce, mais vous repérez des centaines de dalles identiques à celle où vous vous trouvez. Chaque dalle forme un carré de trois mètres de côté et est séparée de la suivante par un intervalle de deux mètres. A cet endroit, le sol est recouvert d'une brume grisâtre. Un personnage voûté se déplace entre les dalles en agitant un encensoir. La fumée grise qui s'en dégage se dépose lentement sur le sol. A son approche, vous remarquez qu'il est vêtu d'une robe de brocart dont les larges épaules se déploient comme des ailes. Une mitre chargée d'ornements compliqués maintient son visage dans l'ombre. De temps à autre, il se penche sur les dalles, mais la fumée et l'obscurité vous empêchent de distinguer ce qu'il observe. Brusquement, la dalle sur laquelle vous vous trouvez s'enfonce dans le sol. Chaque joueur doit soit quitter la dalle, soit demeurer à sa place. Quand chacun aura noté son choix, rendez-vous au <L to='60'>60</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P408);
