/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P229 extends Component {
    render() {
        return (
            <div>

               A peine avez-vous progressé de quelques mètres, qu'une forme monstrueuse surgit de la pénombre. La chose émet un grognement sinistre à votre vue. Au même instant, un raclement s'élève dans votre dos. Une paroi glisse lentement du plafond. Vous devez agir vite : affronter le monstre ou bien rebrousser chemin avant que la muraille ne vous enferme. Chaque joueur prend seul cette décision. Notez votre décision sur votre <Link to='/adv'>Feuille d'Aventure</Link> et rendez-vous au <L to='184'>184</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P229);
