/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P48 extends Component {
    render() {
        return (
            <div>

               La jeune fille recouvre ses esprits : « Je suis de Wyrd, murmure-t-elle. J'ai fui avec mes deux frères. Notre famille a trop souffert de la tyrannie du roi-sorcier. Mais l'Eislaken s'est dressé et a tué mes frères... comme il tue tous ceux qui tentent de... s'enfuir. » Sa voix n'est plus qu'un souffle, elle a perdu trop de forces et sa fin est proche. Si vous voulez lui demander qui est l'Eislaken, rendez-vous au <L to='135'>135</L>. Si vous préférez l'interroger sur la source des pouvoirs du roi, rendez-vous au <L to='544'>544</L>. Si vous voulez lui demander comment il peut être vaincu, rendez-vous au <L to='284'>284</L>. Si vous préférez la laisser mourir en paix, rendez-vous au <L to='59'>59</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P48);
