/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P124 extends Component {
    render() {
        return (
            <div>

               Vous avez le sentiment qu'il existe un lien surnaturel entre le mort et les lanternes. Peut-être qu'en les allumant, vous parviendrez à ranimer votre Double. Si vous désirez utiliser la sphère de Feu, rendez-vous au <L to='545'>545</L>. Si vous préférez recourir au briquet d'ambre, rendez-vous au <L to='176'>176</L>. Si vous ne possédez pas ces objets ou si vous ne voulez pas les employer, vous pouvez attaquer la procession (rendez-vous au <L to='516'>516</L>), ou bien rebrousser chemin (rendez-vous au <L to='547'>547</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P124);
