/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P468 extends Component {
    render() {
        return (
            <div>

               Décidément, tout cela ne vous dit rien qui vaille. Si vous désirez attendre la suite des événements, rendez-vous au <L to='144'>144</L>. Si vous préférez agir maintenant, rendez-vous au <L to='172'>172</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P468);
