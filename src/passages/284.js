/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P284 extends Component {
    render() {
        return (
            <div>

               Elle secoue la tête : « Il vous faudra d'abord atteindre le Château de la Nuit Éternelle. Certains disent qu'il est situé à l'extrême nord de l'île... D'autres prétendent qu'il n'existe que dans nos rêves... » Sa voix devient inaudible. Rendez-vous au <L to='59'>59</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P284);
