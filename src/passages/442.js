/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P442 extends Component {
    render() {
        return (
            <div>

               Vous vous approchez de la civière sur laquelle repose votre compagnon. Vous avez le sentiment qu'il existe un lien surnaturel entre le mort et les lanternes. Peut-être qu'en les allumant, vous parviendrez à ranimer votre compagnon. Mais, pour cela, une flamme ordinaire ne saurait suffire... Si vous possédez la sphère de Feu ou le briquet d'ambre, vous pouvez les utiliser pour allumer les lanternes (la sphère ne fonctionne qu'une fois, vous devrez la rayer de votre <Link to='/adv'>Feuille d'Aventure</Link> si vous l'employez). Vous pouvez aussi tenter une résurrection si vous possédez un objet (une amulettesscarabée, par exemple) permettant ce miracle. Si vous ne possédez aucun de ces objets, vous pouvez fouiller votre compagnon et prendre ses possessions. Ce faisant, vous pouvez aussi prendre la lame sacrificielle posée sur son torse. Sa lame d'argent bruni porte l'inscription : « Pourfendeuse d'Ombre » (notez-la sur votre <Link to='/adv'>Feuille d'Aventure</Link>). Si vous ramenez votre ami à la vie, rendez-vous au <L to='197'>197</L>. Si vous ne pouvez pas le ressusciter, rendez-vous au <L to='174'>174</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P442);
