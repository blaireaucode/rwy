/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P192 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Vous pouvez invoquer le Faltyn (rendez-vous au <L to='332'>332</L>), ou bien la Détection de la Magie (rendez-vous au <L to='423'>423</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P192);
