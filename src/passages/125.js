/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P125 extends Component {
    render() {
        return (
            <div>

               CHEVALIER : Le code d'honneur de votre ordre dicte votre comportement. « Sur ma foi, noble seigneur, je m'engage. Cette diablesse sera punie, j'en fais le serment, pour moi et mes compagnons. » Ces derniers ne sont peutêtre pas satisfaits, mais le vieillard exulte. « Vous êtes des braves parmi les braves. Personne depuis dix ans n'avait accepté de m'aider! » Rendez-vous au <L to='164'>164</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P125);
