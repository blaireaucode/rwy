/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P452 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Si Auguste pouvait manoeuvrer ce tapis, pourquoi n'en feriezvous pas autant? En règle générale, les objets magiques de cette dimension sont liés à une amulette. Si vous désirez utiliser un objet, allez-vous choisir la sphère de Feu (rendez-vous au <L to='529'>529</L>), la pierre blanche (rendez-vous au <L to='373'>373</L>) ou la pierre de touche bleue (rendez-vous au <L to='435'>435</L>) ? Si vous ne possédez aucun de ces objets ou si vous préférez ne pas les utiliser, retournez au <L to='133'>133</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P452);
