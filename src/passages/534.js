/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P534 extends Component {
    render() {
        return (
            <div>

               Vous entamez votre périple vers le nord, longeant la crête d'une vallée encaissée. Vous apercevez les lueurs d'un village en contrebas. La fumée qui s'échappe des cheminées s'élève droit dans le ciel hivernal. Vous pressez le pas, toutes vos pensées concentrées sur la chaleur de l'âtre, mais un doute vous traverse l'esprit. Quelle sera la réaction des villageois face à des étrangers ? Si vous désirez tout de même vous rendre au village, rendez-vous au <L to='517'>517</L>. Si vous préférez poursuivre votre route, rendez-vous au <L to='81'>81</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P534);
