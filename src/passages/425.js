/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P425 extends Component {
    render() {
        return (
            <div>

               Les Elfes ne vous laisseront pas traverser leur forêt. Ils s'avancent d'un pas résolu, tandis que vous vous emparez de votre arme. Vous allez devoir affronter sept Elfes. ELFE (Protection 1, Dommages 1 dé + 1) FORCE : 8 HABILETÉ : 8 POUVOIR : 8 ENDURANCE : 22  Si un joueur possède le crucifix de saint Ashanax, il peut, en le brandissant dans la direction d'un Elfe, empêcher ce dernier de se déplacer sur une case adjacente à celle qu'il occupe. Les Elfes n'ont pas d'âme et sont en effet repoussés par le crucifix. Le joueur désigne en début d'Assaut la case de  son choix. Il peut aussi de cette manière, lorsqu'il se déplace, forcer un Elfe à reculer d'une case. Les Elfes décochent des flèches s'ils ne se trouvent pas sur une case adjacente aux joueurs. La procédure est identique au tir des voleurs et des prêtres. Lorsqu'elles atteignent leur cible, les flèches infligent des Dommages de 1 dé + 1 d'ENDURANCE (moins la Protection). Toute fuite est inutile ; les Elfes, ayant une parfaite connaissance des bois, auraient tôt fait de vous retrouver. Si vous remportez ce combat, rendez-vous au <L to='289'>289</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P425);
