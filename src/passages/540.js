/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P540 extends Component {
    render() {
        return (
            <div>

               La muraille coulisse à nouveau, révélant le spectacle de la créature drainant l'énergie vitale de vos compagnons. Ravigoté, le monstre s'élance.  FONGOÏDE (Protection 3, Dommages 2 dés + 3) FORCE :7 HABILETÉ : 7 POUVOIR :7 ENDURANCE: 5 Si vous parvenez à le tuer, vous pouvez délester vos compagnons morts de leurs possessions ou bien les ressusciter si vous possédez des objets appropriés. Rendez-vous ensuite au <L to='219'>219</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P540);
