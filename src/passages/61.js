/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P61 extends Component {
    render() {
        return (
            <div>

               Vous vous enfoncez dans le sous-bois. Les branches basses et les ronces vous fouettent le visage et lacèrent vos vêtements. La forêt tout entière semble interdire votre fuite. L'air froid déchire vos poumons et, bientôt, une douleur au côté vous force à l'arrêt. Pantelant, vous brandissez vos armes, déterminé malgré tout à en finir avec la meute. CHIEN DE GIVRE (Dommages 2 dés + 1) FORCE : 8 HABILETÉ : 9 POUVOIR : 9 ENDURANCE: 25 Il ne vous reste pas assez d'énergie pour fuir. Si vous survivez à cet affrontement, rendez-vous au <L to='66'>66</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P61);
