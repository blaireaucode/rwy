/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P236 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Le monde s'efface et vous commencez à flotter dans les airs. Prenant appui sur la paroi, vous vous propulsez jusqu'à l'estrade. Vous vous posez en douceur à côté du coffre. Peu à peu, la réalité reprend ses droits. Vous soulevez le couvercle du coffre... Rendez-vous au <L to='294'>294</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P236);
