/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P263 extends Component {
    render() {
        return (
            <div>

               La créature se pulvérise au fond de la barque et ses cendres sont dispersées par le vent nocturne. Seuls subsistent deux joyaux bleus, à l'endroit où se trouvaient ses yeux (notez-les sur votre <Link to='/adv'>Feuille d'Aventure</Link>, si vous le désirez ; ils comptent pour un seul objet). Vous pouvez maintenant briser la glace qui emprisonnait votre embarcation. Vous regagnez la rive, bien décidé à quitter au plus vite cette forêt. Rendez-vous au <L to='165'>165</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P263);
