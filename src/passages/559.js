/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P559 extends Component {
    render() {
        return (
            <div>

               Vous n'avez qu'une vague idée de la direction à prendre, mais vous ne pouvez rester immobile dans ce désert de glace. Vous progressez avec peine. Les jours succèdent aux nuits, tout autour de vous le tapis de neige s'étend à perte de vue. Le froid vous glace jusqu'aux os et semble même avoir gelé vos pensées. Parfois, cinq silhouettes brumeuses apparaissent devant vous. Vous vous croiriez le jouet d'une hallucination si leurs ricanements ne résonnaient pas à vos oreilles... Chaque jour, la morsure du froid se fait plus sévère. Les joueurs qui n'ont pas de rations de survie perdent 1 point d'ENDURANCE par jour. Ceux qui ne possèdent pas de cape de fourrure perdent aussi 1 point par jour. Chaque nuit, les joueurs perdent 1 dé d'ENDURANCE (1 dé - 1, s'ils possèdent un sac de couchage). Si le groupe dispose d'un brasero, la perte d'ENDURANCE pour la nuit se trouve réduite de 1 autre point. Les joueurs qui ne se sont pas munis de gants ont les doigts gelés et subissent une pénalité de 1 point de FORCE, pour la durée de cette aventure. Votre calvaire dure cinq jours et cinq nuits mais, un matin, les survivants aperçoivent les côtes de Wyrd qui se profilent au loin. Rendez-vous au <L to='404'>404</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P559);
