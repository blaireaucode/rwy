/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P50 extends Component {
    render() {
        return (
            <div>

               Votre ancien compagnon est mort, cette fois-ci. Si vous disposez d'un objet permettant de le ressusciter, vous pouvez l'employer. Sinon, vous lui accordez une dernière pensée avant de le fouiller (consultez sa <Link to='/adv'>Feuille d'Aventure</Link>). Ce faisant, vous découvrez une amulette en forme de tête de mort autour de son cou. N'oubliez pas de la noter si vous la prenez. Rendez-vous au <L to='353'>353</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P50);
