/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P135 extends Component {
    render() {
        return (
            <div>

               « C'est un monstre hideux qui vit sous la banquise. Il a huit longs tentacules et un œil unique... Oh ! cet oeil... » Le simple souvenir du monstre ravive son angoisse. Rendez-vous au <L to='59'>59</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P135);
