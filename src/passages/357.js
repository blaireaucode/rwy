/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P357 extends Component {
    render() {
        return (
            <div>

               Vous lancez vos troupes à l'assaut de l'armée du roi-sorcier. Les Squelettes sont supérieurs en nombre, mais vos troupes se défendent vaillamment. La bataille fait rage, et l'issue du combat est loin d'être jouée. Peut -être pourriez-vous profiter de l'affrontement pour tenter d'atteindre le roisorcier... Lancez un dé. Ajoutez 1 au résultat si votre groupe compte un guerrier. Si vous faites 1 ou 2, rendez-vous au <L to='393'>393</L>. Si vous faites de 3 à 6, rendez-vous au <L to='374'>374</L>. Si vous faites 7, rendez-vous au <L to='216'>216</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P357);
