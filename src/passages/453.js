/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P453 extends Component {
    render() {
        return (
            <div>

               De nombreux objets sont impossibles à identifier mais vous repérez tout de même un brasero, un quignon de pain rassis, une bouteille grise pleine de liquide, un plat d'argent au centre duquel est gravé un pentagramme et un rouleau de parchemin doré. Notez les objets qui vous conviennent sur votre Feuille d'Aventure. Rendez-vous au <L to='455'>455</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P453);
