/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P157 extends Component {
    render() {
        return (
            <div>

               Allez-vous traverser la pièce en courant et foncer vers la porte (rendez-vous au <L to='377'>377</L>) ou bien prendre le temps de l'explorer (rendez-vous au <L to='433'>433</L>) ? 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P157);
