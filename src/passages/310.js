/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P310 extends Component {
    render() {
        return (
            <div>

               La sphère de Feu dégage une chaleur qui vous empêche de la tenir. Des flammes en jaillissent et la sphère se transforme en véritable fournaise, capable de faire fondre le métal. La chaleur vous force à reculer. Lorsque le brasier s'éteint, la sphère est réduite en cendres. Retournez au paragraphe que vous venez de quitter. Si vous activez la sphère de Feu alors que vous êtes à bord d'un navire, la fournaise qui s'en dégage embrase le pont et provoque un incendie ! Le bateau coule, ne vous laissant aucune chance de survie dans les eaux glacées de la Mer de Mistral. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P310);
