/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P182 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Vous vous avancez sur le pont phosphorescent, quand la dalle disparaît sous vos pieds ! Vous vous agrippez de justesse à la dalle qui se présente devant vous. Si vous désirez continuer votre progression de cette manière, rendez-vous au <L to='87'>87</L>. Si vous préférez réfléchir à une autre solution, rendez-vous au <L to='381'>381</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P182);
