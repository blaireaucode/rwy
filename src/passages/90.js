/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P90 extends Component {
    render() {
        return (
            <div>

               L'étincelle du briquet se transforme en long ruban de flammes qui allume les quatre lanternes, avant de s'étendre au-dessus du cadavre ! (Si vous ne les avez pas déjà éliminés, les personnages s'enfuient en hurlant de terreur.) Soudain, votre Double se lève tel un automate et s'avance, impassible, en brandissant son arme. Si vous désirez brandir la vôtre pour parer le coup, rendez-vous au <L to='456'>456</L>. Si vous préférez demeurer immobile, rendez-vous au <L to='375'>375</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P90);
