/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P385 extends Component {
    render() {
        return (
            <div>

               La dalle descend à une vitesse vertigineuse et s'arrête. Les joueurs demeurés sur la dalle ne peuvent plus rejoindre leurs compagnons. Un sortilège de Téléportation ne permet pas d'atteindre la salle du haut. Toutefois, un prêtre peut Léviter. Il doit pour cela se rendre au hasard au <L to='512'>512</L> ou au <L to='356'>356</L>. Si aucun prêtre ne se trouve sur la dalle, ou s'il ne désire pas Léviter, rendez-vous au <L to='450'>450</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P385);
