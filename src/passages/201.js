/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P201 extends Component {
    render() {
        return (
            <div>

               En vous approchant avec prudence vous distinguez un homme de haute stature, un jeune garçon et une jeune fille. Les jeunes gens, vêtus de tenues légères, dansent gracieusement, pieds nus dans la neige. L'homme prend bientôt conscience de votre présence. D'un geste lent, il ouvre les pans de sa cape rouge et disparaît progressivement à votre vue. Les jeunes gens chantonnent. Vous ne pouvez comprendre les paroles, mais la mélodie est empreinte de nostalgie. Ils s'éloignent tout en dansant. Si vous désirez les suivre, rendez-vous au <L to='362'>362</L>. Si vous préférez retourner vous coucher, rendez-vous au <L to='91'>91</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P201);
