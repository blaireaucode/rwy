/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P324 extends Component {
    render() {
        return (
            <div>

               Vous murmurez la formule qui active la sphère et vous la posez sur le sol. Quatre longs rubans de flamme en jaillissent, allumant instantanément les quatre lanternes (si vous ne les avez pas déjà éliminés, les porteurs s'enfuient en hurlant de terreur). La sphère dégage quelques dernières flammes avant de disparaître à son tour (rayez-la de votre Feuille d'Aventure). Soudain, votre compagnon se lève et projette au loin le linceul qui le recouvrait (vous pouvez lui rendre sa <Link to='/adv'>Feuille d'Aventure</Link>). Maintenant qu'il a connu la mort et qu'il en est revenu, il combattra sans peur. Son total de départ de FORCE augmente de 1 point. Vous n'avez aucune envie d'emprunter la direction que prenait ce cortège macabre, aussi rebroussez-vous chemin. Rendez-vous au <L to='547'>547</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P324);
