/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P484 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous focalisez votre énergie sur la créature. Le Squelette bondit et se débat, comme s'il voulait éloigner une nuée d'insectes. La lueur bleue qui anime ses orbites augmente d'intensité ! Le monstre chancelant avance maintenant dans votre direction. Si vous désirez prendre la fuite, rendez-vous au <L to='492'>492</L>. Si vous préférez poursuivre votre attaque, rendez-vous au <L to='41'>41</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P484);
