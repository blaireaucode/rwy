/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P299 extends Component {
    render() {
        return (
            <div>

               L'homme déclare s'appeler Auguste de Vanterie. Il manipule la pierre blanche qui pend à son cou et le tapis se pose sur le pont au milieu des marins stupéfaits. Vous rassemblez votre paquetage et prenez congé du capitaine. « Que les dieux soient avec vous ! » dit-il, comme vous prenez place sur le tapis. « Qu'ils veillent sur votre bateau ! » lui répondez-vous. Quelques instants plus tard, vous survolez l'océan à une vitesse stupéfiante. Rendez-vous au <L to='424'>424</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P299);
