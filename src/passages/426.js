/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P426 extends Component {
    render() {
        return (
            <div>

               Vous vous cachez dans l'ombre au pied des marches en attendant que la procession passe à votre hauteur. Vous observez le cadavre et reconnaissez avec horreur votre propre visage, figé dans la mort ! « Votre » corps est recouvert d'un linceul blanc et entouré de quatre lanternes éteintes. Les porteurs n'ont pas remarqué votre présence. Qu'allez-vous faire ? Attaquer la procession (rendez-vous au <L to='3'>3</L>), recourir à un objet (rendez-vous au <L to='109'>109</L>), attendre qu'elle s'éloigne et partir dans la direction opposée (rendez-vous au <L to='547'>547</L>) ou bien la suivre à distance (rendez-vous au <L to='281'>281</L>) ?

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P426);
