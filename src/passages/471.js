/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P471 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous avez étudié la logique au cours de votre apprentissage. Vous comprenez que cette énigme ne peut être résolue. En effet, chacune de ces affirmations renvoie à une autre, et aucune ne peut servir de référence. Tenter de résoudre ce problème serait comme se promener dans un labyrinthe sans issue... Rendez-vous au <L to='291'>291</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P471);
