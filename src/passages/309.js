/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P309 extends Component {
    render() {
        return (
            <div>

               Insensiblement, le visage amical des jeunes gens se transforme et leur regard s'anime d'une lueur mauvaise. Des canines animales apparaissent au coin de leurs lèvres !  VAMPIRE DES NEIGES (Dommages 1 dé + 3) FORCE : 7 HABILETÉ : 8 POUVOIR : 7 ENDURANCE: 25 Si vous préférez fuir, rendez-vous au <L to='366'>366</L>. Si vous parvenez à les vaincre, rendez-vous au <L to='235'>235</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P309);
