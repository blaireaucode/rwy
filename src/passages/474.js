/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P474 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : vous faites appel à vos souvenirs et tentez de retracer mentalement les constellations que vous aviez étudiées sur d'anciennes cartes astronomiques d'Opalar. Bientôt le crépuscule descend et l'étoile polaire apparaît dans le ciel. Grâce à elle et à la course de la Mort Rouge, il vous sera possible de calculer la direction de Wyrd. La température a chuté avec la nuit. Vous devez établir un bivouac et rassembler vos forces car un long périple vous attend. Rendez-vous au <L to='246'>246</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P474);
