/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P345 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Vous repoussez le garde qui vous empoigne et, d'un coup de pied circulaire, vous le fauchez aux jambes. L'homme s'écroule lourdement sur le sol. Rendez-vous au <L to='253'>253</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P345);
