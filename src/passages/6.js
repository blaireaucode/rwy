/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P6 extends Component {
    render() {
        return (
            <div>

               Vos efforts ont finalement raison de l'Élémental. La gigantesque vague explose en un million de gouttelettes et disparaît ! Au-dessus de vos têtes, Auguste laisse échapper un hurlement de rage. Rendez-vous au <L to='328'>328</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P6);
