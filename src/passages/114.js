/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P114 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Le Faltyn apparaît à vos côtés. Il observe la scène d'un air hautain : « Je réclame un objet pour le dérangement, dit-il. Je le veux maintenant... » Voici dans l'ordre de ses préférences, les objets de son choix : une cloche de fer, un briquet d'ambre, une amulette tête de mort, de la poussière d'argent. Si vous n'en possédez aucun, remettez-lui l'un de vos objets à votre choix (rayez-la de votre Feuille d'Aventure). Ceci fait, il s'envole vers la cage et délivre son confrère avant de disparaître. Le Faltyn libéré descend vers vous. Rendez-vous au <L to='568'>568</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P114);
