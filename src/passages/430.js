/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P430 extends Component {
    render() {
        return (
            <div>

               Chaque joueur de l'expédition doit surmonter ce péril ou mourir. Si vous êtes magicien, rendez-vous au <L to='186'>186</L>. Si vous êtes prêtre, rendez-vous au <L to='561'>561</L>. Si vous êtes chevalier, rendez-vous au <L to='74'>74</L>. Si vous êtes voleur, rendez-vous au <L to='409'>409</L>. Lorsque tous les survivants sont réunis, rendez-vous au <L to='35'>35</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P430);
