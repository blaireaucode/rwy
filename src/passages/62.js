/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P62 extends Component {
    render() {
        return (
            <div>

               Vous reprenez connaissance sur l'herbe tendre de la rive d'un lac. Ce décor vous semble étrangement familier. Ce lac ressemble à celui qui entourait le Château de la Nuit Éternelle, mais la monstrueuse bâtisse a disparu. A sa place se dressent quelques ruines rongées par le lierre... les eaux stagnantes ont, elles aussi, cédé la place à une onde claire qui scintille sous le soleil hivernal. Vous vous retournez vers la Forêt des Elfes. Les arbres fantomatiques envahis de ronces sont devenus des sapins majestueux d'un  vert étincelant... Soudain, une ombre vous recouvre. Vous vous dressez d'un bond, mais ce n'est qu'un couple de vieillards au sourire amical. La vieille femme ôte le pendentif qu'elle porte autour du cou et vous le remet. Le pommeau de l'Épée ! Vous alliez la remercier, mais déjà elle s'éloigne, en tenant son vieux compagnon par la main. Ils discutent ensemble et vous pouvez percevoir des éclats de rire joyeux. Votre quête est couronnée de succès. Les joueurs survivants se partagent équitablement un total de 1 000 points d'Expérience. Rendez-vous au <L to='570'>570</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P62);
