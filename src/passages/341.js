/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P341 extends Component {
    render() {
        return (
            <div>

               Shanhans, souriant, vous rejoint. « Mes amis, permettez-moi de vous présenter Ubara... » Vous portez votre attention sur la jeune fille qui vient de s'asseoir à quelques pas de vous. D'un geste, elle renvoie en arrière sa capuche. A sa vue, vous ne pouvez vous empêcher de sursauter. Devant votre surprise, l'adolescente lance un regard complice à Shanhans. Son crâne est entièrement rasé, à part une longue mèche jaune au sommet de sa tête. Une bande de peinture blanche forme un masque autour des yeux et un cercle est tatoué sur son front. « Ubara est une Seler, explique Shanhans. Elle est née dans un village semblable au nôtre. » Les villageois esquissent des sourires devant votre étonnement... Vous discutez une partie de la soirée avec Shanhans et Ubara. Les villageois, de toute évidence, répugnent à parler du roi-sorcier. Même Ubara réprime un frisson lorsque vous en faites mention. Pourtant son attitude est différente de celle des paysans. Elle est fière, sûre d'elle et ne craint pas de parler du roi-sorcier en termes peu flatteurs. A plusieurs reprises, elle le nomme « le vieil essoufflé » ou « le rongeur de rêves ». Bientôt vient l'heure de se coucher. Une femme vous apporte des couvertures. « Vous dormirez près du feu ce soir, dit-elle. Il n'y aura pas de cauchemar, à cause de la Seler. » Vous installez votre couche près de l'âtre tout en vous interrogeant sur le sens de ces curieuses paroles. De fait, nul rêve ne vient troubler votre sommeil. Vous vous réveillez le lendemain matin, en pleine forme. Chaque joueur récupère un nombre de points d'ENDURANCE équivalent à la moitié de son total de départ (arrondi au chiffre supérieur), plus (si nécessaire) 1 point pour le déjeuner que l'on vous sert. Il est temps de partir maintenant. Vous remerciez Shanhans de son hospitalité, mais Ubara demeure invisible. Vous repartez dans l'air vif du matin. Rendez-vous au <L to='330'>330</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P341);
