/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P242 extends Component {
    render() {
        return (
            <div>

               Les marins se déploient autour de vous, leur visage arborant des rictus haineux. Le jeune mousse qui lavait le pont court aussitôt se mettre à l'abri.  JADHAK (Protection 1, Dommages 2 dés + 2) FORCE : 8 HABILETÉ : 7 POUVOIR : 6 ENDURANCE: 30  N'oubliez pas de déduire les dommages que vous avez pu lui causer précédemment. MARIN (Dommages 2 dés) FORCE : 8 HABILETÉ : 7 POUVOIR : 6 ENDURANCE: 20 Si vous remportez ce combat, rendez-vous au <L to='478'>478</L>. Si vous préférez fuir en sautant par-dessus bord, rendez-vous au <L to='29'>29</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P242);
