/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P211 extends Component {
    render() {
        return (
            <div>

               « Je suis désolé, dites-vous. Seule notre mission importe et nous ne laisserons personne se mettre en travers de notre route. » Le seigneur elfe vous toise avec mépris : « Celui qui ne sait tenir sa parole, ne saurait rien accomplir d'important. » Vous haussez les épaules. « Nous n'avons pas de leçons à recevoir des Elfes, répliquez-vous. Tout le monde sait qu'il n'y pas pire menteur et tricheur qu'un Elfe. » Le seigneur elfe vous foudroie du regard et, d'un geste, appelle ses archers. Rendez-vous au <L to='425'>425</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P211);
