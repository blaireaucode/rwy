/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P522 extends Component {
    render() {
        return (
            <div>

               Vous allez affronter Auguste de Vanterie, en équilibre instable sur un tapis volant !  AUGUSTE (Dommages 2 dés + 2) FORCE : 8 HABILETÉ : POUVOIR : 9 ENDURANCE :7 40  Auguste dispose de trois sortilèges et combat avec une étrange dague à double lame. Jetez un dé à chaque Assaut pour déterminer ses actions. Si vous faites 1 ou 2, il combat avec sa dague ; si vous faites de 3 à 6, il lance un sortilège.  Serpent Fantôme (Niveau 2) Ce sortilège mental affecte une seule victime. Si elle ne peut y résister, elle perd 2 dés + 2 d'ENDURANCE. Confusion (Niveau 2) Ce sortilège mental affecte une seule victime. Si elle ne peut y résister, elle perd son arme, et combat à mains nues (- 2 en FORCE et - 2 de Dommages) pendant quatre Assauts. Passé ce délai, la victime reprend son arme, mais ce geste la rend vulnérable pendant un Assaut. Agression (Niveau 3) Ce sortilège d'attaque est dirigé contre une cible unique et provoque la perte de 3 dés d'ENDURANCE (moins la valeur de Protection). Auguste utilise ses sortilèges au hasard, à moins qu'un des joueurs éliminés ne prenne son rôle. Si vous parvenez à l'asservir, rendez-vous au <L to='224'>224</L>. Si vous le tuez, rendez-vous au <L to='241'>241</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P522);
