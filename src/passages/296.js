/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P296 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Tricher ne vous fait pas peur. Un joueur d'échecs expérimenté ne voit aucun déshonneur à vaincre un novice. Chacun utilise ses talents au mieux de ses intérêts. Votre stratégie va consister à détourner l'attention de l'Elfe afin de modifier la disposition des pièces à votre avantage. Il ne faudra bien sûr pas abuser de ce stratagème, car votre adversaire finirait par avoir des doutes, et il observe le plateau en permanence. Si vous préférez ne pas courir le risque et jouer régulièrement, rendez-vous au <L to='546'>546</L>. Si vous  choisissez de tricher, jetez deux dés. Si vous obtenez un résultat inférieur ou égal à votre total d'HABILETÉ avec deux dés, rendez-vous au <L to='239'>239</L>. Si vous obtenez un résultat supérieur, rendez-vous au <L to='218'>218</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P296);
