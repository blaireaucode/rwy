/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P120 extends Component {
    render() {
        return (
            <div>

               Le comportement des marchands vous a laissé une impression bizarre, bien que vous n'en puissiez déterminer les raisons. Si votre groupe compte un prêtre désireux d'intervenir, rendez-vous au <L to='106'>106</L>. Si le magicien désire tenter quelque chose, rendez-vous au <L to='47'>47</L>. Si le voleur désire intervenir, rendez-vous au <L to='132'>132</L>. Sinon, rendez-vous au <L to='144'>144</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P120);
