/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P399 extends Component {
    render() {
        return (
            <div>

               Une tenture de peau masque l'entrée de la caverne, ne laissant filtrer qu'un rai de lumière, telle une flèche indiquant le chemin du salut. Au moins cet étroit passage vous laissera-t-il un léger avantage sur vos poursuivants... Mais toutes vos craintes disparaissent au moment où vous pénétrez dans la grotte. L'intérieur est confortablement aménagé et un ragoût au fumet alléchant mijote dans l'âtre. Une vieille femme est assise près du feu. Malgré votre brusque irruption, elle tourne vers vous un visage engageant. « Bonjour, ditelle. Venez donc vous réchauffer. » D'un geste, vous lui intimez le silence et risquez un oeil à l'extérieur, vous attendant à trouver la meute grondante, rassemblée autour de l'entrée. Bizarrement, vous ne voyez que la neige, tombant en silence sur la forêt déserte. « Les Chiens ne viendront pas ici, reprend la vieille femme. Ils n'aiment pas la chaleur. » Elle vous tend un bol de ragoût. Vous vous approchez de la flambée. D'ordinaire, votre instinct vous dicte la défiance, mais vous êtes frigorifié et la fatigue a raison de vos dernières craintes. Vous vous jetez avec avidité sur la nourriture, sous le regard bienveillant de l'inconnue. « Il est temps de dormir maintenant », dit-elle. Ses paroles rassurantes sont comme une berceuse et vous sombrez dans le sommeil. Elle couvre vos épaules d'une couverture. Pendant un instant, vous la voyez plus jeune. Son visage irradie de bonté. Rendez-vous au <L to='403'>403</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P399);
