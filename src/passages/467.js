/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P467 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous tentez désespérément d'éviter les Crânes qui foncent sur vous, cherchant à vous déséquilibrer. Qui plus est, vous devez garder un œil attentif sur les dalles, qui peuvent disparaître à tout instant. La situation est grave, mais en utilisant votre Clairvoyance, peut-être pourriez-vous prévoir les mouvements des dalles... Si vous désirez tenter cette chance, rendez-vous au <L to='391'>391</L>. Vous pouvez aussi Léviter, pour ne pas risquer la chute (rendez-vous au <L to='526'>526</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P467);
