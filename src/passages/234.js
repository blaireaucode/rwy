/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P234 extends Component {
    render() {
        return (
            <div>

               Vous leur demandez quel est le meilleur chemin pour se rendre au Royaume de Wyrd. Les marchands se regardent avec des airs amusés. « S'il existe une route, elle est à sens unique, ricane l'un d'eux. Peu de voyageurs y vont et aucun n'en revient ! » Sa réplique provoque l'hilarité de ses confrères, mais devant votre air agacé, le marchand reprend son sérieux. Il plonge son doigt dans un verre de bière et dessine le contour de  la côte sur le comptoir. « Si vous tenez à vous y rendre, il faudra franchir la banquise. Prenez d'abord un bateau qui fait route vers le nord. Je vous conseille la Magdalena. Cette galère marchande nous a menés jusqu'ici. Elle devrait repartir dans quelques jours. Vous pourrez débarquer à Port Lukvik ou bien à Dourhaven. Ensuite, il faut compter trois jours de marche sur la glace pour atteindre les côtes de Wyrd. » Vous le remerciez de ses explications et vous vous éloignez. « Prenez garde, le terrain est glissant ! » lance un marchand avec un rire gras... Si vous ne savez pas encore où passer la nuit, rendez-vous au <L to='515'>515</L>. Si vous préférez sortir pour visiter la ville, rendez-vous au <L to='306'>306</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P234);
