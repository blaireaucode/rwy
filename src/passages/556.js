/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P556 extends Component {
    render() {
        return (
            <div>

               Un épais brouillard s'échappe de l'encensoir et vous recouvre d'un voile opaque. Une troupe fantomatique vous entoure, brandissant ses armes en direction des Squelettes. Notez les Spectres de Brume sur votre <Link to='/adv'>Feuille d'Aventure</Link> et rayez l'encensoir. Retournez au <L to='178'>178</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P556);
