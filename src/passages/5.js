/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P5 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous repoussez gentiment les femmes qui entourent le berceau. Le bébé est atrocement brûlé et tremble de tous ses membres. Pour le guérir, vous devez lui rendre 1 point d'ENDURANCE. Procédez aux soins de la manière habituelle. Si vous parvenez à lui rendre 1 point d'ENDURANCE, rendez-vous au <L to='222'>222</L>. Si vous échouez, rendez-vous au <L to='84'>84</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P5);
