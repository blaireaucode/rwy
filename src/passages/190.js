/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P190 extends Component {
    render() {
        return (
            <div>

               Vous reprenez connaissance, étendu sur l'herbe tendre de la rive d'un lac. Ce décor vous semble familier. Ce lac ressemble à celui qui entourait le Château de la Nuit Éternelle, mais la monstrueuse bâtisse a disparu. A sa place, se dressent des ruines rongées par le lierre... Les eaux stagnantes du lac ont elles aussi cédé la place à une onde claire qui scintille sous le soleil hivernal. Vous vous retournez vers la forêt des Elfes. Les arbres fantomatiques envahis de ronces sont devenus des sapins majestueux d'un vert étincelant... Le pommeau de l'Épée de Légende scintille dans l'herbe, à vos pieds ! Votre quête est couronnée de succès. Les joueurs survivants se partagent un total de 800 points d'Expérience. Maintenant, rendez-vous au <L to='570'>570</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P190);
