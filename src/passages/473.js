/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P473 extends Component {
    render() {
        return (
            <div>

               Vous expliquez au capitaine Lazarus qu'il vous faut vous rendre au Royaume de Wyrd. « Ce n'est pas un endroit où je voudrais aller ! s'exclame-t-il. Nous partons pour une campagne de pêche de quinze jours en Mer de Mistral. Nous nous arrêterons ensuite au port de Dourhaven pour décharger les prises. Un peu plus au nord, c'est la banquise. Vous pourrez vous rendre à pied au Royaume de Wyrd, mais il vous faudra du courage ! Je peux vous conduire à Dourhaven pour 15 Pièces d'Or par personne. » Si vous acceptez de payer cette somme, rendez-vous au <L to='247'>247</L>. Si vous ne pouvez pas payer un tel prix, rendez-vous au <L to='293'>293</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P473);
