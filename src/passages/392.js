/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P392 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Vous avancez sur le pont. Allez-vous recourir à la force (rendez-vous au <L to='100'>100</L>) ou à la ruse (rendez-vous au <L to='123'>123</L>) pour affronter les Sylphes ?

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P392);
