/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P111 extends Component {
    render() {
        return (
            <div>

               Vous enjambez le cadavre de Thanatos et pénétrez dans la salle qu'il gardait. Vous découvrez une cellule circulaire, dépourvue d'ornementation et éclairée par une lanterne pendue au plafond. Le faisceau de lumière baigne un objet fiché dans un socle au centre de la pièce. Il s'agit de l'étendard de la première légion du Selentium ! Cet objet doit avoir plus de mille ans. Il n'était connu jusqu'à ce jour dans le monde civilisé que par d'anciennes  estampes. Vous le soulevez avec déférence et, ce faisant, il vous semble entendre le bruit des farouches légions défilant à sa suite. L'étendard vous redonne le courage de rebrousser chemin et d'affronter une fois encore les périls du sanctuaire. Mais en retournant sur vos pas, vous ne traversez que des salles vides. Quelques minutes plus tard, vous quittez le sanctuaire. Rendez-vous au <L to='547'>547</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P111);
