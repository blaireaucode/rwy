/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P132 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Vous vous faufilez à la faveur de l'ombre jusqu'aux limites de la clairière. De là, vous rampez jusqu'à la couche d'un marchand. Vous comprenez au rythme de sa respiration qu'il simule le sommeil. Que peut-il bien attendre ? Vous décidez d'user de vos dons de ventriloque et, imitant le débit saccadé des marchands, vous lui murmurez : « On y va, maintenant? » L'homme émet un grognement agacé : « Silence, imbécile ! siffle-t-il entre ses dents. Les pèlerins risquent de nous entendre ! Tu sais bien que nous devons attendre l'apparition de la Lune Bleue ! » Vous vous éloignez, tout en vous interrogeant sur le sens de ses paroles. Rendez-vous au <L to='468'>468</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P132);
