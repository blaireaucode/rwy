/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P149 extends Component {
    render() {
        return (
            <div>

               L'aubergiste rajuste ses prix en grommelant. Si vous désirez passer une autre nuit dans la chambre commune, il vous en coûtera 1 Pièce d'Or (2 Pièces d'Or si vous voulez la couche près du feu). Vous lui souhaitez le bonjour et partez au hasard des rues. Vous déambulez pendant près d'une heure entre les étals, vous frayant un chemin parmi la foule. Sur la place du marché, vous vous arrêtez un instant pour regarder un spectacle de marionnettes. Vous contemplez, amusé, les figurines articulées quand, soudain, vous sentez qu'on vous déleste de vos armes ! Vous vous retournez, mais la pointe d'une épée vous maintient à distance. La milice  urbaine vous entoure. « Maudits étrangers ! grogne le capitaine de la garde. Vous pensiez échapper à la loi ? Heureusement, ce brave aubergiste est allé aussitôt rapporter votre crime. Chez nous, les meurtriers terminent au bout d'une corde ! » Si le chevalier désire intervenir, rendez-vous au <L to='513'>513</L>. Si le voleur désire intervenir, rendez-vous au <L to='345'>345</L>. Si votre groupe ne compte aucun de ces personnages, rendez-vous au <L to='552'>552</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P149);
