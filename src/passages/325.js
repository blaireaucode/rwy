/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P325 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vos pieds quittent le sol et vous vous élevez dans les airs, jusqu'à hauteur de la cage. Là, vous ouvrez la porte et défaites les liens qui emprisonnent le Faltyn. Quelques instants plus tard, vous vous posez avec lui. Rendez-vous au <L to='568'>568</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P325);
