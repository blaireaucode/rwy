/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P116 extends Component {
    render() {
        return (
            <div>

               Vous reprenez votre route vers l'est et atteignez le port de Quanongu en milieu de journée. Vous vous mêlez à la foule qui se presse dans les rues de la cité marchande. L'air est chargé d'effluves où se mêlent épices et salaisons. Vous vous arrêtez un instant, agressé par le tumulte de la ville, après ces longues heures passées à cheminer dans le silence de la campagne. Soudain, un marin au visage balafré vient à votre rencontre. « Vous cherchez du travail ? Mon navire, le Lacodon, embauche des bras robustes. » L'homme se plante devant vous, attendant votre réponse. Si vous acceptez cette offre, rendez-vous au <L to='232'>232</L>. Si vous préférez lui demander si son navire fait route vers Wyrd, rendez-vous au <L to='395'>395</L>. Si vous voulez répliquer : « Indique-nous une auberge et va voir ailleurs, minable », rendez-vous au <L to='269'>269</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P116);
