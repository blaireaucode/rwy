/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P352 extends Component {
    render() {
        return (
            <div>

               Un vieux colporteur, croulant sous le poids de sa marchandise, allume une pipe malodorante et commence à faire l'article. « Choisissez, tout est à vendre », déclare-t-il en libérant un nuage de fumée. Si vous désirez acheter quelque chose, rendez-vous au <L to='320'>320</L>. Si vous voulez juste lui demander où vous vous trouvez, rendez-vous au <L to='146'>146</L>. Si vous préférez le quitter, vous pouvez vous diriger vers l'auberge (rendez-vous au <L to='429'>429</L>) ou bien traverser le village et poursuivre votre périple (rendez-vous au <L to='267'>267</L>). 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P352);
