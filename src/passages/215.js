/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P215 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Les Sylphes attaquent au moment où vous vous apprêtiez à vous concentrer.  SYLPHE (Dommages 1 dé + 3) FORCE : 7 HABILETÉ : 8 POUVOIR : 8 ENDURANCE: 14 Si vous parvenez à jeter votre sortilège, rendez-vous au <L to='523'>523</L>. Si votre sortilège échoue et que vous désirez combattre, vous devez procéder à vos lancers avec trois dés. Si vous préférez battre en retraite, retournez au <L to='226'>226</L>. Si vous parvenez à éliminer les Sylphes, rendez-vous au <L to='350'>350</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P215);
