/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P446 extends Component {
    render() {
        return (
            <div>

               CHEVALIER : Transpercé de part en part, Auguste exhale un dernier souffle, le regard fixé sur votre épée plongée dans ses entrailles. A l'instant où il tombe à l'eau, l'Élémental disparaît à son tour, comme s'il n'avait jamais existé. Vous avez sauvé l'équipage mais, hélas, vous avez perdu votre épée ! Vous contemplez les eaux grises, l'air maussade. Le capitaine vous tape sur l'épaule. « Vous nous avez sauvés, dit-il. Prenez mon arme en remplacement de celle que vous avez perdue, c'est bien le moins que je puisse faire. » Il vous tend un vieux cimeterre à la lame rouillée. En effet, il ne pouvait pas faire moins... Notez-le sur votre <Link to='/adv'>Feuille d'Aventure</Link> et rendez-vous au <L to='565'>565</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P446);
