/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P541 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous reconnaissez aussitôt la hache légendaire du demi-dieu Héraklos ! Cette arme fantastique est dotée de pouvoirs magiques, mais elle ne peut être maniée que par une seule et unique personne, jusqu'à l'instant de sa mort. Cette hache ferait des merveilles entre les mains d'un chevalier... Choisissez celui qui va bénéficier de la hache et rendez-vous au <L to='454'>454</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P541);
