/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P83 extends Component {
    render() {
        return (
            <div>

               Vous vous frayez un passage parmi les clients et perdez de vue un court instant la table des prêtres. Lorsque vous arrivez enfin à leur hauteur, vous constatez avec surprise que le vieil homme n'y est plus. Les prêtres portent tous des masques de cuir, peints de couleur chair. « Veuillez vous asseoir », dit l'un d'eux d'une voix étouffée par son masque. Il pose sur la table un vieux corbeau afin de vous laisser de la place sur le banc. L'animal irascible se débat un instant en croassant. Vous engagez la conversation et en venez rapidement au but de votre voyage. « Le Royaume de Wyrd est pris par les glaces en cette période de l'année, répond l'un des prêtres. Aucun navire n'acceptera de vous y conduire. » Les prêtres soupirent l'un après l'autre et portent leur attention sur leur oiseau. « Notre ordre est pauvre et nous  sommes obligés de nous séparer de notre vieux compagnon. Voulez-vous l'acheter ? Il se nomme Scribo. Nous le vendons 1 Pièce d'Or... » demande un prêtre. Si vous acceptez de l'acheter, rayez 1 Pièce d'Or de votre Feuille d'Aventure. Les prêtres n'ont plus rien à vous apprendre et vous prenez congé. Rendez-vous au <L to='54'>54</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P83);
