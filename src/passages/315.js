/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P315 extends Component {
    render() {
        return (
            <div>

               Vous empoignez les rames et vous vous éloignez de la berge. Le Squelette émerge des fourrés. Il observe l'embarcation qui s'éloigne. Vous pensiez lui avoir enfin échappé, mais la créature se penche sur l'eau et y plonge son bras. Instantanément, la surface du lac se couvre d'une épaisse couche de glace qui emprisonne la barque ! Le monstre ricanant s'avance vers vous... Rendez-vous au <L to='274'>274</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P315);
