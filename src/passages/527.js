/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P527 extends Component {
    render() {
        return (
            <div>

               Vous atteignez enfin la dernière salle. Une arche s'ouvre sur le mur du fond, mais un gigantesque guerrier garde ce passage. Son armure est composée de plaques noires reliées entre elles par des joyaux. Un heaume hérissé de cornes lui masque le visage. Il se tient immobile, les deux mains sur le pommeau de son épée. Une couche de poussière s'est déposée à ses pieds. Il garde cette entrée depuis des siècles. « Je suis Tanathos, lance-t-il d'une voix tonnante. Je suis prêt à affronter quiconque voudra passer sous cette arche. » Si vous préférez rebrousser chemin et rencontrer une fois encore le Gardien de la Mémoire, rendez-vous au <L to='367'>367</L>. Si un joueur désire affronter Tanathos en combat singulier, rendez-vous au <L to='477'>477</L>. Si vous préférez l'attaquer à plusieurs, rendez-vous au <L to='71'>71</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P527);
