/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P73 extends Component {
    render() {
        return (
            <div>

               Les monstres gisent à vos pieds, ainsi que les marchands qui les servaient. Les Loups reprennent forme humaine et vous reconnaissez les deux joueurs d'échecs. Vous êtes maintenant seul dans la clairière. Les autres voyageurs sont morts ou ont fui dans les bois environnants. Vous remarquez que l'un des Loups-Garous a eu sa patte sectionnée au cours de l'affrontement et qu'elle n'a pas repris forme humaine. Notez la patte de Loup sur votre <Link to='/adv'>Feuille d'Aventure</Link> et rendez-vous au <L to='85'>85</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P73);
