/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P243 extends Component {
    render() {
        return (
            <div>

               Sept guerriers se lèvent en grognant. Manifestement, ils n'apprécient pas que vous ayez eu recours à la magie. Toute l'assemblée vous hue. Vous allez devoir les affronter, mais leur chef asservi combattra à vos côtés.  CHEF BARBARE (Protection 1, Dommages 2 dés + 1) FORCE : 8 HABILETÉ : 7 POUVOIR : 6 ENDURANCE: 30 BARBARE (Protection 1, Dommages 1 dé + 2) FORCE : 8 HABILETÉ : 6 POUVOIR : 6 ENDURANCE: 18 Vous embrassez la salle d'un regard circulaire : il n'y a aucune issue. Si vous remportez ce combat, rendez-vous au <L to='334'>334</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P243);
