/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P148 extends Component {
    render() {
        return (
            <div>

               Les marins vous encerclent. A en juger par leur air mauvais, ils sont déterminés à vous éliminer.  MARIN (Dommages 2 dés) FORCE : 8 HABILETÉ : 7 POUVOIR : 6 ENDURANCE: 20 Si vous remportez le combat, rendez-vous au <L to='251'>251</L>. Si vous préférez abandonner la lutte, rendez-vous au <L to='349'>349</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P148);
