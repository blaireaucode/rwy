/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P395 extends Component {
    render() {
        return (
            <div>

               « Le Royaume de Wyrd ? Aucune chance de l'approcher avant le printemps prochain, ricane-t-il. Les côtes sont prises par les glaces. Bien sûr, vous pourrez toujours aller au nord et tenter la traversée à pied, mais je vous souhaite bon courage ! » L'homme crache sa chique dans le caniveau et s'éloigne. Vous êtes à proximité d'une auberge appelée Les Os d'Ulrik. Si vous désirez y entrer, rendez-vous au <L to='17'>17</L>. Si vous préférez poursuivre votre chemin en direction des docks et de Port Lany, rendez-vous au <L to='306'>306</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P395);
