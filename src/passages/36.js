/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P36 extends Component {
    render() {
        return (
            <div>

               Le chevalier de votre groupe est condamné à perdre 30 points d'Expérience à l'issue de cette aventure pour s'être rendu sans combattre. Les marins mettent un canot à la mer et vous obligent à y descendre sous la menace de leurs harpons. L'embarcation est en mauvais état et, immédiatement, l'eau s'infiltre. Vous empoignez les rames et vous vous éloignez. « Les dieux de Krarth vous ont maudit ! s'exclame Lazarus. Que votre destin s'accomplisse! » Le Lacodon disparaît bientôt dans la brume, vous abandonnant au milieu de l'océan. Rendez-vous au <L to='113'>113</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P36);
