/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P349 extends Component {
    render() {
        return (
            <div>

               Vous jetez vos armes sur le pont. Aussitôt, les marins vous empoignent et vous ligotent. Malgré les protestations du capitaine, ils installent une planche au-dessus du vide et vous forcent à y grimper. « Ce sacrifice va calmer le dieu des flots ! » s'exclament-ils. Bientôt, la pointe d'un harpon vous force à sauter et vous disparaissez au fond de l'océan... Votre aventure est terminée.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P349);
