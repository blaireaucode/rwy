/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P30 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Hélas, tous les événements passés vous ont perturbé et vous ne pouvez plus vous concentrer pour l'instant ! Retournez au <L to='314'>314</L>, si un autre joueur désire libérer le Faltyn. Si les tentatives de tous se sont soldées par un échec, rendez-vous au <L to='221'>221</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P30);
