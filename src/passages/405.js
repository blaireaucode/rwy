/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P405 extends Component {
    render() {
        return (
            <div>

               Malgré leur détermination, les légions du Selentium ne sauraient vaincre cette troupe innombrable de Morts Vivants. Mais manifestement elles n'entreront pas seules dans la bataille... Le cœur des Ténèbres s'échauffe dans votre main et libère un sang noir qui se déverse sur le sable de l'arène. Le sang se coagule et des silhouettes se forment ; des soldats en armes se dressent devant vous. Le Bataillon des Ombres se rassemble, attendant vos ordres... Notez-le sur votre <Link to='/adv'>Feuille d'Aventure</Link> et rayez le cœur des Ténèbres. Retournez au <L to='178'>178</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P405);
