/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P360 extends Component {
    render() {
        return (
            <div>

               La créature ne possède pas les capacités de votre ancien compagnon. Ce Zombie n'est qu'une dépouille sans âme, un monstre assoiffé de sang ayant l'apparence de votre ami. ZOMBIE (Protection inchangée, Dommages 1 dé) FORCE : 6 HABILETÉ : 3 POUVOIR : 6 ENDURANCE : 30  Si vous parvenez à tuer ce monstre, rendez-vous au <L to='50'>50</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P360);
