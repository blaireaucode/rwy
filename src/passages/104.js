/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P104 extends Component {
    render() {
        return (
            <div>

               Cette maudite forêt semble n'avoir pas de limites. Les ronces entremêlées pointent leurs épines, comme autant de couteaux menaçants. Un vent glacial vous fouette le visage, le ciel a la couleur du plomb. Vous perdez bientôt toute notion du temps, ne sachant plus si vous marchez depuis des heures ou des jours. L'esprit embrumé, vous progressez comme dans un rêve monotone, sans plus prendre garde au paysage inchangé... Brusquement, vous prenez conscience d'avoir quitté la forêt. Vous cheminez maintenant dans une lande recouverte de neige vers un lac bordé d'étranges blocs de rochers. Vous vous retournez pour jeter un dernier regard à cette forêt maudite. Ce périple n'est plus qu'un mauvais souvenir et il vous semble même l'avoir rêvé. La surface enneigée qui vous sépare de la lisière des bois est immaculée, sans aucune empreinte... Rendez-vous au <L to='168'>168</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P104);
