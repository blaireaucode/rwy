/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P301 extends Component {
    render() {
        return (
            <div>

               Le vieux chevalier n'est pas disposé à vous laisser partir, maintenant qu'il s'est trouvé un auditoire. Il vous raconte avec force détails de quelle manière la Dame Grise entraîna ses trois frères dans un piège et les fit périr l'un après l'autre. La tour où elle demeure n'est qu'à quelques kilomètres au sud. Chaque fois qu'il mentionne son nom, sa voix tremble de rage. « C'est un Démon de forme humaine. Aucune femme ne peut égaler sa beauté, mais ne vous y fiez pas, le mal est ancré en elle. Je vous en prie, aidez-moi dans ma quête... » Si votre groupe compte un chevalier, rendez-vous au <L to='125'>125</L>. Si vous acceptez de prêter main forte au vieil homme, rendez-vous au <L to='164'>164</L>. Si vous refusez, rendez-vous au <L to='207'>207</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P301);
