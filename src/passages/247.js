/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P247 extends Component {
    render() {
        return (
            <div>

               Le capitaine Lazarus vous sourit, découvrant quelques chicots jaunis. « Maître Bildad, s'écrie-t-il aussitôt. Larguez les amarres ! » Bientôt, les marins déroulent les lourdes voiles et le navire prend la mer. « Accompagnez-moi dans la cabine, dit soudain le capitaine. Je dois vous montrer quelque chose. » Intrigué, vous le suivez jusque dans une pièce obscure jouxtant les cales. Rendez-vous au <L to='11'>11</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P247);
