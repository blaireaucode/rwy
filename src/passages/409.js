/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P409 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Comment allez-vous sortir de cette situation ? En ayant recours à votre agilité (rendez-vous au <L to='368'>368</L>) ou par la ruse (rendez-vous au <L to='112'>112</L>) ?

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P409);
