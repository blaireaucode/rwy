/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P156 extends Component {
    render() {
        return (
            <div>

               Un marin a surpris vos propos et bientôt la rumeur fait le tour du navire. « Lazarus veut chasser le Serpent de Mer! » En l'espace de quelques minutes, une troupe entoure le capitaine. Rendez-vous au <L to='231'>231</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P156);
