/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P481 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous comprenez votre erreur. La hallebarde se déplace, mais votre Clairvoyance vous apprend qu'il ne s'agit que d'une feinte. Ces informations contradictoires vous perturbent et vous perdez le bénéfice de votre sortilège. Vous allez devoir calculer les Dommages que vous infligez avec 3 dés pour la durée du combat.  SYLPHE (Dommages 1 dé + 3) FORCE : 7 HABILETÉ : 8 POUVOIR : 8 ENDURANCE: 14 Un des deux Sylphes vous empêche de progresser le long du pont. Si vous abandonnez le combat, retournez au <L to='226'>226</L>. Si vous les éliminez tous les deux, rendez-vous au <L to='350'>350</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P481);
