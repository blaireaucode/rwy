/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P273 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : En contemplant ce gouffre vertigineux, vos pensées se tournent vers Palamédès, votre maître, qui jamais ne se départait de son calme. Sa voix rassurante résonne dans votre esprit : « Comment peux-tu décrire cet instant ? » Si vous désirez répondre : « Je vais sauter dans le vide », rendez-vous au <L to='94'>94</L>. Si vous préférez répondre : « Je vais flotter comme une plume », rendez-vous au <L to='347'>347</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P273);
