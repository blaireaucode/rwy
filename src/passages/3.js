/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P3 extends Component {
    render() {
        return (
            <div>

               En vous apercevant, les personnages encagoulés s'arrêtent. Ils ne sont que cinq. Vous bondissez sur le plus proche, prêt à frapper. Pour toute riposte, l'homme abaisse lentement son cierge et éclaire son visage... Ce que vous voyez vous arrache un frisson d'horreur et l'air vient à vous manquer. Vous devez obtenir un résultat inférieur ou égal à votre Niveau avec 1 dé + 1 pour survivre à ce choc. Si vous obtenez un résultat supérieur, vous mourez de  peur... Répétez ce lancer chaque fois que vous frappez un des personnages. Ces derniers n'opposent aucune résistance et un seul coup suffit à les éliminer. Si vous parvenez à les tuer tous les cinq, rendez-vous au <L to='102'>102</L>. Si vous préférez fuir, rendez-vous au <L to='547'>547</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P3);
