/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P422 extends Component {
    render() {
        return (
            <div>

               Contrairement à ce que vous vous imaginiez, la banquise n'est pas une surface lisse et uniforme. Au contraire, un paysage tourmenté se déploie, hérissé de pics gelés, sculptés par le vent. Des rafales, soufflant du nord, s'insinuent dans vos vêtements, vous glaçant jusqu'aux os. Vous avancez péniblement. Les jours succèdent aux nuits. Bientôt, vous perdez toute notion du temps et seul votre instinct de survie vous fait encore avancer. Cet éprouvant périple dure trois jours. Chaque joueur perd 5 points d'ENDURANCE par jour. Appliquez les modifications suivantes à cette perte pour chaque jour : - 1 si vous portez une cape de fourrure ; - 1 si vous disposez de rations de survie ; si vous avez un sac de couchage ; - 1 si vous possédez un brasero. Qui plus est, les joueurs qui ne sont pas munis de gants souffrent du gel et perdent 1 point de FORCE pour la durée de cette aventure... Finalement, les côtes de Wyrd se dessinent à l'horizon. Rendez-vous au <L to='404'>404</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P422);
