/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P57 extends Component {
    render() {
        return (
            <div>

               Ce qui suit ne concerne que les joueurs endormis. Un grognement vous arrache à votre sommeil. Vous vous dressez aussitôt sur votre couche. Une face rubiconde, aux traits déformés par la haine, vous observe dans la pénombre. Vous repérez les canines démesurées d'un Vampire des Neiges... mais ce visage vous est connu ! Vous vous relevez d'un bond, arme en main, et hésitez un instant. Non ! Ce n'est plus votre compagnon, mais un Mort Vivant sans âme assoiffé de sang. Si vous êtes confronté à un seul Vampire, rendez-vous au <L to='8'>8</L>. Si plusieurs de vos compagnons sont devenus des Morts Vivants, rendez-vous au <L to='46'>46</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P57);
