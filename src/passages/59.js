/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P59 extends Component {
    render() {
        return (
            <div>

               Ses yeux se ferment définitivement, tandis que le soleil darde ses derniers rayons. Vous ne pouviez la sauver. Il faut maintenant songer à rallier la terre ferme avant la nuit. Vous pouvez prendre la cape et les gants de la jeune fille, ils ne lui seront plus d'aucun secours... Rendez-vous au <L to='365'>365</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P59);
