/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P218 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Vous vous apprêtiez à tricher quand, brusquement, l'Elfe empoigne votre main et la retourne, découvrant la pièce au creux de votre paume. D'un geste enragé, il balaie le plateau et l'envoie au loin. Vous cherchez aussitôt votre épée, mais une flèche vient se ficher dans votre bras, vous infligeant une perte de 5 points d'ENDURANCE (moins votre Protection). Vous l'arrachez en grimaçant. Il va falloir combattre. Rendez-vous au <L to='425'>425</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P218);
