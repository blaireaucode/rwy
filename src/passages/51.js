/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P51 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Vous escaladez la paroi du sanctuaire en prenant appui sur les ossements en saillie. Arrivé à hauteur de la cage, vous vous élancez dans le vide, bras tendus... Vous devez obtenir un résultat inférieur ou égal à votre HABILETÉ avec deux dés pour agripper la cage. Vous délivrez alors le Faltyn et la créature vous dépose sur le sol (rendez-vous au <L to='568'>568</L>). Si vous manquez ce lancer, vous chutez de plusieurs mètres et vous perdez 1 dé  d'ENDURANCE (votre armure n'assure aucune Protection). Dans ce cas, vous n'avez aucune envie de réitérer votre tentative. Retournez au <L to='314'>314</L> si un autre joueur désire à son tour sauver le Faltyn, ou au <L to='221'>221</L> si les essais de tous ont échoué.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P51);
