/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P266 extends Component {
    render() {
        return (
            <div>

               Le roi-sorcier s'esclaffe en vous voyant brandir l'anneau d'or. « N'avez-vous pas encore compris ? Chacune de vos idées est filtrée par mon rêve et peut se retourner contre vous... » A ces mots, il claque des doigts. L'anneau d'or s'envole dans le ciel et génère des dizaines de cercles concentriques qui se déploient et viennent se refermer sur vous ! Les bagues métalliques vous enserrent et vous étouffent. L'air vient à manquer. Vous lâchez les filins et chutez... dans un cauchemar sans fin spécialement conçu pour vous...

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P266);
