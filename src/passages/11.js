/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P11 extends Component {
    render() {
        return (
            <div>

               Le capitaine Lazarus allume une bougie et déroule une vieille carte. « Regardez ! s'exclame-t-il. Nous n'allons pas chasser la baleine, comme tout le monde le croit, mais le Grand Serpent de Mer Jormungand ! » Il pose sur vous un regard illuminé et se concentre à nouveau sur sa carte, désignant une courbe tracée au-dessus des flots. « Il est là, tapi dans les profondeurs  de la Mer de Mistral, et son terrain de chasse suit la course de la Mort Rouge quand elle se lève la nuit. Cette carte m'a coûté cher, vous pouvez me croire ! Nous allons chasser le monstre et ramener sa carcasse ! La gloire et la richesse nous attendent ! Le récit de nos exploits traversera les océans ! » Le capitaine semble pris d'une exaltation mystique. Cet homme est fou ! Hélas, il est trop tard pour rebrousser chemin ! Vous acquiescez avec un sourire gêné pendant qu'il vous explique comment il compte piéger le Serpent. Bientôt, il se tait et contemple la mer, le regard vide. Vous remontez sur le pont, sans toutefois montrer votre inquiétude... A la tombée de la nuit, le navire essuie une violente tempête qui oblige les marins à réduire la voilure. Bientôt des paquets de mer s'écrasent sur le pont et tout le monde est mis à contribution pour écoper. Le roulis commence à vous rendre malade. Chaque joueur doit jeter deux dés. Ajoutez 1 au résultat si vous avez déjeuné avant d'embarquer. Si vous faites 10, vous êtes sujet au mal de mer et vous perdez 2 points d'ENDURANCE (si votre total n'est plus que de 2 points, retranchez seulement 1 point : le mal de mer n'est pas mortel... !). Rendez-vous au <L to='261'>261</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P11);
