/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P237 extends Component {
    render() {
        return (
            <div>

               Il pousse un grognement de rage et dégaine son épée. « On ne parle pas sur ce ton à Jadhak le Balafré. Je vais vous apprendre le respect ! »  JADHAK (Protection 1, Dommages 2 dés + 2) FORCE : 8 HABILETÉ : 7 POUVOIR : 6 ENDURANCE : 30 Si vous le tuez, rendez-vous au <L to='181'>181</L>. Si vous préférez fuir, rendez-vous au <L to='141'>141</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P237);
