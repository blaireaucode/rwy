/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P188 extends Component {
    render() {
        return (
            <div>

               Vous pénétrez dans une zone obscure, mais au fur et à mesure de votre progression des lueurs apparaissent. Des visages au regard hypnotique se rapprochent. Les serviteurs du néant avancent leurs lèvres minces, prêts à déposer un baiser mortel. Si vous possédez de la poudre d'argent, projetez-là en direction des visages. Elle les fera disparaître (dans ce cas, rayez la poudre de votre <Link to='/adv'>Feuille d'Aventure</Link>). Si vous ne possédez pas la poudre d'argent, les visages vous embrassent. Chaque joueur doit alors obtenir un résultat inférieur ou égal à son total de POUVOIR avec deux dés. Si le résultat est supérieur, il disparaît de la surface du monde, avec tout son équipement. Les joueurs survivants peuvent pour suivre l'exploration du sanctuaire (rendez-vous au <L to='367'>367</L>) ou bien retourner sur leurs pas, mais ils devront affronter à nouveau les gémissements de la Destinée (rendez-vous au <L to='557'>557</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P188);
