/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P454 extends Component {
    render() {
        return (
            <div>

               Si le chevalier a pris la hache, rendez-vous au <L to='130'>130</L>. Si elle a été confiée à un autre aventurier, rendez-vous au <L to='400'>400</L>. Si personne ne l'a prise, rendez-vous au <L to='226'>226</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P454);
