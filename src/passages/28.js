/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P28 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Le tapis d'Auguste est protégé contre les attaques psychiques. A peine avez-vous entamé votre exorcisme, que vous subissez un choc en retour ! Pour y résister, vous devez obtenir un résultat inférieur ou égal à votre total de POUVOIR avec deux dés. Si vous échouez, vous perdez vos capacités d'exorcisme pour toute cette aventure. Retournez au <L to='133'>133</L> et choisissez une autre option.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P28);
