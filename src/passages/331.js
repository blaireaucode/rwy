/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P331 extends Component {
    render() {
        return (
            <div>

               Ce n'est pas un hasard si ce sol est recouvert de dalles noires et blanches. Votre intuition vous dit que l'une des couleurs est piégée et pas l'autre. Si vous décidez de pénétrer dans la pièce en ne marchant que sur les dalles blanches, rendez-vous au <L to='98'>98</L>. Si vous préférez ne fouler que les dalles noires, rendez-vous au <L to='136'>136</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P331);
