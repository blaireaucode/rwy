/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P499 extends Component {
    render() {
        return (
            <div>

               Le navire dérive pendant des heures, au gré des courants. Enfin, le vent se lève à nouveau, balayant les nuages. Aussitôt, les marins regagnent leurs postes et le Lacodon reprend sa route. Rendez-vous au <L to='298'>298</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P499);
