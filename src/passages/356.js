/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P356 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous faites le vide dans votre esprit et vous vous élevez dans les airs. Vos compagnons doivent se rendre au <L to='450'>450</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P356);
