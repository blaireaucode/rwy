/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P20 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : « Hélas, je ne possède pas de talent particulier pour la musique ! déclarez-vous, mais je connais quelqu'un qui sait jouer d'un instrument. » L'homme vous observe, un peu surpris, et accepte votre requête. Les yeux fermés, vous vous concentrez et invoquez le Faltyn. Bientôt, la petite créature apparaît à vos seuls yeux. Vous lui demandez de jouer une mélodie. Avec une moue dédaigneuse, le Faltyn sort une flûte et entame une série de pièces, issues de la nuit des temps. Ces étranges complaintes vous bouleversent et, lorsqu'il s'arrête, toutes les personnes alentour ont les larmes aux yeux. Les forestiers s'approchent de vous et  chacun d'eux vous remet 1 Pièce d'Or en gage de gratitude. Mais le Faltyn, avant de disparaître dans son monde, s'empare de la moitié de la somme avec un petit rire. Il vous reste 5 Pièces d'Or, que vous pouvez noter sur votre <Link to='/adv'>Feuille d'Aventure</Link>. Si vous désirez retourner à votre campement pour vous reposer, rendez-vous au <L to='486'>486</L>. Si vous préférez observer les joueurs d'échecs, rendez-vous au <L to='554'>554</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P20);
