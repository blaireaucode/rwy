/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P514 extends Component {
    render() {
        return (
            <div>

               La Dame Grise a développé une barrière psychique pour se protéger. L'énergie de votre sortilège vient d'être détournée, mais cet effort a brisé la concentration qui emprisonnait Varadaxor. A nouveau libre de ses mouvements, le vieux chevalier reprend son attaque. D'un ample mouvement circulaire il abat son épée sur la créature immobilisée et la décapite ! La tête tombe sur le sol avec un bruit sec et se brise en morceaux. Au même instant, les armures s'écroulent à vos pieds. Rendez-vous au <L to='108'>108</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P514);
