/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P494 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : L'observation du sol vous met mal à l'aise. Il serait préférable de pénétrer dans la pièce en ayant recours à la Lévitation. Cela comporte certains risques car vous n'employez pas souvent cette faculté. Si vous désirez essayer malgré tout, rendez-vous au <L to='236'>236</L>. Si vous préférez ne pas courir le risque, retournez au <L to='398'>398</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P494);
