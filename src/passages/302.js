/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P302 extends Component {
    render() {
        return (
            <div>

               A l'instant où vous prononcez le mot qui active ses pouvoirs, des filaments bleus jaillissent de la sphère et se déploient dans l'espace. La majorité des filaments s'enroule autour du possesseur de la sphère, les autres s'attaquant à toute personne, amie ou ennemie, se trouvant à proximité. Le joueur qui tient la sphère doit obtenir un résultat inférieur ou égal à son total de POUVOIR avec 2 dés + 1 pour ne pas mourir sur-le-champ. Toutes les personnes ou créatures (si elles possèdent un total de POUVOIR) procèdent  à un jet de dé similaire, mais avec 1 dé + 3. Les survivants voient la sphère disparaître. Retournez au paragraphe que vous venez de quitter.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P302);
