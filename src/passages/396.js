/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P396 extends Component {
    render() {
        return (
            <div>

               Les pouvoirs de la sphère sont aléatoires. Jetez un dé pour découvrir les effets qu'elle prodigue Vie La sphère ressuscite un joueur mort, à condition que son cadavre soit présent. Jetez un dé pour évaluer les points d'ENDURANCE que lui rend la résurrection. Soins Tous les joueurs blessés récupèrent 2 dés d'ENDURANCE. Puissance Lancez un dé ; le joueur qui obtient le résultat le plus élevé augmente son total de départ de FORCE de 1 point. Astuce Un joueur (choisi au hasard) augmente son total de départ d'HABILETÉ de 1 point. Talent Un joueur (choisi au hasard) augmente son total de départ de POUVOIR de 1 point. Malédiction Tous les joueurs présents perdent définitivement 1 point dans la capacité de leur choix. La sphère se pulvérise après avoir libéré son pouvoir. Retournez au paragraphe que vous venez de quitter.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P396);
