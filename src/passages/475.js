/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P475 extends Component {
    render() {
        return (
            <div>

               A l'instant où vous ouvrez la cassette, une dizaine d'immondes sangsues viennent se coller sur votre main. Votre vision se brouille et vous êtes pris de vertiges. Votre total de départ de FORCE vient de perdre 1 point. Après avoir drainé une partie de votre énergie vitale, les créatures tombent en poussière. Si vous avez ouvert toutes les cassettes, rendez-vous au <L to='291'>291</L>. Sinon, vous pouvez encore ouvrir la cassette de pierre (rendez-vous au <L to='511'>511</L>) ou celle de bois (rendez-vous au <L to='154'>154</L>). 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P475);
