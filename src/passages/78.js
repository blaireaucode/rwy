/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P78 extends Component {
    render() {
        return (
            <div>

               Vous murmurez la formule qui active la sphère. Une intense chaleur s'en dégage et vous devez la lâcher. Elle touche la glace dans un sifflement de vapeur et s'embrase. La fournaise qui s'en dégage vous force à reculer d'un pas. Lentement, la sphère s'enfonce dans la banquise, en direction du monstre... Quelques instants plus tard, un hurlement de douleur retentit ! Des flammes attaquent les tentacules. Sans perdre une seconde, vous vous enfuyez vers la côte. Lorsque vous vous retournez, le monstre en flammes a jailli de la banquise. Vous rejoignez aussitôt la terre ferme. Rendez-vous au <L to='117'>117</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P78);
