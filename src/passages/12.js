/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P12 extends Component {
    render() {
        return (
            <div>

               PRÊTRE et MAGICIEN : De toute évidence, Auguste était un illusionniste. La majorité de ses pouvoirs provenait des objets et non de ses facultés propres. Vous n'avez qu'une faible idée de l'usage des articles que vous apercevez sur les étagères, mais au moins deux d'entre eux retiennent votre attention. Vous vous apprêtez à les prendre quand un doute vous traverse l'esprit : Auguste a peut-être entouré ces objets d'un piège magique pour décourager les voleurs... Rendez-vous au <L to='453'>453</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P12);
