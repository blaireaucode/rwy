/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P228 extends Component {
    render() {
        return (
            <div>

               Le corps du Vampire est agité de spasmes et se raidit. Les traits familiers de votre ancien compagnon réapparaissent avec la mort. Encore sous le choc, vous enterrez sa dépouille et recouvrez sa tombe de pierres. Vous vous asseyez en silence près de cette sépulture, attendant l'aube. Rendez-vous au <L to='91'>91</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P228);
