/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P407 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Vous regardez les dalles phosphorescentes et les Crânes Étincelants qui se rapprochent à toute allure. La cour en contrebas est un gouffre béant qui vous attire... Trop de dangers vous entourent. Vous vous concentrez et téléportez immédiatement tout le groupe de l'autre côté du pont. Rendez-vous au <L to='35'>35</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P407);
