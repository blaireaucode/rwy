/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P566 extends Component {
    render() {
        return (
            <div>

               La zone couverte de runes vous semble dangereuse. Vous la contournez avec prudence et atteignez bientôt la porte du fond. Elle s'ouvre alors avec fracas sur une vision de cauchemar ! Un cadavre en état de décomposition s'avance dans la pièce. Il est vêtu comme un bourreau. Il brandit un lourd cimeterre et l'abat avec force sur le premier selon l'ordre de marche. L'infortuné reçoit le  coup de plein fouet... et disparaît dans un tourbillon de vapeur ! Rendez-vous au <L to='390'>390</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P566);
