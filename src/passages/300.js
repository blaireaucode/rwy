/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P300 extends Component {
    render() {
        return (
            <div>

               Vous gravissez les marches de la tribune royale. Le roi-sorcier vous laisse approcher. « Assez joué, maintenant, déclare-t-il d'une voix glaciale. Vous allez connaître l'étendue de mes pouvoirs... » Il frappe dans ses mains et, instantanément, la scène qui vous entoure explose en un million de fragments... Rendez-vous au <L to='15'>15</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P300);
