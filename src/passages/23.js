/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P23 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Vous vous concentrez sur l'amulette, expérimentant sur elle une série de formules et de gestes magiques. Brusquement, le tapis entame une descente en chute libre et se pose brutalement sur la banquise. Chaque joueur perd 1 point d'ENDURANCE. Cette expérience vous convainc de ne plus tenter d'activer ce tapis. Rendez-vous au <L to='344'>344</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P23);
