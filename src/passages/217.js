/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P217 extends Component {
    render() {
        return (
            <div>

               Le jeune mousse vient s'accouder au bastingage. C'est alors qu'il repère les mouettes. « Capitaine, capitaine ! s'exclame-t-il. Une baleine à bâbord ! » Lazarus accourt sur le pont et félicite le gamin d'une bonne claque dans le dos. « Bien vu, moussaillon ! Le métier entre ! Les mouettes se nourrissent des poissons qui suivent la baleine », vous explique-t-il. Des chaloupes sont mises à la mer. Une activité intense s'est emparée du navire. Allez-vous avouer à Kénoy que le capitaine traque en fait une plus grosse prise (rendez-vous au <L to='156'>156</L>) ou simplement observer les préparatifs (rendez-vous au <L to='510'>510</L>) ?

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P217);
