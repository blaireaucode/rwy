/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P328 extends Component {
    render() {
        return (
            <div>

               Auguste manipule son amulette et le tapis prend de l'altitude. « Ne croyez pas avoir gagné ! enrage-t-il. L'Albane et la Mort Rouge comptent d'innombrables adorateurs. Nous serons toujours sur votre route ! Même le roisorcier, pour des raisons qui lui sont propres, s'opposera à votre quête. Vous comprendrez vite ce qu'il en coûte de défier les dieux ! » Sa malédiction se perd dans le vent tandis qu'il s'éloigne vers l'est. Rendez-vous au <L to='565'>565</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P328);
