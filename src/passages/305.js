/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P305 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Le voile du temps s'entrouvre sur votre avenir. Vous vous voyez confronté à une série d'épreuves : la maladie, la terreur, la destruction et l'oubli. Mais une plus grande encore vous attend... Vous reprenez conscience, le front ruisselant de sueur. Vous êtes trop affaibli pour tenter un deuxième Oracle. Si vous désirez poursuivre votre exploration, rendez-vous au <L to='160'>160</L>. Si vous préférez quitter le sanctuaire, rendez-vous au <L to='547'>547</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P305);
