/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P537 extends Component {
    render() {
        return (
            <div>

               La nuit se déroule sans incident. Dehors, la tempête recouvre les vitres d'un épais tapis de neige, mais le feu dans l'âtre prodigue une douce chaleur. Chaque joueur recouvre un nombre de points d'ENDURANCE équivalent à la moitié de son total de départ (arrondi au chiffre supérieur). Le lendemain, vous vous levez à l'aube et commandez à déjeuner. Pendant que vous attendez, une vieille femme vient nourrir le feu. Bientôt, les fagots pétillent joyeusement et vous vous surprenez à voir dans les flammes des visages qui se dessinent : une jeune femme au regard amical ou un homme aux traits durs. «Certains lisent leurs rêves dans le feu, d'autres dans la glace », murmure la vieille en passant à votre hauteur. Vous alliez lui répondre, mais l'aubergiste arrive à cet instant avec le déjeuner. Ce repas coûte 1 Pièce d'Or et rend 1 point d'ENDURANCE (sans dépasser le total de départ). Tout en mangeant, vous interrogez l'aubergiste sur la direction à prendre pour se rendre à Wyrd. « Suivez la côte vers le nord pendant une journée. Le soir, vous devriez atteindre une montagne. Elle marque le début des glaces. Un de ses versants dessine une mère tenant son enfant. Suivez la direction du profil: nord, nord-est. Il faut compter encore deux jours de marche sur la banquise avant d'atteindre Wyrd. » Rendez-vous au <L to='496'>496</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P537);
