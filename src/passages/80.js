/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P80 extends Component {
    render() {
        return (
            <div>

               Sans attendre, vous plongez entre les draps frais du lit et sombrez dans un profond sommeil. Cette nuit de repos vous permet de récupérer un nombre de points d'ENDURANCE équivalent à la moitié de votre total de départ (arrondi au chiffre supérieur). Le lendemain matin, vous retournez dans la salle commune. L'aubergiste s'affaire autour de l'âtre et fait rôtir un agneau. Vous mangez de bon appétit et ce repas vous permet, si nécessaire, de récupérer 1 point d'ENDURANCE supplémentaire. Maintenant, rendez-vous au <L to='245'>245</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P80);
