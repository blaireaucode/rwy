/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P140 extends Component {
    render() {
        return (
            <div>

               La galerie du milieu possède plusieurs portes, mais elles sont verrouillées, sauf celle d'un laboratoire. Si votre groupe compte un prêtre ou un magicien, rendez-vous au <L to='12'>12</L>. Sinon, rendez-vous au <L to='453'>453</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P140);
