/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P436 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Un flot ininterrompu de pensées contradictoires et d'images se déverse dans votre esprit. Vous entendez distinctement un claquement de mains... et la lueur fulgurante d'un éclair. Si vous désirez briser votre concentration, rendez-vous au <L to='43'>43</L>. Si vous préférez poursuivre votre exploration mentale, rendez-vous au <L to='279'>279</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P436);
