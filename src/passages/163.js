/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P163 extends Component {
    render() {
        return (
            <div>

               La Dame Grise est immobilisée par les rayons du soleil, mais sa magie agit encore. Elle a pétrifié Varadaxor avant qu'il ne lui assène le coup fatal. La sorcière reporte maintenant son attention sur vous. Une voix s'empare de votre esprit. « Fuyez ! ordonne-t-elle. Ou bien je lâche les forces obscures et vous mourrez ! » Si vous désirez fuir, rendez-vous au <L to='394'>394</L>. Si vous ne voulez pas abandonner Varadaxor, rendez-vous au <L to='421'>421</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P163);
