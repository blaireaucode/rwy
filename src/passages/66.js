/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P66 extends Component {
    render() {
        return (
            <div>

               Vous reprenez votre progression. Vous êtes épuisé et, à plusieurs reprises, vos jambes se dérobent. Vos lèvres sont desséchées par le froid et vos paupières gonflées de fatigue semblent peser des tonnes. L'aube s'est-elle levée ? Vous ne sauriez le dire. La forêt est baignée d'une lueur blafarde. Rendez -vous au 104.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P66);
