/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P440 extends Component {
    render() {
        return (
            <div>

               Vous avancez à pas feutrés en direction du retable mais, malgré toutes vos précautions, le monstre a entendu votre approche. C'est une Hydre ! D'ordinaire, ces créatures sont des adversaires implacables, mais celle-ci a déjà essuyé de nombreuses blessures. Les guerriers dans la pièce précédente ont défendu chèrement leur vie. La majorité des têtes du monstre pendent sur le côté et des entailles courent sur ses flancs. Elle s'est réfugiée dans le musée pour mourir mais, malgré cela, elle conserve assez d'énergie pour un ultime combat ! A votre vue, elle fouette l'air de sa longue queue serpentine, interdisant toute fuite.  HYDRE (Protection 3, Dommages 2 dés + 2) FORCE : 7 HABILETÉ : 7 POUVOIR : 9 ENDURANCE :8  Deux têtes sont encore valides. Cela signifie que l'Hydre peut attaquer deux adversaires par Assaut. Si vous parvenez à l'éliminer, rendez-vous au <L to='506'>506</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P440);
