/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P505 extends Component {
    render() {
        return (
            <div>

               Vous vous recroquevillez, encore sous le choc. Vous n'osez vous rendormir, de crainte de revivre ce cauchemar. Bientôt, le soleil apparaît à l'horizon, baignant la contrée de teintes écarlates. Tout en vous préparant à partir, vous ne pouvez vous empêcher de ressasser ce proverbe Tahashim : « Le rêve est la forme nue de la vérité. » Rendez-vous au <L to='422'>422</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P505);
