/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P438 extends Component {
    render() {
        return (
            <div>

               A l'instant où vous lui portez le coup fatal, les vivats des marins retentissent. La vague gigantesque a disparu ! Peut-être n'était-ce là qu'une illusion engendrée par Auguste... Le mage chute de son tapis volant et disparaît dans les flots. Le navire reprend sa route. Le tapis flotte pitoyablement sur les eaux. Rendez-vous au <L to='565'>565</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P438);
