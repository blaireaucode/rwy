/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P513 extends Component {
    render() {
        return (
            <div>

               CHEVALIER : Vous écartez les bras en poussant un grognement de bête fauve ! Surpris, le capitaine de la garde recule d'un pas. D'un geste vif, vous empoignez les deux gardes qui vous entourent et leur fracassez le crâne l'un contre l'autre ! Rendez-vous au <L to='253'>253</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P513);
