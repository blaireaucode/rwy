/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P313 extends Component {
    render() {
        return (
            <div>

               Le monstre sur vos talons, vous vous engouffrez sous le porche. Des serpents venimeux tombent alors du linteau sculpté mais, heureusement, aucun ne vous atteint. Au même instant, la herse s'abat, manquant de vous transpercer ! Le monstre fonce de tout son poids sur la grille, mais la morsure des serpents le force à reculer. Vous reprenez votre souffle et inspectez les lieux. Un escalier s'enfonce en serpentant dans les ténèbres. Lentement, vous descendez les marches. En bas, vous manquez trébucher sur des cadavres. Trois aventuriers gisent sur le sol, baignant dans leur sang. Rendez-vous au <L to='196'>196</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P313);
