/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P119 extends Component {
    render() {
        return (
            <div>

               Quelques pèlerins tentent d'opposer une résistance, mais ils se font rapidement éliminer. Les Loups-Garous portent maintenant leur attention sur vous. Trois marchands leur emboîte le pas, tels des pantins au regard vide.  LOUP-GAROU (Dommages 2 dés) FORCE: 10 HABILETÉ: POUVOIR : 8 ENDURANCE :  9 22  Les Loups-Garous sont résistants aux armes ordinaires. A moins que vous ne possédiez des armes surnaturelles, telles l'épée du Marcheur des Cieux ou bien Tailleclair, la « Buveuse de Sang », vous n'infligerez que la moitié des dommages habituels (arrondis au chiffre supérieur). MARCHAND (Dommages 1 dé) FORCE : 6 HABILETÉ : POUVOIR : 6 ENDURANCE :  5 10  Toute fuite est inutile, les Loups-Garous auraient tôt fait de vous rattraper dans les bois. Si vous remportez ce combat, rendez-vous au <L to='73'>73</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P119);
