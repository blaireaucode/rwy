/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P545 extends Component {
    render() {
        return (
            <div>

               Vous murmurez la formule qui active la sphère et vous la jetez en direction du groupe. Quatre longs rubans de flammes en jaillissent, allumant instantanément les lanternes. Les porteurs s'enfuient en hurlant de terreur. Bientôt, la sphère lance quelques dernières flammes avant de disparaître à son tour (rayez-la de votre <Link to='/adv'>Feuille d'Aventure</Link>). Rendez-vous au <L to='562'>562</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P545);
