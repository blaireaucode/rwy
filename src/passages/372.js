/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P372 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Auguste s'immobilise, le regard vide. Mais il résiste et vous ne pourrez maintenir longtemps votre emprise. Vous lui ordonnez aussitôt d'éliminer l'Élémental d'eau. Ses traits se durcissent, mais il est obligé de vous obéir. « Vade ! » hurle-t-il. La gigantesque vague disparaît instantanément, mais Auguste a repris conscience. Rendez-vous au <L to='328'>328</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P372);
