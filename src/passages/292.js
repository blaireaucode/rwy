/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P292 extends Component {
    render() {
        return (
            <div>

               Le seigneur elfe vous avait dit : « Ma magie vous aidera lors du combat final... » Les pièces d'échecs se parent d'une lueur verte et des senteurs forestières envahissent l'atmosphère. Vous jetez les pièces sur le sol et elles se transforment en une troupe de cavaliers elfes vêtus de vert et armés d'épées d'argent. « Mes troupes ne feront qu'une bouchée de ces misérables Elfes ! » s'exclame le roi-sorcier. Mais le ton de sa voix trahit son inquiétude. Notez la Cavalerie des Bois sur votre Feuille d'Aventure et rayez les pièces d'échecs. Retournez au <L to='21'>21</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P292);
