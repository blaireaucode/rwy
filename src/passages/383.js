/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P383 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Votre sortilège a opéré trop vite ! Le tapis se met en torche et tombe en chute libre ! Vous vous écrasez sur la banquise. Chaque joueur perd 4 dés d'ENDURANCE (l'armure n'offre aucune Protection). Vous êtes éjecté du tapis qui repart aussitôt vers les hauteurs. Si vous avez survécu, rendez-vous au <L to='344'>344</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P383);
