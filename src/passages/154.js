/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P154 extends Component {
    render() {
        return (
            <div>

               A son ouverture, la cassette libère une épaisse fumée grise. Chaque joueur perd définitivement 1 point de POUVOIR. Si vous avez ouvert les trois cassettes, rendez-vous au <L to='291'>291</L>. Sinon, vous pouvez encore tenter celle de pierre (rendez-vous au <L to='511'>511</L>) ou bien celle de fer (rendez-vous au <L to='475'>475</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P154);
