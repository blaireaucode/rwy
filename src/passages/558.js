/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P558 extends Component {
    render() {
        return (
            <div>

               Ceci ne concerne que les joueurs que les Vampires des Neiges ont blessés. La lueur blafarde de la Mort Rouge vous éclaire. Peu à peu, vos blessures vous démangent et vous vous agitez dans votre sommeil. La Mort Rouge resserre son emprise et une mélodie insidieuse naît dans votre esprit. Aurez-vous la force de résister à cet appel ? Chaque joueur blessé doit jeter quatre dés. Si le résultat est inférieur ou égal à votre total de POUVOIR, rendez-vous au <L to='244'>244</L>. Si le résultat est supérieur, rendez-vous au <L to='91'>91</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P558);
