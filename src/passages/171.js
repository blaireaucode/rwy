/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P171 extends Component {
    render() {
        return (
            <div>

               Si vous avez répondu : « Le feu », rendez-vous au <L to='479'>479</L>. Si vous avez trouvé une autre réponse, rendez-vous au <L to='265'>265</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P171);
