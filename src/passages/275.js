/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P275 extends Component {
    render() {
        return (
            <div>

               La porte s'ouvre sur une vaste salle de banquet. Des armes de toute nature sont accrochées aux parois, entre des tentures représentant des scènes épiques (les joueurs à qui il manque une arme peuvent se servir)... Mais la pièce a été mise à sac ! Des cadavres de guerriers gisent sur le sol. Ils ont été surpris en cours de repas, mais la chose qui les a agressés devait être d'une force incroyable ! La lourde table de banquet est brisée et de  profondes morsures ont broyé les corps malgré les armures. Le sang sur les murs est encore chaud ! Le massacre a été perpétré quelques minutes avant votre arrivée. La pièce a deux issues. Une traînée de sang mène à la première porte. Le monstre a dû partir dans cette direction. Si vous désirez suivre ses traces, rendez-vous au <L to='317'>317</L>. Si vous optez pour l'autre porte, rendez-vous au <L to='318'>318</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P275);
