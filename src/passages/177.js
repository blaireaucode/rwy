/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P177 extends Component {
    render() {
        return (
            <div>

               Vous vous cachez dans l'ombre au pied des marches et attendez que la procession passe à votre hauteur. Vous observez le cadavre et reconnaissez avec horreur votre compagnon disparu lors de l'attaque du bourreau. Son visage figé a la même teinte crayeuse que le linceul blanc qui recouvre son corps. Quatre lanternes éteintes sont disposées aux coins de la civière. Les porteurs n'ont pas remarqué votre présence. Allez-vous attaquer la procession (rendez-vous au <L to='538'>538</L>), recourir à un objet (rendez-vous au <L to='563'>563</L>), attendre qu'elle s'éloigne et partir dans la direction opposée (rendez-vous au <L to='152'>152</L>), ou bien la suivre à distance (rendez-vous au <L to='45'>45</L>) ? 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P177);
