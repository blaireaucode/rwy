/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P121 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous vous concentrez sur chaque particule de votre corps, les associant à des images de volume et de lourdeur. Votre magie opère. Le tapis perd de l'altitude et rase bientôt la banquise. A quelques centimètres de la glace, vous le quittez d'un bond. Aussitôt, il repart vers les hauteurs, jusqu'à ne plus former qu'un point dans le ciel. Rendez-vous au <L to='344'>344</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P121);
