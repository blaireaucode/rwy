/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P55 extends Component {
    render() {
        return (
            <div>

               L'homme esquisse un sourire mauvais. « On ne refuse pas l'offre d'Auguste de Vanterie sans en subir les conséquences », dit-il d'une voix froide. A ces mots, il sort une fiole et en déverse le contenu à la mer. « Latet mors in gurgite vasto ! » s'écrie-t-il. Aussitôt, des mouvements désordonnés agitent les flots. Une vague gigantesque se dresse contre le flanc du navire. Les marins effrayés s'égayent sur le pont et le capitaine tombe à genoux. L'ombre noire de la vague recouvre le bateau. « Au moindre geste j'envoie votre navire au fond de la mer, menace l'inconnu. Si vous voulez sauver vos vies, remettez moi le fourreau de l'Épée de Légende ! » Si vous y consentez, rendez-vous au <L to='419'>419</L>. Si vous refusez, rendez-vous au <L to='439'>439</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P55);
