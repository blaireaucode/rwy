/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import SimpleImage from '../SimpleImage.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';
import * as fh from '../fight_helpers';

class P13 extends Component {

    constructor(props) {
        super(props);
        // FIXME --> replace with json ; to put in fh ?
        const z = { id:'z',
                    name: 'Zombie',
                    fighting_prowess:6,
                    psychic_ability:3,
                    awareness:6,
                    endurance:30 };
        this.f ={
            map: fh.new_map(6,6), // rows/col
            encounters: {}
        };
        var map = this.f.map;
        this.f.encounters[z.id] = z;
        map[1][2] = 2;
        map[1][3] = 'z';
        map[1][4] = 3;
        map[2][3] = 1;
        map[2][4] = 4;
        console.log('start fight', this.f);
    }

    render() {
        return (
            <div>

              Les hommes encagoulés s'enfuient et disparaissent dans
              les coins d'ombre. Le Zombie se lève de sa couche tel
              un automate. Toute lueur de vie a disparu du regard de
              votre ami. Il n'est plus qu'un monstre sans âme que
              vous devez détruire.  ZOMBIE (Protection inchangée,
              Dommages 1 dé) FORCE : 6 HABILETÉ : 3 POUVOIR : 6
              ENDURANCE: 30 Si vous parvenez à tuer ce monstre,
              rendez-vous au <L to='329'>329</L>.
              
              <p/>
              
              <SimpleImage width={174} src={'Human_Zombie_0201.jpg'}/>
              {' '}
              <Fight fight={this.f}/>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P13);
