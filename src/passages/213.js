/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P213 extends Component {
    render() {
        return (
            <div>

               Vous chutez dans un tunnel à une vitesse vertigineuse. Des excroissances métalliques sur les parois accrochent vos vêtements. Le fond du gouffre, tapissé d'une inquiétante lumière rouge, se rapproche dangereusement. Si vous désirez vous agripper aux parois, rendez-vous au <L to='354'>354</L>. Si vous préférez vous préparer à l'atterrissage, rendez-vous au <L to='431'>431</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P213);
