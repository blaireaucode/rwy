/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P123 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Le premier Sylphe vous bloque le passage et use de sa hallebarde comme d'une faux. Le deuxième est sur votre gauche et tente de vous embrocher par de petits mouvements linéaires. A aucun moment, ils ne changent leur manière de combattre et, avec un peu d'astuce, vous pourrez tirer parti de ce comportement rigide. En esquivant, il sera possible de les amener à se frapper l'un l'autre : à chaque Assaut, si vous obtenez un résultat inférieur ou égal à votre total d'HABILETÉ avec 2 dés, les Sylphes s'infligent chacun 1 dé + 3 de Dommages. Si vous manquez ce lancer, ils vous frappent de la manière habituelle. Vous pouvez bien sûr abandonner cette tactique et les affronter normalement. Rendez-vous au <L to='100'>100</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P123);
