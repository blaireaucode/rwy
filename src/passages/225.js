/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P225 extends Component {
    render() {
        return (
            <div>

               Vous établissez votre bivouac au pied d'un vieux chêne. La forêt est plongée dans un silence inquiétant. Malgré le froid, vous n'osez allumer un feu. Si vous jouez seul, rendez-vous au <L to='27'>27</L>. Si vous êtes en groupe, rendez-vous au <L to='128'>128</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P225);
