/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P519 extends Component {
    render() {
        return (
            <div>

               Vous lancez vos troupes à l'assaut de l'armée squelette. Les forces en présence sont à égalité numérique mais les Squelettes ignorent la peur et la fatigue. La bataille fait rage, mais l'issue du combat n'est pas jouée. Jetez un dé. Ajoutez 1 au résultat si votre groupe compte un chevalier. Si vous faites de 1 à 3, rendez-vous au <L to='374'>374</L>. Si vous faites de 4 à 6, rendez-vous au <L to='216'>216</L>. Si vous faites 7, rendez-vous au <L to='427'>427</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P519);
