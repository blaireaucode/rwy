/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P449 extends Component {
    render() {
        return (
            <div>

               Allez-vous utiliser le crucifix d'argent (rendez-vous au <L to='38'>38</L>), le fourreau de l'Épée de Légende (rendez-vous au <L to='32'>32</L>) ou les yeux joyaux bleus (rendez-vous au <L to='323'>323</L>) ? 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P449);
