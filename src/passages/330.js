/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P330 extends Component {
    render() {
        return (
            <div>

               « Pas si vite ! » Vous vous retournez au son de cette voix cristalline. Ubara est assise sur un tas de bois  recouvert de neige. Elle n'est vêtue que de sa tunique et, malgré cela, ne semble pas souffrir du froid. « Nous ne pensions pas te revoir », dites-vous en souriant. « Je préférais vous rencontrer à l'écart des autres, répond-elle. Nous devons parler de choses qu'ils ne pourraient pas comprendre... » A ces mots, elle vous tend un petit sac. Vous l'ouvrez et découvrez une cloche de fer. Ubara éclate de rire à votre air décontenancé. « Elle sonnera la fin du passé et le début du renouveau », dit-elle, comme si sa remarque suffisait à tout expliquer. « Tu es une Seler, dites-vous. Qu'est-ce que cela signifie ? » Ubara hausse les épaules. « Je vois le futur, peut-être. Quelquefois, les gens savent déjà ce que je leur annonce... Vous, par exemple, je vois que votre destin n'est pas de tuer le roi-sorcier. Cela pourra survenir, s'il se met en travers de votre route, mais votre destinée est d'un ordre supérieur. » Vous marquez un instant d'étonnement et Ubara poursuit. « Ne me demandez pas de précisions, je ne perçois que des fragments du futur. Je sais en revanche que votre but est d'empêcher le retour dans notre monde des cinq derniers archimages. Pour cela, il vous faudra reconstruire la Lame Essentielle, plus connue sous le nom d'Épée de Légende. Le roi-sorcier en possède le pommeau. C'est la raison de votre venue sur Wyrd. » « C'était la raison, répondez-vous. Mais maintenant que nous avons vu la misère de ce peuple et toute cette souffrance, il semble urgent de les débarrasser de ce roi diabolique. » Ubara hausse les épaules, une moue dubitative au coin des lèvres. « Le bien et le mal sont comme les cases blanches et noires d'un échiquier, ils ne vont pas l'un sans l'autre. Il y a bien longtemps, un homme doté de grands pouvoirs utilisa l'imagination et transforma Wyrd en contrée idyllique. En ce temps-là, les hommes n'avaient pas à creuser la terre pour se nourrir, la corne d'abondance suffisait à satisfaire les besoins de chacun. Le temps et la mort n'existaient pas, le pays était un paradis immuable. Hélas, l'homme n'est pas fait pour le paradis !... ni pour l'enfer d'ailleurs. » « Que se passa-t-il ? » demandez-vous. « Ce que vous voyez à présent, reprend-elle. Les rêves assouvis perdirent leur essence et un voile terne enveloppa le pays. Le roi-sorcier oublia le goût de la vie et son cœur se dessécha. Depuis, il tient Wyrd sous l'emprise de ses noires pensées, comme une rose fanée, pressée entre les pages d'un livre... » Vous sursautez. « Malgré cela, tu ne le considères pas comme mauvais ? » Ubara soupire et regarde vers le nord. « Oubliez le bien et le mal, répond-elle. Faites ce que vous avez à faire. Détruisez-le, si nécessaire... Je peux vous prédire encore ceci : pour atteindre le château, il vous faudra traverser la forêt des Elfes. Ils ne vous laisseront  pas passer. Vous devrez les affronter ou bien vaincre leur chef aux échecs. Dans la forêt, vous rencontrerez les Chiens de Givre. Cette meute constitue la défense extérieure du roi-sorcier, le premier rempart de son rêve magique. Derrière la forêt des Elfes, vous trouverez le château. Le trajet sera semé d'embûches, mais il sera court, car le château n'est pas dans notre monde, il fait partie du rêve du roi-sorcier. Avant d'y pénétrer, vous devrez vaincre Gristun, une créature monstrueuse, gardienne des trois portes... Vous savez l'essentiel. Je peux encore répondre à une question. » Allez-vous demander: « Comment vaincre le chef des Elfes aux échecs ? » (rendez-vous au <L to='277'>277</L>), « Comment éviter les Chiens de Givre ? » (rendez-vous au <L to='483'>483</L>), « Quel est le meilleur moyen de pénétrer dans le château ? » (rendez-vous au <L to='518'>518</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P330);
