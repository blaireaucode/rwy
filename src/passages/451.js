/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P451 extends Component {
    render() {
        return (
            <div>

               Le chef des barbares s'écroule enfin à vos pieds. Vous épongez la sueur qui ruisselle de votre front et portez votre attention sur l'assemblée, prêt à essuyer un nouvel assaut. Mais au lieu de vous attaquer, les hommes applaudissent bruyamment votre victoire. L'un d'eux s'approche en titubant et vous remet une corne à boire. Notez-la sur votre Feuille d'Aventure si vous la conservez et rendez-vous au <L to='334'>334</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P451);
