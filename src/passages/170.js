/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P170 extends Component {
    render() {
        return (
            <div>

               Le crépuscule descend sur la forêt et la partie se termine. Les pièces blanches sont regroupées au centre de l'échiquier et les pièces noires les éliminent une à une. Vous remarquez que pour chaque pièce blanche qui disparaît, un marchand se désintéresse du jeu et va se coucher. Les deux joueurs en fourrure achèvent la partie et vont rejoindre leur couche. Vous les imitez et retournez près du feu de votre bivouac. Si vous désirez veiller, rendez-vous au <L to='120'>120</L>. Si vous préférez vous endormir, rendez-vous au <L to='24'>24</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P170);
