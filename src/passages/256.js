/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P256 extends Component {
    render() {
        return (
            <div>

               Une frêle silhouette est recroquevillée sur une chaise au fond de la pièce, baignée par les rayons de la Lune Bleue. « Les visiteurs sont rares, dit une voix chevrotante. Approchez que je vous voie, mes pauvres yeux sont bien fatigués... » Si vous désirez pénétrer dans la pièce, rendez-vous au <L to='19'>19</L>. Si vous préférez fuir, rendez-vous au <L to='169'>169</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P256);
