/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P384 extends Component {
    render() {
        return (
            <div>

               Le chef des Elfes semble intéressé par votre suggestion. Les soldats qui l'entourent abaissent leurs arcs, tandis que vous installez les pièces sur le plateau. L'Elfe s'assoit dans la neige et fait pivoter le plateau. « Je prends les blancs », dit-il. Puisque vous l'avez défié, il est normal qu'il choisisse sa couleur. Vous vous asseyez en face de lui, tandis qu'il réfléchit à son premier coup. Si vous êtes en groupe, désignez celui qui va jouer contre l'Elfe. Si le voleur désire tricher, rendez-vous au <L to='296'>296</L>. Si vous préférez jouer la partie loyalement (seuls les voleurs peuvent tricher), rendez-vous au <L to='546'>546</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P384);
