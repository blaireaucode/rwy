/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P53 extends Component {
    render() {
        return (
            <div>

               Vous lâchez le fourreau et le regardez disparaître dans la couche nuageuse. Aussitôt, le roi-sorcier lâche un ordre et une gigantesque Araignée descend à la suite du fourreau, suspendue à un fil. La créature disparaît à son tour dans le tapis de nuages, mais remonte bientôt, tenant le fourreau entre ses pattes ! Le roi-sorcier s'en empare. « En temps normal, cet acte stupide vous aurait laissé sans défense face aux cinq archimages. Mais leurs pouvoirs ne s'étendent pas au Rêve de Wyrd. Ici, je suis le monarque absolu et vous allez le comprendre... » La toile d'araignée où vous êtes accroché disparaît et vous chutez dans le vide dans un cauchemar sans fin, spécialement conçu pour vous...

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P53);
