/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P477 extends Component {
    render() {
        return (
            <div>

               Ceci est réservé au joueur qui a accepté le duel. Vous avancez d'un pas et relevez le défi de Thanatos. Un champ de force maintient vos compagnons à distance pour la durée du combat.  THANATOS (Protection 2, Dommages 4 dés) FORCE : 8 HABILETÉ : 7 POUVOIR : 8 ENDURANCE : 45 Si vous possédez l'amulette tête de mort, vous gagnez 7 points de Protection et vous infligerez 7 points de Dommages supplémentaires pour la durée du duel. Si vous êtes vainqueur, rendez-vous au <L to='111'>111</L>. Si vous préférez fuir, il vous faut retourner dans la salle du Gardien de la Mémoire (rendez-vous au <L to='367'>367</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P477);
