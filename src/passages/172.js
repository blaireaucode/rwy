/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P172 extends Component {
    render() {
        return (
            <div>

               L'étrange comportement des joueurs d'échecs ne vous dit rien qui vaille. Votre instinct vous affirme qu'un mauvais coup se prépare. Vous rejoignez le campement des joueurs et appuyez votre lame sur la gorge du plus proche. L'homme vous lance un regard haineux et émet un grognement animal ! Vous reculez de surprise : son visage est recouvert de longs poils gris. C'est un Loup-Garou ! Vous relevez la tête. Heureusement, la lune ne s'est pas encore levée, la transformation du monstre n'est pas achevée. Hélas, la créature profite de ce moment d'inattention pour appeler à la rescousse ! Les marchands se dressent sur les couches et s'élancent dans le camp, armés de longs couteaux ! Avec une lueur de folie dans le regard, ils massacrent un à un les pèlerins endormis. « Nous allons triompher ! s'exclame soudain l'autre joueur, libère mon frère et rends-toi ! » Pour toute réponse, vous plongez votre lame dans la gorge du monstre. Les voyageurs s'enfuient devant les marchands, qui s'acharnent sur tout ce qui bouge. Trois d'entre eux sont venus rejoindre leur maître. SERVITEURS (Dommages 1 dé) FORCE : 6 HABILETÉ : POUVOIR : 6 ENDURANCE :  5 10 chacun  LOUP-GAROU (Protection 1, Dommages 1 dé + 1) FORCE : 7 HABILETÉ : 8 POUVOIR : 7 ENDURANCE: 12 Si vous remportez ce combat, rendez-vous au <L to='85'>85</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P172);
