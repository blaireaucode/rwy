/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P46 extends Component {
    render() {
        return (
            <div>

               Les Vampires vous entourent lentement. Vous comprenez, à leur regard vide, qu'il ne s'agit plus de vos compagnons, mais de créatures macabres, assoiffées de sang. Si votre groupe comprend un prêtre désirant intervenir, rendez-vous au <L to='206'>206</L>. Si vous désirez les combattre, rendez-vous au <L to='37'>37</L>. Si vous préférez recourir à un objet, rendez-vous au <L to='376'>376</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P46);
