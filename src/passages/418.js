/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P418 extends Component {
    render() {
        return (
            <div>

               La scène s'ouvre sur un monde recouvert par les eaux, effet que le montreur symbolise en agitant de longs rubans de soie bleue et verte. Les hommes devaient alors vivre comme des poissons. Mais le Sauveur descendit du ciel et aspira les flots. Hélas, le déluge continuait et des torrents s'abattaient sur terre ! Le Sauveur plaça un bouchon dans le ciel. Il permit seulement quelques averses passagères, créant ainsi les saisons. Sa tâche accomplie, le Sauveur s'en retourna dans les cieux. Alors cinq nobles, parés des couleurs de l'arc-en-ciel, apparurent dans le royaume liquide qui dominait la terre. Ils s'acharnèrent sur le bouchon afin de provoquer un nouveau déluge, mais en vain. Leurs adorateurs sur terre vinrent leur prêter main forte (la marionnettiste présente de petits personnages tirant sur une corde accrochée au bouchon). A ce moment, le héros entre en scène. C'est un troubadour, en quête d'une épée fantastique qui seule pourra arrêter les cinq nobles diaboliques. La poupée de carton déambule le long de la scène, mais chaque fois qu'elle est sur le point de trouver l'épée, la foule assemblée lui hurle de prendre garde car, pendant ce temps, les adorateurs tirent sur la ficelle du bouchon. Invariablement, il accourt pour les disperser, perdant ainsi toute chance de trouver l'épée. Bientôt, le mage vêtu de bleu envoie sur terre deux émissaires recouverts de peaux de loup. Les meurtriers assassinent le héros pendant son sommeil. Cette scène tragique marque la fin de la représentation. La marionnettiste apparaît alors derrière la scène, sous les applaudissements de la foule. Elle rassemble ses marionnettes et ramasse quelques pièces. Si vous désirez partir maintenant que la représentation est terminée, rendez-vous au <L to='306'>306</L>. Si vous voulez discuter avec la vieille femme, rendez-vous au <L to='79'>79</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P418);
