/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P516 extends Component {
    render() {
        return (
            <div>

               Vous bondissez dans la pièce en brandissant votre épée. Dans un ensemble parfait, les hommes abaissent leurs cierges et éclairent leurs visages... Ce que vous voyez vous arrache un frisson d'horreur et l'air vient à vous manquer. Les personnages n'opposent aucune résistance et un seul coup suffit à les éliminer mais, pour ce faire, il vous faut un résultat inférieur ou égal à votre Niveau avec 1 dé + 1. Si vous manquez ce lancer, la simple vue de leur visage vous fait mourir de peur... Si vous parvenez à les tuer tous les cinq, rendez-vous au <L to='194'>194</L>. Si vous préférez fuir, rendez-vous au <L to='547'>547</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P516);
