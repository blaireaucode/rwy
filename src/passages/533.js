/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P533 extends Component {
    render() {
        return (
            <div>

               Vous allumez le brasero et approchez la jeune fille du feu. Son visage reprend quelques couleurs. Vous lui faites avaler lentement de tout petits morceaux d'aliments (rayez une ration) jusqu'à ce qu'elle reprenne conscience. Rendez-vous au <L to='48'>48</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P533);
