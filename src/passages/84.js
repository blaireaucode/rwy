/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P84 extends Component {
    render() {
        return (
            <div>

               Vous contemplez le bébé d'un air désolé. « Mourir si jeune ! dites-vous. C'est injuste. » Shanhans hoche la tête. « Dans nos contrées, mourir est souvent une délivrance... » Manifestement, la vie de ces paysans ne connaît guère de moments de réjouissances. Bientôt, une femme vous apporte des couvertures. « Vous dormirez près du feu ce soir, dit-elle. Il n'y aura pas de cauchemar, à cause de la Seler. » Vous installez votre couche près de l'âtre tout en vous interrogeant sur le sens de ces paroles. Vous vous endormez et, de fait, nul rêve ne vient troubler votre sommeil. Vous vous réveillez en pleine forme. Chaque joueur récupère un nombre de points d'ENDURANCE équivalent à la moitié de son total de départ (arrondi au chiffre supérieur), plus (si nécessaire) 1 point pour le déjeuner qu'on vous sert. Il est temps de partir. Vous remerciez Shanhans de son hospitalité et repartez dans l'air vif du matin. Rendez-vous au <L to='520'>520</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P84);
