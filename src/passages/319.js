/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P319 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : En lançant votre sortilège, vous ressentez la proximité d'une puissante magie, où se mêlent l'illusion et la mort. La porte de la ferme s'ouvre avec fracas, révélant un intérieur plongé dans la pénombre. Rendez-vous au <L to='256'>256</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P319);
