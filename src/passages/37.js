/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P37 extends Component {
    render() {
        return (
            <div>

               Les joueurs se placent sur les cases marquées L. Les joueurs qui se sont transformés en Vampires des Neiges peuvent se placer où bon leur semble, à condition toutefois de laisser un espace de deux cases entre eux et les joueurs. Les joueurs transformés sont devenus des Morts Vivants. Ils ne disposent donc plus des capacités et des pouvoirs qui étaient les leurs. Cependant, si un joueur devenu vampire possédait une épée magique ou un parchemin, il peut encore les utiliser.  VAMPIRE DES NEIGES (Protection inchangée, Dommages 1 dé + 3) FORCE : 7 HABILETÉ 8 POUVOIR : 7 ENDURANCE: 25 Si les Vampires gagnent ce combat, l'aventure est terminée. Si vous remportez la victoire, rendez-vous au <L to='228'>228</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P37);
