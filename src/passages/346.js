/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P346 extends Component {
    render() {
        return (
            <div>

               Mû par une force inconnue, le crucifix bondit droit devant lui, tendant à la rompre la chaîne qui le maintient. Vous poussez un soupir de soulagement et suivez la direction qu'il vous indique. L'espoir renaît. Les joueurs blessés recouvrent 1 point d'ENDURANCE. Rendez-vous au <L to='422'>422</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P346);
