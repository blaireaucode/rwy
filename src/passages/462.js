/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P462 extends Component {
    render() {
        return (
            <div>

               Vous devez maintenant choisir entre les deux portes. Si vous désirez ouvrir celle de gauche, rendez-vous au <L to='398'>398</L>. Si vous préférez celle de droite, rendez-vous au <L to='200'>200</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P462);
