/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P344 extends Component {
    render() {
        return (
            <div>

               Vous regardez vers le nord. Quelque part dans cette direction commence le Royaume de Wyrd. Mais où se trouve-t-il exactement ? Et comment survivre plusieurs jours dans cette immensité glacée, battue par les vents ? Si vous possédez le crucifix d'argent, rendez-vous au <L to='346'>346</L>. Si vous ne l'avez, mais que votre groupe compte un prêtre, rendez-vous au <L to='474'>474</L>. Si votre groupe compte un magicien, rendez-vous au <L to='7'>7</L>. Si vous ne pouvez recourir à aucune de ces options, rendez-vous au <L to='559'>559</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P344);
