/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P381 extends Component {
    render() {
        return (
            <div>

               VOLEUR: Les Crânes Étincelants cherchent à vous déséquilibrer. Qui plus est, vous devez surveiller constamment les dalles. Pourquoi ne pas combattre un mal grâce à l'autre ? Vous empoignez le premier Crâne qui passe à proximité et vous le serrez fermement contre votre poitrine. La créature aveuglée zigzague dans les airs, en direction de la tour, vous emportant avec elle. La paroi se rapproche dangereusement ! Au dernier moment, vous vous laissez choir et atterrissez avec souplesse sur le parapet, tandis que le Crâne éclate contre le mur! Si d'autres joueurs doivent traverser le pont, rendez-vous au <L to='240'>240</L>. Si tous les survivants ont traversé, rendez-vous au <L to='35'>35</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P381);
