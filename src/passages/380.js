/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P380 extends Component {
    render() {
        return (
            <div>

               Le sifflement devient insoutenable. Vous vous bouchez les oreilles, mais le son vous vrille le cerveau. Soudain, le silence se fait et vous pouvez percevoir une respiration sourde. Une gigantesque tache d'ombre s'est formée audessus de vous et descend telle une gigantesque chauve-souris. Une voix sépulcrale retentit dans la pièce : « Vous avez parcouru tant de chemin. Vous allez maintenant visiter les ténèbres... Umborus va vous emporter dans le royaume des ombres... » Une énorme masse sombre s'est posée devant vous. Des choses grouillent dans cette substance informe. Vous croyez  reconnaître des yeux étincelants et des griffes acérées. Si vous possédez la Pourfendeuse d'Ombre, rendez-vous au <L to='34'>34</L>. Sinon, rendez-vous au <L to='532'>532</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P380);
