/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P96 extends Component {
    render() {
        return (
            <div>

               Un silence de mort succède au fracas des armes. Vous vous approchez du cadavre de votre ami, gardé par ces étranges lanternes éteintes. Vous pouvez tenter de le ressusciter, si vous possédez un objet qui permette ce miracle (rendez-vous au <L to='52'>52</L>). Autrement, vous pouvez prendre ce qu'il possédait et poursuivre votre quête (rendez-vous au <L to='152'>152</L>). 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P96);
