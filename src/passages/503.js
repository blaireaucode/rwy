/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P503 extends Component {
    render() {
        return (
            <div>

               « Cette broche appartient à l'archimage Tor, si je ne m'abuse, dit-il, ironique. Voyons ce que je peux en faire... » Il ferme un instant les yeux et les cristaux de sa couronne s'illuminent. Aussitôt, la broche vous échappe des mains et se transforme en Loup d'Argent, porté par de longues ailes membraneuses. La bête volette autour de vous en grognant. « J'espère que vous appréciez ce petit compagnon, dit-il. Il est né de vos propres cauchemars... » Rendez-vous au <L to='304'>304</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P503);
