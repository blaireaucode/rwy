/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P235 extends Component {
    render() {
        return (
            <div>

               Vous vous enfuyez à toutes jambes. Derrière vous, les Vampires entament un chant empreint de moquerie, mais ils ne semblent pas se lancer à votre poursuite. Soudain, deux étoiles filantes écarlates passent au-dessus de votre tête et atterrissent à quelques mètres devant vous ! Aussitôt, deux silhouettes font leur apparition. Retournez au <L to='309'>309</L> et reprenez le combat, mais les Vampires peuvent vous frapper au prochain Assaut, car ils bénéficient de l'effet de surprise.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P235);
