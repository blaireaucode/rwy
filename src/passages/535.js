/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P535 extends Component {
    render() {
        return (
            <div>

               MAGICIEN: Le Faltyn empoche vos 5 Pièces d'Or avec une moue dédaigneuse. « Ta pingrerie te coûtera peut-être plus cher que tu ne l'imagines. Enfin... Le coffre contient cinq objets, mais tu ne pourras en prendre qu'un. Je te conseille la sphère de Feu. Bonne chance... » Sur ces mots, le Faltyn disparaît. Si vous désirez vous avancer vers le coffre, rendez-vous au <L to='331'>331</L>. Vous pouvez aussi retourner au <L to='398'>398</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P535);
