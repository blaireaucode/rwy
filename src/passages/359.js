/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P359 extends Component {
    render() {
        return (
            <div>

               Vous lancez vos troupes à l'assaut de l'armée squelette. Dès la première charge, vous comprenez que la bataille est gagnée. En l'espace de quelques minutes, votre armée décime les Squelettes, ne laissant derrière elle qu'un tapis d'ossements broyés. Vos troupes disparaissent bientôt et vous vous dirigez vers le roi-sorcier. Rendez-vous au <L to='300'>300</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P359);
