/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P100 extends Component {
    render() {
        return (
            <div>

               Vos adversaires sont invisibles, mais leurs armes ne le sont pas. Vous devrez néanmoins procéder à vos lancers avec trois dés au lieu de deux.  SYLPHE (Dommages 1 dé + 3) FORCE : 7 HABILETÉ : 8 POUVOIR : 8 ENDURANCE : 14 Vous ne pouvez avancer sur le pont puisqu'un Sylphe vous bloque le passage. Si vous désirez abandonner le combat, retournez au <L to='226'>226</L>. Si vous les tuez, rendez-vous au <L to='350'>350</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P100);
