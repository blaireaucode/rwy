/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P379 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Votre plan est périlleux, mais que serait la vie sans le danger ? Vous dégagez un fil du tapis, prenez une profonde inspiration et... vous sautez dans le vide ! Le tapis se dévide rapidement, mais il offre assez de résistance pour ralentir votre chute (vos compagnons empoignent à leur tour le fil et descendent à votre suite). Le sol se rapproche à toute vitesse ! En calculant bien, vous pourrez amortir votre chute d'une roulade. Lancez deux dés. Si le résultat est inférieur ou égal à votre total d'HABILETÉ, vous ne perdez que 1 dé d'ENDURANCE à l'atterrissage. Si le résultat est supérieur, vous perdez 2 dés d'ENDURANCE (l'armure n'offre aucune Protection). Si vous survivez à la chute, rendez-vous au <L to='344'>344</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P379);
