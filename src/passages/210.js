/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P210 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Votre clairvoyance va vous permettre de distinguer les créatures invisibles. Allez-vous y avoir recours en plus de votre vision normale (rendez-vous au <L to='481'>481</L>) ou bien fermer les yeux et ne vous fier qu'à vos pouvoirs (rendez-vous au <L to='97'>97</L>) ? 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P210);
