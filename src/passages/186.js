/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P186 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Si vous désirez recourir à la Téléportation, rendez-vous au <L to='555'>555</L>. Si vous possédez le tapis volant, rendez-vous au <L to='369'>369</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P186);
