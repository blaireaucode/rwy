/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P115 extends Component {
    render() {
        return (
            <div>

               Une main glaciale s'abat sur l'épaule du personnage qui ferme la marche ! La victime se débat quelques instants et une intense fatigue l'envahit : elle perd 1 dé d'ENDURANCE (moins la valeur de Protection). Vous laissez échapper un cri de stupeur. L'homme qui vient de vous agresser n'est autre que votre compagnon, disparu lors de l'attaque du bourreau ! Mais son expression a changé. Il vous fixe, le regard vide et les traits déformés. Votre ami est devenu un Zombie ! Rendez-vous au <L to='360'>360</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P115);
