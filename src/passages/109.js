/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P109 extends Component {
    render() {
        return (
            <div>

               Vous avez le sentiment qu'il existe un lien surnaturel entre le mort et les lanternes. Peut-être qu'en les allumant, vous parviendrez à ranimer le cadavre. Mais avez-vous vraiment envie de voir se lever votre Double ? Si vous désirez utiliser la sphère de Feu, rendez-vous au <L to='553'>553</L>. Si vous préférez recourir au briquet d'ambre, rendez-vous au <L to='90'>90</L>. Si vous préférez abandonner cette idée, retournez au <L to='426'>426</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P109);
