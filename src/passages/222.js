/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P222 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous posez votre main sur le front du bébé et vous vous concentrez. Bientôt, les plaies disparaissent. Le bébé vous regarde et se met à crier, mais il retrouve bientôt son calme dans les bras de sa mère. « Un miracle ! s'exclame une vieille femme. Êtes-vous un dieu ? » Vous esquissez un sourire. « Non, répondez-vous. Je connais seulement quelques mystères. Il n'y a donc pas de magiciens dans ce pays ? » Un silence gêné tombe sur l'assemblée. « Il n'y a que le roi-sorcier », dit une voix. Aussitôt, les conversations reprennent, comme si chacun voulait chasser de son esprit le nom de son souverain. La mère du bébé s'approche de vous. « Je ne sais comment vous remercier, sanglote-t-elle. Acceptez cette couverture et ce lait. Je ne possède rien d'autre. Acceptez, je vous en prie... » Notez la couverture et la bouteille de lait sur votre <Link to='/adv'>Feuille d'Aventure</Link>. Vous pourrez la boire en remplacement d'une ration. Rendez-vous au <L to='341'>341</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P222);
