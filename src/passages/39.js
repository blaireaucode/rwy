/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P39 extends Component {
    render() {
        return (
            <div>

               Vous contemplez, stupéfait, la fosse où vos compagnons viennent de tomber, quand quatre nouvelles flèches jaillissent des meurtrières. Chaque joueur présent est touché par une flèche (déterminez au hasard les victimes des flèches en surnombre). Chaque flèche inflige un Dommage de 1 dé d'ENDURANCE (moins la Protection). Vous n'avez d'autre alternative que de vous élancer à votre tour dans le puits. A l'instant où vous disparaissez, de nouveaux traits viennent se briser contre le mur. Rendez-vous au <L to='213'>213</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P39);
