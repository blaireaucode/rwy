/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P297 extends Component {
    render() {
        return (
            <div>

               Dès que vous faites votre choix, les autres mains disparaissent et se fondent dans le noir insondable qui tapisse le fond du coffre. La bouche disparaît à son tour et le couvercle se referme, vous laissant avec la sphère au cr eux de la paume. Vous retournez vers l'entrée. Dans certains paragraphes, vous aurez la possibilité de faire usage de cette sphère mais, de toute manière, vous pourrez le faire à tout moment. Ce faisant, rendez-vous au paragraphe correspondant à la sphère que vous avez choisie, mais n'oubliez pas de noter le numéro de paragraphe que vous quittez : sphère du Carnage (<L to='49'>49</L>), sphère des Mystères (<L to='302'>302</L>), sphère de la Peste (<L to='161'>161</L>), sphère des Dons (<L to='396'>396</L>) et sphère de Feu (<L to='310'>310</L>). Notez le numéro sur votre <Link to='/adv'>Feuille d'Aventure</Link> (si vous êtes en groupe, désignez le joueur qui prend la sphère). Vous quittez la pièce. Vous pouvez maintenant ouvrir l'autre porte (rendez-vous au <L to='200'>200</L>), ou bien quitter la tour (rendez-vous au <L to='406'>406</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P297);
