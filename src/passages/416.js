/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P416 extends Component {
    render() {
        return (
            <div>

               Voyant que vous refusez l'affrontement, le chef des barbares appelle ses hommes. Aussitôt, sept gigantesques guerriers quittent leurs chaises et s'avancent en soupesant leurs haches.  CHEF BARBARE (Protection 1, Dommages 2 dés + 1) FORCE : 8 HABILETÉ : 7 POUVOIR : 6 ENDURANCE : 30 BARBARE (Protection 1, Dommages 1 dé + 2) FORCE : 8 HABILETÉ : 6 POUVOIR : 6 ENDURANCE : 18 Vous embrassez la salle d'un regard circulaire : il n'y a aucune issue. Si vous remportez ce combat, rendez-vous au <L to='334'>334</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P416);
