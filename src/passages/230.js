/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P230 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : le Faltyn apparaît. « N'espère pas que je tente quoi que ce soit contre les anciens mages, mortel », dit-il d'une voix blanche. Aussitôt, il commence à disparaître. Si vous désirez le rappeler, rendez-vous au <L to='70'>70</L>. Si vous préférez le laisser partir et quitter la clairière, rendez-vous au <L to='397'>397</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P230);
