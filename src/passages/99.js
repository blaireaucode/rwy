/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P99 extends Component {
    render() {
        return (
            <div>

               Le chef des Elfes semble intéressé par votre suggestion. Les Elfes, vous le savez, ne peuvent résister à l'attrait du jeu. « Qu'on aille chercher un plateau et des pièces », ordonne-t-il sans vous quitter du regard. L'un des soldats disparaît dans la forêt et revient quelques minutes plus tard avec un échiquier complet. Le seigneur elfe s'assoit et choisit les blancs. Vous prenez place en face de lui. Désignez le joueur qui va l'affronter. Si le  voleur désire tricher, rendez-vous au <L to='134'>134</L>. Si vous préférez jouer loyalement, désignez un autre que le voleur et rendez-vous au <L to='434'>434</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P99);
