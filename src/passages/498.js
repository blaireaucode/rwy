/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P498 extends Component {
    render() {
        return (
            <div>

               Pour atteindre le sol, chaque joueur doit obtenir un résultat inférieur ou égal à son HABILETÉ avec 2 dés pour ne pas perdre l'équilibre... et 2 dés d'ENDURANCE (l'armure n'assure aucune Protection). Lorsque vous avez touché le sol, rendez-vous au <L to='344'>344</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P498);
