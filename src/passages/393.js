/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P393 extends Component {
    render() {
        return (
            <div>

               Le roi-sorcier vient de lever de nouvelles troupes. Pour chaque Squelette que tue votre armée, deux nouveaux jaillissent de terre. Que faire devant un tel déploiement de force ? Les derniers membres de votre armée s'écroulent. Vous lancez des regards inquiets alentour, cherchant une issue qui n'existe pas. Les Squelettes vous acculent contre le mur de l'arène. Vous tentez de contrer les premiers assauts, mais en vain. Les lances et les épées rouillées vous transpercent de part en part. Bientôt, vous irez rejoindre leurs rangs...

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P393);
