/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P355 extends Component {
    render() {
        return (
            <div>

               La vieille femme vous sourit. Ses traits, bien qu'ayant subi les assauts du temps, ne sont pas ceux des mégères qui exercent d'ordinaire cette profession. Elle prend une petite fiole qu'elle portait autour du cou. « Ceci est un onguent à base d'herbes que prépare le peuple des bois. » Parle-t-elle des Elfes ou des bûcherons ? Vous ne sauriez le dire. Vous esquissez un geste de refus, mais elle insiste et place la fiole au creux de votre paume. « Ce baume rendra vos blessures moins douloureuses, dit-elle en rassemblant son paquetage. Que la chance vous accompagne au long de votre quête... » Déjà, elle s'éloigne en direction des arbres. Vous observez un instant la fiole qu'elle vous a remise, mais quand vous relevez la tête elle a disparu. Notez l'onguent sur votre <Link to='/adv'>Feuille d'Aventure</Link>. Le contenu de la fiole permet cinq applications. A chaque application, vous récupérez 1 dé d'ENDURANCE. Vous pouvez utiliser l'onguent à tout instant, sauf au combat. N'oubliez pas de tenir un compte précis des utilisations. Rendez-vous au <L to='126'>126</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P355);
