/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P551 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Le tapis ne fonctionne pas sans l'amulette blanche. Vous empoignez la pierre blanche et bondissez sur le tapis (vos compagnons à votre suite). Il s'élève à l'instant même où la vague allait vous engloutir. Le danger est écarté, mais vous ne parvenez pas à faire bouger le tapis. Vous devez donc l'abandonner et sauter sur le pont. Rendez-vous au <L to='35'>35</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P551);
