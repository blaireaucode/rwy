/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P290 extends Component {
    render() {
        return (
            <div>

               La lumière de l'étendard se propage le long de la flèche. A l'instant où l'aura atteint la pointe, la flèche vous échappe des mains et s'envole en sifflant vers la tribune royale ! Le roi-sorcier recule d'un pas, mais il reprend vite ses esprits et, d'un geste ample, crée un mur de pierre devant lui. La flèche s'y écrase et explose en un millier de particules aveuglantes. Aussitôt, une troupe d'archers en livrée blanche apparaît à vos côtés ! Notez la Compagnie Blanche sur votre <Link to='/adv'>Feuille d'Aventure</Link> et rayez la flèche. Retournez au <L to='178'>178</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P290);
