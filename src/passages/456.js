/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P456 extends Component {
    render() {
        return (
            <div>

               Votre Double disparaît dans un tourbillon de fumée. Seul subsiste le linceul qui le recouvrait (vous pouvez le prendre si vous le désirez). Soudain, un hurlement s'élève dans le lointain. Peut-être une créature s'inquiète-t-elle du retard de la procession ? Vous n'avez aucune envie de le savoir et vous rebroussez chemin. Rendez-vous au <L to='547'>547</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P456);
