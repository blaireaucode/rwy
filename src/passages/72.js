/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P72 extends Component {
    render() {
        return (
            <div>

               « Hors de question ! vous exclamez-vous, indigné. Nous n'allons pas ramer comme des esclaves pendant toute la traversée. Il doit bien exister un autre moyen de se rendre au Royaume de Wyrd. » Vous vous éloignez de la galère. Bientôt, un homme mince vous aborde. Il porte une cape violette. « Bonjour étranger, je suis le mage Auguste de Vanterie, j'ai entendu par hasard votre conversation avec le capitaine. Il vous faudra beaucoup de courage pour vous rendre au Royaume de Wyrd, mais je connais un moyen de rendre ce périple moins éprouvant. » En parlant, il joue avec une perle blanche, maintenue à son cou par un lacet de cuir. Si vous désirez en savoir davantage, rendez-vous au <L to='166'>166</L>. Sinon, vous devez tenter votre chance auprès des navires de pêche amarrés au quai (rendez-vous au <L to='417'>417</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P72);
