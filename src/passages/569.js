/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P569 extends Component {
    render() {
        return (
            <div>

                CHEVALIER et VOLEUR : En unissant vos talents, il sera possible d'atteindre l'estrade sans fouler ce sol dallé qui de toute évidence est piégé. Vous grimpez en équilibre sur les épaules du chevalier, qui vous tient par les pieds. Vous vous accroupissez légèrement et, à un moment convenu, le chevalier vous catapulte de toutes ses forces ! Vous vous envolez dans la pièce et retombez avec agilité sur le rebord de l'estrade ! Varadaxor déroule la corde qu'il porte en permanence autour de la taille et vous l'envoie ; il en noue une extrémité à un anneau de fer scellé dans le mur, tandis que vous faites de même avec une des poignées du coffre. Vous vérifiez la solidité de l'assemblage. Il ne vous reste plus qu'à ouvrir le coffre. Rendez-vous au <L to='294'>294</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P569);
