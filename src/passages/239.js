/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P239 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Au fur et à mesure de la partie, la perplexité du chef des Elfes augmente, mais vous escamotez les pièces avec une telle dextérité qu'il ne se rend compte de rien. Finalement, il doit concéder la défaite. Il balaie les pièces du plateau d'un air las. « Vous avez gagné, dit-il. Nous ne nous opposerons pas à votre passage, mais je dois vous avertir des dangers qui vous attendent. Wadwos rôde dans ces bois, ainsi qu'Étaynes, qui vous suivra à la trace. Je pourrais encore vous parler des Eidolons ou des Chiens de Givre... Cette forêt est maudite, vous y entrez au péril de votre vie. » Vous rassemblez votre paquetage en haussant les épaules. « Nous n'avons pas le choix, répondez-vous. Merci pour la partie. » Vous passez devant les soldats,  un sourire narquois au coin des lèvres, et pénétrez dans la forêt... Rendez-vous au <L to='501'>501</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P239);
