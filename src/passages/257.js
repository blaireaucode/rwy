/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P257 extends Component {
    render() {
        return (
            <div>

               Vous retenez le corps avant qu'il ne tombe du tapis et vous le fouillez. Vous trouvez un anneau d'or, une amulette blanche et 15 Pièces d'Or. Après avoir pris ce que vous vouliez, vous poussez le cadavre et le regardez choir dans le vide. Rendez-vous au <L to='133'>133</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P257);
