/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P10 extends Component {
    render() {
        return (
            <div>

               La bête s'écroule. Vous la contournez et vous vous dirigez sans tarder vers le lac aux eaux stagnantes, recouvertes d'un limon fétide. Vous portez votre attention sur les portes quand un grognement sourd vous pousse à vous retourner. Le monstre vient de se relever ! Toutes ses blessures ont disparu et il s'avance vers vous. N'ayant aucune intention de le combattre indéfiniment, vous vous précipitez vers les portes. Rendez-vous au <L to='432'>432</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P10);
