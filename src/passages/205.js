/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P205 extends Component {
    render() {
        return (
            <div>

               Assurez-vous d'avoir payé la chambre pour cette nuit. Si vous avez seulement réglé pour la nuit précédente, vous devez débourser 1 Pièce d'Or pour une couche ordinaire, 2 si vous voulez dormir près du feu. Cette nuit de sommeil vous rend 1 point d'ENDURANCE pour un lit ordinaire et la moitié de votre total de départ si vous avez dormi près du feu. Le déjeuner coûte 1 Pièce d'Or et vous rend, si nécessaire, 1 nouveau point d'ENDURANCE (n'oubliez pas que vous ne devez en aucun cas dépasser votre total de départ). Si certains de vos compagnons ont passé la nuit dans une autre auberge, rendez-vous au <L to='480'>480</L>. Si le groupe est au complet, rendez-vous au <L to='4'>4</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P205);
