/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P193 extends Component {
    render() {
        return (
            <div>

               Varadaxor est toujours immobilisé et vous ne pouvez prendre le risque de le transporter. Vous l'abandonnez donc à son destin et quittez la tour. Le chevalier de votre groupe perdra 50 points d'Expérience à l'issue de cette aventure, pour avoir brisé le serment fait à son frère d'arme. Vous reprenez votre route vers l'est, en direction de la côte. Rendez-vous au <L to='116'>116</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P193);
