/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P483 extends Component {
    render() {
        return (
            <div>

               Ubara ferme les yeux un instant, semblant réfléchir à sa réponse. « Cherchez la lumière », dit-elle enfin. A votre air éberlué, elle ajoute : « Vous comprendrez quand vous l'aurez trouvée... » Vous remerciez Ubara de ses précieux renseignements et reprenez votre route. (Notez la cloche de fer sur votre Feuille d'Aventure.) Rendez-vous au <L to='520'>520</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P483);
