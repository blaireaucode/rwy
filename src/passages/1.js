/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P1 extends Component {
    render() {
        return (
            <div>

               « Votre destin est tracé, déclare la vieille femme en se penchant sur ses cartes. Vous avez été choisi pour accomplir une grande tâche, une quête d'une importance cruciale. » Elle relève la tête et vous observe un instant avant de désigner une nouvelle carte. « D'après cette carte, le but de votre quête est la réparation d'une injustice... ou d'une chose brisée. Vous allez entreprendre un périple, semé d'embûches, si j'en crois les influences néfastes qui environnent votre carte maîtresse. Cette carte est le Valet de Yeth ; une puissance considérable va se dresser contre vous... La prochaine carte va nous éclairer sur le but de votre quête... Arkéon, le despote. La lame est renversée... Vous rencontrerez un chef ou un roi, et si votre quête va à l'encontre de ses intérêts, il sera votre pire ennemi... Heureusement, Gaya est placée à côté de lui. La Mère de la Sagesse, le réconfort. Son visage est tourné dans la direction opposée d'Arkéon... Si l'homme se dresse sur votre chemin, une femme pourra vous prêter main forte... » La vieille ramasse ses cartes et pousse un profond soupir. Vous vous apprêtez à partir, mais elle avance une main noueuse : « Cette consultation coûte 2 Pièces d'Or, comme convenu. » En effet, vous lui aviez promis cette somme, mais sa prophétie est plutôt vague... Si vous décidez malgré tout de lui remettre 2 Pièces d'Or, rayez-les de votre <Link to='/adv'>Feuille d'Aventure</Link> et rendez-vous au <L to='355'>355</L>. Si vous jugez que sa prophétie ne vaut pas plus de 1 Pièce d'Or, rendez-vous au <L to='343'>343</L>. Si vous refusez de payer, rendez-vous au <L to='259'>259</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P1);
