/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P432 extends Component {
    render() {
        return (
            <div>

               Par quelle porte allez-vous pénétrer dans le Château de la Nuit Éternelle? La porte du Carnage (rendez-vous au <L to='528'>528</L>), la porte de la Confusion (rendez-vous au <L to='199'>199</L>) ou la porte de la Peur (rendez-vous au <L to='313'>313</L>) ?

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P432);
