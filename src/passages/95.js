/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P95 extends Component {
    render() {
        return (
            <div>

               La carapace de ces hideuses créatures a la solidité du métal et leur dard libère un poison virulent.  SCORPION (Protection 4, Dommages 3 dés) FORCE : 6 HABILETÉ : 7 POUVOIR : 6 ENDURANCE: 18 Si vous choisissez de fuir dans la cour, rendez-vous au <L to='272'>272</L>. Si vous parvenez à tuer les monstres, rendez-vous au <L to='249'>249</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P95);
