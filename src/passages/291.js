/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P291 extends Component {
    render() {
        return (
            <div>

               Un bref éclat de lumière blanche vous projette à l'extérieur de la tour, et l'édifice s'écroule, ne laissant derrière lui qu'un tas de cendres g rises. Varadaxor se relève péniblement : « Vous m'avez été d'un grand secours, ditil. En remerciement, permettez-moi de vous remettre ceci... » Il sort de son havresac un crucifix d'argent. « Cette relique contient l'index de saint Ashanax. » Le vieillard vous donne l'accolade et reprend sa route en direction du village. Si votre groupe compte un prêtre, rendez-vous au <L to='286'>286</L>. Sinon, rendez-vous au <L to='444'>444</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P291);
