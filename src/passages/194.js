/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P194 extends Component {
    render() {
        return (
            <div>

               Le cœur battant, vous vous approchez de la civière, mais votre Double a disparu. Est-ce un soulagement ou une malédiction ? Vous ne sauriez le dire. Soudain, votre attention est attirée par un objet abandonné sur le sol. C'est une amulette en forme de tête de mort. Vous pouvez la prendre (notez-la sur votre <Link to='/adv'>Feuille d'Aventure</Link>). Rendez-vous au <L to='174'>174</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P194);
