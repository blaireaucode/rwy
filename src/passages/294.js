/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P294 extends Component {
    render() {
        return (
            <div>

               L'intérieur du coffre semble s'ouvrir sur un gouffre sans fond, d'un noir insondable mais, bientôt, cinq mains font leur apparition, chacune tenant une sphère de couleur différente. La première est rouge, la deuxième bleue, la troisième grise. Les deux dernières sont respectivement blanche et or. Tout à votre observation du phénomène, vous n'avez pas remarqué que l'intérieur du couvercle s'est lui aussi modifié. La surface métallique a pris la forme d'une large bouche, qui s'adresse soudain à vous d'une voie tonnante. « Ces cinq sphères sont chargées d'une énergie surnaturelle. Un simple mot peut libérer leur pouvoir. Le mot "Sang" active la sphère rouge du Carnage. " Mort " active la sphère bleue des Mystères. " Fièvre " active la sphère de la Peste. La sphère dorée des Dons est activée par le mot " faveur ". Enfin, la sphère blanche du Feu est activée par le mot " Vlam ". Tu peux prendre une de ces sphères... » Choisissez celle que vous voulez et rendez-vous au <L to='297'>297</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P294);
