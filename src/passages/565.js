/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P565 extends Component {
    render() {
        return (
            <div>

               La journée se termine sans incident et, bientôt, le crépuscule descend sur la mer. Certains joueurs peuvent être sujets au mal de mer, à l'exception des prêtres, grâce à leur capacité de contrôle mental et physique. Lancez deux dés et ajoutez 1 point au résultat si vous avez déjeuné. Si vous faites 10, vous êtes sujet au mal de mer et vous perdez 2 points d'ENDURANCE (si votre total n'est plus que de 2 points, retranchez seulement 1 point : le mal de mer n'est pas mortel...). Comme les derniers rayons du soleil disparaissent à l'horizon, le capitaine mouille l'ancre à proximité des côtes. Vous sombrez dans un sommeil réparateur. Chaque joueur récupère 1 point d'ENDURANCE, et autre s'il se restaure le lendemain matin. Le navire repart bientôt en longeant la côte. « Nous devrions atteindre Dourhaven à la tombée de la nuit », annonce le capitaine. En effet, le soir même, la Madonna arrive à destination. Malgré l'heure tardive, le port grouille d'activité. Vous débarquez alors que la neige se met à tomber et regardez tout autour de vous. Devant votre hésitation, le capitaine vous rejoint. « Toutes les échoppes que vous voyez ici pourront vous fournir l'équipement nécessaire à votre voyage. Si vous cherchez une auberge, je vous conseille l'Ours Blanc, au bout de la rue. Elle est tenue par un ancien marchand mercanien de mes amis. » Vous remerciez le capitaine de ces précieux renseignements. Si vous désirez entrer dans une boutique, rendez-vous au <L to='288'>288</L>. Si vous préférez vous diriger sans tarder vers l'auberge, rendez-vous au <L to='75'>75</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P565);
