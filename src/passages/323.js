/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P323 extends Component {
    render() {
        return (
            <div>

               Les joyaux s'illuminent dès que vous les sortez de votre sac. Au même instant, une rafale de vent vous fouette le visage. Une longue plainte s'élève dans le lointain et se rapproche... Les joyaux laissent alors échapper deux  jets de fumée et se pulvérisent dans vos mains. Deux gigantesques loups naissent devant vous. Aussitôt, ils se tournent vers les Vampires en grondant. Manifestement, les émissaires de la Lune Bleue et de la Mort Rouge n'entretiennent aucun lien amical... Les créatures se jettent les unes sur les autres, toutes griffes dehors. Tandis que la bataille fait rage, vous rassemblez votre paquetage à la hâte et courez vers les contreforts de la montagne. Vous trouvez une crevasse dans la roche et vous vous y cachez. Les hurlements et les plaintes ne cesseront qu'au matin. Vous n'avez pu trouver le sommeil et la faim vous tenaille. S'il vous reste des provisions, restaurez-vous (rayez une ration de votre Feuille d'Aventure) pour ne pas perdre 1 point d'ENDURANCE. Rendez-vous au <L to='422'>422</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P323);
