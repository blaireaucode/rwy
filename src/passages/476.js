/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P476 extends Component {
    render() {
        return (
            <div>

               CHEVALIER : Vous faites voler deux Crânes en éclats, mais la dalle sur laquelle vous vous trouvez disparaît ! Vous cherchez, affolé, l'endroit où une nouvelle va se matérialiser, mais les Crânes vous entourent ! Vous chutez dans le vide... et votre dernière image est celle des Crânes ricanants... Si d'autres joueurs doivent encore traverser le pont, rendez-vous au <L to='240'>240</L>. Si tous les survivants ont traversé, rendez-vous au <L to='35'>35</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P476);
