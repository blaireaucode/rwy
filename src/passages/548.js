/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P548 extends Component {
    render() {
        return (
            <div>

               Vous atteignez le rez-de-chaussée et, en vous appuyant de tout votre poids, parvenez à ouvrir les lourdes portes de bronze. Le gigantesque Démon qui garde l'entrée vous tourne le dos. Vous foncez aussitôt à l'extérieur, mais le monstre vous ignore. Manifestement, sa fonction est de garder l'entrée et non d'interdire la sortie. Vous vous approchez jusqu'aux limites de la falaise qui surplombe la banquise. Il va falloir descendre... Le magicien ne pourra avoir recours à la Téléportation. Si le prêtre désire Léviter, rendez-vous au <L to='273'>273</L>. Si aucun prêtre n'est présent, rendez-vous au <L to='498'>498</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P548);
