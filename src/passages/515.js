/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P515 extends Component {
    render() {
        return (
            <div>

               Enfin l'aubergiste se faufile entre les tables. « Je n'ai plus de chambres, grommelle-t-il en jetant un rapide coup d'oeil à vos tenues poussiéreuses. Une couchette dans la salle commune coûte 1 Pièce d'Or, et 2 Pièces d'Or si vous voulez être près du feu... On paie d'avance. » Vous n'avez d'autre choix que d'accepter, ne sachant pas combien de temps vous allez demeurer en ville avant de trouver un bateau. Il ne reste qu'une couche près du feu. Si vous êtes en groupe, choisissez le joueur qui en bénéficiera. Pendant que vous parliez à l'aubergiste, les prêtres sont partis.  Vous pouvez aborder les marchands au comptoir (rendez-vous au <L to='358'>358</L>). Autrement, vous quittez l'auberge pour visiter la ville (rendez-vous au <L to='306'>306</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P515);
