/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P16 extends Component {
    render() {
        return (
            <div>

               « Silence ! » intime-t-il, agacé. « Je ne parviens pas à me concentrer ! » Ce brusque changement d'attitude vous déconcerte. L'homme qui, quelques minutes auparavant, était le charme incarné arbore maintenant une expression dure et fermée. Vous renouvelez votre question. « Je dois faire un détour par mon château, grommelle-t-il. Il faut que je récupère quelques affaires. » Si vous désirez protester, rendez-vous au <L to='536'>536</L>. Si vous acceptez ce contretemps, rendez-vous au <L to='65'>65</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P16);
