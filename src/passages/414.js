/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P414 extends Component {
    render() {
        return (
            <div>

               Vous laissez échapper un cri de stupeur. L'homme qui vous suit n'est autre que votre compagnon, disparu lors de l'attaque du bourreau ! Il vous fixe, le regard vide et les traits déformés. Votre ami est devenu un Zombie ! Rendez-vous au <L to='360'>360</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P414);
