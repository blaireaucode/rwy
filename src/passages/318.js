/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P318 extends Component {
    render() {
        return (
            <div>

               Vous débouchez sur une cour intérieure. Des arbres dénudés ploient sous les rafales d'un vent glacial. Une étrange luminosité semble annoncer l'orage à venir. Des lueurs vacillantes éclairent les fenêtres d'une dépendance. Malgré le sifflement du vent, vous croyez percevoir les échos d'une litanie provenant de la bâtisse. Rendez-vous au <L to='35'>35</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P318);
