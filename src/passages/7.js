/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P7 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : De mauvaise grâce, vous invoquez le Faltyn. « Que vas-tu réclamer cette fois-ci ? » demandez-vous à l'instant où il apparaît. « Tu es perdu sur la banquise, à ce que je vois. Je vais devoir parcourir des kilomètres sur cette immensité glacée pour déterminer où tu te trouves. Donne-moi le fourreau de l'Épée de Légende et je consentirai à cet effort. » Si vous acceptez de lui remettre ce qu'il demande, rendez-vous au <L to='220'>220</L>. Si vous refusez, rendez-vous au <L to='155'>155</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P7);
