/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P561 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : De toute évidence, la Lévitation est le meilleur moyen de vous sortir de cette situation. Allez-vous libérer votre esprit de toute pensée se rapportant à cette vague (rendez-vous au <L to='185'>185</L>) ou bien concentrer votre attention sur l'idée de la survoler (rendez-vous au <L to='443'>443</L>) ?

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P561);
