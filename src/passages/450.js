/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P450 extends Component {
    render() {
        return (
            <div>

               Ceci ne concerne que les joueurs qui sont restés dans la pièce. Le thuriféraire vous aperçoit. C'est un adolescent. Vous marquez un instant de surprise, car il se déplaçait comme un vieillard ! Le jeune homme lève alors les bras au ciel et entame une invocation d'une voix criarde. Aussitôt, huit Squelettes apparaissent sur les dalles. Vous vous préparez au combat. L'adolescent s'est enfui entre-temps.  SQUELETTE (Dommages 1 dé + 1) FORCE :6 HABILETÉ : POUVOIR : 6 ENDURANCE :6 7  Si vous désirez fuir en sautant dans la pièce en contrebas pour rejoindre vos compagnons, ôtez-vous 1 dé d'ENDURANCE pour la chute et rendez-vous au <L to='183'>183</L>. Si vous êtes vainqueur des Squelettes, rendez-vous au <L to='472'>472</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P450);
