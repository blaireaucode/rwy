/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P270 extends Component {
    render() {
        return (
            <div>

               La zone couverte de ruines vous semble dangereuse. Vous la contournez avec prudence et atteignez bientôt la porte du fond. Vous pénétrez dans une galerie dont les parois sont ornées de miroirs. Comme vous les dépassez, ils ne reflètent pas votre image... Vous poursuivez votre progression, mal à l'aise. La galerie se termine devant un large escalier qui mène à une salle circulaire, quelques mètres en contrebas. Le plafond est supporté par des colonnes ornées de motifs inconnus. Soudain, une procession passe en silence. Les hommes brandissent des cierges bleus et entourent un cadavre sur une civière. Dans quelques instants, elle arrivera à hauteur de l'escalier. Si vous désirez descendre pour observer le cortège de plus près, rendez-vous au <L to='426'>426</L>. Si vous préférez demeurer dans l'ombre en attendant qu'il soit passé, rendez-vous au <L to='371'>371</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P270);
