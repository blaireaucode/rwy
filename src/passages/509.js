/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P509 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : A peine avez-vous progressé de quelques pas, qu'une hallebarde vous bloque la route. L'autre demeure immobile. Vous savez que les Sylphes, créatures aériennes, ne peuvent être atteintes par les armes de jet. Hélas, cela vaut aussi pour la plupart de vos sortilèges, à part la Main du Diable ! Si vous voulez combattre à l'épée, rendez-vous au <L to='100'>100</L>. Si vous préférez lancer un sortilège, rendez-vous au <L to='415'>415</L>. Vous pouvez aussi recourir à un objet (rendez-vous au <L to='14'>14</L>). Si vous souhaitez rebrousser chemin, retournez au <L to='226'>226</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P509);
