/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P265 extends Component {
    render() {
        return (
            <div>

               La bonne réponse était « l'herbe ». Si vous avez correctement répondu, rendez-vous au <L to='127'>127</L>. Si vous avez donné une autre réponse, la créature démoniaque s'élance vers vous. Rendez-vous au <L to='69'>69</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P265);
