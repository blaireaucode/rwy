/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P54 extends Component {
    render() {
        return (
            <div>

               Si vous avez acheté Scribo le corbeau, désignez le joueur qui en sera propriétaire. L'oiseau ne compte pas comme un objet. Il vous suivra même si vous ne le portez pas sur votre épaule. Vous pouvez aborder les marchands au comptoir (rendez-vous au <L to='358'>358</L>), ou bien demander à l'aubergiste une chambre pour la nuit (rendez-vous au <L to='515'>515</L>). 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P54);
