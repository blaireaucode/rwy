/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P387 extends Component {
    render() {
        return (
            <div>

               Le bourreau s'écroule enfin, mais disparaît en touchant le sol, ne laissant qu'un petit tas de cendres. Les hommes encagoulés se sont enfuis pendant l'affrontement. Soudain, votre compagnon se lève en grognant et repousse d'un geste nerveux le linceul qui le couvrait (vous pouvez lui rendre sa Feuille d'Aventure). La joie des retrouvailles est interrompue par un hurlement qui s'élève des ténèbres. Sans plus attendre, vous rebroussez chemin. Rendez-vous au <L to='547'>547</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P387);
