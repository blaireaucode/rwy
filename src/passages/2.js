/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P2 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Hélas, aucune solution ne vous vient à l'esprit ! Vous auriez bien une idée, mais elle vous semble hasardeuse. Si vous préférez abandonner le problème à vos éventuels compagnons, retournez au <L to='133'>133</L> et faites un autre choix. Si vous choisissez malgré tout de mettre votre idée en pratique, rendez-vous au <L to='379'>379</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P2);
