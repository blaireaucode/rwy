/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P455 extends Component {
    render() {
        return (
            <div>

               Le brasero ne vous sera pas utile dans l'immédiat. Le quignon de pain peut être mangé à tout moment, sauf au combat. Lorsque vous le mangez, notez le numéro du paragraphe où vous vous trouvez et rendez-vous au <L to='327'>327</L>. Le contenu de la bouteille grise peut être bu à tout moment, sauf au combat. Ce faisant, notez le numéro du paragraphe où vous vous trouvez et rendez-vous au <L to='262'>262</L>. Si vous prenez le plat orné d'un pentagramme, rendez-vous tout de suite au <L to='497'>497</L>. Le parchemin peut être lu même au combat. Notez alors le numéro du paragraphe où vous vous trouvez et rendez-vous au <L to='278'>278</L>. Si vous n'avez pris aucun objet, rendez-vous au <L to='470'>470</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P455);
