/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P87 extends Component {
    render() {
        return (
            <div>

               Vous bondissez de dalle en dalle. Jetez deux dés. Si vous obtenez un résultat inférieur ou égal à votre total d'HABILETÉ, vous parvenez sans encombres de l'autre côté. Sinon, vous chutez dans le vide et vous vous écrasez au sol... Si d'autres joueurs doivent encore traverser le pont, rendez-vous au <L to='240'>240</L>. Si tous les survivants ont traversé, rendez-vous au <L to='35'>35</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P87);
