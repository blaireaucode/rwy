/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P231 extends Component {
    render() {
        return (
            <div>

               Deux marins s'emparent du capitaine. « Lâchez-moi, fripouilles ! hurle-t-il, indigné. Je n'accepterai pas une mutinerie à bord de mon navire ! » Indifférents à ses menaces, les hommes le traînent de force dans un canot. Vous poussez un soupir de soulagement. Ses projets risquaient de mettre en péril la vie de l'équipage. « Emparez-vous d'eux aussi ! hurle soudain une voix dans votre dos. Ils sont complices ! » Bildad a pris le commandement du navire et, manifestement, il ne tient pas à partager ses nouvelles charges avec vous... Malgré vos protestations, vous êtes à votre tour placé dans le canot avec Lazarus. Rendez-vous au <L to='482'>482</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P231);
