/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P307 extends Component {
    render() {
        return (
            <div>

               Le roi-sorcier s'esclaffe devant votre menace « Faites, si cela vous amuse. Vous êtes prisonnier de mon rêve. Je peux forger ce monde au gré de ma fantaisie. Lâchez donc le fourreau, il n'ira pas bien loin... » Si vous désirez le laisser tomber, malgré tout, rendez-vous au <L to='53'>53</L>. Si vous préférez utiliser un objet, rendez-vous au <L to='282'>282</L>. Si vous voulez escalader la toile dans sa direction, rendez-vous au <L to='530'>530</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P307);
