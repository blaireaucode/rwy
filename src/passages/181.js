/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P181 extends Component {
    render() {
        return (
            <div>

               Frappé à mort, Jadhak émet un râle et s'écroule, balayant d'un geste les verres alignés sur le comptoir. Vous vous tournez vers l'aubergiste et le fixez d'un air lourd de menaces. « Je suis témoin. Jadhak vous a provoqué ! » s'exclame-t-il d'une voix blanche. Tu as intérêt à confirmer cette version si jamais la garde venait à passer. » Vous décidez de sortir pendant qu'il remet de l'ordre dans la pièce, mais à mi-chemin de la porte, l'aubergiste vous apostrophe. « Attendez, si vous désirez passer la nuit ici, il faut me payer d'avance. 2 Pièces d'Or pour une paillasse et 3 pour celle près du feu. » S i vous désirez protester contre cette augmentation, rendez-vous au <L to='149'>149</L>. Si vous préférez payer la somme qu'il réclame, rendez-vous au <L to='141'>141</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P181);
