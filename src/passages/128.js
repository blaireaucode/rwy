/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P128 extends Component {
    render() {
        return (
            <div>

               L'un de vous doit monter la garde. Désignez lequel et rendez-vous au <L to='285'>285</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P128);
