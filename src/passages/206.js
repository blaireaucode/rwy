/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P206 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous vous rendez compte avec horreur que des entités démoniaques ont profité de la faiblesse de vos amis blessés pour investir leurs corps pendant leur sommeil ! Il en résulte que vous avez devant vous des Vampires des Neiges. Seul un exorcisme peut à présent sauver vos compagnons. Aussitôt, vous vous concentrez. Les joueurs vampirisés jettent deux dés : avec un résultat compris entre 2 et 6, ils demeurent des Vampires, mais si leur résultat est supérieur ou égal à 7, ils retrouvent leur ancienne personnalité. Si vous avez exorcisé tous les Vampires, rendez-vous au <L to='412'>412</L>. Si un ou plusieurs Vampires subsistent, vous devez les combattre (rendez-vous au <L to='37'>37</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P206);
