/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P334 extends Component {
    render() {
        return (
            <div>

               Dans le coffre que gardait le chef barbare, vous découvrez une hache de guerre au manche orné d'incrustations d'or pur. Si votre groupe compte un prêtre, rendez-vous au <L to='541'>541</L>. Sinon, rendez-vous au <L to='454'>454</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P334);
