/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P487 extends Component {
    render() {
        return (
            <div>

               D'un geste vif, vous jetez les dents de l'Hydre sur le sable de l'arène. A l'instant où elles touchent le sol, elles libèrent une lueur verte qui se dresse tel un mur entre vous et les Squelettes. La paroi luminescente se déchire en une centaine de fragments qui deviennent autant de créatures verdâtres d'apparence humaine, mais chacune dotée de neuf têtes serpentines. Notez les Guerriers-Crocs sur votre Feuille d'Aventure et rayez les dents de l'Hydre. Retournez au <L to='178'>178</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P487);
