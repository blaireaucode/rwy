/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P280 extends Component {
    render() {
        return (
            <div>

               Vous n'avez pas de temps à perdre avec ces énigmes stupides. D'un coup de pied, vous projetez les cassettes contre le mur, libérant des nuages de poussière et des insectes. Un hurlement de rage résonne dans la pièce et de profondes lézardes apparaissent sur les parois ! Si votre groupe compte un prêtre, rendez-vous au <L to='471'>471</L>. Sinon, rendez-vous au <L to='291'>291</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P280);
