/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P139 extends Component {
    render() {
        return (
            <div>

               « Vous avez tué ma maîtresse, dit la petite créature, mais je suis toujours sous ses ordres. Une de ces portes mène à son trésor, l'autre abrite une sombre menace. Répondez à cette énigme et je vous indiquerai laquelle choisir. » Plus tranchant que l'acier, Plus doux que la soie, Plus brillant que l'émeraude Et pourtant sans valeur. Lorsque vous aurez décidé d'une réponse, notez-la et rendez-vous au <L to='171'>171</L>. Si vous préférez attaquer la créature sans vous préoccuper de ses énigmes stupides, rendez-vous au <L to='69'>69</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P139);
