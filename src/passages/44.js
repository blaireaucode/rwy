/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P44 extends Component {
    render() {
        return (
            <div>

               « Donnez-moi le fourreau de l'Épée de Légende », ordonne-t-il en tendant la main. Si vous obéissez, rendez-vous au <L to='419'>419</L>. Si vous préférez l'attaquer, rendez-vous au <L to='522'>522</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P44);
