/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P439 extends Component {
    render() {
        return (
            <div>

               « Vous l'aurez voulu ! s'exclame Auguste. Mourez donc ! » L'Élémental d'eau se dresse, menaçant. Dans trois Assauts, il va s'abattre sur le pont du navire. Chaque joueur a la possibilité d'entamer trois actions. Il est permis de lui décocher des flèches ou de jeter des sortilèges (I'Élémental est insensible aux sortilèges mentaux). ÉLÉMENTAL  ENDURANCE :65  Vous préférez peut-être concentrer vos attaques sur le mage. Auguste est à portée de flèches et de sortilèges. Un vent violent s'est levé. Aussi, le premier tir sera calculé avec 2 dés + 2, le temps pour l'archer d'ajuster son tir en fonction du vent. AUGUSTE  POUVOIR :9  ENDURANCE :40  Si vous tuez Auguste, rendez-vous au <L to='438'>438</L>. Si vous parvenez à l'asservir, rendez-vous au <L to='372'>372</L>. Si vous détruisez l'Élémental d'eau, rendez-vous au <L to='6'>6</L>. Si toutes vos attaques ont échoué après trois Assauts, rendez-vous au <L to='89'>89</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P439);
