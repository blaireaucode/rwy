/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P151 extends Component {
    render() {
        return (
            <div>

               La Lune Bleue s'est levée sur la forêt. Vous êtes assis au pied d'un arbre à apprécier le silence. Soudain, une forme ramassée déboule des buissons ! Vous vous levez d'un bond. Heureusement, ce n'était qu'un chat sauvage. Déjà, il disparaît entre les arbres. Vous vous tournez dans la direction d'où il venait et apercevez la face grimaçante du Squelette qui vous observe ! Rendez-vous au <L to='212'>212</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P151);
