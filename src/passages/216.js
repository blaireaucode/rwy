/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P216 extends Component {
    render() {
        return (
            <div>

               Vos troupes ouvrent une brèche dans les armées squelettes afin que vous puissiez atteindre le roi-sorcier. Mais ce dernier a compris votre tactique. D'un geste, il envoie deux Squelettes de sa garde afin de bloquer l'accès à la tribune royale.  SQUELETTE (Protection 3, Dommages 3 dés) FORCE : 9 HABILETÉ : 9 POUVOIR : 9 ENDURANCE : 21 Vous ne pouvez fuir. Si vous parvenez à vous frayer un passage ou bien à tuer la garde d'élite, rendez-vous au <L to='300'>300</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P216);
