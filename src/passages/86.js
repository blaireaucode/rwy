/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P86 extends Component {
    render() {
        return (
            <div>

               MAGICIEN: Mue par un instinct surnaturel, la créature s'avance dans votre direction ! Vous disposez de trois Assauts pour lancer votre sortilège. Passé ce délai, elle sera au contact. Le Squelette a un total de POUVOIR de 6, 30 points d'ENDURANCE et 1 point de Protection. S'il arrive au contact, rendez-vous au <L to='276'>276</L>. Si vous parvenez à l'asservir, rendez-vous au <L to='248'>248</L>. Si vous réussissez à le détruire, rendez-vous au <L to='303'>303</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P86);
