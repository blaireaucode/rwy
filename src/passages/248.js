/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P248 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Le Squelette est sous l'emprise de votre sortilège, mais il lutte de toutes ses forces et vous ne pourrez le garder longtemps sous votre contrôle. Vous sentez qu'une source surnaturelle le régénère en permanence. Si vous désirez le questionner, il faudra faire vite. Allez-vous lui demander qui l'a envoyé (rendez-vous au <L to='410'>410</L>), ou bien qui il est (rendez-vous au <L to='339'>339</L>) ? Si vous préférez le tuer sans attendre, rendez-vous au <L to='303'>303</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P248);
