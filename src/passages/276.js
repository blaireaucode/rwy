/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P276 extends Component {
    render() {
        return (
            <div>

               Le Squelette s'avance inexorablement, tendant vers vous ses bras décharnés.  SQUELETTE (Protection 1, Dommages 2 dés) FORCE : 7 HABILETÉ : 8 POUVOIR : 6 ENDURANCE : 30 N'oubliez pas de déduire les éventuels Dommages que vous lui avez déjà infligés. Si vous choisissez de fuir, rendez-vous au <L to='492'>492</L>. Si vous parvenez à le détruire, rendez-vous au <L to='303'>303</L>. Si vous réussissez à l'asservir, rendez-vous au <L to='248'>248</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P276);
