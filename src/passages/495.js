/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P495 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous projetez la sphère en direction de l'autel de marbre noir en hurlant « Déflagration ! » La sphère éclate au milieu de la pièce, soufflant d'un coup la flamme de l'autel. Puis la sphère jette quelques dernières flammèches et s'éteint, plongeant la pièce dans l'obscurité. Les rubans spiralés s'enfoncent lentement dans le marbre. Rendez-vous au <L to='145'>145</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P495);
