/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P158 extends Component {
    render() {
        return (
            <div>

               « Il fallait me tuer pour échapper à ton destin ! ricane la créature. Maintenant, je ne suis plus en ton pouvoir! » Le Squelette s'illumine et tend ses doigts décharnés en direction de la Lune Bleue. L'air se réfrigère un instant autour de lui. Maintenant, il avance vers vous... Rendez-vous au <L to='212'>212</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P158);
