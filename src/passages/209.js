/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P209 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous faites appel à toutes vos connaissances. Cet étrange royaume est gouverné par le roi-sorcier, depuis son Château de la Nuit Éternelle. Les manuscrits que vous avez pu consulter font remonter son règne à plus de quatre siècles. De nombreuses sources prétendent qu'il possède le pouvoir d'investir les rêves de ses sujets et de les tuer dans leur sommeil s'ils venaient à avoir de mauvaises pensées. Le système social en vigueur sur l'île est strictement compartimenté. Les Armingiens forment la caste guerrière. Il est probable qu'ils aient perdu de leur autorité, la dernière guerre à laquelle ait participé Wyrd remontant à la chute de l'empire du Selentium. Qui plus est, les pouvoirs du roi-sorcier suffisent à maintenir l'ordre. Les Solons, gardiens de la loi, sont chargés de l'administration des terres et assurent l'éducation religieuse du peuple. Chaque moment de la journée est soumis à des rituels immuables. La majorité de la population est d'origine paysanne. Wyrd ne compte guère plus de deux millions d'âmes, la rigueur du climat maintient les paysans dans la misère et les disettes sont constantes. Les Sélers forment une caste indépendante. Ils vont où bon leur semble et agissent au mépris des lois. Personne ne sait pourquoi le roi-sorcier tolère la présence de ces vagabonds mi-prophètes, mi-troubadours qui prêchent la révolte parmi le peuple. Peut-être n'a-t-il aucun pouvoir sur eux ? Rendez-vous au <L to='534'>534</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P209);
