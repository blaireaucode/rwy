/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P189 extends Component {
    render() {
        return (
            <div>

               Devant le crucifix, les Vampires reculent d'un pas. Mais, surmontant leur frayeur, ils repartent à l'assaut (rendez-vous au <L to='37'>37</L>), mais notez que si vous touchez un Vampire avec le crucifix (2 dés + 2 car le crucifix n'a pas la maniabilité d'une arme), vous le détruisez instantanément.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P189);
