/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P199 extends Component {
    render() {
        return (
            <div>

               Vous foncez vers la porte de la Confusion et vous vous engouffrez sous le porche. La lourde herse s'abat dans un déchirement de métal, manquant de peu de vous empaler ! Le monstre s'y heurte avec rage, cherchant à vous atteindre entre les barreaux. Vous voilà hors de danger. Dans la pénombre vous repérez un couloir. Rendez-vous au <L to='229'>229</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P199);
