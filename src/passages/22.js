/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P22 extends Component {
    render() {
        return (
            <div>

               Vous ôtez le parchemin de son étui et en entamez la lecture. Les runes émettent une faible lueur. Le parchemin, réduit en poussière, tombe sur le sol et vous devenez invisible. Les tentacules s'arrêtent un court instant, cherchant leur proie qui vient de disparaître. Vous profitez de ce répit pour briser le cercle et vous enfuir vers la côte. Rendez-vous au <L to='117'>117</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P22);
