/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P568 extends Component {
    render() {
        return (
            <div>

               « Merci de m'avoir libéré, dit le Faltyn. En remerciement, je vais vous aider. » Il désigne l'arche vers laquelle vous vous dirigiez. « Derrière cette arche se trouve une série de salles. Dans chacune d'elles, il vous faudra affronter une épreuve. Certains objets vous seront nécessaires. Vous possédez peut-être une amulette en forme de tête de mort. Il vous faudra aussi de la poudre d'argent. Mais avant tout, je vous conseille de déchirer des lambeaux du linceul et de les tremper dans la vasque au pied de l'autel. Couvrez-vous la bouche et le nez avec ce linge humide... » Le Faltyn esquisse un sourire et disparaît. Chaque joueur décide s'il suit ou non les conseils de la créature. Si vous n'avez pas pris l'amulette tête de mort, vous pouvez rebrousser chemin et aller la ramasser (si vous ne l'avez jamais vue, il sera impossible de la trouver, malgré une fouille minutieuse). Si vous désirez passer sous  l'arche, rendez-vous au <L to='40'>40</L>. Si vous préférez quitter le sanctuaire et retourner sur vos pas, rendez-vous au <L to='547'>547</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P568);
