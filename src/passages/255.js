/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P255 extends Component {
    render() {
        return (
            <div>

               Le roi-sorcier pousse un hurlement de rage à l'instant où vous atteignez son trône. Il semble incapable de lancer de nouveaux sortilèges et brandit maintenant un lourd gourdin de bronze.  ROI-SORCIER (Protection 2, Dommages 1 dé) FORCE : 6 HABILETÉ : 7 POUVOIR : 6 ENDURANCE : 16 Déduisez les Dommages qu'ont occasionnés les flèches du prêtre. Si vous parvenez à le tuer, rendez-vous au <L to='190'>190</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P255);
