/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P268 extends Component {
    render() {
        return (
            <div>

               Les Zombies s'agglutinent autour de vous. Vous êtes bloqué dans votre rêve et incapable de bouger ! Chaque joueur perd 2 dés + 3 d'ENDURANCE (la valeur de Protection n'entre pas en ligne de compte) et doit ensuite obtenir un résultat inférieur ou égal à son total de POUVOIR avec deux dés afin de se réveiller. S'il manque ce lancer, il perd à nouveau 2 dés + 3 d'ENDURANCE. Jetez une nouvelle fois les dés. Si un joueur s'est réveillé et que ses compagnons sont encore endormis, rendez-vous au <L to='254'>254</L>. Si tous sont réveillés, rendez-vous au <L to='420'>420</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P268);
