/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P267 extends Component {
    render() {
        return (
            <div>

               Plusieurs heures plus tard, vous arrivez en vue du port de Quanongu. L'épuisement vous fait perdre 1 point d'ENDURANCE. Si vous avez subi des blessures, vous perdez 2 points supplémentaires pour ne pas les avoir soignées. Vous devez absolument prendre du repos. Vous pénétrez dans la cité et atteignez bientôt une auberge appelée Les Os d'Ulrik. Vous vous accoudez au comptoir et appelez l'aubergiste. Hélas, il ne lui reste plus de  chambres individuelles, tous les trappeurs étant arrivés du nord avec leurs cargaisons de fourrures. Pour 2 Pièces d'Or, il peut encore vous trouver une paillasse près du feu, dans la salle commune. Si le froid ne vous dérange pas, vous pourrez vous installer à l'extrémité de la chambre, pour 1 Pièce d'Or. Le repas vous coûtera aussi 1 Pièce d'Or. Il ne reste qu'une paillasse près du feu. Désignez le joueur qui en bénéficiera, rayez la somme demandée de votre <Link to='/adv'>Feuille d'Aventure</Link> et rendez-vous au <L to='364'>364</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P267);
