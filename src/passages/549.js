/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P549 extends Component {
    render() {
        return (
            <div>

               Malgré la menace des tentacules qui fouettent l'air, vous conservez assez de présence d'esprit pour réfléchir à l'objet que vous pourriez utiliser. Allez vous activer la sphère de Feu (rendez-vous au <L to='78'>78</L>), dérouler un parchemin d'Invisibilité (rendez-vous au <L to='22'>22</L>), jouer de la lyre (rendez-vous au <L to='340'>340</L>) ou bien regarder à travers les yeux joyaux (rendez-vous au <L to='401'>401</L>) ? Si vous ne possédez aucun de ces objets, vous devez combattre le monstre (rendez-vous au <L to='457'>457</L>) ou bien fuir vers la côte (rendez-vous au <L to='316'>316</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P549);
