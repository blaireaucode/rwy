/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P198 extends Component {
    render() {
        return (
            <div>

               Le joueur au fond de la fosse ne peut pas frapper le Squelette avant d'être sorti. Pour s'extirper du trou, il faut un résultat inférieur ou égal à son total d'HABILETÉ avec deux dés. S'il échoue, le joueur peut réitérer sa tentative à chaque Assaut. Le Squelette possède l'avantage de la hauteur sur le joueur au fond de la fosse. Il procède à ses lancers avec 2 dés- 1. Le Squelette concentre ses attaques sur le joueur prisonnier de la fosse. SQUELETTE (Protection 2, Dommages 3 dés) FORCE : 8 HABILETÉ : 8 POUVOIR : 8 ENDURANCE : 31 Même si vous l'avez blessé précédemment, son total d'ENDURANCE est à nouveau à son niveau de départ. Si vous parvenez à le détruire, rendez-vous au <L to='303'>303</L>. Si vous préférez fuir, rendez-vous au <L to='204'>204</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P198);
