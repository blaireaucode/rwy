/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P287 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Cette amulette est indispensable, mais seul le Faltyn pourra la récupérer. De mauvaise grâce, vous invoquez la malicieuse créature, qui apparaît quelques instants plus tard. « Il me faut une amulette blanche pour diriger ce tapis, lui dites-vous. Elle est autour du cou du cadavre qui gît en bas, sur la banquise. » Le Faltyn réfléchit. « Cela est possible, répond-il, mais en retour, j'espère recevoir un présent. En premier, j'aimerais deux joyaux bleus en forme d'yeux. Si tu ne les possèdes pas, je veux une tête de loup en argent, ou bien une sphère de Feu, ou encore le crucifix de saint Ashanax. Si tu n'as rien de tout cela, donne-moi un autre objet, je m'en contenterai. » Vous devez lui remettre un des objets, selon l'ordre de préférence. Cela fait, le Faltyn disparaît. Vous craignez un instant qu'il ne vous ait joué, mais il revient bientôt et vous tend l'amulette comme convenu. Rendez-vous au <L to='23'>23</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P287);
