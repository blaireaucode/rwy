/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P314 extends Component {
    render() {
        return (
            <div>

               La cage est suspendue à quelques mètres au-dessus de vos têtes. Quel aventurier va tenter de délivrer le Faltyn ? Un prêtre (rendez-vous au <L to='351'>351</L>), un voleur (rendez-vous au <L to='51'>51</L>), ou un magicien (rendez-vous au <L to='260'>260</L>) ? Si votre groupe ne compte aucun de ces personnages, rendez-vous au <L to='221'>221</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P314);
