/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P150 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Une seconde plus tard, vous vous matérialisez à hauteur de la cage. Vous défaites les entraves qui emprisonnent le Faltyn et la créature vous aide à rejoindre la terre ferme. Rendez-vous au <L to='568'>568</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P150);
