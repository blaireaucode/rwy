/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P329 extends Component {
    render() {
        return (
            <div>

               Votre ancien compagnon est bien mort cette fois-ci. Si vous disposez d'un objet permettant de le ressusciter (une amulette-scarabée, par exemple), vous pouvez l'employer. Dans ce cas, il renaît, complètement rétabli, et vous pouvez lui rendre sa Feuille d'Aventure. Sinon, vous lui accordez une dernière pensée avant de le fouiller (consultez sa Feuille d'Aventure). Ce faisant, vous découvrez une amulette en forme de tête de mort autour de son cou. N'oubliez pas de la noter si vous la prenez. Rendez-vous au <L to='174'>174</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P329);
