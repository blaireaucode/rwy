/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P420 extends Component {
    render() {
        return (
            <div>

               Vous vous réveillez en sursaut, le front ruisselant de sueur. Autour de vous, les cadavres sont demeurés en place, immobiles sous la Lune Bleue. Tout cela n'était qu'un rêve, pourtant vous sentez encore sur tout votre corps les meurtrissures dues aux coups des Zombies! Rendez-vous au <L to='539'>539</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P420);
