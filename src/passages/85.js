/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P85 extends Component {
    render() {
        return (
            <div>

               Vous fouillez le campement dévasté et découvrez des Pièces d'Or éparpillées (vous en comptez 45). Le plateau d'échecs et les pièces se trouvent encore près du feu. La cape de fourrure d'un joueur était maintenue par une broche d'argent en forme de tête de loup (prenez ce qui vous convient et notez-le sur votre <Link to='/adv'>Feuille d'Aventure</Link>). Soudain, des gémissements vous poussent à vous retourner. Vous foncez dans leur direction et découvrez le musicien. Le malheureux tente de se relever, mais une vilaine blessure au côté l'en empêche. Il a déjà perdu beaucoup de sang, sa fin est proche... Rendez-vous au <L to='335'>335</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P85);
