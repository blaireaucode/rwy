/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P465 extends Component {
    render() {
        return (
            <div>

               Vous atteignez le personnage allongé, mais lorsque vous le retournez, vous constatez qu'il s'agit d'une jeune femme à la chevelure noire. Elle est livide et sa respiration est faible. Si le prêtre désire recourir à des Soins, qu'il procède de la manière habituelle. Lorsque la jeune femme a récupéré au moins 1 point d'ENDURANCE, rendez-vous au <L to='48'>48</L>. Si votre groupe ne compte pas de prêtre, mais si vous avez un brasero et des rations à partager avec elle, rendez-vous au <L to='533'>533</L>. Si vous ne pouvez l'aider d'une manière ou d'une autre, rendez-vous au <L to='59'>59</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P465);
