/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P460 extends Component {
    render() {
        return (
            <div>

               En vous voyant brandir la cloche de fer, le roi sorcier esquisse un sourire méprisant, mais à l'instant où vous l'agitez la terreur le gagne peu à peu ! A chaque sonnerie de la cloche, des fissures naissent sur le trône de cristal. Le roi-sorcier se lève d'un bond et entame une série d'incantations. Des images naissent autour de vous, se déployant par vagues à partir du trône. Vous agitez la cloche à toute allure. La sonnerie devient assourdissante. Soudain, le roi-sorcier se tétanise, les yeux révulsés, et sa couronne de cristal explose en un millier de fragments... Rendez-vous au <L to='62'>62</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P460);
