/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P486 extends Component {
    render() {
        return (
            <div>

               Vous vous enveloppez dans vos couvertures, étalées à même le sol. Un voile d'obscurité tombe sur la clairière, percé çà et là par les braises rougeoyantes. A l'autre bout du champ, le musicien poursuit sa triste complainte. Non loin de vous, les forestiers devisent bruyamment. Les marchands n'ont pas bougé et forment un cercle compact autour des joueurs. Rendez-vous au <L to='24'>24</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P486);
