/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P538 extends Component {
    render() {
        return (
            <div>

               A votre vue, les personnages encagoulés s'arrêtent. Vous bondissez, prêt à frapper. Dans un ensemble parfait, ils abaissent leurs cierges et éclairent leurs visages... Ce que vous voyez vous arrache un frisson d'horreur et l'air vient à vous manquer. Les personnages n'opposent aucune résistance et un seul coup suffit à les éliminer mais pour ce faire, vous devez obtenir un résultat inférieur ou égal à votre Niveau avec 1 dé + 1. Si vous manquez ce lancer, la seule contemplation de leur visage vous fait mourir de peur...  PORTEUR POUVOIR : 9 ENDURANCE : 1 Si vous les tuez tous les cinq, rendez-vous au <L to='96'>96</L>. Si vous préférez fuir, rendez-vous au <L to='152'>152</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P538);
