/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P529 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : La sphère de Feu s'échauffe entre vos mains. En quelques secondes, la température devient insupportable et vous la lâchez avec un cri de douleur. Elle tombe sur le tapis, qui devient la proie des flammes. Constatant votre bévue, vous tentez d'étouffer le feu avec votre cape, mais trop tard... Le tapis en feu perd de l'altitude et vous vous écrasez sur le sol gelé. Vlan ! Votre aventure est terminée.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P529);
