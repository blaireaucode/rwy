/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P518 extends Component {
    render() {
        return (
            <div>

               Ubara fronce les sourcils et son regard se perd un instant dans le lointain. « Gristun est puissant mais stupide, dit-elle enfin. Ne cherchez pas à employer la force contre lui. Esquivez-le et foncez vers les portes. Vous serez confronté à la porte du Carnage, à la porte de la Confusion et à la porte de la Peur, choisissez bien... » Vous remerciez Ubara de ses précieux renseignements et reprenez votre route. N'oubliez pas de noter la cloche de fer sur votre Feuille d’Aventure. Rendez-vous au <L to='520'>520</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P518);
