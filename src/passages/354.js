/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P354 extends Component {
    render() {
        return (
            <div>

               Les lames acérées vous labourent les mains ! Vous lâchez prise en hurlant de douleur. Les joueurs qui ont tenté de s'agripper au mur perdent 2 points d'ENDURANCE (l'armure n'assure aucune Protection). Les entailles sont profondes et votre total de FORCE baisse de 1 point jusqu'à la fin de cette aventure. Rendez-vous au <L to='431'>431</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P354);
