/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P283 extends Component {
    render() {
        return (
            <div>

               CHEVALIER : Tous regardent, hébétés, l'Élémental prêt à s'écraser sur le navire. L'équipage semble résigné à mourir, mais pas vous ! Vous ne périrez pas sans combattre. Auguste s'est rapproché du navire afin de mieux contempler votre agonie. C'est l'occasion que vous attendiez ! Usant de votre épée comme d'une lance, vous la projetez dans sa direction. Si vous obtenez un résultat inférieur ou égal à votre total d'HABILETÉ avec trois dés, rendez-vous au <L to='446'>446</L>. Sinon, votre épée tombe dans l'eau. Vous n'aurez pas à vous soucier de la perte de votre arme, car dans quelques secondes, l'Elémental aura englouti le navire, entraînant tous ses occupants par le fond...

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P283);
