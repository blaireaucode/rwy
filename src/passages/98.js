/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P98 extends Component {
    render() {
        return (
            <div>

               Ce qui suit est réservé au joueur qui ouvre la marche. A l'instant où vous posez le pied sur une dalle blanche, une lumière aveuglante en jaillit. Une odeur de chair brûlée envahit la pièce. La douleur que vous ressentez vous fait comprendre que cette chair est la vôtre ! Vous perdez 4 dés d'ENDURANCE (l'armure n'assure aucune Protection). Si vous êtes encore en vie, vous savez maintenant que les dalles blanches sont dangereuses mais, en ne foulant que les dalles noires, vous parvenez sans encombre jusqu'au coffre et vous en soulevez le couvercle. Rendez-vous au <L to='294'>294</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P98);
