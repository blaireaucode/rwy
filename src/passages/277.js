/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P277 extends Component {
    render() {
        return (
            <div>

               « Bonne question, dit-elle. Vous traitez les problèmes au fur et à mesure qu'ils se présentent... » Son regard se perd dans le lointain. « Ce n'est pas facile, reprend-elle enfin. Les Elfes feront tout pour gagner, allant jusqu'à provoquer des illusions sur les pièces, afin de vous faire croire que vous avez perdu. Pour contrer cette tentative, avancez vos pièces en formation régulière barrant l'échiquier. De cette manière, il leur sera difficile de provoquer des illusions sur des pièces isolées. » Vous remerciez Ubara de ces renseignements et reprenez votre route. N'oubliez pas de noter la cloche de fer sur votre <Link to='/adv'>Feuille d'Aventure</Link>. Rendez-vous au <L to='520'>520</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P277);
