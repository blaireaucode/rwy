/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P219 extends Component {
    render() {
        return (
            <div>

               Vous débouchez sur un balcon surplombant une cour intérieure. L'air vif de la nuit vous fouette le visage. En contrebas, des braseros ardents éclairent le sol dallé. De l'autre côté de la cour se dresse une tour. Comme vous observez la bâtisse, un pont composé de dalles étincelantes apparaît dans les airs, reliant le balcon à la tour. De la pointe de votre épée, vous éprouvez la solidité de la première dalle. Elle pourra supporter votre poids. Mais alors, un curieux phénomène se produit : à intervalles irréguliers certaines dalles disparaissent et d'autres apparaissent. Vous avez beau observer le phénomène, il est impossible de prévoir les modifications. Il va pourtant falloir traverser... Rendez-vous au <L to='240'>240</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P219);
