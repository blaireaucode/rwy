/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P463 extends Component {
    render() {
        return (
            <div>

               CHEVALIER : Les Sylphes sont invisibles, mais vous distinguez les hallebardes ! Vous allez donc concentrer vos attaques sur elles et tenter de désarmer les Sylphes. Pour ce faire, vous devez lancer 2 dés + 3. Tant qu'ils ne sont pas désarmés, les Sylphes ripostent à chaque Assaut. SYLPHE (Dommages 1 dé + 3) FORCE : 7 Vous ne pouvez fuir pendant ce combat. Lorsque vous aurez désarmé vos deux adversaires, rendez-vous au <L to='350'>350</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P463);
