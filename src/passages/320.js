/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P320 extends Component {
    render() {
        return (
            <div>

               Le colporteur déroule une pièce de toile à même le sol et y dépose sa marchandise. « Choisissez, dit-il. Tous mes articles sont de qualité. » En fait, cette pacotille est sans intérêt. Quelques objets, toutefois, retiennent votre attention : une poche de Tahac, pour 18 Pièces d'Or, une pipe en terre pour 6 Pièces d'Or, et six flèches pour 1 Pièce d'Or chacune. Choisissez ce que vous voulez. Le colporteur ramasse sa camelote et s'éloigne. Si vous désirez pénétrer dans l'auberge, rendez-vous au <L to='429'>429</L>. Si vous préférez poursuivre votre route, rendez-vous au <L to='267'>267</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P320);
