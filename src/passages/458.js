/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P458 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Peut-être le tapis est-il mû par une entité démoniaque emprisonnée ? Si cela est, votre Exorcisme risque de ne pas aboutir... Si vous désirez neutraliser le tapis par ce moyen, rendez-vous au <L to='28'>28</L>. Sinon, retournez au <L to='133'>133</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P458);
