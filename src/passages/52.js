/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P52 extends Component {
    render() {
        return (
            <div>

               Allez-vous utiliser la sphère de Feu (rendez-vous au <L to='324'>324</L>), le briquet d'ambre (rendez-vous au <L to='77'>77</L>) ou de la poudre d'argent (rendez-vous au <L to='64'>64</L>) ? Si vous ne possédez pas ces objets ou si vous ne voulez pas les employer, vous partez dans la direction opposée à la procession (rendez-vous au <L to='152'>152</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P52);
