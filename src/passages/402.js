/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P402 extends Component {
    render() {
        return (
            <div>

               CHEVALIER : « Il faut bien mourir un jour », répliquez-vous en armant votre bras. « Non ! s'écrie Auguste. Arrêtez ! Je vais vous conduire jusqu'à Wyrd ! » Sa couardise vous arrache un sourire. Sous la menace de votre lame, vous le forcez à changer de direction et, quelques heures plus tard, les côtes désolées du Royaume de Wyrd apparaissent à l'horizon. Le mage vous dépose et s'enfuit sans demander son reste... Rendez-vous au <L to='117'>117</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P402);
