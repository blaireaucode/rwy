/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P152 extends Component {
    render() {
        return (
            <div>

               Deux colonnes de marbre gris donnent accès à une série de chambres. Les hauts plafonds se perdent dans l'obscurité et les murs sont recouverts de basreliefs et de gargouilles grimaçantes. Des siècles de poussière ont tapissé l'ensemble d'un voile opaque. Après maintes circonvolutions dans ce dédale, vous débouchez sur une cour intérieure. Les dalles craquent sous vos pas, provoquant la fuite désordonnée de milliers d'insectes. Vous arrivez bientôt devant un escalier de pierre. La balustrade est envahie par le lierre et les mousses, mais lorsque vous y posez votre main, la végétation tombe en poussière. Soudain, le craquement d'une dalle vous fait sursauter. Vous êtes suivi... Si vous désirez vous retourner, rendez-vous au <L to='414'>414</L>. Si vous préférez poursuivre votre chemin sans regarder en arrière, rendez-vous au <L to='115'>115</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P152);
