/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P403 extends Component {
    render() {
        return (
            <div>

               Vous vous réveillez en sursaut. La femme a disparu. Vous ne savez pas combien de temps vous avez dormi, mais ce repos vous a remis d'aplomb. Le total d'ENDURANCE de tous les joueurs retrouve son niveau de départ. En rassemblant votre paquetage, vous découvrez un petit cadeau laissé par votre hôte. Il s'agit d'un briquet à silex, sculpté dans de l'ambre. Notez-le sur votre <Link to='/adv'>Feuille d'Aventure</Link> si vous le conservez. Vous émergez de la caverne. Vous pensiez que l'aube s'était levée, mais la forêt est plongée dans une pénombre blafarde qui ne permet pas de déterminer l'heure. A regret, vous vous éloignez de ce havre de paix et reprenez le sentier qui se dirige vers le nord. Rendez-vous au <L to='104'>104</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P403);
