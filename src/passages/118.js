/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P118 extends Component {
    render() {
        return (
            <div>

               Les marins, armés de couteaux à dépecer et de harpons, vous encerclent. Allez-vous utiliser le crucifix d'argent (rendez-vous au <L to='18'>18</L>), la sphère de Feu (rendez-vous au <L to='560'>560</L>) ou une poche de Tahac (rendez-vous au <L to='490'>490</L>) ? Si vous ne possédez aucun de ces objets vous devez combattre (rendez-vous au <L to='148'>148</L>), ou bien déposer les armes (rendez-vous au <L to='36'>36</L>). 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P118);
