/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P546 extends Component {
    render() {
        return (
            <div>

               Vous devez choisir une stratégie, sachant que les Elfes sont des joueurs méthodiques, mais souffrant d'un manque d'inspiration... Allez-vous déplacer vos pièces centrales de manière à pénétrer sa défense (rendez-vous au <L to='56'>56</L>), avancer vos pièces en formation régulière barrant l'échiquier (rendez-vous au <L to='321'>321</L>), simuler un repli de vos pièces centrales et avancer vos pièces latérales de manière à l'encercler (rendez-vous au <L to='138'>138</L>), ou bien déplacer vos pièces au gré de votre intuition (rendez-vous au <L to='542'>542</L>) ?

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P546);
