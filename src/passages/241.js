/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P241 extends Component {
    render() {
        return (
            <div>

               Le cadavre d'Auguste roule vers le bord du tapis ! Si vous désirez le rattraper, rendez-vous au <L to='257'>257</L>. Si vous préférez le laisser choir dans le vide, rendez-vous au <L to='133'>133</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P241);
