/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P75 extends Component {
    render() {
        return (
            <div>

               Vous poussez la lourde porte de l'auberge de l'Ours Blanc. L'auberge est presque vide, mais le patron, un gros homme bedonnant à la barbe noire, vous accueille avec le sourire. « Allez donc vous réchauffer près de la cheminée, dit-il. Je peux vous proposer des chambres individuelles pour 1 Pièce d'Or. Les voyageurs se font rares au début de l'hiver. Le repas coûte 1 Pièce d'Or, vin chaud aux épices compris. Si vous ne pouvez pas vous offrir de chambre, vous pourrez coucher près du feu, c'est gratuit. » Si vous désirez prendre un repas, rendez-vous au <L to='531'>531</L>. Sinon, rendez-vous au <L to='537'>537</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P75);
