/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P500 extends Component {
    render() {
        return (
            <div>

               Vous descendez les marches en silence. La procession disparaît au détour de l'avenue. Vous attendez quelques instants encore avant de partir dans la direction opposée. Rendez-vous au <L to='152'>152</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P500);
