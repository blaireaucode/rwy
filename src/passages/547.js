/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P547 extends Component {
    render() {
        return (
            <div>

               Deux colonnes de marbre gris donnent accès à une série de chambres. Les plafonds se perdent dans l'obscurité et les murs sont recouverts de bas-reliefs et de gargouilles grimaçantes. La poussière a tapissé l'ensemble d'un voile opaque. Après maintes circonvolutions dans ce dédale, vous débouchez sur une terrasse surplombant le château. Les dalles craquent sous vos pas, provoquant la fuite de milliers d'insectes. Un escalier de pierre s'élève devant vous. La balustrade en est rongée par le lierre et les mousses, mais lorsque vous y posez la main la végétation tombe en poussière... Rendez-vous au <L to='353'>353</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P547);
