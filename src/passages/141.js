/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P141 extends Component {
    render() {
        return (
            <div>

               Vous vous fondez dans la foule qui encombre les rues, malgré l'heure matinale. Vous passez devant l'étal d'un bateleur qui présente un spectacle de marionnettes. Vous vous arrêtez un instant, fasciné par cet univers de papier, où évoluent rois et magiciens. Vous ne savez si l'épopée qui est montrée est une pure fiction ou bien une vieille légende de Krarth, mais vous n'avez jamais entendu pareille histoire. Si vous désirez suivre le spectacle, rendez-vous au <L to='418'>418</L>. Si vous préférez passer votre chemin, rendez-vous au <L to='306'>306</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P141);
