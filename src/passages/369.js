/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P369 extends Component {
    render() {
        return (
            <div>

               MAGICIEN: Vous déroulez le tapis. Dans quelques secondes, la vague sera sur vous. Si vous possédez l'amulette blanche d'Auguste, rendez-vous au <L to='551'>551</L>. Sinon, vous êtes incapable d'animer le tapis et le flot vous emporte au fond du gouffre. Les joueurs survivants se rendent au <L to='430'>430</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P369);
