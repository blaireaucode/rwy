/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P303 extends Component {
    render() {
        return (
            <div>

               Le corps squelettique de la créature se décompose en une pâte jaunâtre. Sa robe noire vire au gris et ses lambeaux sont dispersés par le vent nocturne. Seuls subsistent entre les herbes deux yeux bleus brillant d'un éclat cristallin. Vous pouvez les prendre (n'oubliez pas de les noter sur votre <Link to='/adv'>Feuille d'Aventure</Link>). Sans tarder, vous reprenez votre route, bien décidé à ne pas vous arrêter avant d'avoir quitté les bois. Rendez-vous au <L to='165'>165</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P303);
