/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P67 extends Component {
    render() {
        return (
            <div>

               Un cri étouffé vous arrache de votre sommeil. Vous cherchez votre compagnon du regard, mais en vain. Soudain, une masse inerte tombe à côté de vous ! Le cadavre de votre ami, ses yeux figés dans une expression de terreur ! Comme vous le contemplez, interdit, il se décompose et tombe en poussière ! Tous vos sens en alerte, vous scrutez les alentours. Des craquements résonnent dans le sous-bois. Rendez-vous au <L to='521'>521</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P67);
