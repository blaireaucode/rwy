/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P520 extends Component {
    render() {
        return (
            <div>

               Vous reprenez votre route vers le nord, longeant les forêts de pins pour vous abriter du vent. L'air est glacial et vous devez prendre garde à ne pas respirer trop vite. En milieu d'après-midi, une tempête de neige ralentit encore votre progression. Vous arrivez en vue d'une vaste forêt ; un sentier serpente entre les arbres. Vous alliez vous y engager quand surgit une troupe d'archers, vêtus de vert et de gris. Le chef des Elfes vous fixe de ses yeux verts. « Cette forêt est la nôtre, dit-il. Nous n'accepterons pas que des humains violent notre domaine. Rebroussez chemin... » Si vous désirez proposer au chef des Elfes de jouer votre droit de passage aux échecs, rendez-vous au <L to='361'>361</L>. Si vous préférez combattre, rendez-vous au <L to='425'>425</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P520);
