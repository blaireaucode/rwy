/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P526 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Jetez 2 dés + 1. Si le résultat est inférieur ou égal à votre total d'HABILETÉ, vous parvenez sans encombre de l'autre côté. Sinon, hélas, vous chutez dans le vide ! En cas de chute, jetez un dé: si vous faites de 1 à 3, vous vous écrasez au sol , si vous faites de 4 à 6, vous parvenez à Léviter et vous vous posez sur une nouvelle dalle. Jetez alors à nouveau 2 dés + 1. Si vous réussissez cette fois-ci, vous atteignez le parapet opposé. Si d'autres joueurs doivent traverser le pont, rendez-vous au <L to='240'>240</L>. Si tous les survivants ont traversé, rendez-vous au <L to='35'>35</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P526);
