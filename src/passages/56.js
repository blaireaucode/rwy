/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P56 extends Component {
    render() {
        return (
            <div>

               Votre tactique semble porter ses fruits et, une à une, vous empochez les pièces de votre adversaire. Mais, en milieu de partie, la situation s'inverse et l'Elfe gagne systématiquement. Vous ne comprenez rien, il vous aurait donc manoeuvré depuis le début? Vous devez concéder la défaite. L'Elfe se relève, l'air satisfait. «Vous avez perdu, dit-il. Retournez d'où vous venez. » Certes, c'est ce que vous aviez convenu, mais cela signifie aussi la fin de votre quête. Allez-vous abandonner maintenant ? Si vous préférez combattre les Elfes, rendez-vous au <L to='211'>211</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P56);
