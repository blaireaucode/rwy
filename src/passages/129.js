/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P129 extends Component {
    render() {
        return (
            <div>

               Privé de son énergie surnaturelle, le Squelette tombe à genoux. « Aucun mortel ne pourra manier l'Épée... », murmure-t-il avant de disparaître dans les eaux du lac. Vous scrutez la surface, mais il demeure invisible. Vous brisez la glace qui emprisonne la barque et retournez vers la rive. Rendez-vous au <L to='165'>165</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P129);
