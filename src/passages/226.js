/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P226 extends Component {
    render() {
        return (
            <div>

               Soudain, les murs de la pièce s'embrasent mais, curieusement, les flammes ne dégagent aucune chaleur. Bientôt, un passage apparaît sur une des parois. Vous vous y engagez et débouchez devant un pont qui enjambe une large crevasse. Le grondement d'une rivière souterraine s'élève des profondeurs. Le pont étroit est relié à une tour. Vous ne pourrez l'emprunter que l'un après l'autre. Brusquement, deux hallebardes surgissent de la pénombre, flottant dans les airs, de part et d'autre de la construction. Vous observez l'étrange phénomène pendant un instant avant de comprendre que les armes sont tenues par des Sylphes. Ces créatures invisibles sont difficiles à vaincre, mais ce pont est l'unique issue. Si vous désirez tirer une flèche, rendez-vous au <L to='337'>337</L>. Sinon, vous devez choisir celui qui traversera le premier : le magicien (rendez-vous au <L to='509'>509</L>), le voleur (rendez-vous au <L to='392'>392</L>), le prêtre (rendez-vous au <L to='210'>210</L>) ou le chevalier (rendez-vous au <L to='463'>463</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P226);
