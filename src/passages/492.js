/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P492 extends Component {
    render() {
        return (
            <div>

               Vous tournez les talons et vous vous enfuyez dans les bois. Les branches basses vous fouettent le visage. Le disque de la Lune Bleue brille tel un oeil cyclopéen observant le moindre de vos mouvements. Au bout de quelques minutes d'une course éperdue, vous osez vous arrêter. Tous vos sens en alerte, vous observez les alentours. Un silence de mort plane sur la forêt. Si vous désirez établir un bivouac, rendez-vous au <L to='225'>225</L>. Si vous préférez poursuivre à travers bois, rendez-vous au <L to='162'>162</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P492);
