/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P555 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Vous disposez de trois Assauts pour lancer votre sortilège. Si vous réussissez, vous vous téléportez par-dessus la vague. Si vous échouez, vous êtes emporté par le flot et entraîné dans le gouffre... Rendez-vous au <L to='430'>430</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P555);
