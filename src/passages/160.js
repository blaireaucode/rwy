/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P160 extends Component {
    render() {
        return (
            <div>

               Des murmures et des rires s'élèvent des ténèbres mais, en brandissant votre torche, vous ne décelez aucun mouvement. Soudain, un vent tourbillonne dans la pièce, chargé de miasmes fétides. La rafale nauséabonde vous fouette le visage. Le Souffle de la Mort pénètre dans vos poumons. Chaque joueur lance un dé pour chacune des cinq maladies ci-dessous : le joueur contracte la maladie avec un résultat de 1 ou 2. Ajoutez 1 au résultat si vous vous êtes protégé le visage d'un linge humide avant de pénétrer dans la salle. Ajoutez 2 si vous possédez la sphère de la Peste ; elle a pouvoir d'absorber toutes les maladies. Malaria Typhoïde Dégénérescence Hémophilie blessure Rachitisme  - 1 en HABILETÉ - 1 en FORCE - 1 en POUVOIR -1 point supplémentaire d'ENDURANCE à chaque La faiblesse empêche de porter plus de huit objets  Ces effets durent jusqu'au terme de cette aventure. Lorsque chaque joueur aura effectué ses cinq lancers, choisissez entre poursuivre votre exploration (rendez-vous au <L to='557'>557</L>), et quitter le sanctuaire (rendez-vous au <L to='547'>547</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P160);
