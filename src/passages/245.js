/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P245 extends Component {
    render() {
        return (
            <div>

               L'aubergiste vaque à ses occupations, mais il s'approche de vous. « Combien de temps comptez-vous demeurer à Misdraex ? » demande-t-il. Mais, sans vous laisser le temps de répondre, il enchaîne « Vous allez vers l'est ? A Port Quanongu ? Vous allez prendre un bateau, j'en suis sûr. » Vous alliez enfin lui répondre quand sa femme sort de sa cuisine, le sourcil froncé. L'aubergiste vous quitte aussitôt en grommelant : « Après tout, ce n'est pas mon affaire. Tous les mêmes ces étrangers, on ne peut pas leur arracher un mot. » Amusé, vous observez la salle et repérez un vieux chevalier qui mange un bol de soupe près de l'âtre. Si vous désirez l'aborder, rendez-vous au <L to='143'>143</L>. Si vous préférez reprendre la route, rendez-vous au <L to='116'>116</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P245);
