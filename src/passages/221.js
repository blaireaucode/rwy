/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P221 extends Component {
    render() {
        return (
            <div>

               « Désolé, dites-vous en haussant les épaules. Il est impossible de vous aider... » Vous quittez le sanctuaire et passez sous l'arche, sans vous préoccuper des insultes que profère la petite créature. Rendez-vous au <L to='40'>40</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P221);
