/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P378 extends Component {
    render() {
        return (
            <div>

               CHEVALIER : Vous dégainez votre épée et faites scintiller la lame sous le soleil. « Je pourrais tout aussi bien trancher ce tapis en deux, dites-vous d'une voix calme. Cela m'amuserait de te voir tomber. » Auguste vous observe un instant, mal à l'aise. « Tu ne le feras pas, dit-il enfin. Tu ne tiens pas à mourir. » Si vous désirez mettre votre menace à exécution, rendez-vous au <L to='402'>402</L>. Si vous préférez combattre Auguste, rendez-vous au <L to='522'>522</L>. Si vous choisissez de vous rendre, rendez-vous au <L to='44'>44</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P378);
