/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P19 extends Component {
    render() {
        return (
            <div>

               Vous avancez dans la pièce. Le premier joueur selon l'ordre de marche tombe aussitôt dans une fosse hérissée de pieux et perd 3 dés d'ENDURANCE (moins sa Protection) ! Au même instant, la fermette disparaît et vous vous retrouvez au bord d'un gouffre. Le Squelette, baigné par les rayons de la Lune Bleue, se dresse devant vous ! Rendez-vous au <L to='198'>198</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P19);
