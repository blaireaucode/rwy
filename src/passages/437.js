/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P437 extends Component {
    render() {
        return (
            <div>

               PRÊTRE et VOLEUR : Lancez deux dés. Si le résultat est inférieur ou égal à votre total d'HABILETÉ, rendez-vous au <L to='271'>271</L>. Sinon, la lueur atteint le centre de la clairière (rendez-vous au <L to='180'>180</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P437);
