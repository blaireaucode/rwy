/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P278 extends Component {
    render() {
        return (
            <div>

               Vous déroulez le parchemin et lisez à haute voix ce qui y est inscrit : « Ruat fulmen. » Un grondement sourd fait trembler l'air au-dessus de vos têtes. Dans un sifflement, une boule de foudre descend du ciel à une allure vertigineuse. Si vous êtes en combat ou sur le point de débuter un affrontement, vous pouvez diriger la boule de foudre sur une cible unique, lui infligeant 5 dés de Dommages (moins sa valeur de Protection). A la fin de cette attaque, le parchemin se pulvérise. Retournez au paragraphe que vous avez quitté.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P278);
