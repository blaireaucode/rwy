/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P304 extends Component {
    render() {
        return (
            <div>

               La progression vers le centre de cette toile n'est pas une tâche facile. Vous ne pouvez vous déplacer que d'une case par Assaut. Au début de chaque Assaut, avant que vous n'agissiez, une violente décharge d'énergie court le long des fils de la toile et vous fait perdre 2 dés d'ENDURANCE (l'armure n'assure aucune Protection). Vous devez aussi essuyer les attaques du Loup Volant. Il agresse en priorité le joueur qui détenait la broche à partir de laquelle il a été créé.  LOUP VOLANT (Protection 2, Dommages 2 dés) FORCE : 7 ENDURANCE: 38 HABILETÉ : 8 ROI-SORCIER (Protection 2) ENDURANCE: 16  Comme vous êtes accroché aux fils de la toile, il vous est impossible de jeter des sortilèges, lancer des flèches ou une hache. Vous devez atteindre la case centrale, sur laquelle se trouve le trône pour attaquer le roi-sorcier. Si votre groupe compte un prêtre, il peut l'éviter et décocher des flèches en même temps. N'oubliez pas que la lévitation se fait à la verticale ; le prêtre qui lévite ne peut donc pas avancer. (Curieusement, la tentative de lévitation est aussitôt couronnée de succès. Il semblerait que les pouvoirs du prêtre soient renforcés à l'intérieur du rêve.) Si vous parvenez à atteindre le roisorcier, le Loup Volant disparaît et votre ennemi se prépare pour l'assaut final. Rendez-vous au <L to='255'>255</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P304);
