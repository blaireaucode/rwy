/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P327 extends Component {
    render() {
        return (
            <div>

               Le pain est dur et sans goût, mais nourrissant. Vous l'avalez avec gourmandise jusqu'à ce qu'il n'en reste plus que quelques miettes au creux de votre paume. Il se passe alors un phénomène étonnant les miettes gonflent jusqu'à reconstituer un nouveau quignon ! Tant que ce pain magique sera en votre possession, vous n'aurez plus à souffrir de la faim. Chaque fois que le texte vous autorisera à vous restaurer, vous n'aurez plus à payer pour votre nourriture ni à utiliser vos rations. Retournez au paragraphe que vous avez quitté.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P327);
