/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P185 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous faites le vide dans votre esprit, oubliant la gravité de la situation, et atteignez un état de complète sérénité. Vous ouvrez calmement les yeux : vous flottez dans les airs. Le torrent est maintenant loin derrière vous. Lentement, vous reprenez pied sur le pont. Rendez-vous au <L to='430'>430</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P185);
