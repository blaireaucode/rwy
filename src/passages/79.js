/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P79 extends Component {
    render() {
        return (
            <div>

               La marionnettiste vous accueille d'un sourire figé. Décontenancé par son attitude, vous bafouillez « La représentation est-elle terminée ? » La vieille femme emballe ses poupées sans cesser un instant de sourire. « Oh, non ! réplique-t-elle. Cela ne fait que commencer... Je joue aussi à midi et le soir. » Elle a rassemblé son attirail et vous l'aidez à placer sa hotte sur ses épaules. Bientôt, elle s'éloigne d'une démarche sautillante, semblable à celle de ses poupées. Vous la suivez du regard, perplexe, avant de repartir au hasard des rues... Rendez-vous au <L to='306'>306</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P79);
