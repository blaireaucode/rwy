/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P191 extends Component {
    render() {
        return (
            <div>

               Vous sortez l'épée de son fourreau. Elle appartenait au dieu Log et, selon les légendes mercaniennes, Log engendra sept monstres, dont l'un d'eux était Jormungand le Serpent de Mer. La lame transparente émet soudain une vibration mélodieuse qui se répercute à la surface des flots ... Rendez-vous au <L to='510'>510</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P191);
