/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P168 extends Component {
    render() {
        return (
            <div>

               Le Château de la Nuit Éternelle se dresse, sombre architecture massive plantée au centre du lac. Trois excroissances s'en échappent et enjambent les eaux grises. Ces ponts couverts abritent un dédale de pièces obscures. Trois arches de pierre aux linteaux sculptés se dressent sur la rive. Vous reconnaissez les portes de la Confusion, du Carnage et de la Peur. Soudain, le sol tremble et un grondement sourd s'échappe de derrière un rocher. Une créature monstrueuse apparaît, dressée sur ses pattes arrière. Les écailles qui la recouvrent sont de la taille d'un bouclier, ses griffes ressemblent à des faux. Chacun de ses pas fait fondre la neige et laisse des empreintes de terre carbonisée. Gristun, le gardien du château, a repéré des intrus  . GRISTUN (Protection 3, Dommages 3 dés + 1) FORCE : 8 HABILETÉ : 6 POUVOIR : 8 ENDURANCE: 60 Si vous parvenez à forcer le passage, rendez-vous au <L to='432'>432</L>. Si vous tuez ce gardien, rendez-vous au <L to='10'>10</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P168);
