/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P311 extends Component {
    render() {
        return (
            <div>

               Vous vous approchez du trône en prenant garde à la poussière grise qui recouvre le sol. De par ses dimensions, il ferait une excellente cachette pour un trésor. Vous découvrez bientôt un petit levier dissimulé à sa base. Vous actionnez le mécanisme et le trône pivote, révélant un escalier qui s'enfonce dans le sol. Vous allumez une torche et entamez la descente. Les parois du couloir sont recouvertes de fresques à la gloire d'entités démoniaques. L'escalier se coude à plusieurs reprises et débouche finalement devant deux portes. Une petite créature aux traits de batracien en garde l'accès. Si vous désirez attaquer la créature, rendez-vous au <L to='69'>69</L>. Si vous préférez lui parler, rendez-vous au <L to='139'>139</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P311);
