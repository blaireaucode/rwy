/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P200 extends Component {
    render() {
        return (
            <div>

               Vous poussez la porte de droite. Au même instant, une force surnaturelle vous entraîne à l'intérieur de la pièce ! La porte se referme avec fracas et disparaît, cédant la place à un mur compact ! Vous observez les lieux, sur vos gardes. Trois cassettes sont alignées sur le sol. La première est en bois, la deuxième en pierre et la troisième en fer. Le couvercle de chacune porte une inscription. Sur la première, on lit: « La clef de la sortie se trouve dans la cassette de pierre. » Sur la cassette de pierre est inscrit : « La clef n'est pas dans la cassette qui porte une affirmation vraie. » Sur la cassette de fer, vous lisez : « Seule une de ces trois affirmations est vraie. » Si vous désirez ouvrir la cassette de bois, rendez-vous au <L to='154'>154</L>. Si vous préférez la cassette de pierre, rendez-vous au <L to='511'>511</L>. Si vous optez pour celle de métal, rendez-vous au <L to='475'>475</L>. Si vous ne voulez en ouvrir aucune, rendez-vous au <L to='280'>280</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P200);
