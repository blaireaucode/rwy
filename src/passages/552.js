/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P552 extends Component {
    render() {
        return (
            <div>

               La garde vous entraîne sans ménagement. Le récit de votre crime court de bouche en bouche et la foule manifeste son hostilité. Les badauds vous giflent au passage et vous crachent dessus ! Une pierre vous atteint à la tempe ! Un vent de folie souffle sur la populace. Les gardes s'écartent avec prudence ; après tout, votre crime doit être puni ! Les pierres volent de toutes parts, la foule vous lapide ! Bientôt, incapable de résister, vous succombez sous leurs coups répétés... Votre aventure est terminée. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P552);
