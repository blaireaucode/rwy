/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P358 extends Component {
    render() {
        return (
            <div>

               A votre approche, le visage des marchands s'éclaire d'une lueur cupide et tous déballent leurs marchandises. Un trappeur vous présente une fourrure immaculée. « Regardez ! Du Tigre des Neiges ! Il n'y a pas de meilleure protection contre le froid ! » Un marchand vous présente un petit brasero qu'il tient au creux de sa paume. « L'hiver sera rude, croyez-moi. Vous aurez bien besoin de vous réchauffer les mains ! » Après une courte période de marchandage, vous tombez d'accord sur les prix de quelques articles : des capes de fourrure pour 10 Pièces d'Or chacune ; des rations de survie (pour une semaine) pour 2 Pièces d'Or; des gants à 2 Pièces d'Or la paire ; des sacs de couchage à 5 Pièces d'Or ; un brasero et son combustible pour 7 Pièces d'Or. Achetez ce que vous voulez et rayez les sommes correspondantes de votre Feuille d'Aventure. Si vous désirez continuer à discuter avec les marchands, rendez-vous au <L to='234'>234</L>. Si ce n'est déjà fait, vous pouvez aborder les prêtres (rendez-vous au <L to='83'>83</L>) ou bien appeler l'aubergiste (rendez-vous au <L to='515'>515</L>). Si vous préférez quitter l'auberge, rendez-vous au <L to='306'>306</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P358);
