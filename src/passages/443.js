/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P443 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Hélas, vos pensées sont trop ancrées dans la réalité et vous ne parvenez pas à vous abstraire du monde extérieur ! La vague de sang vous submerge et vous emporte au fond du gouffre... Les joueurs survivants se rendent au <L to='430'>430</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P443);
