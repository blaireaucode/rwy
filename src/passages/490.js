/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P490 extends Component {
    render() {
        return (
            <div>

               Leurs regards s'illuminent à la vue de la poche de Tahac. Toute idée de violence semble avoir disparu. Les marins abandonnent leurs armes et chacun sort sa pipe. Vous distribuez le Tahac à la ronde. Les hommes s'assoient sur le pont et tirent de longues bouffées avec des airs satisfaits. Les effets de la drogue ne tardent à se faire sentir et les marins sombrent dans un état de stupeur, ponctué par instants de rires imbéciles. Quelle riche idée vous avez eue d'acquérir ce produit ! Vous repérez les marins qui avaient mené la révolte et, profitant de leur inertie, vous les guidez vers l'arrière du bateau. Là, vous les poussez par-dessus bord ! Une heure plus tard, quand les effets se sont dissipés, les marins constatent que leurs meneurs ont disparu ! Il leur faudra travailler dur pour compenser le manque de bras. Vous leur intimez l'ordre de regagner leurs postes. Les hommes vous obéissent, la tête encore embrumée de vapeurs narcotiques. Rendez-vous au <L to='499'>499</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P490);
