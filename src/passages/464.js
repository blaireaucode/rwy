/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P464 extends Component {
    render() {
        return (
            <div>

               Vous retournez une des créatures mortes. Son corps a la consistance et l'aspect de la glace. Un liquide transparent suinte des blessures et se mêle à la neige. Il est hors de question d'établir votre campement au milieu de ce carnage ! Vous rassemblez vos affaires, éparpillées au cours du combat, et reprenez votre route. Le silence obsédant laisse présager de nouveaux dangers. Des grognements s'élèvent sur votre gauche. D'autres Chiens de Givre se sont lancés à votre poursuite. Rendez-vous au <L to='447'>447</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P464);
