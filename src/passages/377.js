/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P377 extends Component {
    render() {
        return (
            <div>

               Vous traversez la pièce en vous courbant pour éviter les tirs qui pourraient venir des meurtrières. Vous allez atteindre la porte quand le sol se dérobe sous vos pieds ! Si vous êtes en groupe, les deux premiers joueurs tombent dans une trappe. Si tous les joueurs sont tombés dans le piège, rendez-vous au <L to='213'>213</L>. Si certains sont encore dans la pièce, rendez-vous au <L to='39'>39</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P377);
