/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P388 extends Component {
    render() {
        return (
            <div>

               Ceci ne concerne que le joueur qui monte la garde. Vous scrutez le sous-bois, mais ne remarquez plus aucun mouvement. Haussant les épaules, vous retournez vers l'arbre où est dressé votre bivouac. A cet instant, vous sentez un souffle glacial sur votre nuque! Des doigts squelettiques vous compriment la gorge et étouffent votre dernier cri... Vous êtes mort. Passez le livre aux autres joueurs et dites-leur de se rendre au <L to='67'>67</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P388);
