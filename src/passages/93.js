/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P93 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Grâce à votre sortilège de Téléportation, vous et vos compagnons pourrez explorer les étages de la tour. Rendez-vous au <L to='470'>470</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P93);
