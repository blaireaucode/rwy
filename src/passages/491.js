/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P491 extends Component {
    render() {
        return (
            <div>

               PRÊTRE et VOLEUR : Vous disposez de deux Assauts pour décocher une flèche avant que la créature n'arrive à votre contact. Lancez deux dés. Le Squelette possède 30 points d'ENDURANCE et sa valeur de Protection est de 1. Dans deux Assauts, il sera sur vous. Rendez-vous au <L to='276'>276</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P491);
