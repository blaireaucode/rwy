/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P288 extends Component {
    render() {
        return (
            <div>

               Vous pressez le pas vers la plus proche boutique. Par chance, la lanterne est encore allumée. Vous pénétrez dans l'échoppe en secouant la neige qui recouvre votre cape. Le marchand, emmitouflé dans une épaisse cape de laine, se tient près de l'âtre. « J'allais fermer, vous dit-il. Mais le client est roi... » Vous repérez un certain nombre d'articles nécessaires à votre périple : des capes de fourrure pour 14 Pièces d'Or chacune ; des rations de survie (pour une semaine) pour 2 Pièces d'Or ; des gants à 2 Pièces d'Or la paire ; des sacs de couchage à 6 Pièces d'Or; un brasero et son combustible pour 10 Pièces d'Or. Achetez ce que vous voulez et rayez les sommes correspondantes de votre Feuille d'Aventure. Le marchand referme la porte derrière vous, tandis que vous vous dirigez vers l'auberge. Rendez-vous au <L to='75'>75</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P288);
