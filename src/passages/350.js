/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P350 extends Component {
    render() {
        return (
            <div>

               Comme vous traversez le pont, un nuage de fumée noire s'élève en tourbillonnant des profondeurs du gouffre. Le pont s'incline. Aussitôt, vous foncez en direction des portes de la tour, mais elles s'ouvrent avec fracas, libérant un torrent de sang et d'os mêlés ! Cette vague horrible menace de vous submerger ! Chaque joueur doit affronter seul ce péril, suivant l'ordre de marche : le magicien (rendez-vous au <L to='186'>186</L>), le voleur (rendez-vous au <L to='409'>409</L>), le prêtre (rendez-vous au <L to='561'>561</L>) et le chevalier (rendez-vous au <L to='74'>74</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P350);
