/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P97 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Votre vision normale ne vous est d'aucun secours en cette circonstance. En parant la première attaque avec votre bâton, vous comprenez la justesse de votre décision.  SYLPHE (Dommages 1 dé + 3) FORCE : 7 HABILETÉ : POUVOIR : 8 ENDURANCE :  8 14  Un des Sylphes vous empêche de progresser le long du pont. Si vous abandonnez le combat, retournez au <L to='226'>226</L>. Si vous les éliminez tous les deux, rendez-vous au <L to='350'>350</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P97);
