Glossaire
La Lune Bleue: Cette étoile renferme l'esprit d'un des cinq archimages. En
astrologie, elle représente le mystère, l'illusion et la frontière qui sépare la
vie de la mort.
L'anéantissement: Ce désastre vit la destruction de la Citadelle de Spyte et la
fin du règne des archimages. Depuis ce cataclysme, l'ancienne forteresse est
isolée au centre d'un gouffre sans fond.
La Mer Coradienne: Cette mer est bordée par les principaux pays civilisés:
l'Algandie, la Chaubrie, la Courlande, le nouvel empire de Sélentine,
l'Asmulie et Emphidor.
Ferromaine: Cette cité portuaire est la plus prospère de la Mer Coradienne.
Krarth: Cette contrée désolée, située à l'extrême nord des Terres de
Légende, est séparée en douze provinces, chacune contrôlée par un mage.
Krarth est séparée du monde civilisé par la Mer Coradienne et rares sont
ceux qui s'aventurent sur cette terre inhospitalière.
L'Étoile des Dons: Elle renferme l'esprit d'un des cinq archimages. En
astrologie, elle représente la chance (bonne ou mauvaise) et l'avenir.
Les mages: Ils sont les seigneurs de Krarth. Au nombre de treize, ils
règnent en despotes sur leurs provinces et règlent leurs différends grâce à
l'Épreuve du Labyrinthe.
La Mort Rouge: Elle renferme l'esprit d'un des cinq archimages. Cette
étoile est liée aux désastres, mais aussi aux conflits individuels.
L'Étoile des Pestiférés: Elle renferme l'esprit d'un des cinq archimages. En
astrologie, elle est liée à la maladie et à toute forme de corruption.
Le Selentium: Selentium était autrefois la capitale de l'ancien empire de
Sélentine, qui couvrait la plupart
des territoires connus. Depuis la chute de l'empire, voilà sept siècles,
Selentium a acquis une nouvelle importance en devenant le centre de la vraie
foi.
Les Fantômes des archimages: Ces objets lumineux sont visibles dans le
ciel de Krarth. Appelés à tort étoiles, ils sont d'une taille légèrement
inférieure à celle du disque lunaire. De nombreuses superstitions s'y
rattachent. Les populations pensent qu'elles renferment l'esprit de cinq
archimages qui auraient survécu sous cette forme à l'anéantissement. Ces
derniers comploteraient leur retour sur terre pour y semer la terreur.

Spyte : Les archimages se réunissaient dans cette citadelle tous les sept ans
afin de communier avec leurs dieux et de ressourcer leurs pouvoirs. Il n'en
reste plus désormais que des ruines, isolées au centre du Chaudron.
La vraie foi: C'est la religion principale du monde de légende.
Les archimages: Ces puissants sorciers qui régnaient autrefois sur Krarth
disposaient de pouvoirs inimaginables. Tous ont péri lors de
l'anéantissement de Spyte. Les mages actuels ne sont pour la plupart que
les descendants des apprentis ou des intendants des archimages, qui ont pris
le pouvoir à la faveur de la confusion.
Le roi-sorcier: Il règne depuis des siècles sur les territoires de Wyrd et possède
le pouvoir de transformer la réalité. Il peut visiter ses sujets pendant leurs rêves
afin de les questionner ou de les punir.
L'Albane: Elle renferme l'esprit d'un des cinq archimages. En astrologie, elle
représente la connaissance et la pensée positive.
Le Royaume de Wyrd: Cette île, située au nord-est de Krarth et baignée par la
Mer de Mistral, est gouvernée par le roi-sorcier. Ce dernier était le vassal des
archimages jusqu'au jour où l'anéantissement a mis un terme à leur règne.


