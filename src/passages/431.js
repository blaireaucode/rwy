/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P431 extends Component {
    render() {
        return (
            <div>

               Vous atterrissez lourdement dans un tas de fumier. Chaque joueur perd 1 dé d'ENDURANCE (1 dé - 1 pour ceux qui ont tenté de s'agripper aux parois du puits). L'armure assure la protection habituelle. Vous venez de déboucher dans une vaste salle de bois aux parois illuminées de rouge comme si elles allaient s'embraser d'un instant à l'autre. Des barbares à la barbe rousse et au teint vermillon sont attablés et festoient bruyamment. Une silhouette se détache devant l'âtre au fond de la pièce. Le barbare doit bien mesurer deux mètres cinquante et sa hache est tout aussi énorme. Vous remarquez un coffre non loin de lui. Le géant vous aperçoit et vous interpelle aussitôt « Je suis Bors ! dit-il d'une voix tonnante. Je suis le chef ici ! » Vous comprenez à ses intonations qu'il cherche la bagarre... Si vous désirez combattre, rendez-vous au <L to='92'>92</L>. Si vous refusez l'affrontement, rendez-vous au <L to='416'>416</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P431);
