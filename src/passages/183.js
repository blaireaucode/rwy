/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P183 extends Component {
    render() {
        return (
            <div>

               La dalle descend à une vitesse ahurissante et s'arrête quelques secondes plus tard dans une pièce obscure. Une grille qui s'ouvre sur le mur nord donne sur une cour intérieure. Un Démon ailé, juché sur un piédestal, scrute, attentif, les moindres recoins. Une porte, à l'extrémité de la cour, est la seule issue possible, aussi poussez-vous la grille et avancez-vous lentement, le regard fixé sur la créature. Rendez-vous au <L to='249'>249</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P183);
