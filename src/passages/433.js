/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P433 extends Component {
    render() {
        return (
            <div>

               Comme vous inspectez les lieux, six flèches s'envolent en sifflant des meurtrières ! Chaque joueur est touché par une flèche (déterminez au hasard les victimes des flèches en surnombre). Chaque flèche inflige un Dommage de 1 dé d'ENDURANCE (moins la Protection). Si vous survivez, vous foncez en direction de la porte. Rendez-vous au <L to='377'>377</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P433);
