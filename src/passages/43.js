/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P43 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous tentez de faire abstraction des étranges sensations que vous avez perçues, mais cela a pour effet de briser votre concentration. Vous reprenez pied dans la réalité. Rendez-vous au <L to='256'>256</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P43);
