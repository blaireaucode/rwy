/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P112 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Mû par une soudaine inspiration, vous enlevez votre cape. Vous la passez autour de la balustrade et, la tenant par ses extrémités, vous vous élancez dans le vide, à l'instant où la vague allait vous atteindre ! Le flot de sang passe au-dessus de votre tête et vous éclabousse mais, quelques secondes plus tard, vous vous hissez à nouveau sur le pont. Rendez-vous au <L to='430'>430</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P112);
