/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P562 extends Component {
    render() {
        return (
            <div>

               Le cadavre renaît à la lueur des lanternes. Soudain, votre Double se lève tel un automate et s'avance lentement vers vous en brandissant la lame sacrificielle qu'on avait déposée sur sa poitrine. Il se campe devant vous, le regard vide, prêt à abaisser son arme. Si vous désirez brandir la vôtre pour parer le coup, rendez-vous au <L to='322'>322</L>. Si vous préférez demeurer impassible, rendez-vous au <L to='550'>550</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P562);
