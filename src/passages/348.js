/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P348 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous ne pouvez percevoir que l'état d'esprit des joueurs, mais cela suffit pour comprendre qu'ils ne jouent pas aux échecs. Les deux hommes déplacent les pièces de manière à former des symboles sur l'échiquier. Les marchands se concentrent sur eux comme s'ils en cherchaient la signification. Rendez-vous au <L to='170'>170</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P348);
