/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P232 extends Component {
    render() {
        return (
            <div>

               Le marin vous guide par les rues bondées, se frayant un chemin à grands coups de coude. Vous atteignez bientôt le port. Le marin se dirige vers deux grands navires de pêche et, sans vous prévenir, monte sur la passerelle du premier. Si vous désirez le suivre à bord, rendez-vous au <L to='417'>417</L>. Si vous préférez lui fausser compagnie et vous approcher d'une petite galère marchande, nommée la Magdalena, rendez-vous au <L to='326'>326</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P232);
