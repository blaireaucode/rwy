/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P178 extends Component {
    render() {
        return (
            <div>

               Si vous avez les objets qui suivent, reportez-vous au paragraphe approprié. Inspectez les objets dans cet ordre : le coeur des Ténèbres (rendez-vous au <L to='405'>405</L>), la corne à boire (rendez-vous au <L to='31'>31</L>), la flèche blanche (rendez-vous au <L to='290'>290</L>), les dents de l'Hydre (rendez-vous au <L to='487'>487</L>), l'encensoir (rendez-vous au <L to='556'>556</L>) et les pièces d'échec du seigneur elfe (rendez-vous au <L to='292'>292</L>). Lorsque vous aurez pris connaissance des effets des objets que vous possédez, rendez-vous au <L to='21'>21</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P178);
