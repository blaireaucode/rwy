/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P343 extends Component {
    render() {
        return (
            <div>

               La vieille femme vous observe d'un air mystérieux. « Ce n'est pas la somme convenue », murmure-t-elle. « Estime-toi heureuse, répliquez-vous. Tu aurais très bien pu ne rien recevoir. » La vieille rassemble son paquetage en grommelant et part vers les arbres qui bordent la clairière. Vous la regardez s'éloigner, envahi par une sensation de malaise. Haussant les épaules, vous retournez vers le campement. Rendez-vous au <L to='126'>126</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P343);
