/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P38 extends Component {
    render() {
        return (
            <div>

               A l'instant où vous brandissez le crucifix, le Vampire pousse un hurlement de rage et de crainte mêlées. Il tombe à genoux, le corps raidi, et vous implore du regard. « Mon ami... Non ! » dit une voix curieusement familière. Mais votre ami n'est plus, vous le savez. La créature diabolique tente un dernier subterfuge. Vous pressez le crucifix contre son front écarlate, arrachant au Vampire une plainte qui vous fait frémir ! Avec la mort, votre ancien compagnon a repris son apparence première. Vous enterrez sa dépouille et recouvrez sa tombe de pierres (vous pouvez, si vous le désirez, prendre ses possessions). Vous vous asseyez en silence près de sa sépulture, attendant que l'aube se lève. Vous n'avez guère le coeur à vous restaurer, mais si vous ne mangez pas, vous perdez 1 point d'ENDURANCE. Rendez-vous au <L to='422'>422</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P38);
