/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P253 extends Component {
    render() {
        return (
            <div>

               Surpris, les autres gardes reculent d'un pas. Vous profitez de ce répit pour leur arracher vos armes des mains et prendre la fuite ! Hélas, votre course est ralentie par les badauds et, bientôt, les cris de la garde s'élèvent dans votre dos. Vous vous engagez dans une ruelle dans l'espoir de les semer, mais vous débouchez dans un cul-de-sac ! La garde s'arrête à l'entrée de la ruelle, tandis que vous adoptez une position de combat. A cet instant, un homme vêtu d'une robe violette surgit de l'ombre d'une porte cochère. Il évalue la situation d'un coup d'œil et déroule un tapis sur le sol. « Vite ! intime-t-il. Grimpez sur le tapis ! Je suis Guillaume de Vanterie, je vous expliquerai plus tard ! » D'autres gardes sont arrivés en renfort. Sûrs de leur supériorité, ils s'avancent dans l'impasse. Pris de court, vous obéissez à l'inconnu. Celui-ci lance un ordre magique et le tapis s'élève dans les airs sous le regard stupéfait des soldats ! Guillaume de Vanterie caresse la petite amulette qu'il porte autour du cou et le tapis s'éloigne, survolant les toits. Rendez-vous au <L to='424'>424</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P253);
