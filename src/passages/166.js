/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P166 extends Component {
    render() {
        return (
            <div>

               L'inconnu vous guide jusqu'à une échoppe en plein air et vous offre une tasse de thé vert. « Explique-toi ! » intimez-vous en grimaçant car le breuvage est amer. L'homme secoue la tête avec un étrange sourire. « Une démonstration serait plus appropriée... » Il frappe dans ses mains et,  brusquement, vous vous élevez dans les airs, porté par le tapis qui recouvrait le sol ! Vous survolez le port et la foule ébahie qui vous montre du doigt. Vous vous accroupissez, craignant une chute. Auguste s'esclaffe : « N'ayez pas peur, je n'ai jamais perdu de passager. » Rendez-vous au <L to='424'>424</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P166);
