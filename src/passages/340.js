/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P340 extends Component {
    render() {
        return (
            <div>

               Vous égrenez quelques notes sur la lyre et les tentacules s'immobilisent. Votre mélodie se perd bientôt dans le vent... Les tentacules frappent alors avec une force incroyable et chaque joueur perd 2 dés d'ENDURANCE ! A demi assommé, vous vacillez un instant avant de songer à la riposte. La lyre gît en miettes à vos pieds. Rendez-vous au <L to='457'>457</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P340);
