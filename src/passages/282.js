/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P282 extends Component {
    render() {
        return (
            <div>

               Allez-vous employer la cloche de fer (rendez-vous au <L to='460'>460</L>), un anneau d'or (rendez-vous au <L to='266'>266</L>), ou une broche en forme de tête de loup (rendez-vous au <L to='503'>503</L>) ? Si vous ne possédez aucun de ces objets, vous n'avez d'autre choix que d'attaquer (rendez-vous au <L to='530'>530</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P282);
