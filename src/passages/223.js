/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P223 extends Component {
    render() {
        return (
            <div>

               La créature s'écroule sur le sol. Un fragment de flèche blanche dépasse de son épaisse fourrure. Vous pouvez le prendre si vous le désirez. Rendez-vous au <L to='219'>219</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P223);
