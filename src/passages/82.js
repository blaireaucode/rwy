/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P82 extends Component {
    render() {
        return (
            <div>

               MAGICIEN: Les Sylphes s'apprêtent à vous attaquer (rendez-vous au <L to='100'>100</L>). Votre sortilège portera ses fruits dès l'instant que vous l'aurez jeté.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P82);
