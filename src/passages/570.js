/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P570 extends Component {
    render() {
        return (
            <div>

               Vous retournez vers le sud, et dans chaque village que vous traversez la même histoire se répète. Le peuple de Wyrd semble s'être réveillé d'un cauchemar qui durait depuis des siècles. Le territoire hostile s'est transformé en contrée verdoyante. Les Armingiens et les Solons ont abandonné les emblèmes du pouvoir qui les liait au roi-sorcier et ont rejoint la population pour participer à la reconstruction du pays. Tout au long de votre route, vous croisez des visages souriants. Quelques jours plus tard, vous êtes assis près d'un feu, dans une clairière. La population célèbre le solstice en un rituel destiné à apaiser les dieux de l'hiver. Cette cérémonie morose et frappée du sceau de la crainte est devenue l'occasion de réjouissances par tout le pays. Le climat s'est adouci et, malgré le soir, la température est supportable. Vous vous souvenez de la clairière dans laquelle toute cette aventure a commencé. Maintenant, le fourreau et le pommeau de l'Épée de Légende sont réunis, mais votre quête n'est pas achevée. Il vous faut encore trouver la lame... Quels dangers devrez-vous braver pour vous en rendre maître ? Vous le découvrirez dans le troisième volume de la série L'Épée de Légende.  

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P570);
