/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P461 extends Component {
    render() {
        return (
            <div>

               Vous murmurez la formule qui active la sphère et vous la jetez en direction du groupe. Quatre longs rubans de flammes en jaillissent, allumant instantanément les lanternes. L'explosion qui s'ensuit provoque la panique des porteurs, qui s'enfuient en hurlant de terreur. Rendez-vous au <L to='197'>197</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P461);
