/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P65 extends Component {
    render() {
        return (
            <div>

               Quelques heures se sont écoulées et vous survolez une vaste étendue glacée. Vous apercevez à l'horizon un gigantesque pic rocheux, surmonté d'une forteresse immaculée. Auguste murmure quelques paroles magiques et le tapis descend vers le château. Le mage se tourne vers vous. « Assez plaisanté, dit-il d'une voix sifflante. Je suis le serviteur de l'archimage Uru qui vit maintenant dans le ciel sous la forme de l'Albane. Le fourreau que vous avez est sa propriété. Vous allez me le remettre. Si vous refusez... » Auguste vous désigne d'un air menaçant la couche de glace, à plusieurs dizaines de mètres en contrebas. Si vous lui remettez le fourreau, rendez-vous au <L to='419'>419</L>. Si vous refusez, malgré ses menaces, rendez-vous au <L to='504'>504</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P65);
