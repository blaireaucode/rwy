/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P501 extends Component {
    render() {
        return (
            <div>

               La nuit venant, une chape de ténèbres tombe sur la forêt, mais la neige semble émettre sa propre lumière. Les branches épineuses forment une voûte impénétrable. Vos pas crissent dans la neige. Le vent est tombé, rendant le froid plus supportable. Vous songez à établir un bivouac quand un bruit régulier parvient à vos oreilles. Si vous décidez de vous arrêter pour la nuit, rendez-vous au <L to='233'>233</L>. Si vous préférez poursuivre votre route, rendez-vous au <L to='338'>338</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P501);
