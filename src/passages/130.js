/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P130 extends Component {
    render() {
        return (
            <div>

               CHEVALIER: Cette hache est une arme redoutable. En combattant avec elle, vous gagnez 1 point de FORCE (si vous possédez déjà une arme surnaturelle, il vous faut choisir de combattre avec l'une ou avec l'autre). Vous gagnez aussi 1 point d'impact. Il vous est permis de l'utiliser comme hache de lancer. La procédure est identique au tir des voleurs et des prêtres, mais les dommages que provoque la hache se calculent avec 1 dé + 1. Vous pouvez lancer la hache sur une cible distante de trois cases. Elle revient à vous après que vous l'avez lancée. La hache n'est utilisable que par vous seul. Si d'aventure vous veniez à mourir, le prochain qui s'en emparera deviendra son unique possesseur (le nouveau propriétaire de la hache doit se rendre au <L to='400'>400</L> pour en connaître les pouvoirs). Rendez-vous maintenant au <L to='226'>226</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P130);
