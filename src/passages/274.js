/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P274 extends Component {
    render() {
        return (
            <div>

               Le monstre sera sur vous dans trois Assauts. Pendant ce temps, le prêtre peut faire une tentative d'Exorcisme. Il doit, pour réussir, obtenir un résultat supérieur ou égal à 10 avec deux dés. Le magicien peut tenter de Téléporter tout le groupe sur la berge. Le voleur ou le prêtre (s'il n'exorcise pas la créature) peut décocher des flèches. Le Squelette a une Protection de 2. Si vous parvenez à l'Exorciser, rendez-vous au <L to='129'>129</L>. Si vous êtes Téléporté sur la rive, rendez-vous au <L to='308'>308</L>. Si rien n'a abouti en trois Assauts, rendez-vous au <L to='122'>122</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P274);
