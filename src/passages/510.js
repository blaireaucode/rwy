/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P510 extends Component {
    render() {
        return (
            <div>

               La mer se met à bouillonner et une forme grise apparaît à la surface, provoquant une vague qui manque de renverser le navire ! La forme disparaît un instant pour rejaillir de l'autre côté du Lacodon ! Une face monstrueuse se dresse, la gueule béante, libérant des torrents d'eau de mer ! L'équipage, pétrifié d'horreur, ne songe pas à réagir. Jormungur le Serpent de Mer pousse un grondement terrifiant ! Une masse noire s'abat sur le navire. Vous vous évanouissez... Lorsque vous reprenez connaissance, vous êtes agrippé à une épave. Vous avez survécu par miracle, mais il semble qu'aucun membre de l'équipage n'ait réchappé du naufrage. Vous vous cramponnez à votre planche en grelottant. Le contact prolongé des eaux glaciales vous fait perdre 1 dé + 1 d'ENDURANCE. Quelques heures plus tard, les survivants aperçoivent une voile à l'horizon. Heureusement, le navire vous a repéré et, bientôt, des mains secourables vous hissent à bord. Emmitouflé dans des couvertures, vous faites le récit de vos mésaventures au capitaine. « Vous avez de la chance dans votre malheur, dit-il en soupirant. Nous devrions atteindre Dourhaven à la tombée de la nuit. Vous parviendrez finalement à bon port... » En effet, le soir même, le navire arrive à destination. Malgré l'heure tardive, le port grouille d'activité. Vous débarquez alors que la neige se met à tomber et regardez autour de vous. Devant votre hésitation, le capitaine vous rejoint. « Toutes les échoppes que vous voyez ici pourront vous fournir l'équipement nécessaire à votre voyage. Si vous cherchez une auberge, je vous conseille l'Ours Blanc, au bout de la rue. Elle est tenue par un ancien marchand mercanien de mes amis. » Vous remerciez le capitaine de ces renseignements. Si vous désirez entrer dans une boutique, rendez-vous au <L to='288'>288</L>. Si vous préférez vous diriger sans tarder vers l'auberge, rendez-vous au <L to='75'>75</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P510);
