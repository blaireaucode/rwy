/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P15 extends Component {
    render() {
        return (
            <div>

               Vous vous retrouvez suspendu à un filin métallique. Un vent glacial vous fouette le visage. Vous regardez tout autour de vous. Une gigantesque toile d'araignée, suspendue en plein ciel, surplombe un tapis de nuages. Le roisorcier est assis au centre de la toile, sur un trône transparent. Il porte encore sa couronne de cristal et, pendu autour du cou, à la manière d'une amulette, l'objet pour lequel vous avez bravé tant de périls... Remarquant vos regards posés sur le pommeau, le visage parcheminé du roi-sorcier s'éclaire de malice. « Bel objet, n'est-ce pas ? ironise-t-il. Et qui plus est, un précieux talisman qui assure ma protection contre les archimages. Vous comprendrez que je ne veuille pas m'en séparer... Vous voilà en fâcheuse posture, remettez-moi le fourreau et je pourrai peut-être y remédier... » Vous n'avez aucune envie de vous en séparer maintenant ! Si vous désirez le menacer de laisser choir le fourreau, rendez-vous au <L to='307'>307</L>. Si vous préférez recourir à un objet, rendez-vous au <L to='282'>282</L>. Vous pouvez aussi tenter de gravir la distance qui vous sépare du roi-sorcier pour l'attaquer (rendez-vous au <L to='530'>530</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P15);
