/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P4 extends Component {
    render() {
        return (
            <div>

               Vous vous levez une heure avant l'aube et, après avoir rassemblé votre paquetage, vous vous dirigez vers les docks. Une activité intense règne déjà sur le pont de la Magdalena. Le capitaine vous accueille chaleureusement et vous présente à son lieutenant. Quelques minutes plus tard, il vous guide vers le pont inférieur et vous montre la meilleure façon de ramer. Heureusement, le vent du sud s'est levé et les rames ne serviront qu'à sortir la galère du port. Bientôt, le navire prend la mer. Vous ramez de toutes vos forces, au rythme d'une chanson entonnée par les autres marins. Dès l'instant où vous quittez le port, le roulis des vagues commence à avoir des effets sur votre estomac... Certains joueurs peuvent être sujets au mal de mer, à l'exception des prêtres, grâce à leur capacité de contrôle mental et physique. Chaque joueur doit jeter deux dés. Ajoutez 1 au résultat si vous avez déjeuné avant d'embarquer. Si vous faites 10, vous perdez 2 points d'ENDURANCE (si votre total n'est plus que de 2 points, retranchez seulement 1 point le mal de mer n'est pas mortel... !). La journée se termine sans autres désagréments et, à l'heure du crépuscule, la galère mouille l'ancre dans une petite baie. Rendez-vous au <L to='507'>507</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P4);
