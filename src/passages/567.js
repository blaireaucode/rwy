/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P567 extends Component {
    render() {
        return (
            <div>

               Le personnage qui ouvrait la procession se dirige vers l'autel, ouvre une cassette et en sort une amulette en forme de tête de mort. Les autres silhouettes baissent la tête avec déférence et entament un chant funèbre. Les accents de cette litanie vous arrachent un frisson. Le maître de cérémonie place l'amulette autour du cou de votre compagnon et s'empare de l'objet qu'il avait déposé sur son torse. Ce que vous aviez pris pour un symbole n'est autre qu'une lame sacrificielle dotée de deux poignées recourbées. L'homme brandit l'objet au-dessus de sa tête. Le rythme de la litanie s'accélère et atteint son paroxysme à l'instant où la lame plonge dans le corps de votre compagnon. Un silence obsédant retombe sur la pièce. Soudain, votre ami se redresse et se lève ! Son regard vide fixe le néant. Il est devenu un Zombie sans âme. Si vous désirez quitter le sanctuaire, rendez-vous au <L to='152'>152</L>. Si vous voulez passer à l'attaque, rendez-vous au <L to='13'>13</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P567);
