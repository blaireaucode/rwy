/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P391 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Grâce à votre Vision Totale, vous pouvez anticiper tous les mouvements des dalles, mais il vous faut encore esquiver les Crânes qui foncent sur vous, cherchant à provoquer votre chute. Pour traverser sans encombre, vous devez obtenir un résultat inférieur ou égal à votre total d'HABILETÉ avec 2 dés - 1. Si vous échouez, hélas, vous chutez dans le vide et vous vous écrasez au sol!... Si d'autres joueurs doivent encore franchir le pont, rendez-vous au <L to='240'>240</L>. Si tous les survivants l'ont franchi, rendez-vous au <L to='35'>35</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P391);
