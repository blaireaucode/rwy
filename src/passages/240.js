/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P240 extends Component {
    render() {
        return (
            <div>

               Dès que vous vous engagez sur le pont d'énergie, un sifflement retentit dans la cour et, aussitôt, des Crânes Étincelants jaillissent des meurtrières ! Ils s'envolent en faisant claquer leurs mâchoires de cristal. Traversez le pont, chacun votre tour selon l'ordre de marche : le magicien (rendez-vous au <L to='407'>407</L>), le voleur (rendez-vous au <L to='182'>182</L>), le prêtre (rendez-vous au <L to='467'>467</L>) et le chevalier (rendez-vous au <L to='107'>107</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P240);
