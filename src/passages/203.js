/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P203 extends Component {
    render() {
        return (
            <div>

               VOLEUR : « J'aimerais t'accompagner, dites-vous, mais hélas, je ne possède pas d'instrument de musique!... » D'un geste de la tête, il vous désigne une lyre, accrochée à la selle de sa mule, à quelques pas de là. Vous allez prendre l'instrument, fabriqué dans un étrange bois noir. Vous égrenez quelques notes et entamez bientôt une captivante mélodie. Jamais vous n'avez joué aussi bien. Le musicien renonce à vous accompagner, impressionné par votre talent. Quelques minutes plus tard, comme vous vous arrêtez, un silence étonné tombe sur la clairière. Le musicien, ému, lève vers vous un regard humide. « Tu possèdes le don, dit-il. Conserve cette lyre. Elle sera ta compagne et saura éloigner les mauvais esprits de la nuit, qui cherchent à s'emparer de nos âmes... » Vous alliez le remercier, mais le musicien ne vous entend plus. Son regard vide contemple la cime des arbres. Si vous désirez retourner à votre bivouac, rendez-vous au <L to='486'>486</L>. Si vous préférez aller observer la partie d'échecs, rendez-vous au <L to='554'>554</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P203);
