/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P441 extends Component {
    render() {
        return (
            <div>

               Vous interrompez le musicien et vous vous excusez de ne pouvoir vous joindre à lui. L'homme vous répond par un haussement d'épaules et reprend sa mélodie, sans plus se préoccuper de vous. Comme vous vous éloignez, il entame un chant d'une voix grave. Il y est question d'une épée fantastique, brisée en trois morceaux lors de l'anéantissement de Spyte. Les fragments en ont été dispersés, mais son fourreau est en possession d'un troubadour errant. Le reste des paroles se perd dans le brouhaha du camp... La fatigue  se fait sentir. Si vous désirez retourner à votre bivouac pour vous reposer, rendez-vous au <L to='486'>486</L>. Si vous préférez observer les joueurs d'échecs, rendez-vous au <L to='554'>554</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P441);
