/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P70 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : De mauvaise grâce, le Faltyn se matérialise à nouveau. « Explique-toi ! » lui ordonnez-vous, sans quitter un instant du regard la lueur qui se rapproche dangereusement du sol. « La lumière est envoyée par la Lune Bleue ! s'écrie-t-il. Tu as offensé l'archimage qui habite le ciel. Détruis son serviteur avant qu'il ne se renforce au contact de la terre!» Le Faltyn disparaît avant que vous ayez pu lui poser une autre question. Soudain, dans un tourbillon d'étincelles, la lueur s'écrase au centre de la clairière ! Rendez-vous au <L to='180'>180</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P70);
