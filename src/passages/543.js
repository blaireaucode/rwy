/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P543 extends Component {
    render() {
        return (
            <div>

               Si vous êtes en groupe, rendez-vous au <L to='566'>566</L>. Si vous êtes seul, rendez-vous au <L to='270'>270</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P543);
