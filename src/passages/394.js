/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P394 extends Component {
    render() {
        return (
            <div>

               Abandonnant Varadaxor à son destin, vous fuyez la pièce et sortez de la tour. Pour cet acte de lâcheté, le chevalier du groupe perdra 150 points d'Expérience s'il survit à cette aventure. De crainte d'avoir à affronter les questions des villageois, vous vous éloignez vers l'est, en direction de la côte. Rendez-vous au <L to='116'>116</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P394);
