/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P508 extends Component {
    render() {
        return (
            <div>

               Des lanternes placées tout autour de l'arène dispensent une lumière verdâtre. La foule qui se presse dans les gradins n'a rien d'humain. Les créatures ressemblent à de gigantesques insectes drapés dans des capes noires. Elles frappent dans leurs mains en cadence et hurlent leurs chants de haine. Brusquement, les clameurs s'estompent et tous les visages se tournent vers la tribune royale. Un vieillard vous toise du haut de son trône, une moue ironique au coin des lèvres. Il tient un bâton surmonté d'une gemme étincelante et une couronne de cristal ceint son front parcheminé. Les rides qui creusent son visage dessinent de curieux symboles. D'un geste, il impose le silence. « Vous vous êtes introduit dans le Royaume de Wyrd ! crie-t-il en vous désignant. Vous avez forcé les portes de mon château, avec l'intention de me tuer. Pauvres fous... Je règne depuis des siècles sur ce territoire. Vous n'êtes pas les premiers assassins que je rencontre... Ils sont venus nombreux. Ces vermines assoiffées de pouvoir rêvaient, comme vous, de tremper leurs lames dans mon sang... Regardez ce qu'ils sont devenus... » Le sable de l'arène est agité de tremblements. Un monticule se forme à quelques mètres de vous et une main décharnée en sort ! Des centaines de Squelettes, certains encore vêtus d'armures rouillées, jaillissent de terre. Les troupes macabres se rassemblent... Si vous possédez l'étendard des premières légions du Selentium, rendez-vous au <L to='25'>25</L>. Sinon, rendez-vous au <L to='137'>137</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P508);
