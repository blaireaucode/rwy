/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P333 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : En lévitant, vous annulez la gravité. Peut-être pourriez-vous, d'une manière similaire, augmenter votre poids et forcer ainsi le tapis à se poser? La tentative est hasardeuse, car il se peut que le brusque gain de poids provoque la chute du tapis. Si vous désirez courir ce risque, rendez-vous au hasard au <L to='121'>121</L> ou au <L to='383'>383</L>. Si vous préférez tenter autre chose, retournez au <L to='133'>133</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P333);
