/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P511 extends Component {
    render() {
        return (
            <div>

               A l'instant où vous ouvrez la cassette, une ombre vient se placer devant votre visage. Ce voile permanent devant vos yeux vous gêne et chaque joueur voit son total de départ d'HABILETÉ réduit de 1 point. Si vous avez ouvert toutes les cassettes, rendez-vous au <L to='291'>291</L>. Sinon, vous pouvez ouvrir la cassette de bois (rendez-vous au <L to='154'>154</L>) ou celle de fer (rendez-vous au <L to='475'>475</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P511);
