/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P419 extends Component {
    render() {
        return (
            <div>

               Vous flottez dans un univers sans couleurs et sans formes. Depuis combien de temps êtes-vous là ? Le temps s'est aboli et seul le souvenir du fourreau perdu occupe vos pensées. Brusquement, il apparaît, brillant de tous ses feux, tandis que des rires sardoniques se répercutent tout autour de vous. L'image s'estompe, cédant la place à cinq silhouettes majestueuses. Chaque seigneur est entouré d'un halo de couleur. Le premier est vêtu de rouge, le deuxième de bleu céruléen. Le troisième est vert émeraude, l'autre est de couleur or. Quant au dernier, il est d'un blanc immaculé. Le seigneur rouge prend la parole : « Autrefois, de puissants seigneurs guidaient le destin de Spyte et leurs voix étaient écoutées de tous... » Le seigneur bleu reprend « Les murs de Spyte se dressaient fièrement, car le feu ne les avait calcinés... » Le seigneur vert poursuit : « Alors la parole des anciens, portée par des voix mortelles, se propagea dans tout le pays... » La voix du seigneur d'or s'élève à son tour : « Mais la bête qui rôde dans les ténèbres gagna le cœur des hommes. La haine poussa le frère contre le frère... » Le seigneur blanc termine le sermon tandis que les autres s'évanouissent : « Le temps est venu pour nous de retourner en terre du Milieu. Aucune force mortelle ne peut désormais se dresser contre nous... » Les cinq silhouettes disparaissent... et vous laissent à votre échec.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P419);
