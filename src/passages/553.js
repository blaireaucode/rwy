/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P553 extends Component {
    render() {
        return (
            <div>

               Vous murmurez la formule qui active la sphère et vous la projetez aussitôt au-dessus de la civière. Un ruban de flammes s'en échappe et allume les quatre lanternes (si vous ne les avez pas déjà éliminés, les personnages sont réduits en cendres par la fournaise). La sphère émet quelques dernières flammes avant de disparaître à son tour (rayez-la de votre <Link to='/adv'>Feuille d'Aventure</Link>). Soudain, votre Double se lève tel un automate et brandit son arme. Si vous voulez brandir la vôtre pour parer le coup, rendez-vous au <L to='456'>456</L>. Si vous préférez demeurer impassible, rendez-vous au <L to='375'>375</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P553);
