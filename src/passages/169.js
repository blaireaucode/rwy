/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P169 extends Component {
    render() {
        return (
            <div>

               La silhouette se dresse et pousse un hurlement de rage. La ferme disparaît et vous vous retrouvez au bord d'une fosse hérissée de pieux. Vous avez échappé de peu à ce piège ! Le vieillard se transforme et vous reconnaissez le Squelette tombé du ciel ! La lumière bleue qui anime ses orbites scintille par à-coups. « Vous avez déjoué mon piège ! grogne-t-il. Je vous tuerai donc de mes mains... »  SQUELETTE (Protection 2, Dommages 3 dés) FORCE : 8 HABILETÉ : 8 POUVOIR : 8 ENDURANCE : 40 Les blessures que vous lui avez infligées précédemment ont été miraculeusement guéries. Si vous préférez fuir, rendez-vous au <L to='204'>204</L>. Si vous parvenez à le tuer, rendez-vous au <L to='303'>303</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P169);
