/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P560 extends Component {
    render() {
        return (
            <div>

               Vous projetez la sphère sur le pont, où elle répand une gerbe 'de flammes. L'incendie se propage et gagne la mâture. En quelques minutes, le grand mât s'abat dans les flots. La panique s'empare de l'équipage. Vous profitez de la confusion pour sauter pardessus bord. Bientôt, le navire tout entier est la proie des flammes. Agrippé à un bout de mât, vous voyez le Lacodon s'enfoncer lentement dans les flots. Rendez-vous au <L to='113'>113</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P560);
