/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P77 extends Component {
    render() {
        return (
            <div>

               L'étincelle du briquet se transforme en long ruban de flammes qui allume les quatre lanternes, avant de s'étendre au-dessus du cadavre ! (Si vous ne les avez pas déjà éliminés, les personnages s'enfuient en hurlant de terreur.) Soudain, votre compagnon se lève et projette au loin le drap qui le recouvrait (vous pouvez lui rendre sa <Link to='/adv'>Feuille d'Aventure</Link>). Maintenant qu'il a connu la mort et qu'il en est revenu, il pourra combattre sans peur. Son total de départ de FORCE gagne définitivement 1 point. Un hurlement de rage s'élève de la direction que prenait ce cortège macabre. De crainte d'une mauvaise rencontre, vous rebroussez chemin. Rendez-vous au <L to='547'>547</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P77);
