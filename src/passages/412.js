/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P412 extends Component {
    render() {
        return (
            <div>

               Grâce au prêtre, les puissances des ténèbres ont été vaincues. Vous demeurez quelques instants interdit, encore sous le choc... Toutes ces péripéties vous ont ôté l'envie de dormir. Vous attendez que l'aube pointe à l'horizon. S'il vous reste des provisions, vous pouvez vous restaurer (rayez une ration de votre <Link to='/adv'>Feuille d'Aventure</Link>) pour éviter de perdre 1 point d'ENDURANCE. Rendez-vous au <L to='422'>422</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P412);
