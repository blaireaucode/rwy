/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P554 extends Component {
    render() {
        return (
            <div>

               Vous adressez aux marchands un salut amical, mais aucun ne daigne se retourner. Ils demeurent immobiles, le regard fixé sur l'échiquier. Vous reportez votre attention sur la partie, tâchant de suivre les stratégies mises en oeuvre par les joueurs. Si votre groupe comprend un prêtre, rendez-vous au <L to='214'>214</L>. Si votre groupe comprend un chevalier, rendez-vous au <L to='175'>175</L>. Si votre groupe ne compte ni prêtre ni chevalier, vous vous lassez rapidement du jeu et retournez vous reposer à votre bivouac (rendez-vous au <L to='486'>486</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P554);
