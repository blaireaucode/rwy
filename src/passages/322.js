/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P322 extends Component {
    render() {
        return (
            <div>

               Comme vous levez votre arme pour parer le coup, votre Double disparaît. Vous vous retrouvez seul dans ce sanctuaire infâme, observé par les orbites vides de milliers de crânes. A vos pieds, vous remarquez une amulette en forme de tête de mort. Vous pouvez la prendre. Rendez-vous au <L to='174'>174</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P322);
