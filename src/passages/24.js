/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P24 extends Component {
    render() {
        return (
            <div>

               Des hurlements vous tirent de votre sommeil. Vous vous redressez et, d'instinct, portez la main à votre arme. Les feux du campement éclairent une scène de carnage ! Les marchands que vous aviez aperçus quelque temps auparavant traversent le camp, armés de longs couteaux, le visage déformé par un rictus. Ils frappent systématiquement, tels des paysans fauchant les blés, laissant derrière eux des blessés agonisants. Deux gigantesques Loups, le regard brillant d'une lueur écarlate, mènent la curée. Des Loups-Garous ! Devant eux, les femmes et les enfants s'enfuient en hurlant, tandis que les pèlerins brandissent maladroitement des gourdins et des bâtons. La Lune Bleue s'est levée et éclaire le massacre d'une lueur blafarde. Vous vous emparez de votre arme. Rendez-vous au <L to='119'>119</L> et combattez les monstres, mais puisqu'il vous faut vous lever, reprendre vos armes, etc., vous devrez attendre trois Assauts avant d'entamer le combat.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P24);
