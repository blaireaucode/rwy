/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P88 extends Component {
    render() {
        return (
            <div>

               La porte se referme avec un cliquetis à peine perceptible. Aussitôt, un rire résonne dans votre dos. Un visage ironique est apparu dans les airs. « Vous aviez peur du Démon ? ricane-t-il. Il était enchaîné... le véritable danger est ici ! » Le visage disparaît et une horde de créatures hideuses, semblables à des scorpions, mais de la taille d'un loup, surgissent à l'extrémité du couloir. Rendez-vous au <L to='95'>95</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P88);
