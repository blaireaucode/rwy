/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P485 extends Component {
    render() {
        return (
            <div>

               Vous comprenez l'erreur que vous avez commise. Une lueur bleue d'aspect fantomatique recouvre le cadavre de votre Double. Le corps l'absorbe et se dresse tel un diable sur sa couche. Il avance aussitôt vers vous, ses doigts recourbés comme des serres ! Ses capacités sont identiques aux vôtres en début d'aventure, mais puisqu'il combat sans arme, son total de FORCE et les Dommages qu'il inflige sont réduits de 2 points. Votre Double ne  dispose d'aucun pouvoir surnaturel. Vous ne pouvez fuir. Si vous remportez ce combat, rendez-vous au <L to='456'>456</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P485);
