/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P102 extends Component {
    render() {
        return (
            <div>

               Un silence pesant succède au fracas des armes. Les hommes se sont volatilisés, et seuls subsistent de leur passage leurs robes vides et les cierges sur le sol. Vous vous penchez sur la civière, expérimentant l'étrange sensation de contempler votre propre cadavre. Vous avez le sentiment qu'il existe un lien surnaturel entre le mort et les lanternes qui l'entourent. Si vous désirez utiliser la sphère de Feu, rendez-vous au <L to='553'>553</L>. Si vous préférez recourir au briquet d'ambre, rendez-vous au <L to='90'>90</L>. Vous pouvez aussi utiliser un cierge (rendez-vous au <L to='485'>485</L>). Si vous préférez abandonner cette idée et poursuivre votre quête, rendez-vous au <L to='547'>547</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P102);
