/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P339 extends Component {
    render() {
        return (
            <div>

               Le Squelette vous répond d'une voix caverneuse, surgie d'outre-tombe : « J'étais le dévoué serviteur de l'archimage, son sénéchal, avant que le cataclysme nous anéantisse. Il m'a renvoyé dans le monde des vivants pour reprendre un objet qui lui appartenait. Il veut le fourreau de l'Épée de Légende. Donnez-le moi et je partirai. » Si vous acceptez de lui remettre le fourreau, rendez-vous au <L to='419'>419</L>. Si vous préférez le tuer sur-le-champ, rendez-vous au <L to='303'>303</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P339);
