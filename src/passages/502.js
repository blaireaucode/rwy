/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P502 extends Component {
    render() {
        return (
            <div>

               Le musicien est plus jeune qu'il n'y paraît. Cela est dû sans doute à sa chevelure, d'un blanc de neige. A votre approche, il pose sur vous un regard amical et vous sourit. « Joins-toi à moi si tu veux, dit-il sans cesser de jouer. Peut-être connais-tu la musique ? » Si vous êtes voleur, vous êtes capable de jouer d'un instrument (rendez-vous au <L to='203'>203</L>). Si vous êtes magicien, vous pouvez aussi l'accompagner (rendez-vous au <L to='20'>20</L>). Sinon, vous êtes incapable de jouer d'un instrument de musique (rendez-vous au <L to='441'>441</L>). 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P502);
