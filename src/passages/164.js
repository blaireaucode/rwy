/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P164 extends Component {
    render() {
        return (
            <div>

               Varadaxor laisse éclater sa joie : « Partons, maintenant ! s'exclame-t-il. La Dame Grise ne verra pas l'aube se lever. » D'un geste, il ordonne à l'aubergiste de préparer sa monture et vous faites route vers le sud. Chemin faisant, vous croisez quelques paysans se rendant aux champs. Varadaxor, d'humeur guillerette, vous conte ses exploits passés. Sans même vous en rendre compte, vous arrivez en vue de la tour. « En ce jour béni, les âmes de mes frères seront vengées, dit le vieux chevalier. Puisse la force guider mon bras. » Vous vous approchez du bâtiment en ruine et passez sous l'arche d'entrée, encombrée de squelettes aux os blanchis. Vous gravissez l'unique escalier et débouchez bientôt dans une salle obscure. L'aube n'est pas encore levée, mais la lumière de la lune filtre par les meurtrières. Vos yeux s'habituent à la pénombre et vous distinguez un trône au fond de la pièce, flanqué de part et d'autre d'armures de plates vides. Une femme est assise. Elle semble jeune, mais sa carnation est grise et ses cheveux d'un blanc sale. En vous apercevant, elle se lève d'un bond... Au même instant, l'aube pointe et les premiers rayons du jour illuminent la pièce. La Dame Grise laisse échapper un râle et se fige ! « Elle ne supporte pas la lumière ! s'exclame Varadaxor en fonçant vers le trône. La chance est avec nous ! » Il brandit son épée, prêt à frapper, mais s'immobilise à son tour à quelques pas du trône ! Rendez-vous au <L to='163'>163</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P164);
