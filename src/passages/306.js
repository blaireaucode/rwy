/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P306 extends Component {
    render() {
        return (
            <div>

               Vous atteignez bientôt le port. Deux navires attirent votre attention. Ce sont de solides baleiniers, conçus pour la navigation en haute mer. Sur le pont, les marins rassemblent les harpons et chargent les vivres. A côté, vous repérez une petite galère marchande, parfaite pour la navigation côtière, la Magdalena. Vous hélez le capitaine et lui demandez sa destination. Une haute silhouette se penche par-dessus le bastingage. « Nous pouvons vous emmener jusqu'à Dourhaven. Nous ne faisons pas payer le passage, mais il faudra nous aider à ramer. » Si vous acceptez cette offre, rendez-vous au <L to='187'>187</L>. Si la perspective de ramer vous répugne, rendez-vous au <L to='72'>72</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P306);
