/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P400 extends Component {
    render() {
        return (
            <div>

               En combattant avec cette hache, vous gagnez 1 point de FORCE (si vous possédez déjà une arme surnaturelle, il vous faudra choisir de combattre avec l'une ou avec l'autre). La hache n'est utilisable que par vous seul. Si d'aventure vous veniez à mourir, le prochain qui s'en emparera deviendra son unique possesseur. (Si le nouveau propriétaire de la hache est un chevalier, il devra se rendre au <L to='130'>130</L> pour en connaître les pouvoirs.) Rendez-vous au <L to='226'>226</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P400);
