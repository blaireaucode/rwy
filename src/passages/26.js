/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P26 extends Component {
    render() {
        return (
            <div>

               Voyant que vous aviez l'avantage, Auguste s'est replié sur son tapis volant. Vous foncez dans sa direction, mais il manipule son amulette et s'envole à nouveau dans les airs. Hors de portée, il s'écrie, ivre de rage : « Ce château sera votre tombeau ! » Le tapis prend de la vitesse et disparaît à l'horizon. Vous pénétrez aussitôt dans la tour et débouchez sur une plateforme. En contrebas, vous pouvez apercevoir le rez-de-chaussée mais, hélas, il n'y a pas d'escalier ! Si votre groupe compte un magicien, rendez-vous au <L to='93'>93</L>. Sinon, rendez-vous au <L to='238'>238</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P26);
