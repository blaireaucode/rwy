/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P92 extends Component {
    render() {
        return (
            <div>

               Le chef des barbares brandit sa hache avec un rire satisfait. Aussitôt, l'assemblée pousse des vivats et frappe du poing sur la table, se délectant à l'avance.  CHEF BARBARE (Protection 1, Dommages 2 dés + 1) FORCE : 8 HABILETÉ : 7 POUVOIR : 6 ENDURANCE: 30 Si vous parvenez à le vaincre, rendez-vous au <L to='451'>451</L>. Si vous préférez l'asservir, rendez-vous au <L to='243'>243</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P92);
