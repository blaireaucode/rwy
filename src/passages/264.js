/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P264 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Le Faltyn plonge ses petites dents translucides dans votre avant-bras et suce une gorgée de votre sang. Vous ne pouvez réprimer un frisson de dégoût à son contact. Cela fait, il crache sur le sol et désigne un point de l'horizon. « Wyrd est par là. Garde toujours l'étoile polaire droit devant toi. Quand la Mort Rouge se lèvera à l'est, conserve-la toujours dans ton champ de vision. » Sur ces mots, le Faltyn disparaît. Rendez-vous au <L to='422'>422</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P264);
