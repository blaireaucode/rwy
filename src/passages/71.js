/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P71 extends Component {
    render() {
        return (
            <div>

               Vous vous élancez en hurlant. Thanatos le Géant relève son épée et se prépare calmement au combat .  THANATOS (Protection 2, Dommages 4 dés) FORCE : 8 HABILETÉ : 7 POUVOIR : 8 ENDURANCE : 45 Si vous remportez le combat, rendez-vous au <L to='111'>111</L>. Si vous préférez fuir, il vous faudra retourner dans la salle du Gardien de la Mémoire (rendez-vous au <L to='367'>367</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P71);
