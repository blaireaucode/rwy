/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P557 extends Component {
    render() {
        return (
            <div>

               Vous débouchez dans une salle aux parois de boue. A votre entrée, des bouches se forment dans la glaise humide et libèrent une longue plainte macabre : les Gémissements de la Destinée. Si vous possédez la cloche de fer, elle sonne d'elle-même et chasse les Gémissements. Si vous ne possédez pas la cloche, les plaintes ont pour effet de briser votre moral et chaque joueur  perd la moitié de son total de FORCE (arrondi au chiffre supérieur), jusqu'au terme de cette aventure. Si vous désirez poursuivre votre exploration du sanctuaire, rendez-vous au <L to='188'>188</L>. Si vous préférez retourner sur vos pas, il vous faudra subir à nouveau le Souffle de la Mort (rendez-vous au <L to='160'>160</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P557);
