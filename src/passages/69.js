/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P69 extends Component {
    render() {
        return (
            <div>

               En dépit de son apparence, la petite créature s'avère un adversaire redoutable. Varadaxor se replie pour vous laisser le champ libre.  DÉMON (Protection 2, Dommages 2 dés) FORCE : 8 HABILETÉ : 9 POUVOIR : 10 ENDURANCE: 30 La créature est dotée d'une queue fourchue et venimeuse. Toute personne blessée jette un dé. Avec un résultat de 1, elle perd 4 dés d'ENDURANCE sous l'effet du poison. Si vous parvenez à l'éliminer, rendez-vous au <L to='462'>462</L>. Si vous préférez fuir, rendez-vous au <L to='406'>406</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P69);
