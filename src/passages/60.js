/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P60 extends Component {
    render() {
        return (
            <div>

               Si tous les joueurs ont choisi de demeurer sur la dalle, rendez-vous au <L to='183'>183</L>. Si certains ont sauté sur le côté, rendez-vous au <L to='385'>385</L>. Si tous ont quitté la dalle, rendez-vous au <L to='489'>489</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P60);
