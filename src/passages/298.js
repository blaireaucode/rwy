/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P298 extends Component {
    render() {
        return (
            <div>

               Le Lacodon a repris sa route vers le nord. Certains joueurs peuvent être sujets au mal de mer, à l'exception des prêtres, grâce à leur capacité de contrôle mental et physique. Lancez deux dés (et ajoutez 1 point au résultat si vous avez déjeuné). Si vous faites 10, vous perdez 2 points d'ENDURANCE (si votre total n'est plus que de 2 points, retranchez seulement 1 point : le mal de mer n'est pas mortel...). Le matin du troisième jour, le capitaine Lazarus vous aborde avec des airs de conspirateur. « Nous sommes à présent dans les eaux du grand Serpent de Mer. Ce soir, la Mort Rouge brillera au-dessus de nos têtes. La bête va se manifester ! Alors, des dizaines de harpons la transperceront. Nous allons vaincre, et Port Quanongu nous accueillera en héros ! » Le capitaine s'éloigne en se frottant les mains. Si vous possédez l'épée du Marcheur des Cieux, rendez-vous au <L to='191'>191</L>. Sinon, rendez-vous au <L to='469'>469</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P298);
