/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P76 extends Component {
    render() {
        return (
            <div>

               Auguste recule devant vos airs farouches. Il n'est manifestement pas habitué à ce qu'on lui oppose la moindre résistance. Il se tourne vers ses quatre serviteurs, qui s'élancent aussitôt à l'attaque. SERVITEUR (Protection 1, Dommages 1 dé + 1) FORCE : 8 HABILETÉ : 5 POUVOIR : 6 ENDURANCE : 8 Il vous est impossible de fuir. Si vous éliminez les serviteurs d'Auguste, rendez-vous au <L to='26'>26</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P76);
