/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P316 extends Component {
    render() {
        return (
            <div>

               Vous tentez de fuir, mais les tentacules vous percutent de plein fouet. Chaque joueur perd 2 dés d'ENDURANCE (moins sa Protection). Les tentacules sont trop rapides. Vous devez combattre (rendez-vous au <L to='457'>457</L>) ou bien faire usage d'un objet (rendez-vous au <L to='549'>549</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P316);
