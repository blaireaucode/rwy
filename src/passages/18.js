/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P18 extends Component {
    render() {
        return (
            <div>

               Vous brandissez le crucifix contenant l'index de saint Ashanax devant les marins. Brusquement, il vous échappe des mains et se place à l'horizontale, tendant à rompre la chaîne qui le maintient ! « Regardez, misérables ! criez-vous. Craignez le courroux du vrai dieu ! Nous devons suivre la direction que nous désigne le symbole sacré. Sortez les rames, vite ! » Les marins stupéfaits s'empressent de vous obéir. Bientôt, la brise se lève, poussant le Lacodon vers le nord. Rendez-vous au <L to='298'>298</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P18);
