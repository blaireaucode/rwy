/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P472 extends Component {
    render() {
        return (
            <div>

               La dalle s'élève à nouveau, réunissant tout le groupe. L'encensoir, tombé au sol, continue à libérer de petits nuages de fumée. Vous pouvez le prendre. Rendez-vous au <L to='493'>493</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P472);
