/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P153 extends Component {
    render() {
        return (
            <div>

               Vous jetez le cadavre de Lazarus par-dessus bord et vous le regardez s'enfoncer dans les flots. Vous dérivez pendant des heures mais, enfin, vous repérez une voile faisant route dans votre direction. Bientôt, des mains secourables vous hissent à bord. Le capitaine vous informe qu'il se rend à Dourhaven et qu'il compte atteindre le port avant la tombée de la nuit. En effet, comme le crépuscule descend sur la mer, les côtes enneigées apparaissent à l'horizon. Avant de débarquer, le capitaine vous indique une auberge de sa connaissance, à la limite des docks, ainsi qu'une boutique où vous pourrez acheter l'équipement nécessaire à votre périple. Si vous désirez aller à la boutique, rendez-vous au <L to='288'>288</L>. Si vous préférez prendre la direction de l'auberge, rendez-vous au <L to='75'>75</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P153);
