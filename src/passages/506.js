/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P506 extends Component {
    render() {
        return (
            <div>

               Le corps de l'Hydre se décompose et les vapeurs qui s'en dégagent vous donnent la nausée. Bientôt, il ne subsiste plus de la bête que ses dents acérées. Vous pouvez les prendre si vous le désirez. L'ensemble des dents compte comme un objet. Après une rapide exploration, vous découvrez une porte. Rendez-vous au <L to='318'>318</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P506);
