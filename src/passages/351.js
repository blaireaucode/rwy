/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P351 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : De toute évidence, il vous faut léviter. Concentrez-vous et rendez-vous au hasard au <L to='325'>325</L> ou au <L to='30'>30</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P351);
