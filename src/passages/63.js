/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P63 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Vous comprenez la raison de votre malaise : la ferme est mal entretenue, mais il n'y a pas de mauvaises herbes au pied des murs. On pourrait croire que la bâtisse a été déposée au centre de la clairière quelques minutes auparavant. Pour plus de certitude, vous allez arracher un branchage dans les buissons environnants et vous le recouvrez de votre cape. Quelques instants plus tard, vous agitez ce rameau par l'entrebâillement de la porte. « Que se passe-t-il ? s'exclame une voix. Ce n'est pas normal, tu devrais tomber, mortel ! » Vous percevez des craquements secs, tels des ossements que l'on entrechoque. Le bruit se rapproche de l'entrée, aussitôt suivi d'un hurlement ! Au même instant, la ferme disparaît et vous vous retrouvez au bord d'une fosse hérissée de pieux. Au fond du gouffre, vous pouvez apercevoir le Squelette, tentant désespérément de s'extirper du piège qui vous était destiné ! Rendez-vous au <L to='258'>258</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P63);
