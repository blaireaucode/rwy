/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P161 extends Component {
    render() {
        return (
            <div>

               Vous avez activé la sphère de la Peste. Des visions hideuses naissent à la périphérie de votre champ de vision. Des crapauds pustuleux et des rats pelés grouillent autour de cadavres buboniques. Une odeur fétide emplit l'atmosphère. Chaque joueur jette deux dés. Si le résultat est supérieur à son total de POUVOIR, il contracte une maladie qui l'empêche de reprendre des forces par des moyens naturels (sommeil, nourriture, etc.). Seules des potions, ou les soins prodigués par un prêtre pourront désormais rétablir son ENDURANCE perdue. Si le résultat est inférieur ou égal à son total de POUVOIR, le joueur est immunisé contre toute forme de maladie et ce, de manière définitive. Notez que cette insensibilité ne s'applique pas au poison, à la morsure du froid, à la faim, etc. La sphère se volatilise après usage. Rayez-la de votre <Link to='/adv'>Feuille d'Aventure</Link> et retournez au paragraphe que vous avez quitté.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P161);
