/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P457 extends Component {
    render() {
        return (
            <div>

               Vous tournez sur vous-même, tous vos sens en alerte, prêt à contrer une attaque pouvant surgir de toute part.  EISLAKEN (Protection 2, Dommages 2 dés) FORCE : 7 HABILETÉ : 6 ENDURANCE 11 Vous allez affronter les huit tentacules comme s'il s'agissait d'autant d'adversaires différents (chacun a 11 points d'ENDURANCE). Pour combattre les tentacules, vous devez vous déplacer jusqu'à l'un des trous d'où ils émergent. En revanche, les tentacules, de par leur taille, peuvent frapper tous les joueurs. La créature, d'une intelligence faible, est donc insensible aux sortilèges mentaux. Lorsque vous aurez éliminé au moins six tentacules, rendez-vous au <L to='525'>525</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P457);
