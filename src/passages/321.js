/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P321 extends Component {
    render() {
        return (
            <div>

               La perplexité de l'Elfe augmente tout au long de la partie. Manifestement, votre tactique l'embarrasse. Bientôt, il doit concéder sa défaite. « Vous avez gagné, dit-il, maussade. Je vais donc tenir ma promesse et vous laisser passer. Mais je ne suis pas certain que vous auriez tenu la vôtre si vous aviez perdu... » Vous sursautez, indigné. « Cette remarque est désobligeante, vous exclamez-vous. Vous avez perdu, ce n'est pas une raison pour dénigrer votre adversaire ! » L'Elfe soupire, manifestement embarrassé. «Vous avez raison, dit-il enfin. Je me suis emporté... » Il prononce quelques mots dans un langage qui vous est inconnu et les pièces de l'échiquier émettent une lueur surnaturelle. « Le jeu est maintenant chargé de ma propre magie. Elle vous sera utile lors du combat final et, d'ici-là, la peine que je vous ai causée se sera effacée... » Vous ramassez les pièces, non sans remarquer qu'elles sont maintenant glaciales, et rassemblez vos affaires. Vous passez devant les soldats alignés et pénétrez dans la forêt. Rendez-vous au <L to='501'>501</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P321);
