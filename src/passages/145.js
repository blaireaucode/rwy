/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P145 extends Component {
    render() {
        return (
            <div>

               Brusquement, les limites de la pièce s'estompent et, pendant un court instant, vous naviguez dans un océan de brumes. Des bruits s'élèvent tout autour de vous. Vous croyez reconnaître des armes qu'on entrechoque et des plaintes d'agonisants. Mais peu à peu une pulsation sourde recouvre ce tumulte, un bruit semblable au battement d'un coeur. Le son devient insoutenable... Alors les miasmes se déchirent et vous vous retrouvez au centre d'une arène. Les pulsations proviennent des gradins. La foule scande des cris de guerre... Rendez-vous au <L to='508'>508</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P145);
