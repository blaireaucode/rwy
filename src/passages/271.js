/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P271 extends Component {
    render() {
        return (
            <div>

               PRÊTRE et VOLEUR : Votre flèche atteint son but, mais elle traverse la lumière bleue sans effet apparent. Dans une gerbe d'étincelles, la lueur atterrit au centre de la clairière. Rendez-vous au <L to='180'>180</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P271);
