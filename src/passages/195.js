/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P195 extends Component {
    render() {
        return (
            <div>

               L'étincelle du briquet se transforme en long ruban de flammes qui court dans le sanctuaire et allume tour à tour les quatre lanternes ! Les personnages s'enfuient en hurlant de terreur. Rendez-vous au <L to='197'>197</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P195);
