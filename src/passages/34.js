/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P34 extends Component {
    render() {
        return (
            <div>

               Ce qui suit est réservé à celui qui joue avec la Pourfendeuse d'Ombre. Une vibration sourde court le long de la lame. La patine qui la recouvrait disparaît et l'épée commence à briller d'un éclat aveuglant. Une intense luminosité envahit la pièce. Umborus pousse un hurlement de douleur. L'épée s'est animée d'une vie propre ! Soudain, elle guide votre bras et s'enfonce dans la masse ténébreuse du Démon. Il s'ensuit une explosion. D'instinct, vous vous protégez les yeux... Quand vous osez enfin les rouvrir, la créature a disparu. La Pourfendeuse d'Ombre gît à vos pieds, le métal est chauffé à blanc et vous ne pouvez pas la récupérer (rayez-la de votre <Link to='/adv'>Feuille d'Aventure</Link>). A ses côtés, vous remarquez une petite masse sombre. C'est le coeur des Ténèbres, seul souvenir de la présence du Démon. Vous pouvez le prendre. Rendez-vous au <L to='145'>145</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P34);
