/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P528 extends Component {
    render() {
        return (
            <div>

               Les pointes de la herse sont maculées de sang. Des cadavres broyés gisent en dessous, réduits à l'état de squelettes dans leurs armures rouillées. Il vous faudra franchir vivement cet espace pour éviter un sort semblable. Les grognements du monstre dans votre dos coupent court à vos hésitations. D'un bond vous passez sous le porche et la herse retombe aussitôt derrière vous. Le monstre se précipite sur les barreaux de la grille, ses griffes fouettant l'air, mais vous êtes hors d'atteinte. La pièce où vous vous trouvez est plongée dans l'obscurité. Les tapisseries qui en couvrent les murs sont en lambeaux et le mobilier tombe en poussière. Une arche ménagée dans le mur du fond s'ouvre sur une vaste pièce circulaire. Les parois sont percées de meurtrières. Vous repérez une porte à l'extrémité de la pièce. Rendez-vous au <L to='157'>157</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P528);
