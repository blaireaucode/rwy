/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P413 extends Component {
    render() {
        return (
            <div>

               La créature s'élance vers vous, un rictus féroce au coin des lèvres.  FONGOÏDE (Protection 3, Dommages 2 dés + 3) FORCE : 7 HABILETÉ : 7 POUVOIR : 7 ENDURANCE : 35 Si vous remportez le combat et que certains de vos compagnons se trouvent de l'autre côté de la paroi, rendez-vous au <L to='342'>342</L>. Si vous gagnez et que personne n'est de l'autre côté, rendez-vous au <L to='223'>223</L>. Si vous êtes vaincu et que d'autres se trouvent derrière la paroi, ces derniers se rendent au <L to='540'>540</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P413);
