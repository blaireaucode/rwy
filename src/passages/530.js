/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P530 extends Component {
    render() {
        return (
            <div>

               La progression vers le centre de cette toile n'est pas une tâche facile. Vous ne pouvez vous déplacer que d'une case par Assaut. Au début de chaque Assaut, une violente décharge d'énergie court le long des fils de la toile et provoque une perte de 2 dés d'ENDURANCE (l'armure n'assure aucune Protection). ROI-SORCIER (Protection 2)  ENDURANCE :16  Puisque vous êtes agrippé à la toile, il vous est impossible de jeter des sortilèges, de lancer des flèches ou une hache. Vous devez atteindre la case centrale de la toile avant de débuter l'affrontement. Si votre groupe compte un prêtre, il peut Léviter et décocher des flèches en même temps. N'oubliez pas que la Lévitation se fait à la verticale, le prêtre qui Lévite ne peut donc pas avancer (curieusement, la tentative de Lévitation est aussitôt couronnée de succès, comme si les pouvoirs du prêtre étaient renforcés à l'intérieur du rêve). Si vous parvenez à atteindre le roi-sorcier, rendez-vous au <L to='255'>255</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P530);
