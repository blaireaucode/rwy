/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P91 extends Component {
    render() {
        return (
            <div>

               Vous vous réveillez sous un ciel gris. Vous pouvez déjeuner, s'il vous reste encore des rations. Les joueurs qui ne mangent pas perdent 1 point d'ENDURANCE. Bientôt, vous rassemblez votre paquetage et, le regard fixé vers le nord-est, entamez votre périple en direction du Royaume de Wyrd. Rendez-vous au <L to='422'>422</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P91);
