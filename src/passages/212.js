/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P212 extends Component {
    render() {
        return (
            <div>

               La créature s'avance, sa cape déployée comme les ailes d'un oiseau de proie. Si vous l'aviez blessée auparavant, elle a retrouvé toutes ses forces.  SQUELETTE (Protection 2, Dommages 3 dés) FORCE : 8 HABILETÉ : 8 POUVOIR : 8 ENDURANCE : 40 Si vous parvenez à le tuer, rendez-vous au <L to='303'>303</L>. Si vous préférez fuir, rendez-vous au <L to='204'>204</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P212);
