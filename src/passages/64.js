/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P64 extends Component {
    render() {
        return (
            <div>

               D'un mouvement ample, vous projetez la poussière d'argent en direction du groupe. Les personnages encagoulés s'immobilisent aussitôt. Soudain, une rafale de vent jaillit de nulle part et, dans un claquement de métal, le bourreau qui a tué votre compagnon apparaît derrière la civière ! Aussitôt, il brandit son cimeterre et s'élance à l'attaque. BOURREAU (Protection 2, Dommages 4 dés + 1) FORCE : 9 HABILETÉ : 8 POUVOIR : 9 ENDURANCE: 50  Si vous préférez fuir, rendez-vous au <L to='152'>152</L>. Si vous remportez ce combat, rendez-vous au <L to='387'>387</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P64);
