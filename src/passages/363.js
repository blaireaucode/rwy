/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P363 extends Component {
    render() {
        return (
            <div>

               Dans la forêt, l'air est froid et humide et un manteau de ténèbres recouvre les bois. Un petit sentier serpente entre les arbres jusqu'à une nouvelle clairière. Vous vous y arrêtez pour reprendre votre souffle. Surplombant la cime des arbres, la Lune Bleue scintille de tous ses feux. Comme vous le regardez, le disque émet une brève lueur aveuglante. Une tache lumineuse descend lentement vers le sol. Sa taille augmente comme elle se rapproche et vous pouvez percevoir un étrange sifflement continu. Si votre groupe comprend un magicien désirant intervenir, rendez-vous au <L to='33'>33</L>. Si le prêtre ou le  voleur désirent décocher une flèche avant que la lumière n'ait touché le sol, rendez-vous au <L to='437'>437</L>. Si vous préférez quitter la clairière au plus vite, rendez-vous au <L to='397'>397</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P363);
