/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P159 extends Component {
    render() {
        return (
            <div>

               Vous contemplez d'un air désolé le visage livide du musicien, baigné par les lueurs blafardes de la Lune Bleue. La forêt s'est parée de couleurs étranges. Votre attention se porte sur le fourreau orné de joyaux. Vous avez accepté, un peu malgré vous, de poursuivre sa quête. Notez le fourreau sur votre <Link to='/adv'>Feuille d'Aventure</Link>. Le fourreau peut se transmettre d'un personnage à l'autre mais, en aucun cas, le groupe ne doit s'en séparer. Si cela arrivait, rendez-vous immédiatement au <L to='419'>419</L>. Qu'allez-vous faire à présent ? La perspective de rester dans la clairière au milieu des cadavres n'est guère réjouissante. Si vous désirez néanmoins attendre l'aube, rendez-vous au <L to='142'>142</L>. Si vous préférez rassembler votre paquetage et quitter ce sinistre endroit au plus vite, rendez-vous au <L to='363'>363</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P159);
