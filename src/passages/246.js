/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P246 extends Component {
    render() {
        return (
            <div>

               Vous établissez votre bivouac à l'abri d'un monticule de glace qui vous protégera du vent. Recroquevillé et grelottant de froid, vous vous laissez gagner par le sommeil. La Lune Rouge s'est levée sur la banquise et, bientôt, des images sanguinaires peuplent vos rêves... Une antique cité surgit des brumes, ses hautes tours dominant une contrée sauvage... Des fissures courent sur ses murs tandis que, dans le ciel, des légions fantômes se livrent bataille... Un sang épais coule du ciel et recouvre les remparts. La cité s'effrite comme rongée par un feu intérieur, et des torrents de sang écarlate déferlent sur la contrée, gagnent la mer et la banquise... Vous êtes emporté par cette vague et vos cris de terreur se mêlent aux ricanements sinistres de cinq silhouettes qui contemplent votre agonie. Vous luttez de  toutes vos forces pour vous libérer de ce cauchemar. Pour ce faire, chaque joueur doit obtenir un résultat inférieur ou égal à son total de FORCE avec trois dés pour ne pas perdre 3 dés d'ENDURANCE et tente à nouveau sa chance à l'Assaut suivant. Un joueur réveillé peut tirer ses compagnons de leur sommeil, mais il lui faut pour cela un résultat inférieur ou égal à son total de FORCE avec deux dés. Lorsque tous les survivants sont réveillés, rendez-vous au <L to='505'>505</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P246);
