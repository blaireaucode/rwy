/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P364 extends Component {
    render() {
        return (
            <div>

               Vous passez la nuit dans la chambre commune, mais votre sommeil est perturbé par les allées et venues de marins ivres et les ronflements sonores. Chaque joueur récupère 1 point d'ENDURANCE. La couche près de l'âtre est un peu à l'écart et celui qui a eu la chance d'en bénéficier récupère un nombre de points d'ENDURANCE équivalent à la moitié de son total de départ (arrondi au chiffre supérieur). Le lendemain, vous partagez une soupe épaisse avec les autres voyageurs. Ce repas vous permet, si nécessaire, de reprendre 1 autre point d'ENDURANCE (sans que votre total d'ENDURANCE excède son niveau de départ). Bientôt, un marin pénètre dans l'auberge. Il s'accoude au comptoir et commande d'affilée trois verres de l'alcool local, distillé à partir d'algues. Après avoir englouti sa ration, il se retourne vers la salle commune. Une profonde balafre lui traverse le visage. L'homme vous fixe un instant. « Ah ! Des voyageurs, à ce que je vois, lance-t-il d'une voix pâteuse. J'ai peut-être du travail pour vous. » Si vous êtes intéressé, rendez-vous au <L to='232'>232</L>. Si vous préférez refuser, rendez-vous au <L to='237'>237</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P364);
