/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P131 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous vous concentrez sur la créature, cherchant la source de son énergie, afin de l'anéantir. Jetez deux dés. Si vous faites de 2 à 6, rendez-vous au <L to='110'>110</L>. Si vous faites de 7 à 12, rendez-vous au <L to='484'>484</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P131);
