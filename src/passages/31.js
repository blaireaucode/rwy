/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P31 extends Component {
    render() {
        return (
            <div>

               La lumière de l'étendard illumine la corne à boire, qui se pare à son tour d'un halo aveuglant. Un liquide rougeâtre en jaillit et éclabousse le sable de l'arène. Aussitôt, de gigantesques barbares vêtus de peaux de bêtes se matérialisent. A la vue des Squelettes, ils frappent leurs boucliers de leurs épées en grognant leur chant de guerre. Notez les hordes barbares sur votre <Link to='/adv'>Feuille d'Aventure</Link>. Rayez la corne à boire et retournez au <L to='178'>178</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P31);
