/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P244 extends Component {
    render() {
        return (
            <div>

               Ceci ne concerne que les joueurs sous l'emprise de la Mort Rouge. Vous vous dressez sur votre couche, tel un automate. Le sommeil vous a quitté, mais vous n'aurez plus jamais besoin de dormir. Une existence sans rêves s'ouvre devant vous, qui venez de rejoindre les rangs des Morts Vivants... Votre épiderme s'est teinté de reflets écarlates. Un froid intense s'est emparé de votre être et seul le meurtre pourra désormais vous réchauffer. Vos ongles sont devenus de véritables armes. Vous portez votre attention sur les formes assoupies qui vous entourent. Étaient-ils vos amis? La mort a balayé vos souvenirs. Vous ne voyez en eux que des proies faciles... Tendez le livre à vos compagnons, qui doivent se rendre au <L to='57'>57</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P244);
