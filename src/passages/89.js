/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P89 extends Component {
    render() {
        return (
            <div>

               Vos efforts n'ont pu venir à bout de l'Élémental d'Eau. La vague surplombe le navire, prête à l'engloutir. Si votre groupe compte un chevalier, rendez-vous au <L to='283'>283</L>. Si aucun chevalier n'est présent, il ne vous reste plus qu'à faire votre prière ! Des trombes d'eau s'abattent sur le navire et l'entraînent par le fond, corps et biens... 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P89);
