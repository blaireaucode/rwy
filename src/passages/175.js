/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P175 extends Component {
    render() {
        return (
            <div>

               CHEVALIER : Au bout de quelques minutes d'observation, vous finissez par comprendre la stratégie de chacun des joueurs. Soudain, l'un d'eux commet une erreur grossière. Ce ne peut être qu'une ruse destinée à tromper l'ennemi... Non, l'autre joueur s'empare de la pièce et le jeu se poursuit. Les joueurs ne manifestent aucune émotion. Bizarre... Si votre groupe comprend un prêtre, désirant user de sa clairvoyance, rendez-vous au <L to='348'>348</L>. Sinon, rendez-vous au <L to='170'>170</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P175);
