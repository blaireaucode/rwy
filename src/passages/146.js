/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P146 extends Component {
    render() {
        return (
            <div>

               «Vous êtes au village de Misdraex, répond-il tout en tirant sur sa bouffarde. Plus loin vers l'est, c'est la Mer de Mistral, à une demi-journée de marche. En suivant la route, vous atteindrez le port de Quanongu. De là, vous pourrez embarquer pour l'île d'Hydra ou le Royaume de Wyrd... Je dois y aller maintenant. » Le vieil homme s'éloigne. Si vous désirez poursuivre vers la côte, rendez-vous au <L to='267'>267</L>. Si vous préférez faire halte à l'auberge, rendez-vous au <L to='429'>429</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P146);
