/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P272 extends Component {
    render() {
        return (
            <div>

               La porte ne s'ouvre pas ! Retournez au <L to='95'>95</L> et poursuivez le combat, mais les créatures peuvent vous frapper au premier Assaut.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P272);
