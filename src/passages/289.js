/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P289 extends Component {
    render() {
        return (
            <div>

               Les cadavres des Elfes gisent dans la neige. Vous les fouillez sans tarder. Les prêtres et les voleurs peuvent les délester de leurs flèches (ils en trouveront un total de dix-huit, de quoi remplir trois carquois), et de leurs sept arcs mais, maniées par des mains humaines, ces armes provoquent les mêmes Dommages qu'un arc normal. Prenez ce que vous voulez et rendez-vous au <L to='501'>501</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P289);
