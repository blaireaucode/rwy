/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P47 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Vous jetez un sortilège d'Oracle. Bientôt, votre esprit sonde les méandres du futur. La clairière réapparaît. La nuit est tombée et les feux du campement éclairent une scène de carnage ! Des cadavres mutilés gisent sur le sol, et les tueurs armés de longs couteaux s'acharnent sur leurs victimes. Vous reconnaissez les corps des voyageurs et, parmi eux, un cadavre portant votre cape... Brusquement, vous reprenez pied dans le présent, aveuglé un court instant par la lueur des feux. Rendez-vous au <L to='468'>468</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P47);
