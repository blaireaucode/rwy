/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P532 extends Component {
    render() {
        return (
            <div>

               Vous êtes à la merci de la créature des ténèbres. La masse d'ombre vous enveloppe... Il faut un résultat inférieur ou égal à son total de POUVOIR avec trois dés pour ne pas être absorbé par l'Umborus. Même si le lancer est réussi, le joueur voit baisser de 1 point ses totaux de départ de FORCE, de POUVOIR et d'HABILETÉ. Son forfait accompli, le monstre se désintègre. Les survivants se rendent au <L to='145'>145</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P532);
