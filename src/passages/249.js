/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P249 extends Component {
    render() {
        return (
            <div>

               Vous pénétrez dans la cour. Le Démon vous regarde d'un œil torve. Une chaîne l'emprisonne au piédestal. Un sentier d'herbe roussie conduit à la porte de bois. « Il fait toujours ça, dit alors le Démon. Où qu'il aille... » Vous le pressez de s'expliquer, mais il demeure muet et évite votre regard. Le cœur battant, vous vous approchez de la porte. Rendez-vous au <L to='275'>275</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P249);
