/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P25 extends Component {
    render() {
        return (
            <div>

               Le roi-sorcier a levé une armée de Squelettes pour vous tuer. Que faire devant un tel déploiement de forces ? Vous lancez des regards inquiets alentour, cherchant une issue qui n'existe pas. Dans quelques secondes, leurs armes rouillées vous transperceront sans merci. Vous alliez vous résigner à la mort quand, soudain, l'étendard s'illumine ! Des clameurs effrayées parcourent l'assistance, qui se disperse à la hâte, cherchant refuge dans les coins d'ombre. En un geste désespéré, vous brandissez l'étendard. Aussitôt, un faisceau de lumière vous submerge. Les voix des centurions morts murmurent à vos oreilles des paroles de courage. Le roisorcier lui-même esquisse un mouvement de recul et se protège les yeux ! La lumière augmente de puissance ! Peu à peu, des légions fantômes s'interposent entre les Squelettes et l'étendard, prêtes à défendre leur emblème ! Rendez-vous au <L to='178'>178</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P25);
