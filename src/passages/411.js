/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P411 extends Component {
    render() {
        return (
            <div>

               Comme vous vous dirigez vers l'arche, une petite voix vous force à relever la tête vers le malheureux Faltyn, enchaîné dans sa cage. « Délivrez-moi, s'il vous plaît, gémit-il. Je pourrai vous aider... » Si vous acceptez de lui venir en aide, rendez-vous au <L to='314'>314</L>. Si vous préférez l'ignorer, rendez-vous au <L to='40'>40</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P411);
