/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P404 extends Component {
    render() {
        return (
            <div>

               La couche glacée diminue à mesure que vous approchez des côtes et, par endroits, des fissures dans la banquise révèlent une eau grise. Soudain, vous repérez une silhouette immobile, allongée sur la glace à quelques centaines de mètres. Si vous poursuivez votre route, vous pourrez atteindre la terre ferme avant la tombée de la nuit. Si vous désirez porter secours à l'étranger, rendez-vous au <L to='465'>465</L>. Si vous préférez ne pas perdre de temps, rendez-vous au <L to='365'>365</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P404);
