/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P261 extends Component {
    render() {
        return (
            <div>

               La tempête fait rage et, toute la nuit durant, le Lacodon est le jouet des flots déchaînés. Enfin, un peu avant l'aube, le vent tombe. Si le coeur vous en dit, vous pouvez vous restaurer et récupérer de cette manière 1 point d'ENDURANCE. Une brume opaque recouvre l'océan. « Qui peut dire où nous nous trouvons à présent ! grommelle Lazarus. Bildad, peux-tu nous guider? » Le barreur scrute un instant l'horizon et réplique : « Impossible, capitaine, on ne voit même pas le soleil. » Penché au bastingage, vous ne pouvez pas même distinguer les flots. « C'est la faute de ces maudits étrangers ! grogne un marin. Ils ont le mauvais œil. » Des cris s'élèvent. « Restez calme et tout se passera bien, vous chuchote le capitaine. Je ne peux rien faire pour vous ou je risque la mutinerie. Ils vont vous abandonner dans un canot de sauvetage. Cette perspective est préférable à la mort. » Les marins se rassemblent, brandissant des harpons. Certes, ils sont résolus, mais l'issue du combat n'est pas jouée d'avance, comme le capitaine semble le penser. Si vous décidez de les affronter, rendez-vous au <L to='148'>148</L>. Si vous préférez accepter la proposition du capitaine, rendez-vous au <L to='36'>36</L>. Si vous désirez recourir à un objet, rendez-vous au <L to='118'>118</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P261);
