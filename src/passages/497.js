/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P497 extends Component {
    render() {
        return (
            <div>

               Ce qui suit est réservé au joueur qui a pris le plateau. Fatale erreur ! A l'instar de nombreux magiciens, Auguste est jaloux de ses secrets. Même ici, au cœur de sa forteresse, il a posé des pièges. Le pentagramme dessiné se déploie devant vos yeux et une force inconnue vous attire irrésistiblement vers son centre. Jetez trois dés. Vous devez obtenir un résultat inférieur ou égal à votre total de POUVOIR pour résister à ce sortilège. Si vous réussissez, vous pouvez lâcher le plateau. Si vous manquez ce jet de dés, vous êtes happé à l'intérieur du pentagramme. Vos compagnons sont incapables de vous libérer de ce piège. Ils devront vous  abandonner et poursuivre seuls leur quête... Les survivants se rendent au <L to='470'>470</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P497);
