/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P9 extends Component {
    render() {
        return (
            <div>

               A la mort de la Dame Grise, les armures tombent inanimées sur le sol dallé. Elles se pulvérisent, ne laissant qu'un petit tas de cendres grises. Varadaxor, libéré de l'emprise du sortilège, s'étire en grognant et se dirige vers le cadavre de son ennemie jurée. Rendez-vous au <L to='108'>108</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P9);
