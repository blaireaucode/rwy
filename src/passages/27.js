/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P27 extends Component {
    render() {
        return (
            <div>

               Épuisé par votre fuite éperdue dans la forêt, vous sombrez dans le sommeil. Soudain, un craquement sec vous réveille en sursaut. Un frisson d'horreur vous parcourt l'échine ! La face grimaçante du Squelette apparaît entre les buissons, vous fixant de ses yeux étincelants ! Rendez-vous au <L to='212'>212</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P27);
