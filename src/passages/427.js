/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P427 extends Component {
    render() {
        return (
            <div>

               Vos troupes ouvrent une brèche dans les armées squelettes afin que vous puissiez atteindre le roi-sorcier. Votre tactique fonctionne à merveille et l'accès aux marches de la tribune est dégagé. Le roi-sorcier ne cache pas sa nervosité. Vous alliez gravir les marches quand un groupe de Squelettes s'interpose.  SQUELETTE (Dommages 1 dé + 1) FORCE : 7 HABILETÉ : 6 POUVOIR : 7 ENDURANCE :  8  Vous ne pouvez fuir. Si vous parvenez à vous frayer un passage ou à tuer les Squelettes, rendez-vous au <L to='300'>300</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P427);
