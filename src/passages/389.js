/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P389 extends Component {
    render() {
        return (
            <div>

               Vous êtes prisonnier de ce tapis volant, errant au gré des vents glacés ! Dans quelques heures, ou quelques jours peut-être, la morsure du froid aura raison de vous. Brr l... Votre aventure est terminée.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P389);
