/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P366 extends Component {
    render() {
        return (
            <div>

               La chair des Vampires disparaît, ne laissant que deux squelettes aux os blanchis. Vous rebroussez chemin vers votre bivouac. Épuisé, vous vous endormez. La Mort Rouge s'est levée et baigne la contrée d'une lueur écarlate... Vous vous retournez dans votre sommeil avec un grognement. Tous les joueurs qui ont été blessés par les Vampires des Neiges, excepté le possesseur du fourreau de l'Épée de Légende, doivent se rendre au <L to='558'>558</L>. Si personne n'a subi de blessures au combat précédent (ou si le seul blessé est le possesseur du fourreau), rendez-vous au <L to='91'>91</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P366);
