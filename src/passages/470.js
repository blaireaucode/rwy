/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P470 extends Component {
    render() {
        return (
            <div>

               Vous pouvez explorer la galerie supérieure (rendez-vous au <L to='252'>252</L>), la galerie centrale (rendez-vous au <L to='140'>140</L>), ou la galerie inférieure (rendez-vous au <L to='524'>524</L>). Lorsque vous aurez exploré les galeries que vous voulez, vous pouvez descendre au rez-de-chaussée (rendez-vous au <L to='548'>548</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P470);
