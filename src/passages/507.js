/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P507 extends Component {
    render() {
        return (
            <div>

               Le lendemain à l'aube, après avoir partagé le déjeuner de l'équipage, vous reprenez votre place sur le banc des rameurs. Ce repas vous rend 1 point d'ENDURANCE. La galère quitte la baie et, poussée par des vents constants, vogue vers le nord. Vers midi, la vigie repère un point à l'horizon. L'apparition se rapproche et vous constatez, stupéfait, qu'il s'agit d'un homme porté par un tapis volant. L'inconnu s'arrête à hauteur du bastingage et, ignorant le reste de l'équipage, s'adresse directement à vous : « Si vous vous rendez à Wyrd, comme je le suppose, peut-être voudrez-vous voyager d'une manière rapide et efficace. » D'un geste ample de sa cape violette, il désigne son tapis. Si vous acceptez cette offre, rendez-vous au <L to='299'>299</L>. Si vous préférez la décliner, rendez-vous au <L to='55'>55</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P507);
