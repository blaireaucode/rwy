/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P279 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous poursuivez votre investigation par-delà les apparences. La ferme se dissipe peu à peu et, sous le plancher de bois, vous repérez une immense fosse hérissée de piques... Vous êtes le jouet d'une illusion. La porte semble battre au vent. La bâtisse se reforme devant vous. Vous reprenez pied dans le monde réel. Rendez-vous au <L to='256'>256</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P279);
