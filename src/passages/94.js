/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P94 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous vous élancez dans le vide... et vous flottez dans les airs. Quelques secondes plus tard, vous vous posez sur la banquise. Si vous êtes seul, rendez-vous au <L to='344'>344</L>. Si d'autres joueurs doivent descendre, rendez-vous au <L to='498'>498</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P94);
