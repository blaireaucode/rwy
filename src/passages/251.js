/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P251 extends Component {
    render() {
        return (
            <div>

               Les autres marins préfèrent abandonner le combat. La haine se lit encore dans leur regard, mais vous êtes certains qu'ils ne tenteront plus rien contre vous. Après un bref cérémonial, ils jettent les cadavres par-dessus et se regroupent en silence. Rendez-vous au <L to='499'>499</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P251);
