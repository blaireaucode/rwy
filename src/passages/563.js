/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P563 extends Component {
    render() {
        return (
            <div>

               Allez-vous utiliser la sphère de Feu (rendez-vous au <L to='324'>324</L>), le briquet d'ambre (rendez-vous au <L to='77'>77</L>) ou la poudre d'argent (rendez-vous au <L to='64'>64</L>) ? Si vous préférez abandonner ce projet, retournez au <L to='177'>177</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P563);
