/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P281 extends Component {
    render() {
        return (
            <div>

               La procession silencieuse pénètre bientôt dans un sanctuaire aux parois composées d'ossements. Vous y entrez à leur suite et cherchez un coin sombre. Le lieu est éclairé depuis le plafond. En levant les yeux, vous constatez que la lumière est prodiguée par l'aura d'un Faltyn. La pauvre créature est enchaînée à l'intérieur d'une cage de fer. Les porteurs viennent de déposer la civière devant un autel. Celui qui ouvrait la marche sort alors un curieux symbole d'argent et le pose sur la poitrine de votre Double. Le temps est venu d'intervenir. Si vous désirez utiliser un objet, rendez-vous au <L to='124'>124</L>. Si vous préférez attaquer les hommes, rendez-vous au <L to='516'>516</L>. Si vous jugez plus prudent de rebrousser chemin, rendez-vous au <L to='547'>547</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P281);
