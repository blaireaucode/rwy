/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P167 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Quelque chose vous gêne dans ce décor, quelque chose d'inhabituel... Vous froncez les sourcils, d'un air agacé. Jetez deux dés. Si le résultat est inférieur ou égal à votre total d'HABILETÉ, rendez-vous au <L to='63'>63</L>. Si le résultat est supérieur, rendez-vous au <L to='382'>382</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P167);
