/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P335 extends Component {
    render() {
        return (
            <div>

               Vous vous penchez sur lui et tentez de comprendre ses dernières paroles. « Ils étaient venus pour moi, murmure-t-il. Je suis trop vieux, je n'aurais pas pu continuer longtemps... » Un filet de sang s'échappe de ses lèvres, mais il trouve assez de force pour s'emparer de son havresac. Il en sort un objet enveloppé dans une toile violette. Il s'agit d'un magnifique fourreau d'or martelé, incrusté de pierreries et d'émaux. « Les Cinq gagnent du pouvoir, reprend-il d'une voix faible. Bientôt, ils seront de retour sur terre... Seule l'Épée de Légende saura les arrêter. Rassemblez ses fragments. Le roi-sorcier de Wyrd possède le pommeau, mais j'ignore où se trouve la lame... » Soudain, son regard s'illumine. « Ils voulaient le fourreau et maintenant qu'il est en votre possession, les agents des Cinq vont se lancer à votre poursuite... Ne l'abandonnez jamais, il est votre protection... Rendez-vous au Royaume de Wyrd, c'est votre seul espoir... » Le pauvre a épuisé ses dernières forces afin que vous puissiez poursuivre sa quête. Il retombe sur le sol, ses yeux vides fixés sur le fourreau. C'est maintenant le cadavre d'un vieillard que vous contemplez... Rendez-vous au <L to='159'>159</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P335);
