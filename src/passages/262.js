/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P262 extends Component {
    render() {
        return (
            <div>

               Vous avalez une gorgée et vous vous sentez investi d'une vigueur nouvelle : vous récupérez jusqu'à 2 dés d'ENDURANCE (sans excéder votre total de départ). Il reste assez de liquide dans la bouteille pour une autre gorgée. N'importe quel membre de votre groupe (vous y compris) pourra en bénéficier. Maintenant, retournez au paragraphe que vous avez quitté. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P262);
