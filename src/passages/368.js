/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P368 extends Component {
    render() {
        return (
            <div>

               VOLEUR: La vitesse ahurissante de ce torrent pourrait bien jouer en votre faveur. Si elle vous atteint, la vague de sang vous emportera, mais si vous parvenez à bondir par-dessus, vous êtes sauvé... Jetez deux dés : si le résultat est inférieur ou égal à votre total d'HABILETÉ, vous vous élancez par-dessus le flot d'ossements et de sang, vous rétablissant d'une roulade. Si le résultat est supérieur, hélas, hélas, vous êtes balayé hors du pont !... Rendez-vous au <L to='430'>430</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P368);
