/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P35 extends Component {
    render() {
        return (
            <div>

               Dans la bâtisse, une assemblée de silhouettes fantomatiques glisse en silence sur le sol, au rythme d'une mélodie que vous ne pouvez entendre. Progressivement, ils prennent conscience de votre intrusion et tous les regards se tournent vers vous. Vous avancez de quelques pas. Les danseurs disparaissent et vous vous retrouvez dans une salle envahie de toiles d'araignée. Des cris à peine perceptibles s'élèvent alors d'un autre endroit de la bâtisse, accompagnés d'une musique discordante. Une lourde porte de pierre se détache au fond de la salle. Elle porte cette inscription : « VOICI LA FIN DE TON VOYAGE. APRÈS UNE VIE DE FIÈVRE ET DE TOURMENTS, SAVOURE LE REPOS. » Vous vous appuyez de tout votre poids sur la porte, qui s'ouvre en grinçant. Rendez-vous au <L to='428'>428</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P35);
