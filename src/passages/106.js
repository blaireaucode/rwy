/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P106 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous usez de vos perceptions extrasensorielles. Curieusement, les marchands ne sont pas endormis. Ils simulent le sommeil mais leurs pensées sont actives et ils semblent sous le coup d'émotions incontrôlées. Vous commencez à avoir des doutes sur leur véritable nature... Rendez-vous au <L to='468'>468</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P106);
