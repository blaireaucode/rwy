/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P417 extends Component {
    render() {
        return (
            <div>

               Vous montez à bord du Lacodon. L'équipage vous regarde d'un air sombre, mais sans cesser pour autant son ouvrage. Le capitaine vient vers vous. Son visage buriné a l'apparence d'un vieux pruneau cuit. Il vous toise, une lueur étrange dans le regard. « Bien ! dit-il d'une voix rauque. Vous faites l'affaire. Suivez-moi, nous allons signer votre engagement. » D'un signe de tête, il désigne sa cabine et vous lui emboîtez le pas. Avec un geste grandiloquent, il vous cède le passage. « Dans la cale, nous conservons la viande, dit-il. Non sans avoir enlevé la graisse et salé le tout. Vous apprendrez  vite... » Si vous avez affronté précédemment Jadhak le balafré et que vous avez fui, rendez-vous au <L to='250'>250</L>. Si vous ne l'avez jamais rencontré ou si vous l'avez tué, rendez-vous au <L to='473'>473</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P417);
