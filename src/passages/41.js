/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P41 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : La créature étend ses bras décharnés en direction de votre gorge mais au dernier moment, elle s'écroule à vos pieds ! Vous titubez un court instant, épuisé par votre effort de concentration, avant de poser un regard sur les restes du monstre. Rendez-vous au <L to='303'>303</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P41);
