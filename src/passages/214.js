/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P214 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous observez le déroulement de la partie. Au bout de quelques minutes, vous avouez n'y rien comprendre. Ce jeu n'a que peu de rapports avec les échecs krarthiens, tels que vous les connaissez. Si vous désirez user de votre clairvoyance, rendez-vous au <L to='348'>348</L>. Si vous préférez attendre la suite des événements, rendez-vous au <L to='170'>170</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P214);
