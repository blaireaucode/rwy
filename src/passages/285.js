/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P285 extends Component {
    render() {
        return (
            <div>

               Ce qui suit ne concerne que le joueur qui monte la garde. La Lune Bleue éclaire la cime des arbres d'une lueur fantomatique, donnant aux branches l'aspect inquiétant de membres torturés. Soudain, une créature jaillit du sous-bois. Vous portez la main à votre arme. Ce n'était qu'un lapin ! Si vous désirez réveiller vos compagnons, rendez-vous au <L to='179'>179</L>. Si vous préférez explorer le coin d'où venait le lapin, rendez-vous au <L to='388'>388</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P285);
