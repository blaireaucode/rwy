/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P293 extends Component {
    render() {
        return (
            <div>

               « Vous devrez donc travailler à bord, déclare le capitaine. Ne vous inquiétez pas, vous apprendrez vite à manier le harpon. » Rendez-vous au <L to='247'>247</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P293);
