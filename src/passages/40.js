/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P40 extends Component {
    render() {
        return (
            <div>

               Les parois de la pièce sont recouvertes de mains décharnées, figées dans des gestes de supplication. Un malaise incontrôlable s'empare de vous. Si un magicien désire jeter un sortilège d'Oracle, rendez-vous au <L to='305'>305</L>. Si vous préférez avancer dans la pièce malgré vos appréhensions, rendez-vous au <L to='160'>160</L>. Si vous jugez plus prudent de rebrousser chemin, rendez-vous au <L to='547'>547</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P40);
