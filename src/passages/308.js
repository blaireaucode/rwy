/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P308 extends Component {
    render() {
        return (
            <div>

               Instantanément, vous vous retrouvez sur l'autre rive du lac. La créature s'arrête, vous cherchant du regard. Vous vous concentrez et jetez un sortilège de Foudre Noire sur la surface gelée. La glace se volatilise, entraînant le Squelette par le fond ! Seule sa cape noire surnage. Sans plus perdre de temps, vous vous enfoncez dans les bois. Rendez-vous au <L to='165'>165</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P308);
