/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P353 extends Component {
    render() {
        return (
            <div>

               Les marches s'arrêtent devant une porte de bronze. Vous la poussez et pénétrez dans une vaste salle au plafond bas. Les murs et le sol sont de marbre gris mais la voûte rougeoie comme un tapis de braises. Par instants, des paquets de cendres grises s'en détachent, se transformant en insectes dès qu'ils touchent le sol. Un autel de marbre noir se dresse au centre de la pièce. Une flamme brûle en son milieu. Comme vous vous approchez, des filaments naissent du marbre et s'enroulent en spiralant autour de la flamme, projetant d'étranges ombres sur les murs. Un sifflement strident commence à se faire entendre. Si votre groupe compte un prêtre, rendez-vous au <L to='105'>105</L>. Sinon, rendez-vous au <L to='380'>380</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P353);
