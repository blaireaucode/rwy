/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P312 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Le Faltyn se montre plus respectueux après que vous l'avez payé. « Traverse la pièce en n'empruntant que les dalles noires. Des cinq objets que tu trouveras dans le coffre, la sphère de Feu est la meilleure... » La créature disparaît. Suivant ses conseils, vous atteignez le coffre et vous soulevez le couvercle. Rendez-vous au <L to='294'>294</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P312);
