/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P423 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Sous l'effet de votre sortilège, les dalles blanches du sol se mettent à luire d'un éclat surnaturel. Vous avancez dans la pièce en les évitant soigneusement. Parvenu à proximité du coffre, vous en soulevez le couvercle... Rendez-vous au <L to='294'>294</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P423);
