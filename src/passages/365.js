/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P365 extends Component {
    render() {
        return (
            <div>

               Vous pressez le pas en direction des falaises grises qui bordent la côte de Wyrd. Soudain, un tremblement secoue la banquise ! Un gigantesque oeil rouge vous observe, déformé par l'épaisseur de glace. Huit tentacules jaillissent dans un craquement sinistre, fouettant l'air. Si vous désirez utiliser un objet, rendez-vous au <L to='549'>549</L>. Si vous préférez fuir vers la côte, rendez-vous au <L to='316'>316</L>. Si vous êtes prêt à affronter le monstre, rendez-vous au <L to='457'>457</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P365);
