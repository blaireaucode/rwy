/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P488 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Vous sortez la corne à boire, mais cet objet est inutile en pareilles circonstances. Une hallebarde s'envole et, d'un coup circulaire, vous arrache la corne des mains. L'objet disparaît au fond du gouffre (rayez-le de votre <Link to='/adv'>Feuille d'Aventure</Link>). Avant même que vous ayez pu réagir, la seconde hallebarde vous inflige 1 dé + 3 de Dommages. Si vous survivez, rendez-vous au <L to='100'>100</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P488);
