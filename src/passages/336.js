/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P336 extends Component {
    render() {
        return (
            <div>

               « Allez au diable, vous et vos petits secrets ! » grogne-t-il en s'éloignant. Le soleil s'est levé et le vent gonfle les voiles. Certains marins jouent aux osselets sur le pont. Dans le ciel, des mouettes survolent le bateau en criant. Vous remarquez soudain que Kénoy, le mousse, vous observe le sourire aux  lèvres. Si vous désirez l'appeler, rendez-vous au <L to='217'>217</L>. Si vous préférez rester accoudé au bastingage à regarder la mer, rendez-vous au <L to='510'>510</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P336);
