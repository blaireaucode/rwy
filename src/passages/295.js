/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P295 extends Component {
    render() {
        return (
            <div>

               Vous frappez à la lourde porte de chêne mais, en l'absence de réponse, vous poussez le panneau. « Qui est là ? » demande soudain une voix chevrotante. Si le prêtre désire user de ses dons de Clairvoyance, rendez-vous au <L to='436'>436</L>. Si le magicien désire détecter les sortilèges, rendez-vous au <L to='319'>319</L>. Si le voleur veut tenter quelque chose, rendez-vous au <L to='167'>167</L>. Sinon, rendez-vous au <L to='256'>256</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P295);
