/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P428 extends Component {
    render() {
        return (
            <div>

               L'étrange mélodie s'est tue. Vous venez de pénétrer dans une salle dont le plafond se perd dans la pénombre. Quatre bougies sont disposées en carré au centre de la pièce et la faible lueur qu'elles dispensent éclaire une porte. Les bougies sont reliées entre elles par un trait de poussière d'argent. L'intérieur du carré est rempli de symboles tracés à la craie. Vous pouvez prendre les bougies et la poudre d'argent (notez-les sur votre <Link to='/adv'>Feuille d'Aventure</Link>). Rendez-vous au <L to='543'>543</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P428);
