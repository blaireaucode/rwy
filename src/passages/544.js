/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P544 extends Component {
    render() {
        return (
            <div>

               « J'ai entendu dire que sa magie puise sa force dans ce qu'il lit dans nos rêves... Autrefois, Wyrd était un paradis joyeux. » Sa voix devient de plus en plus faible. « Maintenant notre pays et nos rêves sont gris... » Rendez-vous au <L to='59'>59</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P544);
