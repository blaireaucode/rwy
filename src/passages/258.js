/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P258 extends Component {
    render() {
        return (
            <div>

               Le monstre est au fond de la fosse ; tant que vous le surplombez, vous procédez à vos lancers avec 2 dés - 1. Le Squelette tente de s'extirper de la fosse. A chaque Assaut, il jette deux dés et parvient à ses fins s'il obtient un résultat inférieur ou égal à son total d'HABILETÉ. Il pourra à nouveau vous frapper quand il sera sorti.  SQUELETTE (Protection 2, Dommages 3 dés) FORCE : 8 HABILETÉ : 8 POUVOIR : 8 ENDURANCE: 31 Si vous l'avez blessé précédemment, il a recouvré toutes ses forces. Son total d'ENDURANCE est à nouveau à son niveau de départ. Si vous parvenez à le détruire, rendez-vous au <L to='303'>303</L>. Si vous préférez fuir, rendez-vous au <L to='204'>204</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P258);
