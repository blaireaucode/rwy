/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P107 extends Component {
    render() {
        return (
            <div>

               CHEVALIER : Vous ne possédez aucun pouvoir qui vous permettrait de sortir de cette fâcheuse situation. Aussi, rassemblant votre courage, vous bondissez de dalle en dalle jusqu'au milieu du pont. Maintenant les crânes étincelants foncent sur vous, cherchant à vous déséquilibrer ! Si vous désirez les combattre, rendez-vous au <L to='476'>476</L>. Si vous préférez poursuivre votre course éperdue, rendez-vous au <L to='87'>87</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P107);
