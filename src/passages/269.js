/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P269 extends Component {
    render() {
        return (
            <div>

               « Derrière toi, bigleux ! » grogne-t-il, ponctuant sa phrase d'un crachat sur vos bottes. Il s'éloigne en maugréant. Vous vous retournez et découvrez l'enseigne d'une auberge appelée Les Os d'Ulrik. Si vous désirez y entrer, rendez-vous au <L to='17'>17</L>. Si vous préférez continuer en direction des docks et de Port Lany, rendez-vous au <L to='306'>306</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P269);
