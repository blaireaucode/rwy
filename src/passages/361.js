/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P361 extends Component {
    render() {
        return (
            <div>

               Si vous possédez le plateau et les pièces de l'échiquier, rendez-vous au <L to='384'>384</L>. Sinon, rendez-vous au <L to='99'>99</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P361);
