/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P32 extends Component {
    render() {
        return (
            <div>

               Vous avancez vers le Vampire, le fourreau brandi au-dessus de votre tête. La créature tombe à genoux, le regard fixé sur les joyaux scintillants du fourreau. Elle semble pétrifiée et ne réagit pas à l'instant où vous posez le fourreau sur son front. Immédiatement, son épiderme perd ses reflets écarlates. Ses griffes et ses crocs disparaissent, tandis que l'expression de son visage passe de la haine à la stupeur. Vous retrouvez votre compagnon. Il a rejoint le royaume des vivants et ne semble avoir gardé aucun souvenir de sa métamorphose. Toutes ces péripéties vous ont ôté l'envie de dormir. Vous attendez patiemment que l'aube pointe à l'horizon. S'il vous reste des provisions, vous pouvez vous restaurer (rayez une ration de votre <Link to='/adv'>Feuille d'Aventure</Link>). Les joueurs qui ne mangent pas perdent 1 point d'ENDURANCE. Rendez-vous au <L to='422'>422</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P32);
