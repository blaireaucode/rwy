/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P126 extends Component {
    render() {
        return (
            <div>

               Tout en ressassant les propos de la vieille femme, vous vous dirigez vers le centre de la clairière afin d'établir votre campement pour la nuit. Le soleil disparaît derrière la cime des arbres. Quelques jours auparavant, vous avez rejoint une caverne, car il est dangereux de s'aventurer seul dans les vastes forêts du sud-est de Krarth. Les marchands et les pèlerins s'affairent, plantent des tentes et allument des feux. Non loin de votre campement, une famille de forestiers fait rôtir un jeune chevreuil. Alléché par l'arôme, vous vous approchez et liez connaissance. Comme il est de coutume entre gens du voyage, ils vous invitent à partager leur repas. Après vous être rassasié, vous décidez d'une petite promenade digestive. Bientôt, les notes mélanco liques d'une complainte parviennent à vos oreilles. Un homme vêtu d'une tunique et d'un pantalon de lin blanc caresse les cordes d'une lyre. Ses traits sont empreints de fierté et sa musique n'est en rien comparable aux joyeuses mélodies que jouent d'ordinaire les ménestrels. Non loin de lui, une troupe de marchands, engoncés dans de lourds manteaux, observent avec attention deux hommes de haute stature qui disputent une partie d'échecs. Les joueurs sont emmitouflés dans d'épaisses fourrures bleu nuit. Vous observez la scène un court instant. Les marchands semblent captivés par la partie. Étrange, les échecs Krarthiens sont d'une extrême complexité et,  d'ordinaire, la classe des marchands n'entend rien aux subtilités de ce jeu. Si vous désirez vous approcher du ménestrel afin de mieux profiter de sa musique, rendez-vous au <L to='502'>502</L>. Si vous préférez suivre la partie d'échecs, rendez-vous au <L to='554'>554</L>. Si vous jugez plus sage de retourner à votre bivouac, rendez-vous au <L to='486'>486</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P126);
