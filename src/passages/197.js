/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P197 extends Component {
    render() {
        return (
            <div>

               Soudain, votre compagnon se lève et jette au loin le linceul qui le recouvrait (vous pouvez lui rendre sa <Link to='/adv'>Feuille d'Aventure</Link>). Il ne se souvient de rien, et la lame qui lui a défoncé le torse n'a laissé aucune blessure ! Maintenant qu'il a connu la mort et qu'il en est revenu, il combattra sans peur. Son total de départ de FORCE augmente de 1 point... Il porte encore la lame sacrificielle d'argent bruni. Une inscription y est gravée : Pourfendeuse d'Ombre. Au sol, vous trouvez aussi l'amulette en forme de tête de mort. Notez ces objets sur votre <Link to='/adv'>Feuille d'Aventure</Link> si vous les conservez. Rendez-vous au <L to='174'>174</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P197);
