/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P536 extends Component {
    render() {
        return (
            <div>

               Vous avancez d'un pas, mais Auguste soutient votre regard. « Ne soyez pas stupide ! s'exclame-t-il.Nous sommes en plein ciel, vous semblez l'oublier. Sans moi, vous êtes perdu ! » Pour illustrer ses dires, le tapis volant fait une embardée, manquant de vous faire chuter ! Si le chevalier désire intervenir, rendez-vous au <L to='378'>378</L>. Sinon, vous n'avez d'autre choix que de l'attaquer (rendez-vous au <L to='522'>522</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P536);
