/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P523 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Vous vous téléportez de l'autre côté du pont. Si vos compagnons n'ont pas encore traversé, ils doivent retourner au 226. Si vous êtes seul ou si les tentatives de vos compagnons se sont soldées par un échec, rendez-vous au <L to='350'>350</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P523);
