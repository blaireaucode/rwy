/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P155 extends Component {
    render() {
        return (
            <div>

               MAGICIEN: « Hors de question ! vous exclamez-vous. Je ne peux pas disposer ainsi du fourreau ! Demande autre chose ou disparais. » Le Faltyn sursaute. « Fort bien, dit-il. Je me contenterai d'une gorgée de ton sang. » Si vous acceptez, ôtez-vous 1 point d'ENDURANCE et rendez-vous au <L to='264'>264</L>. Si vous refusez, la créature se volatilise (rendez-vous au <L to='559'>559</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P155);
