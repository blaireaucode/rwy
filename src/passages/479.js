/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P479 extends Component {
    render() {
        return (
            <div>

               « Le Feu » n'était pas la bonne réponse ! La créature sort ses griffes et s'élance à l'attaque ! Rendez-vous au <L to='69'>69</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P479);
