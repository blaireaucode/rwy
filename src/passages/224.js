/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P224 extends Component {
    render() {
        return (
            <div>

               Auguste est sous l'emprise de votre sortilège, mais vous devez faire vite... Vous lui intimez l'ordre d'atterrir. Le tapis se pose doucement sur la banquise. Auguste reprend ses esprits mais, d'un coup d'épée en travers de la gorge, vous mettez un terme définitif à ses agissements ! En le fouillant, vous découvrez un anneau d'or, une amulette blanche et 15 Pièces d'Or. Prenez ce que vous voulez ; quant au tapis, il compte pour trois articles. Rendez-vous au <L to='344'>344</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P224);
