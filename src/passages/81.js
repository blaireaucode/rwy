/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P81 extends Component {
    render() {
        return (
            <div>

               Vous poursuivez votre route et établissez votre campement à l'abri d'une colline qui vous masque du village. Vous pouvez de cette manière allumer un feu sans crainte d'être repéré. Le sommeil vous gagne, mais il est peuplé de rêves étranges. Le lendemain matin, vous vous réveillez, l'esprit embrumé de sensations malsaines. Vous ranimez le feu et vous vous restaurez. Les joueurs qui ont dormi dans un sac de couchage récupèrent 1 point d'ENDURANCE ; ceux qui n'avaient pas de sac perdent 1 point. Si vous vous êtes restauré, n'oubliez pas de rayer une ration sur votre <Link to='/adv'>Feuille d'Aventure</Link>. Ce repas vous rend aussi 1 point d'ENDURANCE. Rendez-vous au <L to='520'>520</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P81);
