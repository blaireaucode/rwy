/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P421 extends Component {
    render() {
        return (
            <div>

               « Préparez-vous à mourir ! » hurle-t-elle dans vos têtes. Au même instant, les armures vides qui se tenaient de part et d'autre du trône s'emparent de leurs lances et s'avancent d'un pas mécanique. ARMURE (Protection 5, Dommages 3 dés) FORCE : 7 HABILETÉ : 5 POUVOIR : 9 ENDURANCE : 21  Varadaxor ne peut pas se déplacer. La Dame Grise non plus, mais elle conserve l'usage de ses pouvoirs surnaturels. DAME GRISE (Protection 4) POUVOIR : 9 ENDURANCE :  20  Si vous jetez un sortilège à la Dame Grise, rendez-vous au <L to='514'>514</L>. Si vous parvenez à la tuer, rendez-vous au <L to='9'>9</L>. Si vous choisissez de fuir, rendez-vous au <L to='193'>193</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P421);
