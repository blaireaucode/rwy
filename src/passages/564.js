/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P564 extends Component {
    render() {
        return (
            <div>

               Vous repoussez gentiment les femmes qui entourent le berceau. Le bébé est atrocement brûlé et tremble de tous ses membres. La jeune fille avait raison : il ne survivra pas longtemps. Heureusement, vous pouvez intervenir. Vous appliquez le baume sur les régions brûlées, qui se régénèrent instantanément ! Les femmes poussent des cris de stupéfaction. Le bébé vous regarde un instant et entame un joyeux babillage. « C'est un miracle ! s'exclame une vieille. Vous connaissez donc les secrets des dieux. » Vous esquissez un sourire. « L'usage de la magie est habituel dans les contrées d'où nous venons, répondez-vous. Vous n'avez donc pas de magicien à Wyrd  ? » Un silence pesant s'abat sur l'assemblée. « Seul le roi-sorcier possède de tels pouvoirs », déclare un homme d'une voix sombre. Aussitôt, tout le village se met à parler des affaires quotidiennes, comme s'ils voulaient chasser de leur esprit la référence faite à leur monarque. Rendez-vous au <L to='341'>341</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P564);
