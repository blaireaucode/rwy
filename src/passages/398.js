/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P398 extends Component {
    render() {
        return (
            <div>

               A l'instant où vous poussez la porte, des torches s'allument sur les parois, révélant un sol recouvert de dalles noires et blanches, à la manière d'un échiquier. Un coffre de bronze repose sur une estrade, au centre de la pièce. Si le magicien désire tenter quelque chose, rendez-vous au <L to='192'>192</L>. Si le prêtre désire intervenir, rendez-vous au <L to='494'>494</L>. Si le chevalier et le voleur veulent agir, rendez-vous au <L to='569'>569</L>. Sinon, vous pouvez marcher vers le coffre (rendez-vous au <L to='331'>331</L>) ou bien quitter la pièce et ouvrir l'autre porte (rendez-vous au <L to='200'>200</L>). Si vous préférez quitter la tour sans attendre, rendez-vous au <L to='406'>406</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P398);
