/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P142 extends Component {
    render() {
        return (
            <div>

               Vous retournez vous coucher et vous vous enveloppez dans vos couvertures, rendues humides par le brouillard qui s'est levé sur la clairière. Épuisé par la bataille, vous sombrez dans un profond sommeil... Vous revivez en rêve les événements des heures précédentes. Vous survolez le champ de bataille jonché de cadavres. Des lambeaux de brume se déplacent en serpentant, laissant dans leur sillage des dizaines de champignons, jaillissant telles des bulles à la surface du sol. Soudain, le rêve devient cauchemar... Les orbites des cadavres s'illuminent, reflétant les sinistres rayons de la Lune Bleue. Les corps sont animés de mouvements désordonnés et se lèvent lentement. Les cadavres se regroupent autour de vous. Vous devez vous réveiller ou mourir, mais le pouvez-vous encore ? Rendez-vous au <L to='268'>268</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P142);
