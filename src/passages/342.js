/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P342 extends Component {
    render() {
        return (
            <div>

               Le panneau coulisse à la mort du monstre, permettant au groupe de se reformer et de poursuivre sa progression dans le couloir. Rendez-vous au <L to='219'>219</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P342);
