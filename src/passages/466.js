/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P466 extends Component {
    render() {
        return (
            <div>

               A l'écoute de vos propos, le visage de Bildad devient livide. « Par tous les dieux ! s'exclame-t-il, horrifié. Le capitaine est fou ! Chasser le Serpent de Mer ! » Il s'élance sur le pont, appelant à lui tous les marins. Un attroupement se forme et vous pouvez lire l'angoisse sur tous les visages, à mesure qu'ils apprennent la nouvelle. Rendez-vous au <L to='231'>231</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P466);
