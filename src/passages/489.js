/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P489 extends Component {
    render() {
        return (
            <div>

               Le thuriféraire prend conscience de votre présence. Aussitôt, il lâche son encensoir et prend la fuite. Ne voyant aucun intérêt à le poursuivre, vous vous approchez de l'objet, qui continue à libérer sa fumée sur le sol. Vous pouvez le prendre. Rendez-vous au <L to='493'>493</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P489);
