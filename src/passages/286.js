/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P286 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Ashanax est le saint patron des voyageurs égarés. Fort de ce renseignement, rendez-vous au <L to='444'>444</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P286);
