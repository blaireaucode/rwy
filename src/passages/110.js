/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P110 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Malgré tous vos efforts, vous ne pouvez localiser la source de son pouvoir. Mais le Squelette a pris conscience de votre présence. Il pose sur vous son regard enflammé. Si vous choisissez de fuir, rendez-vous au <L to='492'>492</L>. Si vous préférez combattre, rendez-vous au <L to='276'>276</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P110);
