/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P103 extends Component {
    render() {
        return (
            <div>

               L'aubergiste perd tout à coup son air jovial et, d'un signe de tête, vous désigne l'étable qui jouxte la salle commune. Vous vous endormez dans la paille. Grâce à ce repos, chaque joueur blessé récupère 1 point d'ENDURANCE. Rendez-vous au <L to='245'>245</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P103);
