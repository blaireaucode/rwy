/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P332 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Le Faltyn apparaît bientôt en sifflotant. «Je dispose de trois renseignements concernant cette salle. Pour le premier, je ne te demanderai que 5 Pièces d'Or, mais pour les deux autres, je préfère des objets. Tu as sûrement glané quelques curiosités au long de ton voyage... » Si vous refusez cette proposition, retournez au <L to='398'>398</L>. Si vous acceptez de payer 5 Pièces d'Or, mais refusez de vous séparer d'objets, rendez-vous au <L to='535'>535</L>. Si vous acceptez d'offrir des objets, rayez ceux de votre choix de votre <Link to='/adv'>Feuille d'Aventure</Link>. Si vous désirez ne vous séparer que d'un objet, rendez-vous au <L to='312'>312</L>. Si vous donnez deux objets, rendez-vous au <L to='58'>58</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P332);
