/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P184 extends Component {
    render() {
        return (
            <div>

               La muraille tombe avec un bruit sourd, obstruant le couloir. Si tous les joueurs ont choisi de rebrousser chemin, rendez-vous au <L to='408'>408</L>. Si un ou plusieurs joueurs ont décidé d'affronter le monstre, rendez-vous au <L to='413'>413</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P184);
