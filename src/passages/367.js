/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P367 extends Component {
    render() {
        return (
            <div>

               Vous pénétrez dans une salle dont le sol est recouvert de feuilles mortes. A votre approche, une tornade se forme, rassemblant les feuilles à l'image d'un géant. C'est le Gardien de la Mémoire... Si vous possédez le briquet d'ambre ou la sphère de Feu, vous pouvez embraser la créature. Elle disparaîtra aussitôt. Si vous employez le briquet d'ambre, vous pouvez le conserver, mais si vous employez la sphère de Feu, rayez-la de votre Feuille d'Aventure. Si vous ne possédez aucun de ces objets, la tornade de feuilles vous enveloppe. Chaque joueur perd le souvenir de ses actions passées et de son apprentissage et retrouve des capacités de Niveau 2... Si vous désirez poursuivre l'exploration et affronter l'ultime épreuve, rendez-vous au <L to='527'>527</L>. Si vous préférez rebrousser chemin, il vous faudra rencontrer une nouvelle fois les Serviteurs du Néant (rendez-vous au <L to='188'>188</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P367);
