/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P252 extends Component {
    render() {
        return (
            <div>

               Vous pénétrez dans les appartements d'Auguste. Dans ses armoires, vous trouvez de nombreuses capes de fourrure et d'épaisses couvertures. Tout cela vous sera utile pour braver le climat de Wyrd. Rendez-vous au <L to='470'>470</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P252);
