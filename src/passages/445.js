/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P445 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous n'avez aucune idée de la manière de contrôler le tapis, mais peut-être vos pouvoirs permettront-ils de résoudre ce problème. Si vous désirez tenter la Lévitation, rendez-vous au <L to='333'>333</L>. Si vous préférez l'Exorcisme, rendez-vous au <L to='458'>458</L>. Si aucune de ces options ne vous satisfait, retournez au <L to='133'>133</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P445);
