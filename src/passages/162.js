/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P162 extends Component {
    render() {
        return (
            <div>

               Chemin faisant, la sensation de malaise se transforme en une frayeur irraisonnée. Quelque chose ou quelqu'un vous suit, vous en êtes persuadé ! Malgré la fatigue, vous pressez le pas et atteignez bientôt les eaux calmes d'une rivière. Le cours d'eau vous mène à une clairière, aux abords d'une fermette isolée. Une fenêtre est éclairée. Si vous désirez demander asile, rendez-vous au <L to='295'>295</L>. Si vous préférez poursuivre votre route, rendez-vous au <L to='151'>151</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P162);
