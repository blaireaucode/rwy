/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P74 extends Component {
    render() {
        return (
            <div>

               CHEVALIER : La vague déferlante sera sur vous dans quelques secondes, mais vous n'allez pas vous laisser emporter comme un fétu de paille ! Vous vous accroupissez et empoignez fermement le bastingage. Un torrent de sang et d'os vous submerge... Vous devez obtenir un résultat inférieur ou égal à votre total d'ENDURANCE pour ne pas être entraîné au fond du ravin. Si vous manquez ce lancer, vous chutez dans le vide... Rendez-vous au <L to='430'>430</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P74);
