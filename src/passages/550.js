/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P550 extends Component {
    render() {
        return (
            <div>

               Vous attendez, comme dans un rêve, le coup fatal. En un éclair, vous voyez votre Double abattre son épée et... disparaître. Votre trouble cède bientôt la place à une immense sérénité. Vous avez affronté votre peur de la mort et vous avez vaincu. Votre total de départ de FORCE augmente de 1 point. Vous tenez maintenant l'épée de votre Double. La lame en argent bruni porte une inscription gravée « Pourfendeuse d'Ombre ». Vous pouvez la garder. A vos pieds, vous remarquez une amulette en forme de tête de mort. Vous pouvez la prendre, elle aussi. Rendez-vous au <L to='174'>174</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P550);
