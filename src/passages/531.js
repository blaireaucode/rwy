/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P531 extends Component {
    render() {
        return (
            <div>

               L'aubergiste revient avec une cruche de vin chaud aux épices et des tourtes à la viande au fumet alléchant. Ce délicieux repas vous redonne 2 points d'ENDURANCE. Rendez-vous au <L to='537'>537</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P531);
