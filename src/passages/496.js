/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P496 extends Component {
    render() {
        return (
            <div>

               Vous prenez congé de l'aubergiste et partez d'un bon pas. Bientôt vous laissez derrière vous les murailles grises de Dourhaven. L'air est vif et vos bottes crissent dans la neige fraîche. En fin d'après midi, vous atteignez la montagne décrite par l'aubergiste. Vous n'avez aucune envie de passer la nuit sur la banquise, aussi établissez-vous un bivouac au pied d'un rocher. Quand la nuit tombe, vous vous endormez. Vers minuit, une étrange mélodie vous tire de votre sommeil. Au loin, à l'est, trois silhouettes couleur de rubis dansent dans la neige. Si vous désirez les approcher, rendez-vous au <L to='201'>201</L>. Si vous préférez vous rendormir, rendez-vous au <L to='91'>91</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P496);
