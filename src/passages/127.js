/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P127 extends Component {
    render() {
        return (
            <div>

               Le Démon hoche lentement la tête : « Vous avez trouvé la bonne réponse. Ouvrez la porte de gauche. » Si vous désirez suivre son avis, rendez-vous au <L to='398'>398</L>. Si vous préférez ouvrir la porte de droite, rendez-vous au <L to='200'>200</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P127);
