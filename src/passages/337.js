/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P337 extends Component {
    render() {
        return (
            <div>

               Les Sylphes sont des créatures aériennes qui ne peuvent être atteintes par les armes de jet. Rayez une flèche. Vous devez maintenant choisir celui qui traversera le premier : le magicien (rendez-vous au <L to='509'>509</L>), le voleur (rendez-vous au <L to='392'>392</L>), le prêtre (rendez-vous au <L to='210'>210</L>) ou le chevalier (rendez-vous au <L to='463'>463</L>) ?

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P337);
