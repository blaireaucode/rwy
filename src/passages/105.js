/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P105 extends Component {
    render() {
        return (
            <div>

               PRÊTRE : Vous reconnaissez le rituel qui précède l'apparition de l'Umborus Noir, Démon de l'ombre doté de pouvoirs effrayants. Les ombres sur les parois se rassemblent pour former une vague silhouette. Vous devez absolument interrompre ce rituel. Si vous désirez employer la sphère de Feu, rendez-vous au <L to='495'>495</L>. Si vous préférez utiliser une bouteille de lait, rendez-vous au <L to='101'>101</L>. Si vous ne possédez aucun de ces objets, rendez-vous au <L to='380'>380</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P105);
