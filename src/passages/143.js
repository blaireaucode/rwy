/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P143 extends Component {
    render() {
        return (
            <div>

               Vous vous approchez du vieux chevalier et engagez la conversation. Il vous conte son histoire : « Il fut une époque où j'étais moi aussi jeune et vigoureux. Jamais mon bras ne faiblissait. Mais le temps est toujours le plus fort. Maintenant, moi, Varadaxor, chevalier de Luzon, je suis réduit à attendre mon heure dernière sans avoir pu assouvir ma vengeance. Et j'enrage, car je sais que la Dame Grise demeurera impunie de ses crimes... » Si vous  désirez en savoir plus, rendez-vous au <L to='301'>301</L>. Si vous préférez briser là et reprendre votre route, rendez-vous au <L to='116'>116</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P143);
