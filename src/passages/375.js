/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P375 extends Component {
    render() {
        return (
            <div>

               Vous attendez, comme dans un rêve, que tombe le coup fatal. En un éclair, votre Double abat son épée et... disparaît. Vous avez affronté votre peur de la mort et vous avez vaincu. Votre total de départ de FORCE augmente de 1 point. Vous tenez maintenant l'épée de votre Double. La lame en argent bruni porte une inscription gravée : « Pourfendeuse d'Ombre ». Vous pouvez la garder si vous le désirez. Soudain, un hurlement s'élève dans le lointain. Peut-être une créature s'inquiète-t-elle du retard de la procession ? Vous n'avez aucune envie de le savoir et vous rebroussez chemin. Rendez-vous au <L to='547'>547</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P375);
