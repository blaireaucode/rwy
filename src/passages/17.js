/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P17 extends Component {
    render() {
        return (
            <div>

               Vous pénétrez dans l'auberge et aussitôt un nuage de fumée vous assaille. La salle est bondée et de nombreux marins fument la pipe. Vous repérez un groupe de prêtres au fond de la pièce, s'entretenant avec un vieil homme en costume de brocart. Accoudés au comptoir, une assemblée de marchands et de trappeurs discutent d'une voix forte. Si vous désirez aborder les prêtres, rendez-vous au <L to='83'>83</L>. Si vous préférez aller au bar et voir ce que les marchands ont à vendre, rendez-vous au <L to='358'>358</L>. Si vous préférez d'abord demander à l'aubergiste s'il reste des chambres, rendez-vous au <L to='515'>515</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P17);
