/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P482 extends Component {
    render() {
        return (
            <div>

               Les heures passent. Vous ramez sur les eaux grises, sans avoir la moindre idée de votre destination. Lazarus est prostré dans un coin de la barque depuis que vous avez quitté le navire. Soudain, il bondit tel un diable et s'empare de l'ancre. « Misérable ! hurle-t-il. Tout est de votre faute ! J'ai perdu mon navire ! » LAZARUS (Dommages 1 dé + 2) FORCE : 8 HABILETÉ : 6 POUVOIR : 6 ENDURANCE: 14  Si vous remportez ce combat, rendez-vous au <L to='153'>153</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P482);
