/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P117 extends Component {
    render() {
        return (
            <div>

               Les côtes de Wyrd sont bordées de falaises escarpées. Une terre désolée s'étend à perte de vue. Quelque part, au nord, se dresse le Château de la Nuit Éternelle, demeure du roi-sorcier. Vous allez devoir affronter cet adversaire implacable ; c'est le prix qu'il faudra payer pour posséder le pommeau de l'Épée de Légende. Si votre groupe compte un prêtre, rendez-vous au <L to='209'>209</L>. Sinon, rendez-vous au <L to='534'>534</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P117);
