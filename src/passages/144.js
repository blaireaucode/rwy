/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P144 extends Component {
    render() {
        return (
            <div>

               Vous attendez plusieurs heures, emmitouflé dans votre couverture et ne dormant que d'un œil. Vous commenciez à douter de la validité de vos soupçons quand, soudain, la Lune Bleue apparaît dans le ciel, baignant la clairière d'une lumière métallique. Aussitôt, les deux joueurs d'échecs sont agités de tremblements ! Leurs mains se recouvrent d'une fourrure épaisse tandis que leur visage s'allonge jusqu'à prendre la forme d'une gueule de loup ! Bientôt, les créatures se dressent sur leurs quatre pattes et hurlent à la mort ! Au même instant, les marchands se lèvent tels des pantins et s'emparent de longs couteaux. Le visage déformé par un rictus, ils se ruent sur les voyageurs endormis, plongeant frénétiquement leurs lames dans les chairs. La panique s'empare du campement. Quelques pèlerins tentent maladroitement de se défendre à l'aide de bâtons tandis que les femmes et les enfants s'égaillent en hurlant de terreur dans les bois. Rendez-vous au <L to='119'>119</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P144);
