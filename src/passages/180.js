/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P180 extends Component {
    render() {
        return (
            <div>

               Le sifflement s'arrête, tandis qu'une épaisse fumée s'élève du point d'impact. Dans le brouillard, vous apercevez une pierre chauffée à blanc, qui se fissure bientôt, comme un neuf qui éclôt. Une silhouette se déploie. Vous voyez un Squelette revêtu d'une robe noire. Une flamme bleue brûle dans ses orbites vides. Il tourne la tête dans votre direction. Rendez-vous au <L to='276'>276</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P180);
