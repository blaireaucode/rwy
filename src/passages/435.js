/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P435 extends Component {
    render() {
        return (
            <div>

               MAGICIEN: Empoignant la pierre de touche, vous vous concentrez. Une énergie surnaturelle y est emprisonnée et vous vous employez à désactiver la barrière qui la contient. Soudain, dans un fracas de tonnerre, une silhouette d'un bleu éclatant apparaît tandis que la pierre, réduite en cendres, est emportée par le vent. « Si tu veux atterrir, je m'y emploierai, déclare le Djinn en vous saquant. J'étais prisonnier de cette pierre depuis cent mille jours... » La créature écarte les bras et, dirigeant les vents à sa guise, pose le tapis en toute sécurité sur la banquise. Le Djinn vous salue à nouveau et s'envole. Vous êtes brusquement pris de vertiges. En libérant le Djinn, vous l'avez aussi nourri de votre propre énergie. Votre total de départ de POUVOIR perd 1 point. Rendez-vous au <L to='344'>344</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P435);
