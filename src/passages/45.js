/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P45 extends Component {
    render() {
        return (
            <div>

               La procession silencieuse pénètre bientôt dans un sanctuaire dont les parois sont composées d'ossements humains ! Vous y entrez à leur suite et cher chez aussitôt un coin d'ombre. Mis à part les cierges que portent ces étranges personnages, le lieu est éclairé depuis le plafond. En levant les yeux, vous constatez que la lumière est prodiguée par l'aura d'un Faltyn. La pauvre créature est pendue la tête en bas, dans une cage de fer. Les porteurs viennent de déposer la civière devant un autel. Celui qui ouvrait la marche sort alors un curieux symbole d'argent et le pose sur la poitrine de votre compagnon inanimé. Le temps est venu d'intervenir. Si vous désirez utiliser un objet, rendez-vous au <L to='448'>448</L>. Si vous préférez attaquer, rendez-vous au <L to='202'>202</L>. Si vous jugez plus prudent de rebrousser chemin, rendez-vous au <L to='152'>152</L>. Vous pouvez aussi attendre la suite des événements (rendez-vous au <L to='567'>567</L>).

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P45);
