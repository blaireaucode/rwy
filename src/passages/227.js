/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P227 extends Component {
    render() {
        return (
            <div>

               Vous avancez vers les Vampires, le fourreau brandi au-dessus de votre tête. Les créatures tombent à genoux, le regard fixé sur les joyaux scintillants du fourreau. Elles semblent pétrifiées et ne réagissent pas quand vous leur posez tour à tour le fourreau sur le front. Immédiatement, leur épiderme perd ses reflets écarlates, leurs griffes et leurs crocs disparaissent, et l'expression de leur visage passe de la haine à la stupeur. Vos compagnons ont rejoint le royaume des vivants et semblent n'avoir gardé aucun souvenir de leur métamorphose. Toutes ces péripéties vous ont ôté l'envie de dormir. Vous attendez que l'aube pointe. S'il vous reste des provisions, vous devez vous restaurer (rayer une ration de votre Feuille d'Aventure) pour ne pas perdre 1 point d'ENDURANCE. Rendez-vous au <L to='422'>422</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P227);
