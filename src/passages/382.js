/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P382 extends Component {
    render() {
        return (
            <div>

               VOLEUR : Vous ne pouvez expliquer les raisons de votre malaise, mais vous demeurez sur vos gardes, comme la porte s'ouvre, révélant une silhouette assise dans la pénombre. Rendez-vous au <L to='256'>256</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P382);
