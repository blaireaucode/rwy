/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P179 extends Component {
    render() {
        return (
            <div>

               Vous réveillez vos compagnons. Au même instant, vous percevez des craquements secs et un bruissement de feuillage... Rendez-vous au <L to='521'>521</L>.

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P179);
