/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P415 extends Component {
    render() {
        return (
            <div>

               MAGICIEN : Quel sortilège allez-vous employer la Main du Diable (rendez-vous au <L to='173'>173</L>), l'œil du Tigre (rendez-vous au <L to='82'>82</L>) ou la Téléportation (rendez-vous au <L to='215'>215</L>) ?

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P415);
