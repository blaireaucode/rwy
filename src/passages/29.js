/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../default_props.js';

class P29 extends Component {
    render() {
        return (
            <div>

               Vous prenez votre élan et, d'un bond, plongez dans les eaux glaciales. Vous remontez aussitôt à la surface, cherchant votre souffle, quand un harpon meurtrier passe à quelques centimètres de votre tête. Les marins vous prennent pour cible ! Soudain, une ombre plane au-dessus de vous et vous constatez que les marins s'arrêtent, stupéfaits. Un tapis volant, transportant un homme en robe violette, survole les flots ! L'inconnu vous tend la main et vous hisse à ses côtés. « Le tapis d'Auguste est idéal pour une fuite précipitée », dit-il en souriant. Sans plus tarder, il lance un ordre et le tapis s'envole dans les airs, sous les hurlements des marins. Rendez-vous au <L to='424'>424</L>. 

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(P29);
