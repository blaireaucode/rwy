
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

var fs = require('fs');

// read passages (570)
var max_p = 570;
var n = 1;
var graph = {};

while (n<=max_p) {
    graph[n.toString()] = { i:[], o:[] }; // input output
    n = n+1;
}

n = 1;
while (n<=max_p) {
    // read file
    var fn = './src/passages/'+n+'.js';
    const data = fs.readFileSync(fn, 'utf-8');
    // get output
    var reg = /to='([0-9]*)'/g;
    var result = reg.exec(data);
    const num = n.toString();
    while (result) {
        // console.log('ici', num, result[1]);
        graph[num].o.push(result[1]);
        graph[result[1]].i.push(num);
        result = reg.exec(data);
    }
    n = n+1;
}

var final = [];
var queue = ['1'];
var depth = 0;

// breadth-first search algorithm
function BFS(node) {

    var queue=[node];
    var n;
    graph[node].depth = 0;

    while(queue.length>0) {
        n = queue.shift(); // remove first
        if (graph[n].done === true) continue;
        const d = graph[n].depth+1;
        console.log('n', n, queue.length, graph[n]);
        if (graph[n].o.length == 0) {
            console.log('n no output', n, graph[n]);
            continue;
        }
        for (var i of graph[n].o) {
            if (graph[i].done === true){
                console.log('already done', n);
            }
            else {
                console.log('push', i, graph[i]);
                graph[i].depth = d;
                queue.push(i);
            }
        }
        graph[n].done = true;
    }
}

BFS(1);

// console.log(graph);
var g = JSON.stringify(graph);
fn = './src/passages/graph-path.json';
fs.writeFileSync(fn, g);
// console.log(g);
