/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React from 'react';
import PropTypes from 'prop-types';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import ZoomIn from '@material-ui/icons/ZoomIn';
import ZoomOut from '@material-ui/icons/ZoomOut';
import ZoomReset from '@material-ui/icons/YoutubeSearchedFor';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

const drawerWidth = 170;

const styles = theme => ({
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    // shift down left menu
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
        // marginLeft: 'calc(vw-1000px)',
    }
});

class LayoutLeftBar extends React.Component {

    handleDrawerToggle = () => {
        this.props.handleDrawerToggle();
    };

    scaleUp() {
        const s = this.props.book.globalScale+0.05;
        const newb = update(this.props.book, {globalScale: {$set:s}});
        this.props.set('book', newb);
    }
    
    scaleReset() {
        const newb = update(this.props.book, {globalScale: {$set:1.0}});
        this.props.set('book', newb);
    }
    
    scaleDown() {
        const s = this.props.book.globalScale-0.05;
        const newb = update(this.props.book, {globalScale: {$set:s}});
        this.props.set('book', newb);
    }

    
    render() {
        const { classes, theme } = this.props;

        // Menu to current passage        
        const current_passage = '/passage/'+this.props.book.current_passage;
        const h = (<span>Livre {' '}
                     <span className='Book-help'> 
                       {this.props.book.current_passage}
                     </span>
                   </span>);

        // const s = require('./images/w/lw.png');
        // // Perso <br/>
        // const c = (<span>
        //            <img alt='item' width={25} src={s}/><br/>
        
        //            <span className='Book-fight-help'> 
        //              H {this.props.character.combat_skill}
        //               {' / '}
        //              E {this.props.character.endurance}
        //              </span>
        //            </span>);
        
        const drawer = (
            <div>
              <div className={classes.toolbar} />
              <Divider />
              <List>
                {/* <ListItem button component={Link} to='/character'> */}
                {/*   <ListItemText primary={c}></ListItemText> */}
                {/* </ListItem> */}
                <ListItem button component={Link} to={current_passage}>
                  <ListItemText primary={h}></ListItemText>
                </ListItem>
                {/* <ListItem button component={Link} to='/map'> */}
                {/*   <ListItemText primary={'Carte'}></ListItemText> */}
                {/* </ListItem> */}
                {/* <ListItem button> */}
                {/*   <ListItemText primary={'Encyclo'}></ListItemText> */}
                {/* </ListItem> */}

                <ListItem button component={Link} to='/team'> 
                  <ListItemText primary={'Groupe'}></ListItemText> 
                </ListItem>
                
                <ListItem button component={Link} to='/test'> 
                  <ListItemText primary={'Chemins'}></ListItemText> 
                </ListItem>
                
                <ListItem button component={Link} to='/debug'> 
                  <ListItemText primary={'Debug'}></ListItemText> 
                </ListItem>
                
              </List>
              <Divider />
              <List>
                <ListItem button>
                  <ListItemText primary={'A propos'}></ListItemText>
                </ListItem>
              </List>
              <Divider />
              <div>
                <IconButton onClick={this.scaleUp.bind(this)}>
                  <ZoomIn/>
                </IconButton> 
                <IconButton onClick={this.scaleReset.bind(this)}> 
                  <ZoomReset/>
                </IconButton> 
                <IconButton onClick={this.scaleDown.bind(this)}> 
                  <ZoomOut />
                </IconButton> 
              </div>
            </div>
        );

        // var w = window.innerWidth; // not updated 
        // console.log('w', w);
        // console.log('w', classes.drawerPaper);
        return (
            <div>
              {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
              <Hidden smUp implementation="css">
                <Drawer
                  container={this.props.container}
                  variant="temporary"
                  anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                  open={this.props.mobileOpen}
                  onClose={this.handleDrawerToggle}
                  classes={{ paper: classes.drawerPaper }}
                >
                  {drawer}
                </Drawer>
              </Hidden>
              <Hidden xsDown implementation="css">
                <Drawer
                  classes={{ paper: classes.drawerPaper }}
                  variant="permanent"
                  open
                >
                  {drawer}
                </Drawer>
              </Hidden>
            </div>
        );
    }
}

LayoutLeftBar.propTypes = {
    classes: PropTypes.object.isRequired,
    // Injected by the documentation to work in an iframe.
    // You won't need it on your project.
    container: PropTypes.object,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(LayoutLeftBar));
//export default withStyles(styles, { withTheme: true })(LayoutLeftBar);
