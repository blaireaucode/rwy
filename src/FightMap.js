/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import update from 'immutability-helper';
import FightMapCell from './FightMapCell.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import * as fh from './fight_helpers';
import * as bh from './book_helpers';
import './Map.css';

class FightMap extends Component {

    constructor(props) {
        super(props);
        this.state = { selected:false };
        this.onClick = this.onClick.bind(this);
    }

    onClick(fight, index) {

        // Do nothing if fight status is not 'selection'
        if (fight.status !== 'selection') return;
        
        const selected = this.state.selected;

        // select if unselected and not empty
        if (!selected) {
            const c = fh.map_get_cell(fight.map, index);
            // check if empty
            if (c !== 0)
                this.setState({ selected:index});
            return;
        }

        // unselect if selected
        if (bh.arraysMatch(selected, index)) {
            this.setState({ selected:false});
            return;
        }

        // move if cell is empty
        const map = fight.map;
        const c = fh.map_get_cell(map, index);
        if (c === 0) {
            const m = fh.map_move(map, selected, index);
            const f = fh.fight_set_map(fight, m);
            fh.fight_update(this.props, f);
            this.setState({ selected:index});
            return;
        }

        // attack if encounter nearby
        if (c === 'z') { // FIXME check encounter
            const f = fh.fight_set_status(fight, 'attack');
            fh.fight_update(this.props, f);
            this.setState({ selected:false});
            return;
        }
    }


    render() {
        //        const {fight, fight_state, fight_set_state} = this.props;
        const {fight} = this.props;
        const map = fight.map;
        var m = [];
        var k=0;
        for (var i = 0; i < map.length; i++) {
            m.push(
                <tr key={k}>
                  {map[i].map((m,j) =>
                              <FightMapCell
                                {...this.props}
                                index={[k,j]}
                                selected={this.state.selected}
                                onClick={this.onClick}
                                key={k+' '+j}
                              /> )}
                </tr>
            );
            k++;
        }

        return (
            <table className='fightMap'>
              <tbody>
                {m}
              </tbody>
            </table>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FightMap);
