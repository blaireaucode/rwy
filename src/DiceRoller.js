/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dices, { DICE_TYPES } from './Dices.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class DiceRoller extends Component {

    

    render() {
        // if (!this.props.options.display_dice) return '';
        const p = this.props.dice;
        if (!p || p<0) return(<span/>);
        return (
            <Dices
              height='150px'
              posY={8}
              scaleFactor={1.7}
              dices={[                 
                  {
                      type: DICE_TYPES.D12,
                      backColor: "black",
                      fontColor: "white",
                      value: p+1
                  }
              ]}
            />
        );        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DiceRoller);
