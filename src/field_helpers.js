/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React from 'react';
import update from 'immutability-helper';
import Field4 from './Field4.js';
import HelpPopover from './HelpPopover.js';
import * as charac from './characteristics.js';

// ----------------------------------------------------------------------------
export function get_field_help(props)
{
    var h = '';
    if (props.nohelp) return h;
    if (props.children) {
        h  = (<HelpPopover>
                {props.children}
              </HelpPopover>);
    }
    else {
        const ch = charac.characteristics[props.f];
        if (ch) {
            if (ch.help) {
                h = (<HelpPopover>
                         {ch.help(props)}
                       </HelpPopover>);
            }
        }
    }
    return h;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_field_str_value(props)
{
    const c = charac.characteristics[props.f];
    var element = field_get_element(props);
    const value = element[props.f];
    if (c.to_str)
        return c.to_str(value);
    return value;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function field_get_element(props)
{
    // console.log('field_get_element', props);
    // if (props.e) return props.encounter;
    // if (props.i) return props.i;
    // default
    return props.adv;    
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function field_set_element(props, element)
{
    // console.log('field_set_element', props, element);
    // if (props.e) return props.set('encounter', element);
    // if (props.i) return props.set_items(element);
    // default
    // console.log('set',element);
    const t = update(props.team, {[props.adv.class]: {$set:element}});
    // console.log('t',t);
    return props.set('team', t);
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
export function get_field_adj(props)
{
    var adj='';
    const element = field_get_element(props);
    const field_adj = props.f+'_adj';
    if (!(field_adj in element)) return adj;

    const v = parseInt(element[props.f],10) + parseInt(element[field_adj],10);
    const st1 = { width: '2ch', textAlign:'center'};
    const st2 = { width: '4ch', textAlign:'center'};

    var before = (<span style={st1}>{'+'} {' '}</span>);
    var after =  (<span style={st2}>{' = '} {v}</span>);

    adj = (
        // <Grid item>
        <Field4
          {... props}
          nolabel
          before={before}
          after={after}
          vw={'3ch'}
          f={field_adj}>
        </Field4>
        // </Grid>
    );


    return adj;
}
// ----------------------------------------------------------------------------
