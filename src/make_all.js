
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

var fs = require('fs');

// read template
var fn = './src/passages/allpassages_work_v1.txt';
var data = fs.readFileSync(fn, 'utf-8');

// split passage (570)
var max_p = 570;
var n = 1;
var passages = [];

while (n<=max_p) {
    var regex = new RegExp("\n"+n.toString()+"", 'i');
    var found = regex.exec(data);
    var p = data.substring(0, found.index);
    passages.push(p);
    // console.log('\nPassage ', n-1, p);
    data = data.substring(found.index, data.length);
    // console.log('\n\n DATA  ===> ', data.substring(0,100));
    n = n+1;
}

// last one
passages.push(data);

n = 0;
for (p of passages) {

    // ignore passage 0
    if (n === 0) {
        n = n+1;
        continue;
    }
    
    // remove first line (with passage nb)
    //console.log('p',p);
    regex = new RegExp(""+n.toString()+"", 'i');
    //console.log('reg', regex);
    found = regex.exec(p);
    var txt = p.substring(found.index+n.toString().length, p.length);
    //console.log('p2',txt);

    // replace break line with space
    txt = txt.replace(/(\r\n|\n|\r)/gm, " ");

    var reg = /Feuille d Aventure/g;
    var txt2 = txt.replace(reg, "<Link to='/adv'>Feuille d'Aventure</Link>");
    var import_link= false;
    if (txt !== txt2) import_link = true;
    txt = txt2;
    
    fn = './src/passage_template.js';
    var t = fs.readFileSync(fn, 'utf-8');

    // TEMPLATE : replace passage
    t = t.replace(/passage_number/g, n.toString());

    // TEMPLATE : replace ici with the txt
    t = t.replace("ici", txt);

    // TEMPLATE : import Link if needed
    if (import_link) {
        t = t.replace("lalala", "\nimport { Link } from 'react-router-dom';");
    }
    else {
        t = t.replace("lalala", "");
    }

    // To replace all ^M
    t = t.replace(/\r/g, "");

    // write
    fn = './src/passages/'+n+'.js';
    // fs.copyFileSync(fn, fn+'.backup');

    fs.writeFileSync(fn, t);
    console.log("Successfully written to file "+fn);

    n = n+1;
}
