/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import { createStore } from 'redux';
import * as at from './action-types.js';
import * as gr from './action-reducers.js';
import default_book from './default_book.js';
import default_team from './default_team.js';

const store = setupStore();
export default store;

export function setupStore()
{
    // read from local store (if exist)
    const elements = ['team',
                      'book',
                      'options',
                      'log'];
    const default_elem = {
        'team': default_team,
        'book': default_book,
        'options': {},
        'log': []};

    var initialState = {};
    for(var elem of elements) {
        const name = 'rwy_'+elem;
        const e =  JSON.parse(global.localStorage.getItem(name))
              || default_elem[elem];
        initialState[elem] = e;
    }    
    console.log('LocalStorage read', initialState);
    
    // list of action
    const rootReducer = (state = initialState, action) => {
        switch (action.type) {
        case at.SET:
            return gr.set(state, action.element_name, action.value);
        default:
            return state;
        }
    };

    return createStore(rootReducer);
}

