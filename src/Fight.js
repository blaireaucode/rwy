/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import update from 'immutability-helper';
import FightMap from './FightMap.js';
import FightEncounters from './FightEncounters.js';
import FightAttack from './FightAttack.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import * as charac from './characteristics.js';
import * as bh from './book_helpers';
import * as fh from './fight_helpers';

class Fight extends Component {

    render() {
        const fight = fh.get_current_fight(this.props);
        // console.log('Fight', fight, p);
        return (            
            <span>
              <FightMap fight={fight}/>
              <p/>
              <FightAttack fight={fight}/>
              <FightEncounters fight={fight}/>
              {/* <FightActions fight={fight}/> */}
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Fight);
