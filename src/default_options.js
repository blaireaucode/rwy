/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

const default_options = {
    display_dice: true,
    display_cheat_top: true,
    can_modify: true,
    history_link: true,
    items_nolimit: true,
    disc_nolimit: true,
};

export default default_options;

