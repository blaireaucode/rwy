
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import update from 'immutability-helper';


/**
   Init an element of a passage if does not exist ; return it if exist.
*/
export function book_init_element(props, elem) {
    console.log('book_init_element', props, elem);
    const name = elem.name;
    const passage_title = props.book.current_passage.toString();

    // Get current passage
    var passage = props.book.p[passage_title] || false;

    if (!passage) {
        passage = [];
    }

    const element = passage[name] || false;
    if (!element) {
        passage[name] = elem;
        // console.log('no elem in this passage, create it', passage);
        // SET IT, return
        // console.log('book before', props.book);
        var book_copy = JSON.parse(JSON.stringify(props.book));
        // console.log('book copy', book_copy);
        book_copy.p[passage_title] = passage;
        // console.log('book after', book_copy);
        props.set('book', book_copy);
    }

    // console.log('return the elem', passage[name]);
    return passage[name];
}

/** Check if two arrays are equal */
export function arraysMatch(arr1, arr2) {
    // Check if the arrays are the same length
    if (arr1.length !== arr2.length) return false;
    // Check if all items exist and are in the same order
    for (var i = 0; i < arr1.length; i++) {
	if (arr1[i] !== arr2[i]) return false;
    }
    // Otherwise, return true
    return true;
};



// export const items_description = it.items_description;

// export function item_get_description(id) {
//     for (var item of items_description) {
//         if (item.id === id) {
//             return item;
//         }
//     }
//     return {id:'item_not_found', name:'item_not_found'};
// }

// export function item_get_index(list, id) {
//     var index = 0;
//     for (var item of list) {
//         if (item.id === id) return index;
//         index = index + 1;
//     }
//     return -1;
// }

// export function item_list_add(list, item) {

//     // already similar item ? (except if has a cost)
//     // console.log('list item', item);
//     if (!('cost' in item) || item.cost === 0) {
//         const index = item_get_index(list, item.id);
//         if (index !== -1) {
//             const already_item = list[index];
//             var item_nb = 1;
//             if ('nb' in item) item_nb = item.nb;
//             var already_nb = 1;
//             if ('nb' in already_item) already_nb = already_item.nb;
//             const n = item_nb+already_nb;
//             const new_item = update(list[index],
//                                     {nb: {$set: n}});
//             const new_list = update(list,
//                                     {[index]: {$set:new_item}});
//             return new_list;
//         }
//     }
    
//     const new_list = update(list,{$push:[item]});
//     return new_list;
// }

// export function item_list_rm(list, id, all=false) {
//     const index = item_get_index(list, id);
//     if (index === -1) return list; // not found
//     const item = list[index];
//     if (all === false && 'nb' in item) {
//         const n = list[index].nb;
//         if (n !== 1)  {
//             const new_item = update(list[index],
//                                     {nb: {$set: n-1}});
//             const new_list = update(list,
//                                     {[index]: {$set:new_item}});
//             return new_list;
//         }
//     }
//     const new_list = update(list, {$splice: [[index,1]]});
//     return new_list;
// }

// export function item_pick(character, item) {
//     const new_items = item_list_add(character.items, item);
//     const c = update(character, {items: {$set:new_items}});
//     return c;        
// }

// export function item_drop(character, id, all=false) { // FIXME option to drop all/n
//     const new_items = item_list_rm(character.items, id, all);
//     const c = update(character, { items: {$set: new_items}});
//     return c;        
// }

// export function item_get_nb_of_weapon(character) {
//     var n = 0;
//     for(const item of character.items) {
//         const d = item_get_description(item.id);
//         if (d.type === 'weapon') n = n+1;
//     }
//     return n;
// }

// export function item_get_nb_of_item_in_sacados(character) {
//     var n = 0;
//     for(const item of character.items) {
//         const d = item_get_if_in_sacados(item.id);
//         if (d) {
//             if (item.nb) n = n+item.nb;
//             else n = n+1;
//         }
//     }
//     return n;
// }

// export function item_get_if_in_sacados(id) {
//     const desc = item_get_description(id);
//     const normal = 
//           desc.type !== 'weapon' &&
//           desc.special !== true &&
//           desc.id !== 'PO' &&
//           desc.id !== 'repas';
//     return normal;
// }    


// export function enabled_if_fight_win(props, f) {
//     const fight = book_get_element(props, f);
//     const r = fight.status === 'victory';    
//     return r;
// }

// export function fight_combat_ratio(character, fight) {
//     //console.log('fight_compute_cr', character, fight);

//     // FIXME : include bonus from disciplines
//     // FIXME : include bonus from passage
//     // FIXME : help explication

//     /*
//       var combat_skill_bonus = 0;
//       const gs = character.items.some(e => e.id === 'glaive_sommer');
//       if (gs) combat_skill_bonus += 8;
//       combat_skill_bonus += fight_get_weapon_bonus(character);
//       const cr = combat_skill_bonus+character.combat_skill - fight.combat_skill + fight.combat_bonus;
//     */
//     const cs = fight_get_combat_skill_total(character,fight);
//     const cr = cs - fight.combat_skill;
    
//     return cr;
// }


// export function book_fight_update(character, f) {
//     var fight = JSON.parse(JSON.stringify(f));

//     // console.log('book_fight_update', character, f);
//     // First time init
//     if (!fight.round) fight.round = 1;
//     if (!fight.bonus) fight.bonus = 0;
//     if (!fight.help) fight.help = [];
//     if (!fight.status) fight.status = 'fighting';

//     // round specific bonus
//     fight.combat_bonus = fight.bonus;

//     // help
//     fight.combat_help = [];
//     for (var h of fight.help) fight.combat_help.push(h);
//     const help = fight_get_combat_skill_bonus_help(character, fight);
//     for (var hh of help) fight.combat_help.push(hh);
//     const helpe = fight_get_endurance_bonus_help(character, fight);
//     for (var hhe of helpe) fight.combat_help.push(hhe);

//     // status
//     //console.log('fight', fight, character);
//     if (fight_get_endurance_total(character, fight) <= 0)
//         fight.status = 'defeat';
//     else if (fight.endurance <= 0)
//         fight.status = 'victory';

//     return fight;
// }


// export function book_get_element(props, element) {
//     // console.log('book_get_element', element);
    
//     if (!element.passage_title) {
//         console.log('book_get_element ERROR element need passage_title', element);
//         return element;
//     }
//     if (!element.name) {
//         // console.log('book_get_element ERROR element need name', element);
//         //return element;
//         element.name = 'default';
//     }
//     if (!props.book) {
//         console.log('book_get_element ERROR props need book', props);
//         return element;
//     }
//     const passage_title = element.passage_title;
//     const name = element.name;
//     const book = props.book;
//     const pt = passage_title.toString();
//     const p = book.p[pt] || false;
//     if (!p) {
//         // console.log('Book: no passage ', passage_title);
//         return element;
//     }
//     const e = p[name] || element;
//     // console.log('book get elem', props.book, e);
//     if (!p[name]) {
//         // console.log('Book: no element ', passage_title, name);
//         if (!props.book_set_element) {
//             console.log('book_get_element ERROR props need book_set_element', props);
//             return element;
//         }
//         const e = update(element, {created:{$set:true}});
//         // console.log('book set elem', e);
//         props.book_set_element(e);
//     }
//     return e;
// }


// export function gold_add(character, value) {
//     // console.log('gold_add', character, value);
//     const index = item_get_index(character.items, 'PO');
//     if (index === -1) {
//         var g = item_get_description('PO');
//         g.nb = value;
//         character = item_pick(character, g);
//         return character;
//     }

//     // Current value
//     var n = character.items[index].nb+value;
//     var c = {};
//     if (n>0) {
//         const item = update(character.items[index], {nb: {$set:n}});
//         c = update(character,
//                    {items: {[index]: {$set:item}}});
//     }
//     else
//         c = update(character,
//                    {items: {$splice: [[index, 1]]}});
//     // props.character_set(c);
//     return c;
// }

// export function gold_get(character) {
//     var n = 0;
//     const index = item_get_index(character.items, 'PO');
//     if (index !== -1) {
//         if (!character.items[index].nb) n = 1;
//         else n = character.items[index].nb;
//     }
//     return n;
// }

// export function endurance_set(character, value) {
//     var v = parseInt(value, 10);
//     // if (v<0) v = 0;
//     if (v>character.endurance_max) v = character.endurance_max;
//     const c = update(character, { endurance: {$set:v}});
//     return c;
// }

// export function endurance_add(character, value) {
//     var v = parseInt(value, 10) + parseInt(character.endurance, 10);
//     return endurance_set(character, v);
// }


// export function has_discipline(character, disc) {
//     const d = character.kai_disciplines.some(e => e.id === disc);
//     return d;
// }


// export function fight_get_endurance_total(character, fight) {
//     var endurance = character.endurance;
//     endurance += fight_get_endurance_bonus(character, fight);
//     return endurance;    
// }

// export function fight_get_combat_skill_total(character, fight) {
//     var cs = character.combat_skill;
//     cs += fight_get_combat_skill_bonus(character);
//     return cs;
// }

// export function fight_get_combat_skill_bonus(character, fight) {
//     var bonus = 0;
//     if (fight && fight.combat_skill_bonus) bonus += fight.combat_skill_bonus;
//     bonus += fight_get_combat_skill_bonus_weapon(character, fight);   
//     bonus += fight_get_combat_skill_bonus_psy(character, fight);
//     bonus += fight_get_combat_skill_bonus_sommer(character, fight);
//     bonus += fight_get_combat_skill_bonus_shield(character, fight);
//     bonus += fight_get_combat_skill_bonus_no_weapon(character, fight);
//     return bonus;    
// }

// export function fight_get_combat_skill_bonus_sommer(character, fight) {
//     if (fight && fight.no_weapon === true) return 0;
//     const gs = character.items.some(e => e.id === 'glaive_sommer');
//     if (gs) return 8;
//     else return 0;
// }

// export function fight_get_combat_skill_bonus_shield(character, fight) {
//     if (fight && fight.no_weapon === true) return 0;
//     const gs = character.items.some(e => e.id === 'shield');
//     if (gs) return 2;
//     else return 0;
// }

// export function fight_get_combat_skill_bonus_no_weapon(character, fight) {
//     if (fight && fight.no_weapon === true) return 0;
//     var w = 0;
//     for (var it of character.items) {
//         var item = item_get_description(it.id);
//         if (item.type === 'weapon') w += 1;
//     }
//     if (w === 0) return -4;
//     return 0;
// }

// export function fight_get_combat_skill_bonus_weapon(character, fight) {
//     if (fight && fight.no_weapon === true) return 0;
//     const d = has_discipline(character, "maitrise_arme");
//     if (!d) return 0;
//     if (fight && fight.no_bonus_weapon === true)
//         return 0;
//     const index = item_get_index(character.items, character.weapon_master);
//     if (index === -1) return 0;
//     else return 2;
// }

// export function fight_get_combat_skill_bonus_psy(character, fight) {
//     var bonus = 0;
//     const d = character.kai_disciplines.some(e => e.id === "puissance_psychique");
//     if (d) {
//         bonus = 2;
//         if (fight && fight.no_bonus_psy) bonus = 0;
//     }
//     return bonus;
// }

// export function fight_get_combat_skill_bonus_help_sommer(character, fight) {
//     var help = '';
//     if (fight && fight.dont_use_weapon === true) return help;
//     if (fight_get_combat_skill_bonus_sommer(character, fight) > 0)
//         help = "Habileté +8 grâce au Glaive de Sommer";
//     return help;
// }

// export function fight_get_combat_skill_bonus_help_shield(character, fight) {
//     var help = '';
//     if (fight && fight.dont_use_weapon === true) return help;
//     if (fight_get_combat_skill_bonus_shield(character, fight) > 0)
//         help = "Habileté +2 grâce au bouclier";
//     return help;
// }

// export function fight_get_combat_skill_bonus_help_weapon(character, fight) {
//     var help = '';
//     if (fight && fight.dont_use_weapon === true) return help;
//     if (fight_get_combat_skill_bonus_weapon(character, fight) > 0) {
//         const n = item_get_description(character.weapon_master).name;
//         help = "Habileté +2 grâce à la maîtrise d'arme ("+n+')';
//     }
//     if (fight && fight.no_bonus_weapon === true)
//         help = "Pas de bonus d'arme";
//     return help;
// }

// export function fight_get_combat_skill_bonus_help_psy(character, fight) {
//     var help = '';
//     const d = character.kai_disciplines.some(e => e.id === "puissance_psychique");
//     if (d) {
//         if (fight && fight.no_bonus_psy) help = 'Pas de bonus puissance psychique';
//         else help = 'Habileté +2 grâce à puissance psychique';
//     }    
//     return help;
// }

// export function fight_get_combat_skill_bonus_help_no_weapon(character, fight) {
//     var help = '';
//     if (fight && fight.dont_use_weapon === true) return help;
//     const d = fight_get_combat_skill_bonus_no_weapon(character, fight);
//     if (d !== 0) {
//         help = "Habileté -4 car vous n'avez pas d'arme";
//     }    
//     return help;
// }

// export function fight_get_combat_skill_bonus_help(character, fight) {
//     var help = [];
//     var h = fight_get_combat_skill_bonus_help_psy(character, fight);
//     if (h !== '') help.push(h);
//     h = fight_get_combat_skill_bonus_help_weapon(character, fight);
//     if (h !== '') help.push(h);
//     h = fight_get_combat_skill_bonus_help_sommer(character, fight);
//     if (h !== '') help.push(h);
//     h = fight_get_combat_skill_bonus_help_shield(character, fight);
//     if (h !== '') help.push(h);
//     h = fight_get_combat_skill_bonus_help_no_weapon(character, fight);
//     if (h !== '') help.push(h);
//     if (fight) {
//         if (fight.bonus_factor_dmg_encounter)
//             help.push('Dégâts multipliés par '+fight.bonus_factor_dmg_encounter);
//         if (fight.bonus_dmg_character > 0)
//             help.push('Malus dégâts +'+fight.bonus_dmg_character);
//         if (fight.no_damage_first_rounds >= fight.round)
//             help.push('Pas de dégât pour vous ce tour');
//     }
//     return help;
// }

// export function fight_get_endurance_bonus(character, fight) {
//     var bonus = 0;
//     if (fight && fight.endurance_bonus) bonus += fight.endurance_bonus;
//     bonus += fight_get_endurance_bonus_mail(character, fight);
//     return bonus;
// }

// export function fight_get_endurance_bonus_help(character, fight) {
//     var help = [];
//     var h = fight_get_endurance_bonus_help_mail(character, fight);
//     if (h !== '') help.push(h);
//     return help;
// }

// export function fight_get_endurance_bonus_mail(character, fight) {
//     var bonus = 0;
//     if (fight && fight.dont_use_weapon === true) return 0;
//     if (fight && fight.endurance_bonus) bonus += fight.endurance_bonus;
//     const gs = character.items.some(e => e.id === 'mail');
//     if (gs) bonus += 4;
//     return bonus;
// }

// export function fight_get_endurance_bonus_help_mail(character, fight) {
//     var help = [];
//     if (fight && fight.dont_use_weapon === true) return help;
//     const b = fight_get_endurance_bonus_mail(character, fight);
//     if (b > 0) help.push('Endurance +4 avec la cotte de mailles');
//     return help;
// }


// export function get_element(t, passage_title, dic, name) {
//     if (!name) name = 'default';
//     //console.log('get_element', passage_title, dic, name);
//     const pt = passage_title.toString();
//     const p = t.props.book.p[pt] || false;
//     if (!p[name]) {
//         // this is the first time, we create the element
//         const f = { 'name': name, 'passage_title': passage_title };
//         const e = Object.assign({}, f, dic);
//         t.props.book_set_element(e);
//         return e;
//     }
//     return p[name];    
// }

// export function get_done(t, passage_title, name) {
//     if (!name) name = 'default';
//     const f = { already_done: false };
//     const e = get_element(t, passage_title, f, name);
//     return e.already_done;
// }

// export function update_done(t, passage_title, name) {
//     // console.log('handleClick', this, t, passage_title); // this exist
//     if (!name) name = 'default';
//     const f = { already_done: false };
//     const e = get_element(t, passage_title, f, name);
//     const ee = update(e, {already_done: {$set:true}});
//     if (!this.props)
//         t.props.book_set_element(ee);
//     else
//         this.props.book_set_element(ee);
// }


// export function history_add_passage(props, p) {
//     const h = props.character.history;
//     if (p === '') return;
//     if (!p.startsWith) return;
//     var n = p.startsWith("/passage/");
//     if (n)
//         p = p.substr(9, p.length);
//     const last = h[h.length-1];
//     if (last === p) return;
//     const c = update(props.character, {history: {$push:[p]}} );
//     props.character_set(c);
// }


// export function notes_save(props, content) {
//     // console.log('notes_save', props, content);
//     const c = update(props.character, {notes: {$set:content}} );
//     props.character_set(c);    
// }


// export function is_dead(character) {
//     if (fight_get_endurance_total(character) <= 0)
//         return true;
//     return false;
// }

// export function is_fight_in_passage(book, title) {
//     const p = book.p[title];
//     if (!p) return false;
//     for(var e in p) {
//         const elem = p[e];
//         if (elem.encounter) {
//             return true;
//         }
//     }
//     return false;
// }

// export function book_get_current_fight(props, flist) {
//     var f = '';
//     var i = 0;
//     // add continue to the fight
//     for (var fi of flist) {
//         const fight = book_get_element(props, fi);
//         if (i !== flist.length-1) {
//             if (!fight.continue) {
//                 const fc = update(fight, {continue: {$set:true}} );
//                 props.book_set_element(fc);
//             }
//         }
//         i = i+1;
//     }

    
//     i = 0;
//     for ( fi of flist) {
//         const fight = book_get_element(props, fi);
//         const win = enabled_if_fight_win(props, fight);
//         if (win) {
//             if (i === flist.length-1 || !fight.continue_terminated) {
//                 f = fight;
//                 break;
//             }
//         }
//         if (!win) {
//             f = fight;
//             break;
//         }
//         i = i+1;
//     }
//     if (f === '') f = flist[flist.length-1]; // this is the last
//     return f;
// }


// export function reset_book(props) {
//     props.book_set(default_book);
// }
// export function reset_character(props) {
//     props.character_set(default_character);
// }
// export function reset_options(props) {
//     props.options_set(default_options);
// }
