/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import {ForceGraph, InteractiveForceGraph, ForceGraphNode, ForceGraphLink,ForceGraphArrowLink} from 'react-vis-force';
import graph_path from './passages/graph-path.json';

const maxg = 15;

function perc2color(perc) {
    var r, g, b = 0;
    if(perc < 50) {
	r = 255;
	g = Math.round(5.1 * perc);
    }
    else {
	g = 255;
	r = Math.round(510 - 5.10 * perc);
    }
    var h = r * 0x10000 + g * 0x100 + b * 0x1;
    return '#' + ('000000' + h.toString(16)).slice(-6);
}

var gg = [];
// gg.push(<ForceGraphNode key='1' node={{ id: '1' }} fill="red" />);
// gg.push(<ForceGraphNode key='2' node={{ id: '2' }} fill="blue" />);
// gg.push(<ForceGraphLink key='3' link={{ source: '1', target: '2', strokeWidth:'1'  }} />);

for(var n in graph_path) {
    const node = graph_path[n];    
    if (node.depth>maxg) continue;

    const id = n.toString()+'('+node.depth+')';
    // console.log(node, id);
    
    var s = {id:id};
    var c = perc2color(100/80*graph_path[n].depth);
    if (node.o.length === 0) c = "white";
    if (n === '1') c = "green";
    if (n === '570') c = "green";
    if (n === '534') c = "red";
    if (n === '432') c = "red"; // porte ?
    gg.push(<ForceGraphNode key={n} node={s} strokeWidth='0' fill={c} />);
}

var k = 0;
for(n in graph_path) {
    const node = graph_path[n];    
    //console.log(graph_path[n]);
    if (node.depth>maxg) continue;
    const id = n.toString()+'('+node.depth+')';
    for(var t of node.o) {
        // if (t>49) continue;
        const nt = graph_path[t];
        if (nt.depth>maxg) continue;
        const idt = t.toString()+'('+nt.depth+')';
        s = {source:id, target:idt, strokeWidth:1};
        // var k = n.toString()+'_'+t.toString();
        // console.log(n,s,t,k);
        gg.push(<ForceGraphLink key={k} link={s} />);
        k = k+1;
    }
}        

class Test extends Component {

    constructor(props) {
        super(props);       
    }

    render() {
        return (
            <div>              
              Hello world
              <p/>

              <InteractiveForceGraph zoom zoomSpeed='1500'
                          showLabels
                          simulationOptions={{ animate:false, height: 600, width: 1200 }}>
                {gg}
              </InteractiveForceGraph>
              
            </div>
        );
    }
}

export default Test;
