/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Field4 from './Field4.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class AdventurerSummary extends Component {

    constructor(props) {
        super(props);
        console.log('adv', this.props.adv);
    }

    render() {
        const a = this.props.adv;
        const p = {nohelp:true, lw:'4ch', adv:a};

        return (
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start">

              <Grid item xs={12}>
                <Field4 {...p} vw={'40ch'} nolabel f={'name'}/>
              </Grid>

              <Grid item xs={12}>
                <Field4 {...p} vw={'40ch'} noedit nolabel f={'class'}/>
              </Grid>

              <Grid item xs={3}>
                <Field4 {...p} f={'fighting_prowess'} />
              </Grid>
              <Grid item xs={3}>
                <Field4 {...p} f={'psychic_ability'} />
              </Grid>
              <Grid item xs={3}>
                <Field4 {...p} f={'awareness'} />
              </Grid>
              <Grid item xs={3}>
                <Field4 {...p} f={'endurance'} />
              </Grid>

            </Grid>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdventurerSummary);
