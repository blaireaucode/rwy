/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import update from 'immutability-helper';
import Grid from '@material-ui/core/Grid';
import Field4 from './Field4.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import * as bh from './book_helpers';

class FightEncounters extends Component {

    constructor(props) {
        super(props);        
    }


    render() {
        // return '';
        const fight = this.props.fight;
        // console.log('FightEncounters', fight);

        const a = fight.encounters['z'];
        const p = {nohelp:true, lw:'4ch', adv:a};
        // console.log('enc', a);

        const before = <span>(z) &nbsp;</span>;

        return (            
            <span>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start">

                <Grid item xs={12}>
                  <Field4 {...p} vw={'40ch'} nolabel f={'name'} before={before}/>
                </Grid>            

                <Grid item xs={3}>
                  <Field4 {...p} f={'fighting_prowess'} />
                </Grid>
                <Grid item xs={3}>
                  <Field4 {...p} f={'psychic_ability'} />
                </Grid>
                <Grid item xs={3}>
                  <Field4 {...p} f={'awareness'} />
                </Grid>
                <Grid item xs={3}>
                  <Field4 {...p} f={'endurance'} />
                </Grid>

              </Grid>

              
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FightEncounters);
