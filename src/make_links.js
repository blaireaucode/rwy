
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

var fs = require('fs');

// read passage (570)
var max_p = 570;
var n = 1;

while (n<=max_p) {
    // read file
    var fn = './src/passages/'+n+'.js';
    var txt = fs.readFileSync(fn, 'utf-8');

    // [[Rendez-vous au 305|305]]
    var reg = /rendez-vous au ([0-9]+)/g;
    txt = txt.replace(reg, "rendez-vous au <L to='\$1'>$1</L>");

    reg = /rendez-  vous au ([0-9]+)/g;
    txt = txt.replace(reg, "rendez-vous au <L to='\$1'>$1</L>");

    reg = /rendez vous au ([0-9]+)/g;
    txt = txt.replace(reg, "rendez-vous au <L to='\$1'>$1</L>");

    reg = /Rendez-vous au ([0-9]+)/g;
    txt = txt.replace(reg, "Rendez-vous au <L to='\$1'>$1</L>");

    reg = /Rendez-vous (.+) au ([0-9]+)/g;
    txt = txt.replace(reg, "Rendez-vous $1 au <L to='\$2'>$2</L>");

    reg = /rendre au ([0-9]+)/g;
    txt = txt.replace(reg, "rendre au <L to='\$1'>$1</L>");
    
    reg = /Rendez vous au ([0-9]+)/g;
    txt = txt.replace(reg, "Rendez-vous au <L to='\$1'>$1</L>");

    reg = /rendezvous au ([0-9]+)/g;
    txt = txt.replace(reg, "rendez-vous au <L to='\$1'>$1</L>");

    //(49)
    reg = /\(([0-9]+)\)/g;
    txt = txt.replace(reg, "(<L to='\$1'>$1</L>)");

    reg = /au hasard au ([0-9]+) ou au ([0-9]+)/g;
    txt = txt.replace(reg, "au hasard au <L to='\$1'>$1</L> ou au <L to='\$2'>$2</L>");    
    
    reg = /Rendezvous au ([0-9]+)/g;
    txt = txt.replace(reg, "Rendez-vous au <L to='\$1'>$1</L>");

    reg = /Retournez au ([0-9]+)/g;
    txt = txt.replace(reg, "Retournez au <L to='\$1'>$1</L>");
    
    reg = /retournez au ([0-9]+)/g;
    txt = txt.replace(reg, "retournez au <L to='\$1'>$1</L>");

    reg = /se rendent au ([0-9]+)/g;
    txt = txt.replace(reg, "se rendent au <L to='\$1'>$1</L>");

    reg = /rendezvous (.+) au ([0-9]+)/g;
    txt = txt.replace(reg, "rendez-vous $1 au <L to='\$2'>$2</L>");

    reg = /rendez-vous (.+) au ([0-9]+)/g;
    txt = txt.replace(reg, "rendez-vous $1 au <L to='\$2'>$2</L>");

    fs.writeFileSync(fn, txt);
    console.log("Successfully written to file "+fn);

    n = n+1;
}
