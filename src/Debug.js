/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import L from './L.js';
import { Link } from 'react-router-dom';
import default_book from './default_book.js';
import default_options from './default_options.js';
import default_team from './default_team.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class Debug extends Component {

    reset(elem, val) {
        console.log('reset', elem, val);
        this.props.set(elem, val);
    }

    getElem(elements) {
        var elem = [];
        for (const [key, value] of Object.entries(elements)) {
            var v = value;
            //console.log('key, val', key, value);
            if (v.constructor === Object) { v = 'obj keys '+Object.keys(value).length; }
            if (Array.isArray(value)) { v = 'array '+value.length; }
            elem.push(<span key={key}>{key} = {v} {' '}<br/> </span>);
        }
        return elem;
    }
    
    render() {
        const book = this.getElem(this.props.book);
        const options = this.getElem(this.props.options);
        var team = [];
        team.push(this.getElem(this.props.team.warrior));
        team.push(this.getElem(this.props.team.sage));
        team.push(this.getElem(this.props.team.trickster));
        team.push(this.getElem(this.props.team.enchanter));
        
        return (
            <div className="Book">
              
              <Link to='#' onClick={()=>this.reset('book', default_book)}> Reset Book </Link><br/>
              <Link to='#' onClick={()=>this.reset('options', default_options)}> Reset Options </Link><br/>
              <Link to='#' onClick={()=>this.reset('team', default_team)}> Reset Team </Link><br/>

              Fight <L to="13">13</L>

              <br/>           
              <br/>
              {book}<br/>
              {options}<br/>
              {team}<br/>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Debug);
