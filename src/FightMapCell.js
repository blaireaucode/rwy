/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import * as bh from './book_helpers';
import * as fh from './fight_helpers';
import './Map.css';

class FightMapCell extends Component {

    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        this.props.onClick(this.props.fight, this.props.index);
    }

    render() {
        const { fight, index, selected } = this.props;
        var c = fh.map_get_cell(fight.map, index);
        if (c === 0) c = '';

        // Change visual if selected 
        if (bh.arraysMatch(selected, index)) {
             c = '-'+c+'-';
         }

        return (
            <td className='cell' onClick={this.onClick}>
              {c}
            </td>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FightMapCell);
