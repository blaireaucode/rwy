/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

const default_team = {
    order: ['warrior', 'sage', 'enchanter', 'trickster'],
    warrior: {
        name:'Thorfinn Bjornolfrsson',
        class:'warrior',
        fighting_prowess:8,
        psychic_ability:6,
        awareness:6,
        endurance:12,
        xp:250,
        level:2,
        gp:10,
        items: []
    },
    trickster:{
        name:'Derek Van Rhyn',
        class:'trickster',
        fighting_prowess:7,
        psychic_ability:6,
        awareness:8,
        endurance:12,
        xp:250,
        level:2,
        gp:10,
        items: []
    },
    sage:{
        name:'Adrienne Devereux',
        class:'sage',
        fighting_prowess:7,
        psychic_ability:7,
        awareness:6,
        endurance:10,
        xp:250,
        level:2,
        gp:10,
        items: []
    },
    enchanter:{
        name:'Gaia De Wolf',
        class:'enchanter',
        fighting_prowess:6,
        psychic_ability:8,
        awareness:6,
        endurance:10,
        xp:250,
        level:2,
        gp:10,
        items: []
    }
};

/*
  Fighting_prowess Force
  Psychic_ability  Pouvoir
  Awareness        Habileté
  Endurance        Endurance
  Gold Pieces
  Items
*/

export default default_team;

