/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

const default_book = {
    current_passage: 1,
    p: {},
    title: 'Le Royaume de Wyrd',
    globalScale: 1.0
};

export default default_book;

