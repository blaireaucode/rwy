/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import update from 'immutability-helper';
import { DragSource, DragPreviewImage } from 'react-dnd';
import { useDrag } from 'react-dnd';
import MultiBackend from 'react-dnd-multi-backend';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';
import * as bh from './book_helpers';
import objectAssign from 'object-assign';
import './Map.css';

const Types = {
    Cell: 'card',
};

const UnitActionSource = {
    canDrag(props) {
        console.log('canDrag', props);
        // You can disallow drag based on props
        //return props.canDrag;//
        return true;//props.isReady;
    },

    // Only `beginDrag` function is required.
    beginDrag(props, monitor, component) {
        console.log('beginDrag', props, monitor, component);
        // Return the data describing the dragged item
        const item = {
            bidon:12
        };
        return item;
        //console.log('beginDrag');
        // return {};
    },

    endDrag(props, monitor, component) {
        console.log('endDrop');
        if (!monitor.didDrop()) {
            console.log('not did drop');
            // You can check whether the drop was successful
            // or if the drag ended but nobody handled the drop
            return;
        }

        const dropResult = monitor.getDropResult();
        // const token = dropResult.token;
        const bidon = dropResult.bidon;
        console.log('drop ok, a', bidon, props);
        // props.moves.activation_activate_area(props.token, area);
    }
};


function UnitActionCollect(connect, monitor) {
    // console.log('UnitActionCollect', connect, monitor);
    return {
        // Call this function inside render()
        // to let React DnD handle the drag events:
        connectDragSource: connect.dragSource(),
        // You can ask the monitor about the current drag state:
        isDragging: monitor.isDragging(),
        connectDragPreview: connect.dragPreview()
    };
}


class FightMapCell extends Component {

    constructor(props) {
        super(props);        
    }

    
    componentDidMount() {        
        const { connectDragPreview } = this.props;
        const img = new Image();
        img.src = './test.png';
        img.onload = () => connectDragPreview(img);
        console.log('ici img', img);
    }
    

    render() {
        const c = this.props.cell;
        var cell = c;
        // <span>
        //   <Preview generator={this.generatePreview} />
        //   {c}
        // </span>;
        if (cell === 0) cell = '';

        const { isDragging, connectDragSource} = this.props;
        // , connectDragPreview
        console.log('isDragging', this.props, isDragging, connectDragSource);
        //console.log('preview', connectDragPreview);

        return connectDragSource(
            <td className='cell'> 
              {cell}
            </td>
        );
    }
}

var a = connect(mapStateToProps, mapDispatchToProps)(FightMapCell);

export default DragSource(Types.Cell, UnitActionSource, UnitActionCollect)(a);
