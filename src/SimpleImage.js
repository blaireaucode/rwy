/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';

class SimpleImage extends Component {

    constructor(props) {
        super(props);
        this.s = require('./images/'+this.props.src);
    }

    render() {
        const w = this.props.width;
        return (
            <span className='simpleImageDiv'>
              <img alt='4' className='PassageImage' width={w} src={this.s}/>
            </span>
        );
    }
}

// Set default props
SimpleImage.defaultProps = {
    width: 250
};


export default SimpleImage;
