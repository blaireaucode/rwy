/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import { set } from './actions.js';

export const mapDispatchToProps = (dispatch) => ({
    set(element_name, value) { dispatch(set(element_name,value)); },
});

export const mapStateToProps = store => {
    return {
        book:store.book,
        team:store.team,
        options:store.options,
        log:store.log,
    };
};
