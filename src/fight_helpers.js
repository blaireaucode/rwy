
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import update from 'immutability-helper';
import * as bh from './book_helpers';

/** Create a new empty fight map */
export function new_map(rows_count, cols_count)
{
    var map = new Array(rows_count);
    for (var i = 0; i < map.length; i++) {
        map[i] = new Array(cols_count);
        for (var j = 0; j < map[i].length; j++)
            map[i][j] = 0;
    }
    return map;
}

/** Return cell at given index */
export function map_get_cell(map, index)
{
    return map[index[0]][index[1]];
}

/** Set a content cell at given index */
export function map_set_cell(map, index, value)
{
    const i = index[0];
    const j = index[1];
    const r = update(map[i], {[j]:{$set:value}});
    const m = update(map, {[i]:{$set:r}});
    return m;
}

/** Move a cell to another index */
export function map_move(map, source, dest)
{
    const s = map_get_cell(map, source);
    const d = map_get_cell(map, dest);
    var m = map_set_cell(map, source, d);
    m = map_set_cell(m, dest, s);
    return m;
}

/** Update the map of a fight */
export function fight_set_map(fight, map)
{
    const f = update(fight, {map:{$set:map}});
    return f;
}


/** Init an element of a passage if does not exist ; return it if exist  */
export function book_set_fight(book, f)
{
    const current_passage = f.current_passage;
    const name = f.name;
    const b = update(book, {p:{
        [current_passage]: {
            [name]: {$set:f}}}});
    return b;
}

/** Create a new fight for the current passage **/
export function new_fight(props, name, current_passage, fight)
{
    var fight_copy = JSON.parse(JSON.stringify(fight));
    fight_copy.name = name;
    fight_copy.current_passage = current_passage;
    fight_copy.status = 'selection';
    bh.book_init_element(props, fight_copy);
    console.log('Create fight:', fight_copy);
    return fight_copy;
}


/** Return the fight at the current passage
   - props.book.current_passage
   - props.name (defautl = 'f')
   - props.book.p
   - props.fight <-- initial fight creation
**/
export function get_current_fight(props)
{
    const current_passage = props.book.current_passage || false;
    if (!current_passage) return '';
    const name = props.name || 'f';
    const p = props.book.p[current_passage] || false;
    if (!p) return new_fight(props, name, current_passage, props.fight);
    const element = props.book.p[current_passage][name] || false;
    if (!element) return new_fight(props, name, current_passage, props.fight);
    return element;
}

/** Change the status of a fight **/
export function fight_set_status(fight, status)
{
    const f = update(fight, {status: {$set:status}});
    return f;
}


/** Helper to update the fight **/
export function fight_update(props, fight)
{
    const b = book_set_fight(props.book, fight);
    props.set('book', b);
}
