/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import P from './passages/passages.js';
import Passage from './Passage.js';
import { withRouter } from "react-router";
import Start from './Start.js';
import { mapStateToProps } from './default_props.js';

class Book extends Component {

    constructor(props) {
        super(props);
        this.routes = [];
        const keys = Object.keys(P);
        const n = keys.length;
        // create a route for all passages
        for(var i=0; i<n; i++){
            // console.log('p', i, keys[i]);
            const C = P[keys[i]];
            const k = keys[i];
            var a = {
                path: '/passage/'+keys[i],
                k: keys[i],
                component: (props) => { return <Passage
                                                 character={this.props.character}
                                                 book={this.props.book}
                                                 path={k}><C/></Passage>; }
            };
            this.routes.push(a);
        }
        // additional pages
        const c = this.props.book.current_passage || 'Start';
        const C = P[c];
        // console.log('current', c);
        this.routes.push({
            path: '/',
            k: c,
            component: (props) => { return <Passage
                                             character={this.props.character}
                                             book={this.props.book}
                                             path={c}><C/></Passage>; }

        });
    }

    render() {
        const style = {
            transformOrigin: 'top left',
            transform: 'scale('+this.props.book.globalScale+')'
        };

        // console.log('Book ', this.props, this.routes);
        if (this.props.book.current_passage === ' ')
            return(<Start/>);
        
        return (
            <div id={'Book'} className="Book">
              <div style={style}>                
                {this.routes.map((route) => (
                    <Route
                      key={route.path}
                      exact
                      path={route.path}
                      render={(props) =>
                              <route.component
                                {...props}
                                current_passage={route.k}
                                character={this.props.character}
                                book={this.props.book}/>}
                    />
                ))}
              </div>
            </div>
        );
    }
}

export default withRouter(connect(mapStateToProps)(Book));
