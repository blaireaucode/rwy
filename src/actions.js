/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as at from './action-types.js';

export const set =
    (element_name, value) => ({ type:at.SET,
                                element_name:element_name, value:value });

