/*
 * Copyright 2018
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

// import update from 'immutability-helper';

export function set(state, element_name, value)
{
    // console.log('Store set ', element_name, value);
    const name = 'rwy_'+element_name;
    global.localStorage.setItem(name, JSON.stringify(value));
    const s = {...state, [element_name]:value};
    // console.log('stored state', s);
    return s;
}



// export function book_set_element(state, element)
// {
//     //console.log('book_set_element', element.passage_title, element.name);
//     const t = element.passage_title || {};
//     if (t === {}) {
//         console.log('ERROR element has no passage_title key');
//         return state;
//     }
//     const n = element.name || {};
//     if (!n) {
//         console.log('ERROR element has no name key');
//         return state;
//     }

//     const previous = state.book.p[t] || {};

//     // set the p[passage_title][name] to the given element
//     const newt = update(previous, { [n]: { $set: element } });

//     // set the book.p[passage_title] to newt
//     const newp = update(state.book.p, { [t]: {$set: newt} });

//     // set the book.p[passage_title] to newt
//     const newb = update(state.book, { p: {$set: newp} });

//     // set the new book to the state
//     return book_set(state, newb);
// }
