/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { HashRouter as Router } from 'react-router-dom';
import Book from './Book.js';
import Debug from './Debug.js';
import Team from './Team.js';
import Test from './Test.js';
import Layout from './Layout.js';

class Routes extends Component {

    render() {
        // console.log('Public url', process.env.PUBLIC_URL);
        return (
            <Router basename={process.env.PUBLIC_URL}>
              <div>
                {/* <Layout path="/t/" component={TestFight} /> */}
                <Layout path="/test/" component={Test} />
                {/* <Layout path="/character" component={Character} /> */}
                {/* <Layout path="/map" component={Map} /> */}
                {/* <Layout path="/options" component={Options} /> */}
                <Layout path="/debug" component={Debug} />
                <Layout path="/team" component={Team} />
                {/* <Layout path="/debugitems" component={DebugItems} /> */}
                {/* <Layout path="/edit" component={CharacterEditor} /> */}
                {/* <Layout path="/notes" component={NotesEditor} /> */}
                {/* <Layout path="/start" component={Start} /> */}
                <Layout path="/passage" component={Book} />
                <Layout exact path="/" component={Book} />
              </div>
            </Router>
        );
    }
}

export default Routes;

