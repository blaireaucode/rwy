/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import AdventurerSummary from './AdventurerSummary.js';
import Field4 from './Field4.js';
import { mapStateToProps, mapDispatchToProps } from './default_props.js';

class Team extends Component {

    constructor(props) {
        super(props);
        console.log('group');
    }
    
    render() {
        var p = [];
        for(var adv of this.props.team.order)
            p.push(<span key={adv}>
                     <AdventurerSummary adv={this.props.team[adv]}/>
                     <hr/>
                   </span>
                  );
        return (
            <div className="Book">              

              {p}
              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Team);
